﻿using System;
using System.Numerics;

using PXL.Units;

namespace PXL.Structures.ProofOfWork
{
	/// <summary>
	/// Represents a proof that computational effort was expended
	/// </summary>
	public interface IProofOfWork : IComparable<IProofOfWork>
	{
		/// <summary>
		/// The number of distinct trials to reach maximum difficulty
		/// </summary>
		BigInteger MaxDifficulty { get; }
		
		/// <summary>
		/// The estimated number of trials the proof of work represents
		/// </summary>
		BigInteger Difficulty { get; }
	}

	/// <summary>
	/// A proof of work whose difficulty is related to the hash of a value falling below a certain number
	/// </summary>
	/// <typeparam name="TH">The type of hash</typeparam>
	public interface IHashProofOfWork<out TH> : IProofOfWork
		where TH : IHash
	{
		/// <summary>
		/// The starting hash which will be hashed against the nonce
		/// </summary>
		TH Basis { get; }

		/// <summary>
		/// A value written applied against the basis to produce a result hash
		/// </summary>
		BigInteger Nonce { get; }

		/// <summary>
		/// The result of proof of work computation
		/// </summary>
		TH Result { get; }
	}
}