﻿using System;
using System.IO;
using System.Numerics;

using PXL.Units;

namespace PXL.Structures.ProofOfWork
{
	/// <summary>
	/// A proof of work whose difficulty is related to the hash of a value falling below a certain number
	/// </summary>
	/// <typeparam name="TH"></typeparam>
	public class HashProofOfWork<TH> : IHashProofOfWork<TH> where TH : IHash
	{
		#region Fields

		private readonly IHasher<TH> _hasher;
		private readonly double _logMaxDifficulty;

		#endregion Fields

		#region Constructor

		/// <summary>
		/// Creates a new hash proof of work
		/// </summary>
		/// <param name="basis">The starting hash which will be hashed against the nonce</param>
		/// <param name="nonce">A value written applied against the basis to produce a result hash</param>
		/// <param name="hasher">The result of proof of work computation</param>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the basis or hasher parameters are null
		/// </exception>
		/// <exception cref="System.ArgumentOutOfRangeException">
		/// Thrown if the nonce parameter is negative
		/// </exception>
		public HashProofOfWork(TH basis, BigInteger nonce, IHasher<TH> hasher)
		{
			if (ReferenceEquals(basis, null))
				throw new ArgumentNullException("basis");

			if (hasher == null)
				throw new ArgumentNullException("hasher");

			if( nonce < 0 )
				throw new ArgumentOutOfRangeException("nonce", "Nonce cannot be negative");

			_hasher = hasher;

			Basis = basis;
			Nonce = nonce;

			MaxDifficulty = hasher.MaxValue.ToByteArray().ToNonNegativeBigInteger();
			_logMaxDifficulty = BigInteger.Log(MaxDifficulty, 2);

			ComputeResultAndDifficulty();
		}

		#endregion Constructor

		#region Properties

		public TH Basis { get; private set; }
		public BigInteger Nonce { get; private set; }
		public TH Result { get; private set; }
		public BigInteger MaxDifficulty { get; private set; }
		public BigInteger Difficulty { get; private set; }

		#endregion Properties

		#region Methods

		/// <summary>
		/// Advances the nonce by one and recomputes the difficulty
		/// </summary>
		public void Advance()
		{
			Nonce++;
			ComputeResultAndDifficulty();
		}

		#endregion Methods

		#region Comparison

		public int CompareTo(IProofOfWork other)
		{
			if (other == null)
				throw new ArgumentNullException("other");

			if( MaxDifficulty != other.MaxDifficulty )
				throw new InvalidOperationException("Cannot compare proofs of work with different max difficulties");

			return MaxDifficulty.CompareTo(other.Difficulty);
		}

		#endregion Comparison

		#region Conversions

		public override string ToString()
		{
			return string.Format(
				"Proof of work ({0}) = {1}",
				typeof (TH).Name, Difficulty
			);
		}

		#endregion Conversions

		#region Utilities

		private void ComputeResultAndDifficulty()
		{
			Result = _hasher.Of(
				new ISerializable[] { Basis, (ByteSerializable) Nonce.ToByteArray() }
			);

			var trueDifficulty = Result.ToByteArray().ToNonNegativeBigInteger(isBigEndian: true);
			var logDifficulty = _logMaxDifficulty - BigInteger.Log(trueDifficulty, 2);

			Difficulty = (BigInteger) Math.Pow(2, logDifficulty);
		}

		#endregion Utilities
	}
}