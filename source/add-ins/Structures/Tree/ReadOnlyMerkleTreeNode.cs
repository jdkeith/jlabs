﻿using System;

namespace PXL.Structures.Tree
{
	/// <summary>
	/// A readonly Merkle tree structure
	/// </summary>
	public class ReadOnlyMerkleTreeNode : MerkleTreeNode
	{
		private static byte[] SafeCloneByteArray(byte[] array)
		{
			if (array == null)
				return null;

			return (byte[])array.Clone();
		}

		/// <summary>
		/// Creates a readonly tree node from an existing tree node
		/// </summary>
		/// <param name="node">The node to clone</param>
		/// <returns>
		/// <para>A reference to the parameter if the parameter is a ReadOnlyMerkleTreeNode</para>
		/// <para>Null if the parameter is null</para>
		/// <para>A readonly copy of the parameter and its children otherwise</para>
		/// </returns>
		/// <remarks>Contents are cloned</remarks>
		public static ReadOnlyMerkleTreeNode CopyFrom(MerkleTreeNode node)
		{
			if (node == null)
				return null;

			var ron = node as ReadOnlyMerkleTreeNode;

			if (ron != null)
				return ron;

			return new ReadOnlyMerkleTreeNode
			{
				Left = CopyFrom(node.Left),
				Right = CopyFrom(node.Right),
				Value = node.Value,
				Content = SafeCloneByteArray(node.Content),
				Type = node.Type
			};
		}

		private ReadOnlyMerkleTreeNode()
		{
		}

		/// <summary>
		/// Trims children from the node
		/// </summary>
		/// <returns>Throws NotSupportedException</returns>
		public override bool Trim()
		{
			throw new NotSupportedException("Node is readonly.");
		}

		public override string ToString()
		{
			return "RO " + base.ToString();
		}
	}
}