﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;

using PXL.Units;

namespace PXL.Structures.List
{
	/// <summary>
	/// Provides a cryptographically-secure list of items which may be trimmed back to front
	/// </summary>
	/// <typeparam name="T">The type of item to store in the list</typeparam>
	public class TrimmableHashList<T> : IEnumerable<T>, ISerializable where T : ISerializable
	{
		#region Factories

		/// <summary>
		/// Gets a factory for deserializing and reading trimmable hash lists
		/// </summary>
		/// <typeparam name="TT">The type of the item stored in the list</typeparam>
		/// <param name="deserializer">A method to deserialize items in the list</param>
		/// <returns>A factory for deserializing and reading trimmable hash lists</returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the deserializer argument is null
		/// </exception>
		public static IDeserializer<TrimmableHashList<TT>> Factory<TT>(IDeserializer<TT> deserializer)
			where TT : T
		{
			if (deserializer == null)
				throw new ArgumentNullException("deserializer");

			return new TrimmableHashListFactory<TT>(deserializer.ReadFrom);
		}

		/// <summary>
		/// Gets a factory for deserializing and reading trimmable hash lists
		/// </summary>
		/// <typeparam name="TT">The type of the item stored in the list</typeparam>
		/// <param name="deserializer">A method to deserialize items in the list</param>
		/// <returns>A factory for deserializing and reading trimmable hash lists</returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the deserializer argument is null
		/// </exception>
		public static IDeserializer<TrimmableHashList<TT>> Factory<TT>(Func<Stream, TT> deserializer)
			where TT : T
		{
			if (deserializer == null)
				throw new ArgumentNullException("deserializer");

			return new TrimmableHashListFactory<TT>(deserializer);
		}

		private class TrimmableHashListFactory<TT> : IDeserializer<TrimmableHashList<TT>> where TT : ISerializable
		{
			private readonly Func<Stream, TT> _deserializer;

			public TrimmableHashListFactory(Func<Stream, TT> deserializer)
			{
				_deserializer = deserializer;
			}

			public TrimmableHashList<TT> Deserialize(byte[] serialized)
			{
				if (serialized == null)
					throw new ArgumentNullException("serialized");

				using (var ms = new MemoryStream())
				{
					ms.Write(serialized, 0, serialized.Length);
					ms.Seek(0, SeekOrigin.Begin);
					return ReadFrom(ms);
				}
			}

			public TrimmableHashList<TT> ReadFrom(Stream s)
			{
				if (s == null)
					throw new ArgumentNullException("s");

				var vuif = VarUInt.Factory;

				var oCount = vuif.ReadFrom(s);
				var cCount = vuif.ReadFrom(s);
				var remainderHash = SHA256Hash.Factory.ReadFrom(s);

				var list = new List<TT>();

				for (uint i = 0; i < cCount; i++)
				{
					list.Add(_deserializer(s));
				}

				return new TrimmableHashList<TT>(list, (int)oCount, remainderHash);
			}
		}

		#endregion Factories

		#region Fields

		private readonly List<T> _internalList;
		private SHA256Hash _remainder;
		private SHA256Hash? _cachedId;

		#endregion Fields

		#region Constructors

		private TrimmableHashList(List<T> items, int oCount, SHA256Hash remainderHash)
		{
			_internalList = items;
			OriginalCount = oCount;
			_remainder = remainderHash;
		}

		/// <summary>
		/// Creates a trimmable hash list of a given set of items
		/// </summary>
		/// <param name="items">The items to store in the hash list</param>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the items parameter is null
		/// </exception>
		/// <exception cref="System.ArgumentException">
		/// Thrown if the items parameter contains no items or contains null items
		/// </exception>
		public TrimmableHashList(IEnumerable<T> items)
		{
			if( items == null )
				throw new ArgumentNullException("items");

			_internalList = items.ToList();

			if( ! _internalList.Any())
				throw new ArgumentException("Items must contain at least one item", "items");

			if (_internalList.Any(i => ReferenceEquals(i, null)))
				throw new ArgumentException("Items list cannot contain null entries", "items");

			OriginalCount = _internalList.Count;

			_remainder = SHA256Hash.Factory.MinValue;
		}

		#endregion Constructors

		#region Properties

		/// <summary>
		/// An identifier derived from the items in the original (untrimmed) list
		/// which is retained through future trimmings
		/// </summary>
		public SHA256Hash Id
		{
			get
			{
				if (! _cachedId.HasValue)
				{
					_cachedId = _remainder;

					for (var i = _internalList.Count - 1; i >= 0; i--)
					{
						_cachedId = SHA256Hash.Factory.Of(
							SHA256Hash.Factory.Of(_internalList[i]), _cachedId
						);
					}
				}

				return _cachedId.Value;
			}
		}

		/// <summary>
		/// The number of items in the original (untrimmed) list
		/// </summary>
		public int OriginalCount { get; private set; }

		/// <summary>
		/// The number of items in the current list
		/// </summary>
		public int Count
		{
			get { return _internalList.Count; }
		}

		#endregion Properties

		#region Manipulation

		/// <summary>
		/// Reduces the count of items in the list to the number specified
		/// </summary>
		/// <param name="newCount">The number of items to retain in the list</param>
		/// <remarks>
		/// Items are trimmed from the end of the list. That is, setting this value to 2
		/// will always retain the first two items in the list discarding all others.
		/// </remarks>
		/// <exception cref="System.ArgumentOutOfRangeException">
		/// Thrown if the newCount parameter is less than zero or if it is
		/// greater than the current number of items in the list
		/// </exception>
		public void TrimTo(int newCount)
		{
			if (newCount < 0)
				throw new ArgumentOutOfRangeException("newCount", "New count cannot be less than zero");

			if( newCount > Count )
				throw new ArgumentOutOfRangeException("newCount", "Cannot untrim list");

			if (newCount == Count)
				return;

			var cCount = _internalList.Count;
			
			for (var i = 0; i < cCount - newCount; i++)
			{
				var idx = _internalList.Count - 1;

				_remainder = SHA256Hash.Factory.Of(
					SHA256Hash.Factory.Of(_internalList[idx]), _remainder
				);

				_internalList.RemoveAt(idx);
			}
		}

		#endregion Manipulation

		#region Enumeration

		/// <summary>
		/// Gets an enumerator for iterating through the contents of the list
		/// </summary>
		/// <returns>An enumerator</returns>
		public IEnumerator<T> GetEnumerator()
		{
			return _internalList.GetEnumerator();
		}

		/// <summary>
		/// Gets an enumerator for iterating through the contents of the list
		/// </summary>
		/// <returns>An enumerator</returns>
		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}

		#endregion Enumeration

		#region Serialization

		/// <summary>
		/// Returns the byte representation of the trimmable list
		/// </summary>
		/// <returns></returns>
		public byte[] ToByteArray()
		{
			using (var ms = new MemoryStream())
			{
				WriteTo(ms);
				return ms.ToArray();
			}
		}

		/// <summary>
		/// Writes the trimmable list to a stream
		/// </summary>
		/// <param name="s">The stream to write the list to</param>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the stream parameter is null
		/// </exception>
		public void WriteTo(Stream s)
		{
			if (s == null)
				throw new ArgumentNullException("s");

			((VarUInt) (uint) OriginalCount).WriteTo(s);
			((VarUInt)(uint) Count).WriteTo(s);
			_remainder.WriteTo(s);

			foreach (var item in _internalList)
			{
				item.WriteTo(s);
			}
		}

		#endregion Serialization
	}
}