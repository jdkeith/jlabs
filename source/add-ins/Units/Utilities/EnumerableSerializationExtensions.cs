﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

// ReSharper disable once CheckNamespace
namespace PXL.Units
{
	/// <summary>
	/// Provides convenience and utility methods for or related to enumerables
	/// </summary>
	public static class EnumerableSerializationExtensions
	{
		#region ISerializable

		/// <summary>
		/// Writes an array of ISerializables to a stream prefixing the data with a VarUInt for length
		/// </summary>
		/// <typeparam name="T">The type of items to write to the stream</typeparam>
		/// <param name="items">An enumeration of items to write to the stream</param>
		/// <param name="stream">The stream to write to</param>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if either parameter is null
		/// </exception>
		public static void WriteTo<T>(this IEnumerable<T> items, Stream stream)
			where T : ISerializable
		{
			if (items == null)
				throw new ArgumentNullException("items");

			if (stream == null)
				throw new ArgumentNullException("stream");

			var array = items.ToArray();

			((VarUInt)(uint)array.Length).WriteTo(stream);

			foreach (var item in array)
			{
				item.WriteTo(stream);
			}
		}

		/// <summary>
		/// Writes an array of ISerializables to a stream prefixing the data with a VarUInt for length
		/// </summary>
		/// <typeparam name="T">The type of items to write to the stream</typeparam>
		/// <param name="items">An enumeration of items to write to the stream</param>
		/// <param name="stream">The stream to write to</param>
		/// <param name="serializer">The function which serializes the item</param>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if either parameter is null
		/// </exception>
		public static void WriteTo<T>(this IEnumerable<T> items, Stream stream, Action<Stream, T> serializer)
		{
			if (items == null)
				throw new ArgumentNullException("items");

			if (stream == null)
				throw new ArgumentNullException("stream");

			if( serializer == null )
				throw new ArgumentNullException("serializer");

			var array = items.ToArray();

			((VarUInt)(uint)array.Length).WriteTo(stream);

			foreach (var item in array)
			{
				serializer(stream, item);
			}
		}

		/// <summary>
		/// Writes an array of ISerializables to a stream prefixing the data with a VarUInt for length
		/// </summary>
		/// <typeparam name="T">The type of items to write to the stream</typeparam>
		/// <param name="items">An enumeration of items to write to the stream</param>
		/// <param name="stream">The stream to write to</param>
		/// <param name="serializer">The function which serializes the item</param>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if either parameter is null
		/// </exception>
		public static void WriteTo<T>(this IEnumerable<T> items, Stream stream, Action<BinaryWriter, T> serializer)
		{
			if (items == null)
				throw new ArgumentNullException("items");

			if (stream == null)
				throw new ArgumentNullException("stream");

			if (serializer == null)
				throw new ArgumentNullException("serializer");

			var array = items.ToArray();

			((VarUInt)(uint)array.Length).WriteTo(stream);

			var bw = new BinaryWriter(stream);

			foreach (var item in array)
			{
				serializer(bw, item);
			}
		}

		/// <summary>
		/// Reads an array of items from a stream
		/// </summary>
		/// <typeparam name="T">The type of items to read from the stream</typeparam>
		/// <param name="deserializer">A deserializer which reads items from the stream</param>
		/// <param name="stream">The stream to read from</param>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if either parameter is null
		/// </exception>
		public static T[] ReadArrayFrom<T>(this Stream stream, Func<Stream, T> deserializer)
		{
			if (stream == null)
				throw new ArgumentNullException("stream");

			if (deserializer == null)
				throw new ArgumentNullException("deserializer");

			var array = new T[VarUInt.Factory.ReadFrom(stream)];

			for (var i = 0; i < array.Length; i++)
			{
				array[i] = deserializer(stream);
			}

			return array;
		}

		/// <summary>
		/// Reads an array of items from a stream
		/// </summary>
		/// <typeparam name="T">The type of items to read from the stream</typeparam>
		/// <param name="deserializer">A deserializer which reads items from the stream</param>
		/// <param name="stream">The stream to read from</param>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if either parameter is null
		/// </exception>
		public static T[] ReadArrayFrom<T>(this Stream stream, Func<BinaryReader, T> deserializer)
		{
			if (stream == null)
				throw new ArgumentNullException("stream");

			if (deserializer == null)
				throw new ArgumentNullException("deserializer");

			var array = new T[VarUInt.Factory.ReadFrom(stream)];

			var br = new BinaryReader(stream);

			for (var i = 0; i < array.Length; i++)
			{
				array[i] = deserializer(br);
			}

			return array;
		}

		/// <summary>
		/// Reads an array of items from a stream
		/// </summary>
		/// <typeparam name="T">The type of items to read from the stream</typeparam>
		/// <param name="deserializer">A deserializer which reads items from the stream</param>
		/// <param name="stream">The stream to read from</param>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if either parameter is null
		/// </exception>
		public static T[] ReadArrayFrom<T>(this Stream stream, IDeserializer<T> deserializer)
		{
			if (stream == null)
				throw new ArgumentNullException("stream");

			if (deserializer == null)
				throw new ArgumentNullException("deserializer");

			var array = new T[VarUInt.Factory.ReadFrom(stream)];

			for (var i = 0; i < array.Length; i++)
			{
				array[i] = deserializer.ReadFrom(stream);
			}

			return array;
		}

		/// <summary>
		/// Reads an array of items from a stream
		/// </summary>
		/// <typeparam name="T">The type of items to read from the stream</typeparam>
		/// <typeparam name="D">The type of the deserializer</typeparam>
		/// <param name="stream">The stream to read from</param>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if either parameter is null
		/// </exception>
		// ReSharper disable once InconsistentNaming
		public static T[] ReadArrayFrom<T, D>(this Stream stream)
			where D : IDeserializer<T>, new()
		{
			return ReadArrayFrom(stream, new D());
		}

		#endregion ISerializable
	}
}