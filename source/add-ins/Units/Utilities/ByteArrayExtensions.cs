﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace PXL.Units.Utilities
{
	/// <summary>
	/// Provides extension methods to for byte arrays
	/// </summary>
	public static class ByteArrayExtensions
	{
		#region Hex

		/// <summary>
		/// Converts an enumerable of bytes to a hex string representation
		/// </summary>
		/// <param name="data">The enumeration of bytes to convert</param>
		/// <returns>A hex string representing the byte data</returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the data parameter is null
		/// </exception>
		public static string ToHexString(this IEnumerable<byte> data)
		{
			if (data == null)
				throw new ArgumentNullException("data");

			var sb = new StringBuilder();

			foreach (var b in data)
			{
				sb.Append(b.ToString("x2"));
			}

			return sb.ToString();
		}

		/// <summary>
		/// Converts a hex string to a byte array
		/// </summary>
		/// <param name="hex">The hex string to convert</param>
		/// <returns>A hex string representing the byte data</returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the hex parameter is null
		/// </exception>
		/// <exception cref="System.FormatException">
		/// Thrown if the hex string
		/// <para>doesn't contain an even number of characters</para>
		/// <para>contains characters other than 0-9, A-F, a-f</para>
		/// </exception>
		public static byte[] FromHexString(this string hex)
		{
			if (hex == null)
				throw new ArgumentNullException("hex");

			if (hex.Length % 2 != 0)
				throw new FormatException("Hex string must contain an even number of characters");

			hex = hex.ToLower();

			var result = new byte[hex.Length / 2];

			for (var i = 0; i < result.Length; i++)
			{
				result[i] = (byte)((GetHexVal(hex[i * 2]) << 4) + GetHexVal(hex[i * 2 + 1]));
			}

			return result;
		}

		/// <summary>
		/// Converts a hex character to a nybble
		/// </summary>
		/// <param name="hex">The hex character to convert</param>
		/// <returns>The nybble represented by the hex character</returns>
		/// <exception cref="System.FormatException">
		/// Thrown if the hex string is a character other than 0-9, A-F, a-f
		/// </exception>
		public static byte GetHexVal(char hex)
		{
			var val = (int)hex;
			val = val - (val < 58 ? 48 : 87);

			if (val < 0 || val > 15)
				throw new FormatException("Hex string may only contain the values 0-9, A-F, a-f");

			return (byte) val;
		}

		#endregion Hex

		#region Comparison

		/// <summary>
		/// Compares two byte arrays and returns an integer indicating their relative ranking
		/// </summary>
		/// <param name="a">The first byte array to compare</param>
		/// <param name="b">The second byte array to compare</param>
		/// <returns>
		/// <para>A negative value if the first byte array should be sorted before the second byte array</para>
		/// <para>A positive value if the first byte array should be sorted after the second byte array</para>
		/// <para>0 if the two byte arrays are the equivalent sequence or both are null</para>
		/// <para>1 if the second byte array is null and the first byte array is not null</para>
		/// <para>-1 if the first byte array is null and the second byte array is not null</para>
		/// </returns>
		/// <remarks>
		/// Sequences are compared element by element. If one sequence is a subset of the other,
		/// the shorter array is considered first
		/// </remarks>
		public static int CompareByteArrays(byte[] a, byte[] b)
		{
			if (ReferenceEquals(a, null))
				return ReferenceEquals(b, null) ? 0 : 1;

			if (ReferenceEquals(b, null))
				return -1;

			var length = Math.Min(a.Length, b.Length);

			for (var i = 0; i < length; i++)
			{
				var cmp = a[i].CompareTo(b[i]);

				if (cmp != 0) return cmp;
			}

			// the shorter array is first
			return a.Length.CompareTo(b.Length);
		}

		#endregion Comparison

		#region Serialization

		/// <summary>
		/// Writes an array of bytes to a stream, prefixing the data with a VarUInt for length
		/// </summary>
		/// <param name="bytes">An enumeration of items to write to the stream</param>
		/// <param name="stream">The stream to write to</param>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if either parameter is null
		/// </exception>
		public static void WriteTo(this byte[] bytes, Stream stream)
		{
			if (bytes == null)
				throw new ArgumentNullException("bytes");

			if (stream == null)
				throw new ArgumentNullException("stream");

			((VarUInt)(uint)bytes.Length).WriteTo(stream);

			stream.Write(bytes, 0, bytes.Length);
		}

		/// <summary>
		/// Reads an array of bytes from a stream
		/// </summary>
		/// <param name="stream">The stream to read from</param>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if either parameter is null
		/// </exception>
		public static byte[] ReadByteArrayFrom(this Stream stream)
		{
			if (stream == null)
				throw new ArgumentNullException("stream");

			var array = new byte[VarUInt.Factory.ReadFrom(stream)];

			stream.Read(array, 0, array.Length);

			return array;
		}

		#endregion Serialization
	}
}