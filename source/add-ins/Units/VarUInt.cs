﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;

namespace PXL.Units
{
	/// <summary>
	/// A factory for creating variable-length unsigned integers
	/// </summary>
	public interface IVarUIntFactory : IParser<VarUInt>, IDeserializer<VarUInt>
	{
	}

	/// <summary>
	/// A variable-length unsigned integer
	/// </summary>
	public struct VarUInt : ISerializable, IComparable
	{
		#region Singletons

		private static IComparer<VarUInt> _comparerSingleton;
		private static IVarUIntFactory _factorySingleton;

		#endregion Singletons

		#region Inner Classes and Singleton Access

		/// <summary>
		/// Gets a VarUInt comparer singleton
		/// </summary>
		public static IComparer<VarUInt> Comparer
		{
			get { return _comparerSingleton ?? (_comparerSingleton = new VarUIntComparer()); }
		}

		/// <summary>
		/// Gets a VarUInt factory singleton
		/// </summary>
		public static IVarUIntFactory Factory
		{
			get { return _factorySingleton ?? (_factorySingleton = new VarUIntFactory()); }
		}

		private class VarUIntComparer : IComparer<VarUInt>
		{
			public int Compare(VarUInt x, VarUInt y)
			{
				return x.CompareTo(y);
			}
		}

		private class VarUIntFactory : IVarUIntFactory
		{
			public VarUInt Deserialize(byte[] serialized)
			{
				if (serialized == null)
					throw new ArgumentNullException("serialized");

				using (var ms = new MemoryStream())
				{
					ms.Write(serialized, 0, serialized.Length);
					ms.Seek(0, SeekOrigin.Begin);
					return ReadFrom(ms);
				}
			}

			public VarUInt ReadFrom(Stream s)
			{
				if (s == null)
					throw new ArgumentNullException("s");

				var szv = (byte)s.ReadByte();

				byte[] buffer;

				switch (szv)
				{
					case 0xfd:
						buffer = new byte[2];
						break;

					case 0xfe:
						buffer = new byte[4];
						break;

					case 0xff:
						buffer = new byte[8];
						break;

					default:
						return new VarUInt(szv);
				}

				s.Read(buffer, 0, buffer.Length);

				ulong value;

				switch (szv)
				{
					case 0xfd:
						value = BitConverter.ToUInt16(buffer, 0);
						break;

					case 0xfe:
						value = BitConverter.ToUInt32(buffer, 0);
						break;

					default:
						value = BitConverter.ToUInt64(buffer, 0);
						break;
				}

				return new VarUInt(value);
			}

			public VarUInt Parse(string value)
			{
				return ulong.Parse(value);
			}

			public IStatus<VarUInt> TryParse(string value)
			{
				ulong ulr;

				if (!ulong.TryParse(value, out ulr))
				{
					return Status.Fail.WithDefaultData<VarUInt>();
				}

				return Status.Succeed.WithData((VarUInt) ulr);
			}
		}

		#endregion Inner Classes and Singleton Access

		#region Fields

		private readonly ulong _value;

		#endregion Fields

		#region Constructor

		/// <summary>
		/// Creates a VarUInt with the given integer value
		/// </summary>
		/// <param name="value">The value to assign to the VarUInt</param>
		public VarUInt(ulong value)
		{
			_value = value;
		}

		#endregion Constructor

		#region Serialization

		/// <summary>
		/// Serializes the VarUInt to a byte array
		/// </summary>
		/// <returns>The serialized representation of the VarUInt</returns>
		public byte[] ToByteArray()
		{
			using (var ms = new MemoryStream())
			{
				WriteTo(ms);
				return ms.ToArray();
			}
		}

		/// <summary>
		/// Writes the VarUInt to a stream
		/// </summary>
		/// <param name="s">The stream to write the VarUInt to</param>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the stream parameter is null
		/// </exception>
		public void WriteTo(Stream s)
		{
			if (s == null)
				throw new ArgumentNullException("s");

			byte[] buffer;

			if (_value > uint.MaxValue)
			{
				s.WriteByte(0xff);
				buffer = BitConverter.GetBytes(_value);
			}
			else if (_value > ushort.MaxValue)
			{
				s.WriteByte(0xfe);
				buffer = BitConverter.GetBytes((uint)_value);
			}
			else if (_value >= 0xfd)
			{
				s.WriteByte(0xfd);
				buffer = BitConverter.GetBytes((ushort)_value);
			}
			else
			{
				s.WriteByte((byte)_value);
				return;
			}

			s.Write(buffer, 0, buffer.Length);
		}

		#endregion Serialization

		#region Equality and Comparison

		/// <summary>
		/// Compares a VarUInt to an object
		/// </summary>
		/// <param name="obj">The object to compare to</param>
		/// <returns>
		/// An integer indicating the relative ordering of the invocant to the parameter
		/// </returns>
		public int CompareTo(object obj)
		{
			if (obj is VarUInt)
				return _value.CompareTo(((VarUInt)obj)._value);

			return _value.CompareTo(obj);
		}

		/// <summary>
		/// Compares a VarUInt to an object for equality
		/// </summary>
		/// <param name="obj">The object to compare to</param>
		/// <returns>True if the invocant is considered equal to the parameter, false otherwise</returns>
		public override bool Equals(object obj)
		{
			return _value.Equals(obj);
		}

		/// <summary>
		/// Serves as a hash function for VarUInt
		/// </summary>
		/// <returns>An integer derived from the contents of the invocant</returns>
		public override int GetHashCode()
		{
			return _value.GetHashCode();
		}

		#endregion Equality and Comparison

		#region Conversion

		/// <summary>
		/// Converts a VarUInt to a unsigned 64 bit value
		/// </summary>
		/// <param name="value">The VarUInt to convert</param>
		/// <returns>An unsigned 64 bit representation of the VarUInt</returns>
		public static implicit operator ulong(VarUInt value)
		{
			return value._value;
		}

		/// <summary>
		/// Converts a VarUInt to a signed 64 bit value
		/// </summary>
		/// <param name="value">The VarUInt to convert</param>
		/// <returns>An signed 64 bit representation of the VarUInt</returns>
		public static explicit operator long(VarUInt value)
		{
			return (long)value._value;
		}

		/// <summary>
		/// Converts an unsigned 64 bit value to a VarUInt
		/// </summary>
		/// <param name="value">The unsigned 64 bit value to convert</param>
		/// <returns>A VarUInt representation of the unsigned 64 bit value</returns>
		public static implicit operator VarUInt(ulong value)
		{
			return new VarUInt(value);
		}

		/// <summary>
		/// Provides a string representation of the VarUInt
		/// </summary>
		/// <returns>A string representation of the VarUInt</returns>
		public override string ToString()
		{
			return _value.ToString(CultureInfo.InvariantCulture);
		}

		#endregion Conversion
	}
}
