﻿using System;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

using PXL.Units.Utilities;

namespace PXL.Units
{
	/// <summary>
	/// Represents a class which produces hashes from various types of input
	/// </summary>
	public interface IHasher
	{
		/// <summary>
		/// Creates a random hash useful for unique identifiers
		/// </summary>
		// ReSharper disable once InconsistentNaming
		IHash UID { get; }

		#region Hashing

		/// <summary>
		/// Computes the hash of a string using UTF-8 encoding
		/// </summary>
		/// <param name="message">The message to compute the hash of</param>
		/// <returns>The hash of the message</returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the message parameter is null
		/// </exception>
		IHash Of(string message);

		/// <summary>
		/// Computes the hash of a string using UTF-8 encoding
		/// </summary>
		/// <param name="message">The message to compute the hash of</param>
		/// <param name="encoding">The text encoding to use</param>
		/// <returns>The hash of the message</returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the message parameter is null
		/// </exception>
		IHash Of(string message, Encoding encoding);

		/// <summary>
		/// Computes the hash of a byte array
		/// </summary>
		/// <param name="data">The data to compute the hash of</param>
		/// <returns>The hash of the data</returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the data parameter is null
		/// </exception>
		IHash Of(byte[] data);

		/// <summary>
		/// Computes the hash of the bytes on a stream starting from the current position
		/// </summary>
		/// <param name="s">The stream to read from</param>
		/// <returns>The hash of the bytes on the stream starting from the current position</returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the data parameter is null
		/// </exception>
		IHash Of(Stream s);

		/// <summary>
		/// Computes the hash of one or more hashes
		/// </summary>
		/// <param name="hashes">A param array of hashes to compute a hash of</param>
		/// <returns>A hash of all combined hashes</returns>
		/// <remarks>
		/// Hashes are concatenated into a byte array in the order they're passed before being hashed
		/// </remarks>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the hashes parameter is null
		/// </exception>
		/// <exception cref="System.ArgumentException">
		/// Thrown if any hashes are null
		/// </exception>
		IHash Of(params IHash[] hashes);

		/// <summary>
		/// Computes the hash of one or more items
		/// </summary>
		/// <param name="items">A param array of items to compute the total has of</param>
		/// <returns>A hash of all combined items</returns>
		/// <remarks>
		/// Items are concatenated into a byte array in the order they're passed before being hashed
		/// </remarks>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the items parameter is null
		/// </exception>
		/// <exception cref="System.ArgumentException">
		/// Thrown if any items are null
		/// </exception>
		IHash Of(params ISerializable[] items);

		#endregion Hashing

		#region HMAC

		/// <summary>
		/// Creates a hash message authentication code from the given key and message
		/// </summary>
		/// <param name="key">The key to create an HMAC from</param>
		/// <param name="messageStream">
		/// The stream containing the message from which to create an HMAC
		/// </param>
		/// <returns>An HMAC for the given key and message</returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the key or message arguments are null
		/// </exception>
		/// <exception cref="System.ArgumentException">
		/// Thrown if the key or messageStream arguments are empty
		/// </exception>
		/// <exception cref="System.InvalidOperationException">
		/// Thrown if the messageStream does not support reading or seeking
		/// </exception>
		// ReSharper disable once InconsistentNaming
		IHash HMAC(byte[] key, Stream messageStream);

		/// <summary>
		/// Creates a hash message authentication code from the given key and message
		/// </summary>
		/// <param name="key">The key to create an HMAC from</param>
		/// <param name="message">The message to create an HMAC from</param>
		/// <returns>An HMAC for the given key and message</returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the key or message arguments are null
		/// </exception>
		/// <exception cref="System.ArgumentException">
		/// Thrown if the key or message arguments are empty
		/// </exception>
		// ReSharper disable once InconsistentNaming
		IHash HMAC(byte[] key, byte[] message);

		/// <summary>
		/// Creates a hash message authentication code from the given key and message
		/// </summary>
		/// <param name="key">The key to create an HMAC from</param>
		/// <param name="message">The message to create an HMAC from</param>
		/// <returns>An HMAC for the given key and message</returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the key or message arguments are null
		/// </exception>
		/// <exception cref="System.ArgumentException">
		/// Thrown if the key is empty or the message produces no bytes when serialized
		/// </exception>
		// ReSharper disable once InconsistentNaming
		IHash HMAC(byte[] key, ISerializable message);

		#endregion HMAC

		#region "Constants"

		/// <summary>
		/// The minimum value for the given hash type
		/// </summary>
		IHash MinValue { get; }

		/// <summary>
		/// The maximum value for the given hash type
		/// </summary>
		IHash MaxValue { get; }

		/// <summary>
		/// The type of hash which the hasher produces
		/// </summary>
		Type HashType { get; }

		/// <summary>
		/// Gets the number of bytes in the given hash implementation
		/// </summary>
		int HashSize { get; }

		#endregion "Constants"
	}

	/// <summary>
	/// Provides methods to create, parse, and deserialize hashes
	/// </summary>
	/// <typeparam name="H">The type of hash to create</typeparam>
	// ReSharper disable once InconsistentNaming
	public interface IHasher<out H> : IHasher, IParser<H>, IDeserializer<H> where H : IHash
	{
		/// <summary>
		/// Creates a random hash useful for unique identifiers
		/// </summary>
		// ReSharper disable once InconsistentNaming
		new H UID { get; }

		#region Hashing

		/// <summary>
		/// Computes the hash of a string using UTF-8 encoding
		/// </summary>
		/// <param name="message">The message to compute the hash of</param>
		/// <returns>The hash of the message</returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the message parameter is null
		/// </exception>
		new H Of(string message);

		/// <summary>
		/// Computes the hash of a string using UTF-8 encoding
		/// </summary>
		/// <param name="message">The message to compute the hash of</param>
		/// <param name="encoding">Text encoding</param>
		/// <returns>The hash of the message</returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the message parameter is null
		/// </exception>
		new H Of(string message, Encoding encoding);

		/// <summary>
		/// Computes the hash of a byte array
		/// </summary>
		/// <param name="data">The data to compute the hash of</param>
		/// <returns>The hash of the data</returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the data parameter is null
		/// </exception>
		new H Of(byte[] data);

		/// <summary>
		/// Computes the hash of the bytes on a stream starting from the current position
		/// </summary>
		/// <param name="s">The stream to read from</param>
		/// <returns>The hash of the bytes on the stream starting from the current position</returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the data parameter is null
		/// </exception>
		new H Of(Stream s);

		/// <summary>
		/// Computes the hash of one or more hashes
		/// </summary>
		/// <param name="hashes">A param array of hashes to compute a hash of</param>
		/// <returns>A hash of all combined hashes</returns>
		/// <remarks>
		/// Hashes are concatenated into a byte array in the order they're passed before being hashed
		/// </remarks>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the hashes parameter is null
		/// </exception>
		/// <exception cref="System.ArgumentException">
		/// Thrown if any hashes are null
		/// </exception>
		new H Of(params IHash[] hashes);

		/// <summary>
		/// Computes the hash of one or more items
		/// </summary>
		/// <param name="items">A param array of items to compute the total has of</param>
		/// <returns>A hash of all combined items</returns>
		/// <remarks>
		/// Items are concatenated into a byte array in the order they're passed before being hashed
		/// </remarks>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the items parameter is null
		/// </exception>
		/// <exception cref="System.ArgumentException">
		/// Thrown if any items are null
		/// </exception>
		new H Of(params ISerializable[] items);

		#endregion Hashing

		#region HMAC

		/// <summary>
		/// Creates a hash message authentication code from the given key and message
		/// </summary>
		/// <param name="key">The key to create an HMAC from</param>
		/// <param name="message">The message to create an HMAC from</param>
		/// <returns>An HMAC for the given key and message</returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the key or message arguments are null
		/// </exception>
		/// <exception cref="System.ArgumentException">
		/// Thrown if the key or message arguments are empty
		/// </exception>
		// ReSharper disable once InconsistentNaming
		new H HMAC(byte[] key, byte[] message);

		/// <summary>
		/// Creates a hash message authentication code from the given key and message
		/// </summary>
		/// <param name="key">The key to create an HMAC from</param>
		/// <param name="messageStream">
		/// The stream containing the message from which to create an HMAC
		/// </param>
		/// <returns>An HMAC for the given key and message</returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the key or messageStream arguments are null
		/// </exception>
		/// <exception cref="System.ArgumentException">
		/// Thrown if the key argument is empty
		/// </exception>
		/// <exception cref="System.InvalidOperationException">
		/// Thrown if the messageStream does not support reading
		/// </exception>
		// ReSharper disable once InconsistentNaming
		new H HMAC(byte[] key, Stream messageStream);

		#endregion HMAC

		#region "Constants"

		/// <summary>
		/// The minimum value for the given hash type
		/// </summary>
		new H MinValue { get; }

		/// <summary>
		/// The maximum value for the given hash type
		/// </summary>
		new H MaxValue { get; }

		#endregion "Constants"
	}

	internal class Hasher
	{
		// ReSharper disable once InconsistentNaming
		protected static readonly RandomNumberGenerator RNG = new RNGCryptoServiceProvider();
	}

	// ReSharper disable once InconsistentNaming
	internal class Hasher<H> : Hasher, IHasher<H> where H : IHash
	{
		#region Fields

		private readonly int _size;
		private readonly Func<byte[], H> _deserializer;
		private readonly Func<byte[], H> _fromBytes;
		private readonly Func<Stream, H> _fromStream; 

		#endregion Fields

		#region Constructor

		public Hasher(
			int size, Func<byte[], H> deserializer,
			Func<byte[], H> fromBytes, Func<Stream, H> fromStream
		)
		{
			_size = size;
			_deserializer = deserializer;
			_fromBytes = fromBytes;
			_fromStream = fromStream;
		}

		#endregion Constructor

		#region Random

		public H UID
		{
			get
			{
				var data = new byte[_size];
				RNG.GetBytes(data);

				return Deserialize(data);
			}
		}

		IHash IHasher.UID
		{
			get { return UID; }
		}

		#endregion Random

		#region Parse

		public H Parse(string hex)
		{
			if (hex == null)
				throw new ArgumentNullException("hex");

			if (hex.Length != _size * 2)
				throw new FormatException(
					string.Format("Hex string must be {0} characters long", _size * 2)
				);

			return Deserialize(hex.FromHexString());
		}

		public IStatus<H> TryParse(string hex)
		{
			try
			{
				return Status.Succeed.WithData(Parse(hex));
			}
			catch(Exception ex)
			{
				return Status.FailWithException<H>(ex);
			}
		}

		#endregion Parse

		#region Hashing

		public H Of(string message)
		{
			return Of(message, Encoding.UTF8);
		}

		IHash IHasher.Of(string message)
		{
			return Of(message, Encoding.UTF8);
		}

		public H Of(string message, Encoding encoding)
		{
			if (message == null)
				throw new ArgumentNullException("message");

			if( encoding == null )
				throw new ArgumentNullException("encoding");

			return Of(encoding.GetBytes(message));
		}

		IHash IHasher.Of(string message, Encoding encoding)
		{
			return Of(message, encoding);
		}

		public H Of(byte[] data)
		{
			if (data == null)
				throw new ArgumentNullException("data");

			return _fromBytes(data);
		}
		
		IHash IHasher.Of(byte[] data)
		{
			return Of(data);
		}

		public H Of(ISerializable data)
		{
			if (data == null)
				throw new ArgumentNullException("data");

			return Of(data.ToByteArray());
		}

		public H Of(Stream stream)
		{
			if (stream == null)
				throw new ArgumentNullException("stream");

			return _fromStream(stream);
		}

		IHash IHasher.Of(Stream s)
		{
			return Of(s);
		}

		public H Of(params IHash[] hashes)
		{
			if (hashes == null)
				throw new ArgumentNullException("hashes");

			if (hashes.Length == 0)
				throw new ArgumentException("Hash must be of one or more items", "hashes");

			using (var ms = new MemoryStream())
			{
				foreach (var hash in hashes)
				{
					if (hash == null)
						throw new ArgumentException("No parameters may be null", "hashes");

					hash.WriteTo(ms);
				}

				return Of(ms.ToArray());
			}
		}

		IHash IHasher.Of(params IHash[] hashes)
		{
			return Of(hashes);
		}

		public H Of(params ISerializable[] items)
		{
			if (items == null)
				throw new ArgumentNullException("items");

			if (items.Length == 0)
				throw new ArgumentException("Hash must be of one or more items", "items");

			using (var ms = new MemoryStream())
			{
				foreach (var item in items)
				{
					if (item == null)
						throw new ArgumentException("No parameters may be null", "items");

					item.WriteTo(ms);
				}

				return Of(ms.ToArray());
			}
		}

		IHash IHasher.Of(params ISerializable[] items)
		{
			return Of(items);
		}

		#endregion Hashing

		#region HMAC

		public H HMAC(byte[] key, byte[] message)
		{
			if (key == null)
				throw new ArgumentNullException("key");

			if (message == null)
				throw new ArgumentNullException("message");

			if (!key.Any())
				throw new ArgumentException("Key parameter is empty array", "key");

			if (!message.Any())
				throw new ArgumentException("Message parameter is empty array", "message");

			ByteSerializable kbs = key;
			ByteSerializable mbs = message;

			return Of(kbs, Of(kbs, mbs));
		}

		public H HMAC(byte[] key, Stream messageStream)
		{
			if (key == null)
				throw new ArgumentNullException("key");

			if (messageStream == null)
				throw new ArgumentNullException("messageStream");

			if (!key.Any())
				throw new ArgumentException("Key parameter is empty array", "key");

			if (!messageStream.CanRead)
				throw new InvalidOperationException("Cannot read from messageStream");

			ByteSerializable kbs = key;

			FileInfo tempFi = null;

			try
			{
				tempFi = new FileInfo(Path.GetTempFileName());

				using (var sOut = tempFi.OpenWrite())
				{
					sOut.Write(key, 0, key.Length);
					messageStream.CopyTo(sOut);
				}

				using (var sIn = tempFi.OpenRead())
				{
					return Of(kbs, Of(sIn));
				}
			}
			finally
			{
				if (tempFi != null)
					tempFi.Delete();
			}
		}

		IHash IHasher.HMAC(byte[] key, byte[] message)
		{
			return HMAC(key, message);
		}

		IHash IHasher.HMAC(byte[] key, Stream messageStream)
		{
			return HMAC(key, messageStream);
		}

		IHash IHasher.HMAC(byte[] key, ISerializable message)
		{
			if (message == null)
				throw new ArgumentNullException("message");

			return HMAC(key, message.ToByteArray());
		}

		#endregion HMAC

		#region "Constants"

		public H MinValue
		{
			get { return Deserialize(new byte[_size]); }
		}

		IHash IHasher.MinValue
		{
			get { return MinValue; }
		}

		public H MaxValue
		{
			get { return Deserialize(Enumerable.Repeat(byte.MaxValue, _size).ToArray()); }
		}

		IHash IHasher.MaxValue
		{
			get { return MaxValue; }
		}

		int IHasher.HashSize
		{
			get { return _size; }
		}

		#endregion "Constants"

		#region Serialization

		public H Deserialize(byte[] serialized)
		{
			if (serialized == null)
				throw new ArgumentNullException("serialized");

			return _deserializer(serialized);
		}

		public H ReadFrom(Stream s)
		{
			if (s == null)
				throw new ArgumentNullException("s");

			var br = new BinaryReader(s);

			return Deserialize(br.ReadBytes(_size));
		}

		#endregion Serialization

		Type IHasher.HashType
		{
			get { return typeof(H); }
		}
	}
}
