﻿using System;
using System.IO;

using PXL.Units.Utilities;

namespace PXL.Units
{
	/// <summary>
	/// A conversion class to allow byte arrays to be passed with other
	/// ISerializables to hash functions. Implicitly convertible to
	/// and from a byte array.
	/// </summary>
	/// <example>
	/// <para>ByteSerializable bytes = new byte[] { 0xf0, 0xb0, 0xc0 };</para>
	/// <para>var otherSerializable = new SomeOtherSerializable();</para>
	/// <para>var hash = SHA256Hash.Factory.Combine(bytes, otherSerializable)</para>
	/// </example>
	public struct ByteSerializable : ISerializable, IComparable<ByteSerializable>, IComparable<byte[]>
	{
		private byte[] _bytes;

		#region Conversion

		/// <summary>
		/// Converts a byte array to a ByteSerializable
		/// </summary>
		/// <param name="bytes">The bytes to convert</param>
		/// <returns>
		/// The ByteSerializable if the bytes parameter is not null,
		/// a ByteSerializable representing an empty byte array otherwise
		/// </returns>
		public static implicit operator ByteSerializable(byte[] bytes)
		{
			return new ByteSerializable { _bytes = bytes };
		}

		/// <summary>
		/// Converts a ByteSerializable to a byte array
		/// </summary>
		/// <param name="bs">The ByteSerializable to convert</param>
		/// <returns>
		/// The byte array represented by the bs parameter
		/// </returns>
		public static implicit operator byte[](ByteSerializable bs)
		{
			return bs.ToByteArray();
		}

		#endregion Conversion

		#region Serialization

		/// <summary>
		/// Converts the ByteSerializable to a byte array
		/// </summary>
		/// <returns>The byte array representation of the ByteSerializable</returns>
		public byte[] ToByteArray()
		{
			return _bytes ?? new byte[0];
		}

		/// <summary>
		/// Writes the ByteSerializable to a stream
		/// </summary>
		/// <param name="s">The stream to write to</param>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the s parameter is null
		/// </exception>
		public void WriteTo(Stream s)
		{
			if (s == null)
				throw new ArgumentNullException("s");

			var b = ToByteArray();

			s.Write(b, 0, b.Length);
		}

		#endregion Serialization

		#region Comparable

		public int CompareTo(ByteSerializable other)
		{
			return ByteArrayExtensions.CompareByteArrays(_bytes, other._bytes);
		}

		public int CompareTo(byte[] other)
		{
			return ByteArrayExtensions.CompareByteArrays(_bytes, other);
		}

		#endregion Comparable
	}
}