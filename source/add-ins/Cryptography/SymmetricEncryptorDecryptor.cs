﻿using System;
using System.IO;
using System.Linq;
using System.Security.Cryptography;

using PXL.Units;

namespace PXL.Cryptography
{
	/// <summary>
	/// Provides an easy way to symmetrically encrypt and decrypt data
	/// with automatic handling of initialization vectors and
	/// hash message authentication codes
	/// </summary>
	public class SymmetricEncryptorDecryptor : IDisposable
	{
		#region Fields

		private static readonly RandomHelper _defaultRandomHelper;

		private readonly SymmetricAlgorithm _algorithm;
		private readonly IRandomHelper _rh;

		/// <summary>
		/// The size of the required key, in bytes
		/// </summary>
		public readonly int KeySize;

		/// <summary>
		/// The size of the block, in bytes
		/// </summary>
		public readonly int BlockSize;

		private bool _isDisposed;

		#endregion Fields

		#region Factories

		/// <summary>
		/// Gets a new AES symmetric encryptor/decryptor
		/// with a CBC ciphermode and PKCS7 padding
		/// </summary>
		/// <param name="rng">
		/// An optional random number generator. If not specified, a static RNG is used
		/// </param>
		/// <returns>An AES SymmetricEncryptorDecryptor</returns>
		// ReSharper disable once InconsistentNaming
		public static SymmetricEncryptorDecryptor AES(RandomNumberGenerator rng = null)
		{
			return new SymmetricEncryptorDecryptor(
				new AesManaged
				{
					BlockSize = 128,
					KeySize = 128,
					Mode = CipherMode.CBC,
					Padding = PaddingMode.PKCS7,
				}, rng
			);
		}

		/// <summary>
		/// Gets a new AES symmetric encryptor/decryptor
		/// with a CBC ciphermode and PKCS7 padding
		/// </summary>
		/// <param name="rh">
		/// The random helper to generate keys and initialization vectors
		/// </param>
		/// <returns>An AES SymmetricEncryptorDecryptor</returns>
		/// <exception cref="System.ArgumentException">
		/// Thrown if the rh parameter is null
		/// </exception>
		/// <exception cref="System.ArgumentException">
		/// Thrown if the rh parameter is not cryptographically strong
		/// </exception>
		// ReSharper disable once InconsistentNaming
		public static SymmetricEncryptorDecryptor AES(IRandomHelper rh)
		{
			return new SymmetricEncryptorDecryptor(
				new AesManaged
				{
					BlockSize = 128,
					KeySize = 128,
					Mode = CipherMode.CBC,
					Padding = PaddingMode.PKCS7,
				}, rh
			);
		}

		/// <summary>
		/// Gets a new Rijnadael symmetric encryptor/decryptor
		/// with a key size compatible with AES encryptors/decryptors
		/// Uses CBC ciphermode and PKCS7 padding
		/// </summary>
		/// <param name="rng">
		/// An optional random number generator. If not specified, a static RNG is used
		/// </param>
		/// <returns>An AES-compatible Rijnadael SymmetricEncryptorDecryptor</returns>
		// ReSharper disable once InconsistentNaming
		public static SymmetricEncryptorDecryptor RijnadaelAESCompatible(RandomNumberGenerator rng = null)
		{
			return new SymmetricEncryptorDecryptor(
				new RijndaelManaged
				{
					BlockSize = 256,
					KeySize = 128,
					Mode = CipherMode.CBC,
					Padding = PaddingMode.PKCS7,
				}, rng
			);
		}

		/// <summary>
		/// Gets a new Rijnadael symmetric encryptor/decryptor
		/// with a key size compatible with AES encryptors/decryptors
		/// Uses CBC ciphermode and PKCS7 padding
		/// </summary>
		/// <param name="rh">
		/// The random helper to generate keys and initialization vectors
		/// </param>
		/// <returns>An AES-compatible rijnadael SymmetricEncryptorDecryptor</returns>
		/// <exception cref="System.ArgumentException">
		/// Thrown if the rh parameter is null
		/// </exception>
		/// <exception cref="System.ArgumentException">
		/// Thrown if the rh parameter is not cryptographically strong
		/// </exception>
		// ReSharper disable once InconsistentNaming
		public static SymmetricEncryptorDecryptor RijnadaelAESCompatible(IRandomHelper rh)
		{
			return new SymmetricEncryptorDecryptor(
				new RijndaelManaged
				{
					BlockSize = 256,
					KeySize = 128,
					Mode = CipherMode.CBC,
					Padding = PaddingMode.PKCS7,
				}, rh
			);
		}

		/// <summary>
		/// Gets a new Rijnadael encryptor/decryptor
		/// with a 256 bit key size, CBC ciphermode and PKCS7 padding
		/// </summary>
		/// <param name="rng">
		/// An optional random number generator. If not specified, a static RNG is used
		/// </param>
		/// <returns>An Rijnadael SymmetricEncryptorDecryptor</returns>
		public static SymmetricEncryptorDecryptor Rijnadael(RandomNumberGenerator rng = null)
		{
			return new SymmetricEncryptorDecryptor(
				new RijndaelManaged
				{
					BlockSize = 256,
					KeySize = 256,
					Mode = CipherMode.CBC,
					Padding = PaddingMode.PKCS7,
				}, rng
			);
		}

		/// <summary>
		/// Gets a new Rijnadael encryptor/decryptor
		/// with a 256 bit key size, CBC ciphermode and PKCS7 padding
		/// </summary>
		/// <param name="rh">
		/// The random helper to generate keys and initialization vectors
		/// </param>
		/// <returns>An Rijnadael SymmetricEncryptorDecryptor</returns>
		/// <exception cref="System.ArgumentException">
		/// Thrown if the rh parameter is null
		/// </exception>
		/// <exception cref="System.ArgumentException">
		/// Thrown if the rh parameter is not cryptographically strong
		/// </exception>
		public static SymmetricEncryptorDecryptor Rijnadael(IRandomHelper rh)
		{
			return new SymmetricEncryptorDecryptor(
				new RijndaelManaged
				{
					BlockSize = 256,
					KeySize = 256,
					Mode = CipherMode.CBC,
					Padding = PaddingMode.PKCS7,
				}, rh
			);
		}

		#endregion Factories

		#region Constructors

		static SymmetricEncryptorDecryptor()
		{
			_defaultRandomHelper = RandomHelper.Strong;
		}

		/// <summary>
		/// Creates a new SymmetricEncryptorDecryptor around the given symmetric algorithm
		/// </summary>
		/// <param name="algorithm">
		/// The symmetric algorithm around which to build an encryptor/decryptor
		/// </param>
		/// <param name="rng">
		/// An optional random number generator. If not specified, a static RNG is used
		/// </param>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the algorithm parameter is null
		/// </exception>
		public SymmetricEncryptorDecryptor(SymmetricAlgorithm algorithm, RandomNumberGenerator rng = null)
		{
			if (algorithm == null)
				throw new ArgumentNullException("algorithm");

			_algorithm = algorithm;
			KeySize = algorithm.KeySize / 8;
			BlockSize = algorithm.BlockSize / 8;

			_rh = rng == null
				? _defaultRandomHelper
				: new RandomHelper(rng, true);
		}

		/// <summary>
		/// Creates a new SymmetricEncryptorDecryptor around the given symmetric algorithm
		/// </summary>
		/// <param name="algorithm">
		/// The symmetric algorithm around which to build an encryptor/decryptor
		/// </param>
		/// <param name="rh">
		/// The random helper to generate keys and initialization vectors
		/// </param>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the algorithm or rh parameters are null
		/// </exception>
		/// <exception cref="System.ArgumentException">
		/// Thrown if the rh parameter is not cryptographically strong
		/// </exception>
		public SymmetricEncryptorDecryptor(SymmetricAlgorithm algorithm, IRandomHelper rh)
		{
			if (algorithm == null)
				throw new ArgumentNullException("algorithm");

			if( rh == null )
				throw new ArgumentNullException("rh");
			
			if( ! rh.IsCryptographicallyStrong )
				throw new ArgumentException("Random helper must be cryptographically strong", "rh");

			_algorithm = algorithm;

			KeySize = algorithm.KeySize / 8;
			BlockSize = algorithm.BlockSize / 8;

			_rh = rh;
		}

		#endregion Constructors

		#region Encrypt

		/// <summary>
		/// Encrypts the given item using the specified key and a random initialization vector
		/// </summary>
		/// <param name="item">The item to encrypt</param>
		/// <param name="key">The key to encrypt the item with</param>
		/// <returns>
		/// An encrypted version of the data with built in initialization vector and HMAC
		/// </returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the item or key parameters are null
		/// </exception>
		/// <exception cref="ArgumentException">
		/// Thrown if the item serializes to an empty or the key size is not valid
		/// for the symmetric algorithm used
		/// </exception>
		public virtual byte[] Encrypt(ISerializable item, byte[] key)
		{
			if( item == null )
				throw new ArgumentNullException("item");

			return Encrypt(item.ToByteArray(), key);
		}

		/// <summary>
		/// Encrypts the given item using the specified key and initialization vector
		/// </summary>
		/// <param name="item">The item to encrypt</param>
		/// <param name="key">The key to encrypt the data with</param>
		/// <param name="iv">The initialization vector to use</param>
		/// <returns>
		/// An encrypted version of the data with built in initialization vector and HMAC
		/// </returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the item, key, or iv parameters are null
		/// </exception>
		/// <exception cref="ArgumentException">
		/// Thrown if
		/// <para>the item serializes to an empty array</para>
		/// <para>the key size is not valid for the symmetric algorithm used</para>
		/// <para>the initialization vector size is not valid for the symmetric algorithm used</para>
		/// </exception>
		/// <exception cref="System.InvalidOperationException">
		/// Thrown if the encryptor/decryptor has been disposed
		/// </exception>
		public virtual byte[] Encrypt(ISerializable item, byte[] key, byte[] iv)
		{
			if (item == null)
				throw new ArgumentNullException("item");

			return Encrypt(item.ToByteArray(), key, iv);
		}

		/// <summary>
		/// Encrypts the given data using the specified key and a random initialization vector
		/// </summary>
		/// <param name="data">The data to encrypt</param>
		/// <param name="key">The key to encrypt the data with</param>
		/// <returns>
		/// An encrypted version of the data with built in initialization vector and HMAC
		/// </returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the data or key parameters are null
		/// </exception>
		/// <exception cref="ArgumentException">
		/// Thrown if the data array is empty or the key size is not valid
		/// for the symmetric algorithm used
		/// </exception>
		public virtual byte[] Encrypt(byte[] data, byte[] key)
		{
			return Encrypt(data, key, _rh.NextBytes(BlockSize));
		}

		/// <summary>
		/// Encrypts the given data using the specified key and initialization vector
		/// </summary>
		/// <param name="data">The data to encrypt</param>
		/// <param name="key">The key to encrypt the data with</param>
		/// <param name="iv">The initialization vector to use</param>
		/// <returns>
		/// An encrypted version of the data with built in initialization vector and HMAC
		/// </returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the data, key, or iv parameters are null
		/// </exception>
		/// <exception cref="ArgumentException">
		/// Thrown if
		/// <para>the data array is empty</para>
		/// <para>the key size is not valid for the symmetric algorithm used</para>
		/// <para>the initialization vector size is not valid for the symmetric algorithm used</para>
		/// </exception>
		/// <exception cref="System.InvalidOperationException">
		/// Thrown if the encryptor/decryptor has been disposed
		/// </exception>
		public virtual byte[] Encrypt(byte[] data, byte[] key, byte[] iv)
		{
			if (data == null)
				throw new ArgumentNullException("data");

			if (!data.Any())
				throw new ArgumentException("Data cannot be empty", "data");

			using (var msIn = new MemoryStream())
			{
				msIn.Write(data, 0, data.Length);
				msIn.Seek(0, SeekOrigin.Begin);

				using (var msOut = new MemoryStream())
				{
					Encrypt(msIn, msOut, key, iv);

					return msOut.ToArray();
				}
			}
		}

		/// <summary>
		/// Encrypts the given data using the specified key and a random initialization vector
		/// </summary>
		/// <param name="clearStream">The stream containing clear text data to read from</param>
		/// <param name="key">The key to encrypt the data with</param>
		/// <returns>
		/// An encrypted version of the data with built in initialization vector and HMAC
		/// </returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the data or key parameters are null
		/// </exception>
		/// <exception cref="ArgumentException">
		/// Thrown if the key size is not valid
		/// for the symmetric algorithm used
		/// </exception>
		/// <exception cref="System.InvalidOperationException">
		/// Thrown if the encryptor/decryptor has been disposed or
		/// if the clearStream does not allow reading or seeking
		/// </exception>
		public virtual byte[] Encrypt(Stream clearStream, byte[] key)
		{
			return Encrypt(clearStream, key, _rh.NextBytes(BlockSize));
		}

		/// <summary>
		/// Encrypts the given data using the specified key and initialization vector
		/// </summary>
		/// <param name="clearStream">The stream containing clear text data to read from</param>
		/// <param name="key">The key to encrypt the data with</param>
		/// <param name="iv">The initialization vector to use</param>
		/// <returns>
		/// An encrypted version of the data with built in initialization vector and HMAC
		/// </returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the data, key, or iv parameters are null
		/// </exception>
		/// <exception cref="ArgumentException">
		/// Thrown if
		/// <para>the key size is not valid for the symmetric algorithm used</para>
		/// <para>the initialization vector size is not valid for the symmetric algorithm used</para>
		/// </exception>
		/// <exception cref="System.InvalidOperationException">
		/// Thrown if the encryptor/decryptor has been disposed or
		/// if the clearStream does not allow reading or seeking
		/// </exception>
		public virtual byte[] Encrypt(Stream clearStream, byte[] key, byte[] iv)
		{
			using (var msOut = new MemoryStream())
			{
				Encrypt(clearStream, msOut, key, iv);

				return msOut.ToArray();
			}
		}

		/// <summary>
		/// Encrypts the given item using the specified key and a random initialization vector
		/// </summary>
		/// <param name="data">The data to encrypt</param>
		/// <param name="cipherStream">The stream to which cipher text will be written</param>
		/// <param name="key">The key to encrypt the data with</param>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if any parameters are null
		/// </exception>
		/// <exception cref="ArgumentException">
		/// Thrown if
		/// <para>the data array is empty</para>
		/// <para>the key size is not valid for the symmetric algorithm used</para>
		/// <para>the initialization vector size is not valid for the symmetric algorithm used</para>
		/// </exception>
		/// <exception cref="System.InvalidOperationException">
		/// Thrown if the encryptor/decryptor has been disposed or
		/// if the clearStream does not allow reading or seeking or the cipherStream does not allow writing
		/// </exception>
		public virtual void Encrypt(byte[] data, Stream cipherStream, byte[] key)
		{
			Encrypt(data, cipherStream, key, _rh.NextBytes(BlockSize));
		}

		/// <summary>
		/// Encrypts the given item using the specified key and initialization vector
		/// </summary>
		/// <param name="data">The data to encrypt</param>
		/// <param name="cipherStream">The stream to which cipher text will be written</param>
		/// <param name="key">The key to encrypt the data with</param>
		/// <param name="iv">The initialization vector to use</param>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if any parameters are null
		/// </exception>
		/// <exception cref="ArgumentException">
		/// Thrown if
		/// <para>the data array is empty</para>
		/// <para>the key size is not valid for the symmetric algorithm used</para>
		/// <para>the initialization vector size is not valid for the symmetric algorithm used</para>
		/// </exception>
		/// <exception cref="System.InvalidOperationException">
		/// Thrown if the encryptor/decryptor has been disposed or
		/// if the clearStream does not allow reading or seeking or the cipherStream does not allow writing
		/// </exception>
		public virtual void Encrypt(byte[] data, Stream cipherStream, byte[] key, byte[] iv)
		{
			if (data == null)
				throw new ArgumentNullException("data");

			if (!data.Any())
				throw new ArgumentException("Data cannot be empty", "data");

			using (var msIn = new MemoryStream())
			{
				msIn.Write(data, 0, data.Length);
				msIn.Seek(0, SeekOrigin.Begin);

				Encrypt(msIn, cipherStream, key, iv);
			}
		}

		/// <summary>
		/// Encrypts the given item using the specified key and a random initialization vector
		/// </summary>
		/// <param name="item">The item to encrypt</param>
		/// <param name="cipherStream">The stream to which cipher text will be written</param>
		/// <param name="key">The key to encrypt the data with</param>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if any parameters are null
		/// </exception>
		/// <exception cref="ArgumentException">
		/// Thrown if
		/// <para>the key size is not valid for the symmetric algorithm used</para>
		/// <para>the initialization vector size is not valid for the symmetric algorithm used</para>
		/// </exception>
		/// <exception cref="System.InvalidOperationException">
		/// Thrown if the encryptor/decryptor has been disposed or
		/// if the clearStream does not allow reading or seeking or the cipherStream does not allow writing
		/// </exception>
		public virtual void Encrypt(ISerializable item, Stream cipherStream, byte[] key)
		{
			if (item == null)
				throw new ArgumentNullException("item");

			Encrypt(item, cipherStream, key, _rh.NextBytes(BlockSize));
		}

		/// <summary>
		/// Encrypts the given item using the specified key and initialization vector
		/// </summary>
		/// <param name="item">The item to encrypt</param>
		/// <param name="cipherStream">The stream to which cipher text will be written</param>
		/// <param name="key">The key to encrypt the data with</param>
		/// <param name="iv">The initialization vector to use</param>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if any parameters are null
		/// </exception>
		/// <exception cref="ArgumentException">
		/// Thrown if
		/// <para>the key size is not valid for the symmetric algorithm used</para>
		/// <para>the initialization vector size is not valid for the symmetric algorithm used</para>
		/// </exception>
		/// <exception cref="System.InvalidOperationException">
		/// Thrown if the encryptor/decryptor has been disposed or
		/// if the clearStream does not allow reading or seeking or the cipherStream does not allow writing
		/// </exception>
		public virtual void Encrypt(ISerializable item, Stream cipherStream, byte[] key, byte[] iv)
		{
			if (item == null)
				throw new ArgumentNullException("item");

			using (var ms = new MemoryStream())
			{
				item.WriteTo(ms);
				ms.Seek(0, SeekOrigin.Begin);
				Encrypt(ms, cipherStream, key, iv);
			}
		}

		/// <summary>
		/// Encrypts the given data using the specified key and a random initialization vector
		/// </summary>
		/// <param name="clearStream">The stream containing clear text data to read from</param>
		/// <param name="cipherStream">The stream to which cipher text will be written</param>
		/// <param name="key">The key to encrypt the data with</param>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if any parameters are null
		/// </exception>
		/// <exception cref="ArgumentException">
		/// Thrown if
		/// <para>the key size is not valid for the symmetric algorithm used</para>
		/// <para>the initialization vector size is not valid for the symmetric algorithm used</para>
		/// </exception>
		/// <exception cref="System.InvalidOperationException">
		/// Thrown if the encryptor/decryptor has been disposed or
		/// if the clearStream does not allow reading or seeking or the cipherStream does not allow writing
		/// </exception>
		public virtual void Encrypt(Stream clearStream, Stream cipherStream, byte[] key)
		{
			Encrypt(clearStream, cipherStream, key, _rh.NextBytes(BlockSize));
		}

		/// <summary>
		/// Encrypts the given data using the specified key and initialization vector
		/// </summary>
		/// <param name="clearStream">The stream containing clear text data to read from</param>
		/// <param name="cipherStream">The stream to which cipher text will be written</param>
		/// <param name="key">The key to encrypt the data with</param>
		/// <param name="iv">The initialization vector to use</param>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if any parameters are null
		/// </exception>
		/// <exception cref="ArgumentException">
		/// Thrown if
		/// <para>the key size is not valid for the symmetric algorithm used</para>
		/// <para>the initialization vector size is not valid for the symmetric algorithm used</para>
		/// </exception>
		/// <exception cref="System.InvalidOperationException">
		/// Thrown if the encryptor/decryptor has been disposed or
		/// if the clearStream does not allow reading or seeking or the cipherStream does not allow writing
		/// </exception>
		public virtual void Encrypt(Stream clearStream, Stream cipherStream, byte[] key, byte[] iv)
		{
			if (clearStream == null)
				throw new ArgumentNullException("clearStream");

			if( ! clearStream.CanRead )
				throw new InvalidOperationException("Cannot read from clearStream");

			if (!clearStream.CanSeek)
				throw new InvalidOperationException("Cannot seek in clearStream");

			if (cipherStream == null)
				throw new ArgumentNullException("cipherStream");

			if (!cipherStream.CanWrite)
				throw new InvalidOperationException("Cannot write to cipherStream");

			if (key == null)
				throw new ArgumentNullException("key");

			if (key.Length != KeySize)
				throw new ArgumentException("Invalid key size", "key");

			if (iv == null)
				throw new ArgumentNullException("iv");

			if (iv.Length != BlockSize)
				throw new ArgumentException("Invalid initialization vector size", "iv");

			if (_isDisposed)
				throw new InvalidOperationException("Encryptor/Decryptor has been disposed");

			var previousStreamPosition = clearStream.Position;

			// hmac =  H(key + H(key + message)) 
			var hmac = SHA256Hash.Factory.HMAC(key, clearStream);

			clearStream.Seek(previousStreamPosition - clearStream.Position, SeekOrigin.Current);

			_algorithm.Clear();

			try
			{
				var encryptor = _algorithm.CreateEncryptor(key, iv);

				cipherStream.Write(iv, 0, iv.Length);

				var es = new CryptoStream(cipherStream, encryptor, CryptoStreamMode.Write);

				es.Write(hmac.ToByteArray(), 0, SHA256Hash.Size);

				int read;
				var buffer = new byte[ushort.MaxValue];

				while ((read = clearStream.Read(buffer, 0, buffer.Length)) > 0)
				{
					es.Write(buffer, 0, read);
				}

				es.FlushFinalBlock();
			}
			finally
			{
				_algorithm.Clear();
			}
		}

		#endregion Encrypt

		#region Decrypt

		/// <summary>
		/// Decrypts previously encrypted given data using the specified key into an item
		/// </summary>
		/// <typeparam name="T">The type of item to decrypt into</typeparam>
		/// <param name="encrypted">The data to decrypt</param>
		/// <param name="key">The key to decrypt the data with</param>
		/// <param name="deserializer">The deserializer to decrypt the item with</param>
		/// <returns>
		/// An decrypted version of the data deserialized into an item
		/// </returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the encrypted, key, or deserializer parameters are null
		/// </exception>
		/// <exception cref="ArgumentException">
		/// Thrown if the key size is not valid for the symmetric algorithm used
		/// </exception>
		/// <exception cref="System.FormatException">
		/// Thrown if
		/// <para>Encrypted is not long enough to define an acceptable message</para>
		/// <para>The HMAC fails to validate indicating a malformed or tampered-with message</para>
		/// </exception>
		/// <exception cref="System.InvalidOperationException">
		/// Thrown if the encryptor/decryptor has been disposed
		/// </exception>
		public virtual T DecryptTo<T>(byte[] encrypted, byte[] key, IDeserializer<T> deserializer)
		{
			if (deserializer == null)
				throw new ArgumentNullException("deserializer");

			return deserializer.Deserialize(Decrypt(encrypted, key));
		}

		/// <summary>
		/// Decrypts previously encrypted given data using the specified key into an item
		/// </summary>
		/// <typeparam name="T">The type to decrypt into</typeparam>
		/// <typeparam name="D">The deserializer type</typeparam>
		/// <param name="encrypted">The data to decrypt</param>
		/// <param name="key">The key to decrypt the data with</param>
		/// <returns>
		/// An decrypted version of the data deserialized into an item
		/// </returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the encrypted or key parameters are null
		/// </exception>
		/// <exception cref="ArgumentException">
		/// Thrown if the key size is not valid for the symmetric algorithm used
		/// </exception>
		/// <exception cref="System.FormatException">
		/// Thrown if
		/// <para>Encrypted is not long enough to define an acceptable message</para>
		/// <para>The HMAC fails to validate indicating a malformed or tampered-with message</para>
		/// </exception>
		/// <exception cref="System.InvalidOperationException">
		/// Thrown if the encryptor/decryptor has been disposed
		/// </exception>
		// ReSharper disable once InconsistentNaming
		public virtual T DecryptTo<T, D>(byte[] encrypted, byte[] key)
			where D : IDeserializer<T>, new()
		{
			return new D().Deserialize(Decrypt(encrypted, key));
		}

		/// <summary>
		/// Decrypts previously encrypted given data using the specified key
		/// </summary>
		/// <param name="encrypted">The data to decrypt</param>
		/// <param name="key">The key to decrypt the data with</param>
		/// <returns>
		/// An decrypted version of the data
		/// </returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the encrypted or key parameters are null
		/// </exception>
		/// <exception cref="ArgumentException">
		/// Thrown if the key size is not valid for the symmetric algorithm used
		/// </exception>
		/// <exception cref="System.FormatException">
		/// Thrown if
		/// <para>Encrypted is not long enough to define an acceptable message</para>
		/// <para>The HMAC fails to validate indicating a malformed or tampered-with message</para>
		/// </exception>
		/// <exception cref="System.InvalidOperationException">
		/// Thrown if the encryptor/decryptor has been disposed
		/// </exception>
		public virtual byte[] Decrypt(byte[] encrypted, byte[] key)
		{
			if (encrypted == null)
				throw new ArgumentNullException("encrypted");

			if (key == null)
				throw new ArgumentNullException("key");

			if (encrypted.Length <= KeySize)
				throw new FormatException("Not enough encrypted bytes to define an encrypted message acceptible to this encrypter");

			if (key.Length != KeySize)
				throw new ArgumentException("Invalid key size", "key");

			using (var sCipher = new MemoryStream())
			{
				sCipher.Write(encrypted, 0, encrypted.Length);
				sCipher.Seek(0, SeekOrigin.Begin);

				using (var sClear = new MemoryStream())
				{
					Decrypt(sCipher, sClear, key);
					return sClear.ToArray();
				}
			}
		}

		/// <summary>
		/// Decrypts previously encrypted given data using the specified key into an item
		/// </summary>
		/// <typeparam name="T">The type of item to decrypt into</typeparam>
		/// <param name="cipherStream">The stream containing the data to decrypt</param>
		/// <param name="key">The key to decrypt the data with</param>
		/// <param name="deserializer">The deserializer to decrypt the item with</param>
		/// <returns>
		/// An decrypted version of the data deserialized into an item
		/// </returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the encrypted, key, or deserializer parameters are null
		/// </exception>
		/// <exception cref="ArgumentException">
		/// Thrown if the key size is not valid for the symmetric algorithm used
		/// </exception>
		/// <exception cref="System.FormatException">
		/// Thrown if
		/// <para>Encrypted is not long enough to define an acceptable message</para>
		/// <para>The HMAC fails to validate indicating a malformed or tampered-with message</para>
		/// </exception>
		/// <exception cref="System.InvalidOperationException">
		/// Thrown if the encryptor/decryptor has been disposed
		/// or if the cipherStream does not support reading
		/// or if the clearStream does not support writing
		/// </exception>
		public virtual T DecryptTo<T>(Stream cipherStream, byte[] key, IDeserializer<T> deserializer)
		{
			if (deserializer == null)
				throw new ArgumentNullException("deserializer");

			FileInfo fi = null;

			try
			{
				fi = new FileInfo(Path.GetTempFileName());

				using (var sOut = fi.OpenWrite())
				{
					Decrypt(cipherStream, sOut, key);
				}

				using (var sIn = fi.OpenRead())
				{
					return deserializer.ReadFrom(sIn);
				}
			}
			finally
			{
				if (fi != null)
					fi.Delete();
			}
		}

		/// <summary>
		/// Decrypts previously encrypted given data using the specified key into an item
		/// </summary>
		/// <typeparam name="T">The type to decrypt into</typeparam>
		/// <typeparam name="D">The deserializer type</typeparam>
		/// <param name="cipherStream">The stream containing the data to decrypt</param>
		/// <param name="key">The key to decrypt the data with</param>
		/// <returns>
		/// An decrypted version of the data deserialized into an item
		/// </returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the encrypted or key parameters are null
		/// </exception>
		/// <exception cref="ArgumentException">
		/// Thrown if the key size is not valid for the symmetric algorithm used
		/// </exception>
		/// <exception cref="System.FormatException">
		/// Thrown if
		/// <para>Encrypted is not long enough to define an acceptable message</para>
		/// <para>The HMAC fails to validate indicating a malformed or tampered-with message</para>
		/// </exception>
		/// <exception cref="System.InvalidOperationException">
		/// Thrown if the encryptor/decryptor has been disposed
		/// or if the cipherStream does not support reading
		/// or if the clearStream does not support writing
		/// </exception>
		// ReSharper disable once InconsistentNaming
		public virtual T DecryptTo<T, D>(Stream cipherStream, byte[] key)
			where D : IDeserializer<T>, new()
		{
			return DecryptTo(cipherStream, key, new D());
		}

		/// <summary>
		/// Decrypts previously encrypted given data using the specified key
		/// </summary>
		/// <param name="encrypted">The data to decrypt</param>
		/// <param name="clearStream">The stream to which clear text will be written</param>
		/// <param name="key">The key to decrypt the data with</param>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the any parameters are null
		/// </exception>
		/// <exception cref="System.FormatException">
		/// Thrown if
		/// <para>Encrypted is not long enough to define an acceptable message</para>
		/// <para>The HMAC fails to validate indicating a malformed or tampered-with message</para>
		/// </exception>
		/// <exception cref="System.InvalidOperationException">
		/// Thrown if the encryptor/decryptor has been disposed
		/// or if the clearStream does not support writing
		/// </exception>
		public virtual void Decrypt(byte[] encrypted, Stream clearStream, byte[] key)
		{
			if( encrypted == null )
				throw new ArgumentNullException("encrypted");

			if (encrypted.Length <= KeySize)
				throw new FormatException("Not enough encrypted bytes to define an encrypted message acceptible to this encrypter");

			using (var ms = new MemoryStream())
			{
				ms.Write(encrypted, 0, encrypted.Length);
				ms.Seek(0, SeekOrigin.Begin);
				Decrypt(ms, clearStream, key);
			}
		}

		/// <summary>
		/// Decrypts previously encrypted given data using the specified key
		/// </summary>
		/// <param name="cipherStream">The stream containing data to decrypt</param>
		/// <param name="clearStream">The stream to which clear text will be written</param>
		/// <param name="key">The key to decrypt the data with</param>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the any parameters are null
		/// </exception>
		/// <exception cref="ArgumentException">
		/// Thrown if the key size is not valid for the symmetric algorithm used
		/// </exception>
		/// <exception cref="System.FormatException">
		/// Thrown if
		/// <para>The HMAC fails to validate indicating a malformed or tampered-with message</para>
		/// </exception>
		/// <exception cref="System.InvalidOperationException">
		/// Thrown if the encryptor/decryptor has been disposed
		/// or if the cipherStream does not support reading
		/// or if the clearStream does not support writing
		/// </exception>
		public virtual void Decrypt(Stream cipherStream, Stream clearStream, byte[] key)
		{
			if (cipherStream == null)
				throw new ArgumentNullException("cipherStream");

			if( ! cipherStream.CanRead )
				throw new InvalidOperationException("Cipher stream does not support reading");

			if( clearStream == null )
				throw new ArgumentNullException("clearStream");

			if( ! clearStream.CanWrite )
				throw new InvalidOperationException("Clear stream does not support writing");

			if (key == null)
				throw new ArgumentNullException("key");

			if (key.Length != KeySize)
				throw new ArgumentException("Invalid key size", "key");

			if (_isDisposed)
				throw new InvalidOperationException("Encryptor/Decryptor has been disposed");

			FileInfo tempFi = null;

			try
			{
				var iv = new byte[BlockSize];
				cipherStream.Read(iv, 0, iv.Length);

				_algorithm.Clear();

				var decryptor = _algorithm.CreateDecryptor(key, iv);

				tempFi = new FileInfo(Path.GetTempFileName());

				using (var sOut = tempFi.OpenWrite())
				{
					using (var ds = new CryptoStream(sOut, decryptor, CryptoStreamMode.Write))
					{

						int read;
						var buffer = new byte[ushort.MaxValue];

						while ((read = cipherStream.Read(buffer, 0, buffer.Length)) > 0)
						{
							ds.Write(buffer, 0, read);
						}
					}
				}

				using (var sIn = tempFi.OpenRead())
				{
					// ReSharper disable once InconsistentNaming
					var storedHMAC = SHA256Hash.Factory.ReadFrom(sIn);

					var startPosition = sIn.Position;

					// hmac =  H(key + H(key + message)) 
					// ReSharper disable once InconsistentNaming
					var computedHMAC = SHA256Hash.Factory.HMAC(key, sIn);

					if (storedHMAC != computedHMAC)
						throw new FormatException("Invalid HMAC or message");

					sIn.Seek(startPosition - sIn.Position, SeekOrigin.Current);

					sIn.CopyTo(clearStream);
				}
			}
			finally
			{
				_algorithm.Clear();

				if( tempFi != null )
					tempFi.Delete();
			}
		}

		#endregion Decrypt

		#region Dispose

		public virtual void Dispose()
		{
			_algorithm.Dispose();
			_isDisposed = true;
		}

		#endregion Dispose
	}
}
