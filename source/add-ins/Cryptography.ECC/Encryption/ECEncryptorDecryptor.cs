﻿using System;
using System.IO;
using System.Numerics;
using System.Security.Cryptography;

namespace PXL.Cryptography.ECC.Encryption
{
	/// <summary>
	/// Provides high(ish) level methods for encrypting and decrypting data
	/// </summary>
	// ReSharper disable once InconsistentNaming
	public class ECEncryptorDecryptor
	{
		#region Fields

		private static readonly RandomHelper _defaultRandomHelper;

		private readonly SymmetricEncryptorDecryptor _ed;
		private readonly ECCurve _curve;
		private readonly IRandomHelper _rh;

		#endregion Fields

		#region Constructors

		static ECEncryptorDecryptor()
		{
			_defaultRandomHelper = RandomHelper.Strong;
		}

		/// <summary>
		/// Creates a new encrypter with the given domain parameters
		/// </summary>
		/// <param name="curve">The elliptic curve to use for the encrypter</param>
		/// <param name="rng">
		/// The random number generator to use for generating K values. If unspecifed,
		/// a singleton instance of RNGCryptoServiceProvider is used.
		/// </param>
		/// <param name="symmetricEncryptor">
		/// The symmetric encryptor/decryptor to perform encryption/decryption
		/// </param>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the curve parameter is null
		/// </exception>
		public ECEncryptorDecryptor(
			ECCurve curve, RandomNumberGenerator rng = null,
			SymmetricEncryptorDecryptor symmetricEncryptor = null
		)
		{
			if (curve == null)
				throw new ArgumentNullException("curve");

			_curve = curve;
			_ed = symmetricEncryptor ?? SymmetricEncryptorDecryptor.Rijnadael(rng);

			_rh = rng != null
				? new RandomHelper(rng, true)
				: _defaultRandomHelper;
		}

		/// <summary>
		/// Creates a new encrypter with the given domain parameters
		/// </summary>
		/// <param name="curve">The elliptic curve to use for the encrypter</param>
		/// <param name="rh">
		/// The random helper to use for generating K values.
		/// </param>
		/// <param name="symmetricEncryptor">
		/// The symmetric encryptor/decryptor to perform encryption/decryption
		/// </param>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the curve or rh parameters are null
		/// </exception>
		/// <exception cref="System.ArgumentException">
		/// Thrown if the rh parameter is not cryptographically strong
		/// </exception>
		public ECEncryptorDecryptor(
			ECCurve curve, IRandomHelper rh,
			SymmetricEncryptorDecryptor symmetricEncryptor = null
		)
		{
			if (curve == null)
				throw new ArgumentNullException("curve");

			if (rh == null)
				throw new ArgumentNullException("rh");

			_curve = curve;
			_ed = symmetricEncryptor ?? SymmetricEncryptorDecryptor.Rijnadael(rh);

			if( ! rh.IsCryptographicallyStrong )
				throw new ArgumentException("Random helper must be cryptographically strong", "rh");

			_rh = rh;
		}

		#endregion Constructors

		#region Encrypt

		/// <summary>
		/// Encrypts a message to a given public key
		/// </summary>
		/// <param name="message">The data to encrypt</param>
		/// <param name="publicKey">
		/// The public key to encrypt the message to, only the associated private key
		/// will be able to decrypt it
		/// </param>
		/// <returns>An encrypted payload</returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the message or public key parameters are null
		/// </exception>
		/// <exception cref="System.ArgumentException">
		/// Thrown if the message is an empty array
		/// </exception>
		/// <exception cref="PointMismatchException">
		/// Thrown if the publicKey parameter is not on the same curve
		/// as the curve specified in the parameters passed on encrypter construction
		/// </exception>
		public virtual ECEncryptedMessage EncryptTo(byte[] message, ECPoint publicKey)
		{
			if (message == null)
				throw new ArgumentNullException("message");

			if (publicKey == null)
				throw new ArgumentNullException("publicKey");

			if( message.Length == 0 )
				throw new ArgumentException("Message cannot be empty", "message");

			if( ! _curve.IsOnCurve(publicKey) )
				throw new PointMismatchException("Public key must be on the curve specified in the constructor parameters");

			var bias = _rh.NextBigInteger(1, _curve.N);

			var secretPoint = publicKey * bias;
			var publicPoint = _curve.G * bias;

			var key = new byte[_ed.KeySize];
			var copyIntoKey = secretPoint.X.Value.ToByteArray();
			Array.Copy(copyIntoKey, key, Math.Min(key.Length, copyIntoKey.Length));

			var cipherMessage = _ed.Encrypt(message, key);

			return new ECEncryptedMessage(publicPoint, cipherMessage);
		}

		/// <summary>
		/// Encrypts an item to a given public key
		/// </summary>
		/// <param name="item">The item to encrypt</param>
		/// <param name="publicKey">
		/// The public key to encrypt the item to, only the associated private key
		/// will be able to decrypt it
		/// </param>
		/// <returns>An encrypted payload</returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the item or public key parameters are null
		/// </exception>
		/// <exception cref="System.ArgumentException">
		/// Thrown if the item serializes to an empty array
		/// </exception>
		/// <exception cref="PointMismatchException">
		/// Thrown if the publicKey parameter is not on the same curve
		/// as the curve specified in the parameters passed on encrypter construction
		/// </exception>
		public virtual ECEncryptedMessage EncryptTo(ISerializable item, ECPoint publicKey)
		{
			if (item == null)
				throw new ArgumentNullException("item");

			return EncryptTo(item.ToByteArray(), publicKey);
		}

		#endregion Encrypt

		#region Decrypt

		/// <summary>
		/// Decrypts an encrypted message
		/// </summary>
		/// <param name="payload">The encrypted payload to decrypt</param>
		/// <param name="privateKey">
		/// The private key associated with the public key the message was encrypted to
		/// </param>
		/// <returns>The decrypted messsage</returns>
		/// <exception cref="System.FormatException">
		/// Thrown if the encrypted payload is invalid, wasn't encrypted with the
		/// given key, or has been tampered with
		/// </exception>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the payload parameter is null
		/// </exception>
		/// <exception cref="System.ArgumentOutOfRangeException">
		/// Thrown if the private key is outside of the bounds of
		/// the curve domain given in the construction parameters for the encrypter
		/// or if it is less than or equal to zero
		/// </exception>
		public virtual byte[] Decrypt(ECEncryptedMessage payload, BigInteger privateKey)
		{
			if (payload == null)
				throw new ArgumentNullException("payload");

			if (privateKey <= 0 || privateKey >= _curve.Q)
				throw new ArgumentOutOfRangeException("privateKey", "Private key must be within the domain specified in the constructor parameters");

			if (payload.SharedSecret.Curve != _curve)
				throw new ArgumentException(
					"The message was encrypted on a different curve than the curve this decryptor was constructed with"
				);

			var secretPoint = payload.SharedSecret * privateKey;

			var key = new byte[_ed.KeySize];
			var copyIntoKey = secretPoint.X.Value.ToByteArray();
			Array.Copy(copyIntoKey, key, Math.Min(key.Length, copyIntoKey.Length));

			return _ed.Decrypt(payload.SymmetricEncryptedMessage, key);
		}

		/// <summary>
		/// Decrypts an encrypted message into an item
		/// </summary>
		/// <typeparam name="T">The type to decrypt into</typeparam>
		/// <param name="payload">The encrypted payload to decrypt</param>
		/// <param name="privateKey">
		/// The private key associated with the public key the message was encrypted to
		/// </param>
		/// <param name="deserializer">The deserializer to decrypt the item with</param>
		/// <returns>The decrypted item</returns>
		/// <exception cref="System.FormatException">
		/// Thrown if the encrypted payload is invalid, wasn't encrypted with the
		/// given key, or has been tampered with
		/// </exception>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the payload or deserializer parameters are null
		/// </exception>
		/// <exception cref="System.ArgumentOutOfRangeException">
		/// Thrown if the private key is outside of the bounds of
		/// the curve domain given in the construction parameters for the encrypter
		/// or if it is less than or equal to zero
		/// </exception>
		public virtual T DecryptTo<T>(ECEncryptedMessage payload, BigInteger privateKey, IDeserializer<T> deserializer)
			where T : ISerializable
		{
			if( deserializer == null )
				throw new ArgumentNullException("deserializer");

			return deserializer.Deserialize(Decrypt(payload, privateKey));

		}

		/// <summary>
		/// Decrypts an encrypted message into an item
		/// </summary>
		/// <typeparam name="D">The deserializer type</typeparam>
		/// <typeparam name="T">The type to decrypt into</typeparam>
		/// <param name="payload">The encrypted payload to decrypt</param>
		/// <param name="privateKey">
		/// The private key associated with the public key the message was encrypted to
		/// </param>
		/// <returns>The decrypted item</returns>
		/// <exception cref="System.FormatException">
		/// Thrown if the encrypted payload is invalid, wasn't encrypted with the
		/// given key, or has been tampered with
		/// </exception>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the payload parameter is null
		/// </exception>
		/// <exception cref="System.ArgumentOutOfRangeException">
		/// Thrown if the private key is outside of the bounds of
		/// the curve domain given in the construction parameters for the encrypter
		/// or if it is less than or equal to zero
		/// </exception>
		// ReSharper disable once InconsistentNaming
		public virtual T DecryptTo<D,T>(ECEncryptedMessage payload, BigInteger privateKey)
			where D : IDeserializer<T>, new()
			where T : ISerializable
		{
			return new D().Deserialize(Decrypt(payload, privateKey));
		}

		#endregion Encrypt / Decrypt
	}
}