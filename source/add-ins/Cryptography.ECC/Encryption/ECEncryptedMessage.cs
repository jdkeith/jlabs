﻿using System;
using System.IO;

using PXL.Units;

namespace PXL.Cryptography.ECC.Encryption
{
	/// <summary>
	/// Provides a container for encrypted messages
	/// and the information required to decrypt and verify them
	/// </summary>
	// ReSharper disable once InconsistentNaming
	public class ECEncryptedMessage : ISerializable
	{
		#region Factory

		private class EncryptedMessageDeserializer : IDeserializer<ECEncryptedMessage>
		{
			private readonly ECCurve _curve;

			public EncryptedMessageDeserializer(ECCurve curve)
			{
				_curve = curve;
			}

			public ECEncryptedMessage Deserialize(byte[] serialized)
			{
				if (serialized == null)
					throw new ArgumentNullException("serialized");

				using (var ms = new MemoryStream())
				{
					ms.Write(serialized, 0, serialized.Length);
					ms.Seek(0, SeekOrigin.Begin);
					return ReadFrom(ms);
				}
			}

			public ECEncryptedMessage ReadFrom(Stream s)
			{
				if (s == null)
					throw new ArgumentNullException("s");

				var pointLength = VarUInt.Factory.ReadFrom(s);
				var pointBytes = new byte[(int) pointLength];
				s.Read(pointBytes, 0, pointBytes.Length);

				var messageLength = VarUInt.Factory.ReadFrom(s);
				var messageBytes = new byte[(int) messageLength];
				s.Read(messageBytes, 0, messageBytes.Length);

				var point = _curve.Deserialize(pointBytes);

				return new ECEncryptedMessage(point, messageBytes);
			}
		}

		/// <summary>
		/// Gets an ECEncryptedMessage deserializer for the given curve
		/// </summary>
		/// <param name="curve">The curve to get the deserializer for</param>
		/// <returns>An ECEncryptedMessage deserializer for the given curve</returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the curve parameter is null
		/// </exception>
		public static IDeserializer<ECEncryptedMessage> Factory(ECCurve curve)
		{
			if (curve == null)
				throw new ArgumentNullException("curve");

			return new EncryptedMessageDeserializer(curve);
		}

		#endregion Factory

		#region Constructor

		/// <summary>
		/// Creates a new encrypted payload
		/// </summary>
		/// <param name="sharedSecret">
		/// The shared secret point required to obtain the decryption key
		/// </param>
		/// <param name="symmetricEncryptedMessage">Encrypted data which is padded and checksummed</param>
		public ECEncryptedMessage(ECPoint sharedSecret, byte[] symmetricEncryptedMessage)
		{
			if (sharedSecret == null)
				throw new ArgumentNullException("sharedSecret");

			if (symmetricEncryptedMessage == null)
				throw new ArgumentNullException("symmetricEncryptedMessage");

			SharedSecret = sharedSecret;
			SymmetricEncryptedMessage = symmetricEncryptedMessage;
		}
		
		#endregion Constructor

		#region Properties

		/// <summary>
		/// The shared secret point required to obtain the decryption key
		/// </summary>
		public ECPoint SharedSecret { get; private set; }

		/// <summary>
		/// Encrypted data which is padded and checksummed
		/// </summary>
		public byte[] SymmetricEncryptedMessage { get; private set; }

		#endregion Properties
		
		#region Serialization

		/// <summary>
		/// Converts the item to a byte array
		/// </summary>
		/// <returns>A serialized representation of the item</returns>
		public virtual byte[] ToByteArray()
		{
			using (var ms = new MemoryStream())
			{
				WriteTo(ms);
				return ms.ToArray();
			}
		}

		/// <summary>
		/// Writes the item to a stream
		/// </summary>
		/// <param name="s">The stream to write to</param>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the stream is null
		/// </exception>
		public virtual void WriteTo(Stream s)
		{
			if (s == null)
				throw new ArgumentNullException("s");

			var ecPointBytes = SharedSecret.ToByteArray(true);

			((VarUInt)(uint)ecPointBytes.Length).WriteTo(s);
			s.Write(ecPointBytes, 0, ecPointBytes.Length);

			((VarUInt)(uint)SymmetricEncryptedMessage.Length).WriteTo(s);
			s.Write(SymmetricEncryptedMessage, 0, SymmetricEncryptedMessage.Length);
		}

		#endregion Serialization
	}
}