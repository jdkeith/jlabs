﻿using System;
using System.IO;
using System.Numerics;
using System.Security.Cryptography;

using PXL.Units;

namespace PXL.Cryptography.ECC.Signing
{
	/// <summary>
	/// Provides high(ish) level methods for signing and verifying digital signatures
	/// </summary>
	/// <see cref="http://cs.ucsb.edu/~koc/ccs130h/notes/ecdsa-cert.pdf"/>
	// ReSharper disable once InconsistentNaming
	public class ECDSASigner
	{
		#region Fields

		private static readonly RandomHelper _defaultR;
		private static readonly IHasher _defaultHasher;
		
		private readonly ECCurve _curve;
		private readonly IRandomHelper _r;
		private readonly IHasher _hasher;

		#endregion Fields

		#region Constructors

		static ECDSASigner()
		{
			_defaultR = new RandomHelper(new RNGCryptoServiceProvider(), true);
			_defaultHasher = SHA256Hash.Factory;
		}

		/// <summary>
		/// Creates a new signer with the given domain parameters
		/// </summary>
		/// <param name="curve">The elliptic curve to use for the signer</param>
		/// <param name="rng">
		/// The random number generator to use for generating K values. If unspecifed,
		/// a singleton instance of RNGCryptoServiceProvider is used.
		/// Note that the passed rng is assumed to be cryptographically strong
		/// </param>
		/// <param name="hasher">The hash method to use when computing message summaries</param>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the curve parameter is null
		/// </exception>
		public ECDSASigner(ECCurve curve, RandomNumberGenerator rng = null, IHasher hasher = null)
		{
			if (curve == null)
				throw new ArgumentNullException("curve");

			_curve = curve;
			_hasher = hasher ?? _defaultHasher;

			_r = rng != null
				? new RandomHelper(rng, true)
				: _defaultR;
		}

		/// <summary>
		/// Creates a new signer with the given domain parameters
		/// </summary>
		/// <param name="curve">The elliptic curve to use for the signer</param>
		/// <param name="rh">
		/// The random number generator to use for generating K values. If unspecifed,
		/// a singleton instance of RNGCryptoServiceProvider is used.
		/// </param>
		/// <param name="hasher">The hash method to use when computing message summaries</param>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the curve or rh parameters are null
		/// </exception>
		/// <exception cref="System.ArgumentException">
		/// Thrown if the rh parameter is not cryptographically strong
		/// </exception>
		public ECDSASigner(ECCurve curve, IRandomHelper rh, IHasher hasher = null)
		{
			if (curve == null)
				throw new ArgumentNullException("curve");

			if (rh == null)
				throw new ArgumentNullException("rh");

			if (! rh.IsCryptographicallyStrong)
				throw new ArgumentException("Random helper must be cryptographically strong", "rh");

			_curve = curve;
			_hasher = hasher ?? _defaultHasher;
			_r = rh;
		}

		#endregion Constructors

		#region Sign

		/// <summary>
		/// Produces a signature for a serializable / privateKey pair
		/// </summary>
		/// <param name="serializable">The serializable to sign</param>
		/// <param name="privateKey">The private key to use for signing</param>
		/// <returns>
		/// A digital signature which can be used to verify the
		/// integrity of a message or identity of a signer
		/// </returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the serializable argument is null
		/// </exception>
		/// <exception cref="System.ArgumentOutOfRangeException">
		/// Thrown if the private key is outside of the bounds of
		/// the curve domain given in the construction parameters for the signer
		/// or if it is less than or equal to zero
		/// </exception>
		public ECDSASignature Sign(ISerializable serializable, BigInteger privateKey)
		{
			if (serializable == null)
				throw new ArgumentNullException("serializable");

			return Sign(_hasher.Of(serializable), privateKey);
		}

		/// <summary>
		/// Produces a signature for a message/privateKey pair
		/// </summary>
		/// <param name="message">The message to sign</param>
		/// <param name="privateKey">The private key to use for signing</param>
		/// <returns>
		/// A digital signature which can be used to verify the
		/// integrity of a message or identity of a signer
		/// </returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the message argument is null
		/// </exception>
		/// <exception cref="System.ArgumentOutOfRangeException">
		/// Thrown if the private key is outside of the bounds of
		/// the curve domain given in the construction parameters for the signer
		/// or if it is less than or equal to zero
		/// </exception>
		public ECDSASignature Sign(byte[] message, BigInteger privateKey)
		{
			if (message == null)
				throw new ArgumentNullException("message");

			return Sign(_hasher.Of(message), privateKey);
		}

		/// <summary>
		/// Produces a signature for a hashed message/privateKey pair
		/// </summary>
		/// <param name="hashedMessage">The message to sign</param>
		/// <param name="privateKey">The private key to use for signing</param>
		/// <returns>
		/// A digital signature which can be used to verify the
		/// integrity of a message or identity of a signer
		/// </returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the hashedMessage argument is null
		/// </exception>
		/// <exception cref="System.ArgumentOutOfRangeException">
		/// Thrown if the private key is outside of the bounds of
		/// the curve domain given in the construction parameters for the signer
		/// or if it is less than or equal to zero
		/// </exception>
		public ECDSASignature Sign(IHash hashedMessage, BigInteger privateKey)
		{
			if (hashedMessage == null)
				throw new ArgumentNullException("hashedMessage");

			if (privateKey <= 0 || privateKey > _curve.Q)
				throw new ArgumentOutOfRangeException("privateKey", "Private key is outside of domain in constructor parameters");

			var n = _curve.N;

			var e = CalculateE(hashedMessage.ToByteArray());

			BigInteger r;
			BigInteger s;

			do
			{
				var k = _r.NextBigInteger(BigInteger.One, n);
				// ReSharper disable once InconsistentNaming
				var K = _curve.G * k;
				r = K.X.Value % n;

				s = (k.ModInverse(n) * (e + privateKey * r)) % n;
			}
			while (s == 0);

			return new ECDSASignature(r, s);
		}

		#endregion Sign

		#region Verify

		/// <summary>
		/// Verifies that a given serializable was signed by the private
		/// key associated with the given public key
		/// </summary>
		/// <param name="serializable">The serializable to verify</param>
		/// <param name="signature">The signature of the message summary</param>
		/// <param name="publicKey">
		/// The public key associated with the signing private key
		/// </param>
		/// <returns>
		/// True if the message was signed by the private key associated
		/// with the given public key and neither the message nor the
		/// signature has been tampered with, almost certainly false otherwise
		/// </returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if any of the parameters are null
		/// </exception>
		/// <exception cref="PointMismatchException">
		/// Thrown if the publicKey parameter is not on the same curve
		/// as the curve specified in the parameters passed on signer construction
		/// </exception>
		public bool Verify(ISerializable serializable, ECDSASignature signature, ECPoint publicKey)
		{
			if (serializable == null)
				throw new ArgumentNullException("serializable");

			return Verify(_hasher.Of(serializable), signature, publicKey);
		}

		/// <summary>
		/// Verifies that a given message was signed by the private
		/// key associated with the given public key
		/// </summary>
		/// <param name="message">The message to verify</param>
		/// <param name="signature">The signature of the message</param>
		/// <param name="publicKey">
		/// The public key associated with the signing private key
		/// </param>
		/// <returns>
		/// True if the message was signed by the private key associated
		/// with the given public key and neither the message nor the
		/// signature has been tampered with, almost certainly false otherwise
		/// </returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if any of the parameters are null
		/// </exception>
		/// <exception cref="PointMismatchException">
		/// Thrown if the publicKey parameter is not on the same curve
		/// as the curve specified in the parameters passed on signer construction
		/// </exception>
		public bool Verify(byte[] message, ECDSASignature signature, ECPoint publicKey)
		{
			if (message == null)
				throw new ArgumentNullException("message");

			return Verify(_hasher.Of(message), signature, publicKey);
		}

		/// <summary>
		/// Verifies that a given hashedMessage was signed by the private
		/// key associated with the given public key
		/// </summary>
		/// <param name="hashedMessage">The hashed message to verify</param>
		/// <param name="signature">The signature of the message</param>
		/// <param name="publicKey">
		/// The public key associated with the signing private key
		/// </param>
		/// <returns>
		/// True if the message was signed by the private key associated
		/// with the given public key and neither the message nor the
		/// signature has been tampered with, almost certainly false otherwise
		/// </returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if any of the parameters are null
		/// </exception>
		/// <exception cref="PointMismatchException">
		/// Thrown if the publicKey parameter is not on the same curve
		/// as the curve specified in the parameters passed on signer construction
		/// </exception>
		public bool Verify(IHash hashedMessage, ECDSASignature signature, ECPoint publicKey)
		{
			if (hashedMessage == null)
				throw new ArgumentNullException("hashedMessage");

			if (signature == null)
				throw new ArgumentNullException("signature");

			if (publicKey == null)
				throw new ArgumentNullException("publicKey");

			if (!_curve.IsOnCurve(publicKey))
				throw new PointMismatchException("Public Key is not on the curve specified by the constructor parameters");

			var n = _curve.N;

			var e = CalculateE(hashedMessage.ToByteArray());

			if (signature.R < 1 || signature.R > n || signature.S < 1 || signature.S > n)
				return false;

			var w = signature.S.ModInverse(n);
			var u1 = (e * w) % n;
			var u2 = (signature.R * w) % n;

			// ReSharper disable once InconsistentNaming
			var X = _curve.G * u1 + publicKey * u2;

			if (X.IsInfinity)
				return false;

			var v = X.X.Value % n;

			return v == signature.R;
		}

		#endregion Verify

		#region Recover

		/// <summary>
		/// Recovers the public key from a serializable / signature pair
		/// </summary>
		/// <param name="serializable">The signed serializable</param>
		/// <param name="signature">The signature to extract the public key from</param>
		/// <returns>
		/// An array of two points, one of which is the public key which signed the serializable,
		/// or an empty array if the public key cannot be recovered
		/// </returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if either argument is null
		/// </exception>
		public ECPoint[] RecoverPublicKeyFromSignature(ISerializable serializable, ECDSASignature signature)
		{
			if (serializable == null)
				throw new ArgumentNullException("serializable");

			return RecoverPublicKeyFromSignature(_hasher.Of(serializable), signature);
		}

		/// <summary>
		/// Recovers the public key from a message / signature pair
		/// </summary>
		/// <param name="message">The signed message</param>
		/// <param name="signature">The signature to extract the public key from</param>
		/// <returns>
		/// An array of two points, one of which is the public key which signed the serializable,
		/// or an empty array if the public key cannot be recovered
		/// </returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if either argument is null
		/// </exception>
		public ECPoint[] RecoverPublicKeyFromSignature(byte[] message, ECDSASignature signature)
		{
			if (message == null)
				throw new ArgumentNullException("message");

			return RecoverPublicKeyFromSignature(_hasher.Of(message), signature);
		}

		/// <summary>
		/// Recovers the public key from a hash / signature pair
		/// </summary>
		/// <param name="hashedMessage">The signed hash</param>
		/// <param name="signature">The signature to extract the public key from</param>
		/// <returns>
		/// An array of two points, one of which is the public key which signed the serializable,
		/// or an empty array if the public key cannot be recovered
		/// </returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if either argument is null
		/// </exception>
		public ECPoint[] RecoverPublicKeyFromSignature(IHash hashedMessage, ECDSASignature signature)
		{
			if (hashedMessage == null)
				throw new ArgumentNullException("hashedMessage");

			if( signature == null )
				throw new ArgumentNullException("signature");

			var z = CalculateE(_curve, hashedMessage.ToByteArray());

			if( signature.R == 0 || signature.S == 0 || z == 0 )
				return new ECPoint[0];

			var pointR1 = _curve.CreatePoint(signature.R);
			var pointR2 = _curve.CreatePoint(signature.R, true);
			var rInverse = signature.R.ModInverse(_curve.N);

			if (pointR1.X >= _curve.N || pointR2.X >= _curve.N)
			{
				// point is not recoverable
				// http://en.wikipedia.org/wiki/Hasse%27s_theorem_on_elliptic_curves
				return new ECPoint[0];
			}
				
			return new []
			{
				(pointR1 * signature.S - _curve.G * z) * rInverse,
				(pointR2 * signature.S - _curve.G * z) * rInverse
			};
		}

		#endregion Recover

		#region Utilities

		/// <summary>
		/// Converts a message hash into a BigInteger within a curve's domain
		/// </summary>
		/// <param name="curve">The curve to fit the message hash within</param>
		/// <param name="hashMessage">The message hash to convert to a BigInteger</param>
		/// <returns>
		/// The E (also called Z) value of a message used in signing and verification
		/// in the range of {0,2**Ceiling(Log2(Curve.N))}
		/// </returns>
		public static BigInteger CalculateE(ECCurve curve, byte[] hashMessage)
		{
			if (curve == null)
				throw new ArgumentNullException("curve");

			if (hashMessage == null)
				throw new ArgumentNullException("hashMessage");

			var messageBitLength = hashMessage.Length * 8;

			var messageAsNumber = hashMessage.ToNonNegativeBigInteger(isBigEndian: true);
			var domainBitLength = curve.N.GetBitLength();

			if (domainBitLength < messageBitLength)
				messageAsNumber = messageAsNumber >> (messageBitLength - domainBitLength);

			return messageAsNumber;
		}

		private BigInteger CalculateE(byte[] hashMessage)
		{
			return CalculateE(_curve, hashMessage);
		}

		#endregion Utilities
	}
}