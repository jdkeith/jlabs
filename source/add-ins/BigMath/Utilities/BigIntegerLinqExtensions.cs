﻿using System.Collections.Generic;
using System.Numerics;

// ReSharper disable once CheckNamespace
namespace System.Linq
{
	/// <summary>
	/// Adds support for BigInteger to Linq mathematical operations
	/// </summary>
	public static class BigIntegerLinqExtensions
	{
		#region Sum

		/// <summary>
		/// Computes the sum of a sequence of BigIntegers
		/// </summary>
		/// <param name="source">
		/// A sequence of BigIntegers to calculate the sum of
		/// </param>
		/// <returns>The sum of the sequence</returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the source parameter is null
		/// </exception>
		/// <exception cref="System.InvalidOperationException">
		/// Thrown if the source sequence is empty
		/// </exception>
		public static BigInteger Sum(this IEnumerable<BigInteger> source)
		{
			if (source == null)
				throw new ArgumentNullException("source");

			var result = source.Cast<BigInteger?>().Sum();

			if (! result.HasValue )
				throw new InvalidOperationException("Sequence contains no elements");

			return result.Value;
		}

		/// <summary>
		/// Computes the sum of a sequence of nullable BigIntegers
		/// </summary>
		/// <param name="source">
		/// A sequence of nullable BigIntegers to calculate the sum of
		/// </param>
		/// <returns>
		/// The sum of the sequence excluding null entries or null
		/// if the sequence is empty or contains only null values
		/// </returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the source parameter is null
		/// </exception>
		public static BigInteger? Sum(this IEnumerable<BigInteger?> source)
		{
			if (source == null)
				throw new ArgumentNullException("source");

			return source.Aggregate(
				(BigInteger?) null,
				(current, value) =>
				{
					if( ! value.HasValue )
						return current;

					if( ! current.HasValue )
						return value;

					return value + current;
				}
			);
		}

		/// <summary>
		/// Computes the sum of a sequence of nullable BigIntegers
		/// that are obtained by invoking a transform on each element of the input sequence
		/// </summary>
		/// <param name="source">
		/// A sequence of BigIntegers to calculate the sum of
		/// </param>
		/// <param name="selector">
		/// A transform function to apply to each element
		/// </param>
		/// <returns>The sum of the sequence</returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the either source or selector parameter is null
		/// </exception>
		/// <exception cref="System.InvalidOperationException">
		/// Thrown if the source sequence is empty
		/// </exception>
		public static BigInteger Sum<T>(this IEnumerable<T> source, Func<T,BigInteger> selector)
		{
			if (source == null)
				throw new ArgumentNullException("source");

			if (source == null)
				throw new ArgumentNullException("selector");

			return source.Select(selector).Sum();
		}

		/// <summary>
		/// Computes the sum of a sequence of nullable BigIntegers
		/// that are obtained by invoking a transform on each element of the input sequence
		/// </summary>
		/// <param name="source">
		/// A sequence of nullable BigIntegers to calculate the sum of
		/// </param>
		/// <param name="selector">
		/// A transform function to apply to each element
		/// </param>
		/// <returns>
		/// The sum of the sequence excluding null entries or null
		/// if the sequence is empty or contains only null values
		/// </returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the either source or selector parameter is null
		/// </exception>
		public static BigInteger? Sum<T>(this IEnumerable<T> source, Func<T, BigInteger?> selector)
		{
			if (source == null)
				throw new ArgumentNullException("source");

			if (selector == null)
				throw new ArgumentNullException("selector");

			return source.Select(selector).Sum();
		}

		#endregion Sum

		#region Min

		/// <Summary>
		/// Returns the minimum value in a sequence of BigIntegers
		/// </Summary>
		/// <param name="source">
		/// A sequence of BigIntegers to determinine the minimum value of
		/// </param>
		/// <returns>The minimum value in the sequence</returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the source parameter is null
		/// </exception>
		/// <exception cref="System.InvalidOperationException">
		/// Thrown if the source sequence is empty
		/// </exception>
		public static BigInteger Min(this IEnumerable<BigInteger> source)
		{
			if (source == null)
				throw new ArgumentNullException("source");

			var result = source.Cast<BigInteger?>().Min();

			if (!result.HasValue)
				throw new InvalidOperationException("Sequence contains no elements");

			return result.Value;
		}

		/// <Summary>
		/// Returns the minimum value in a sequence of nullable BigIntegers
		/// </Summary>
		/// <param name="source">
		/// A sequence of nullable BigIntegers to determinine the minimum value of
		/// </param>
		/// <returns>
		/// The minimum value in the sequence excluding null entries or null
		/// if the sequence is empty or contains only null values
		/// </returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the source parameter is null
		/// </exception>
		public static BigInteger? Min(this IEnumerable<BigInteger?> source)
		{
			if (source == null)
				throw new ArgumentNullException("source");

			return source.Aggregate(
				(BigInteger?)null,
				(current, value) =>
				{
					if (!value.HasValue)
						return current;

					if (!current.HasValue || current > value )
						return value;

					return current;
				}
			);
		}

		/// <Summary>
		/// Returns the minimum value in a sequence of nullable BigIntegers
		/// that are obtained by invoking a transform on each element of the input sequence
		/// </Summary>
		/// <param name="source">
		/// A sequence of BigIntegers to determinine the minimum value of
		/// </param>
		/// <param name="selector">
		/// A transform function to apply to each element
		/// </param>
		/// <returns>The minimum value in the sequence</returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the either source or selector parameter is null
		/// </exception>
		/// <exception cref="System.InvalidOperationException">
		/// Thrown if the source sequence is empty
		/// </exception>
		public static BigInteger Min<T>(this IEnumerable<T> source, Func<T, BigInteger> selector)
		{
			if (source == null)
				throw new ArgumentNullException("source");

			if (source == null)
				throw new ArgumentNullException("selector");

			return source.Select(selector).Min();
		}

		/// <Summary>
		/// Returns the minimum value in a sequence of nullable BigIntegers
		/// that are obtained by invoking a transform on each element of the input sequence
		/// </Summary>
		/// <param name="source">
		/// A sequence of nullable BigIntegers to determinine the minimum value of
		/// </param>
		/// <param name="selector">
		/// A transform function to apply to each element
		/// </param>
		/// <returns>
		/// The minimum value in the sequence excluding null entries or null
		/// if the sequence is empty or contains only null values
		/// </returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the either source or selector parameter is null
		/// </exception>
		public static BigInteger? Min<T>(this IEnumerable<T> source, Func<T, BigInteger?> selector)
		{
			if (source == null)
				throw new ArgumentNullException("source");

			if (selector == null)
				throw new ArgumentNullException("selector");

			return source.Select(selector).Min();
		}

		#endregion Min

		#region Max

		/// <Summary>
		/// Returns the maximum value in a sequence of BigIntegers
		/// </Summary>
		/// <param name="source">
		/// A sequence of BigIntegers to determinine the maximum value of
		/// </param>
		/// <returns>The maximum value in the sequence</returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the source parameter is null
		/// </exception>
		/// <exception cref="System.InvalidOperationException">
		/// Thrown if the source sequence is empty
		/// </exception>
		public static BigInteger Max(this IEnumerable<BigInteger> source)
		{
			if (source == null)
				throw new ArgumentNullException("source");

			var result = source.Cast<BigInteger?>().Max();

			if (!result.HasValue)
				throw new InvalidOperationException("Sequence contains no elements");

			return result.Value;
		}

		/// <Summary>
		/// Returns the maximum value in a sequence of nullable BigIntegers
		/// </Summary>
		/// <param name="source">
		/// A sequence of nullable BigIntegers to determinine the maximum value of
		/// </param>
		/// <returns>
		/// The maximum value in the sequence excluding null entries or null
		/// if the sequence is empty or contains only null values
		/// </returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the source parameter is null
		/// </exception>
		public static BigInteger? Max(this IEnumerable<BigInteger?> source)
		{
			if (source == null)
				throw new ArgumentNullException("source");

			return source.Aggregate(
				(BigInteger?)null,
				(current, value) =>
				{
					if (!value.HasValue)
						return current;

					if (!current.HasValue || current < value)
						return value;

					return current;
				}
			);
		}

		/// <Summary>
		/// Returns the maximum value in a sequence of nullable BigIntegers
		/// that are obtained by invoking a transform on each element of the input sequence
		/// </Summary>
		/// <param name="source">
		/// A sequence of BigIntegers to determinine the maximum value of
		/// </param>
		/// <param name="selector">
		/// A transform function to apply to each element
		/// </param>
		/// <returns>The maximum value in the sequence</returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the either source or selector parameter is null
		/// </exception>
		/// <exception cref="System.InvalidOperationException">
		/// Thrown if the source sequence is empty
		/// </exception>
		public static BigInteger Max<T>(this IEnumerable<T> source, Func<T, BigInteger> selector)
		{
			if (source == null)
				throw new ArgumentNullException("source");

			if (source == null)
				throw new ArgumentNullException("selector");

			return source.Select(selector).Max();
		}

		/// <Summary>
		/// Returns the maximum value in a sequence of nullable BigIntegers
		/// that are obtained by invoking a transform on each element of the input sequence
		/// </Summary>
		/// <param name="source">
		/// A sequence of nullable BigIntegers to determinine the maximum value of
		/// </param>
		/// <param name="selector">
		/// A transform function to apply to each element
		/// </param>
		/// <returns>
		/// The maximum value in the sequence excluding null entries or null
		/// if the sequence is empty or contains only null values
		/// </returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the either source or selector parameter is null
		/// </exception>
		public static BigInteger? Max<T>(this IEnumerable<T> source, Func<T, BigInteger?> selector)
		{
			if (source == null)
				throw new ArgumentNullException("source");

			if (selector == null)
				throw new ArgumentNullException("selector");

			return source.Select(selector).Max();
		}

		#endregion Max

		#region Average

		/// <Summary>
		/// Returns the average value in a sequence of BigIntegers
		/// </Summary>
		/// <param name="source">
		/// A sequence of BigIntegers to determinine the average value of
		/// </param>
		/// <returns>The average value in the sequence</returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the source parameter is null
		/// </exception>
		/// <exception cref="System.InvalidOperationException">
		/// Thrown if the source sequence is empty
		/// </exception>
		public static double Average(this IEnumerable<BigInteger> source)
		{
			if (source == null)
				throw new ArgumentNullException("source");

			var result = source.Cast<BigInteger?>().Average();

			if (!result.HasValue)
				throw new InvalidOperationException("Sequence contains no elements");

			return result.Value;
		}

		/// <Summary>
		/// Returns the average value in a sequence of nullable BigIntegers
		/// </Summary>
		/// <param name="source">
		/// A sequence of nullable BigIntegers to determinine the average value of
		/// </param>
		/// <returns>
		/// The average value in the sequence excluding null entries or null
		/// if the sequence is empty or contains only null values
		/// </returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the source parameter is null
		/// </exception>
		public static double? Average(this IEnumerable<BigInteger?> source)
		{
			if (source == null)
				throw new ArgumentNullException("source");

			BigInteger? result = null;

			var count = 0;

			foreach (var element in source)
			{
				if (!element.HasValue)
					continue;

				count++;

				result = ! result.HasValue
					? element
					: result + element;
			}

			if (!result.HasValue)
				return null;

			return (double) result / count;
		}

		/// <Summary>
		/// Returns the average value in a sequence of nullable BigIntegers
		/// that are obtained by invoking a transform on each element of the input sequence
		/// </Summary>
		/// <param name="source">
		/// A sequence of BigIntegers to determinine the average value of
		/// </param>
		/// <param name="selector">
		/// A transform function to apply to each element
		/// </param>
		/// <returns>The average value in the sequence</returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the either source or selector parameter is null
		/// </exception>
		/// <exception cref="System.InvalidOperationException">
		/// Thrown if the source sequence is empty
		/// </exception>
		public static double Average<T>(this IEnumerable<T> source, Func<T, BigInteger> selector)
		{
			if (source == null)
				throw new ArgumentNullException("source");

			if (source == null)
				throw new ArgumentNullException("selector");

			return source.Select(selector).Average();
		}

		/// <Summary>
		/// Returns the average value in a sequence of nullable BigIntegers
		/// that are obtained by invoking a transform on each element of the input sequence
		/// </Summary>
		/// <param name="source">
		/// A sequence of nullable BigIntegers to determinine the average value of
		/// </param>
		/// <param name="selector">
		/// A transform function to apply to each element
		/// </param>
		/// <returns>
		/// The average value in the sequence excluding null entries or null
		/// if the sequence is empty or contains only null values
		/// </returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the either source or selector parameter is null
		/// </exception>
		public static double? Average<T>(this IEnumerable<T> source, Func<T, BigInteger?> selector)
		{
			if (source == null)
				throw new ArgumentNullException("source");

			if (selector == null)
				throw new ArgumentNullException("selector");

			return source.Select(selector).Average();
		}

		#endregion Average
	}
}
