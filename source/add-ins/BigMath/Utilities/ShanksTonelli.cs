﻿using System.Numerics;

namespace PXL.BigMath.Utilities
{
	/// <summary>
	/// A helper class for finding the square root of
	/// a number in a finite field
	/// </summary>
	/// <see cref="http://shankstonelli.blogspot.com/2010/12/shanks-tonelli-algorithm-in-c.html"/>
	/// <seealso cref="http://en.wikipedia.org/wiki/Tonelli%E2%80%93Shanks_algorithm"/>
	public static class ShanksTonelli
	{
		/// <summary>
		/// Computes the square root of a number within a finite field
		/// </summary>
		/// <param name="a">The value to get the square root of</param>
		/// <param name="p">The domain of the finite field</param>
		/// <returns>
		/// The square root of the number within the given finite field
		/// or -1 if no such root exists
		/// </returns>
		public static BigInteger SquareRoot(BigInteger a, BigInteger p)
		{
			if (BigInteger.ModPow(a, (p - 1) / 2, p) == (p - 1))
				return -1;
			//No Sqrt Exists

			if (p % 4 == 3)
				return BigInteger.ModPow(a, (p + 1) / 4, p);

			//Initialize
			BigInteger s, e;

			FindSAndE(p, out s, out e);

			BigInteger g;
			BigInteger n = 2;

			while (BigInteger.ModPow(n, (p - 1) / 2, p) == 1)
			{
				n++;
			}//Finds Generator

			var x = BigInteger.ModPow(a, (s + 1) / 2, p);
			var b = BigInteger.ModPow(a, s, p);

			g = BigInteger.ModPow(n, s, p);
			
			var r = e;
			var m = Ord(b, p);

			if (m == 0)
			{
				return x;
			}

			while (m > 0)
			{
				x = (x * BigInteger.ModPow(g, TwoExp(r - m - 1), p)) % p;
				b = (b * BigInteger.ModPow(g, TwoExp(r - m), p)) % p;
				g = BigInteger.ModPow(g, TwoExp(r - m), p);
				r = m;
				m = Ord(b, p);
			}

			return x;
		}

		#region Utilities

		private static void FindSAndE(BigInteger p, out BigInteger s, out BigInteger e)
		{
			e = BigInteger.Zero;
			s = p - 1;

			while (s % 2 == 0)
			{
				s /= 2;
				e += 1;
			}
		}

		private static BigInteger Ord(BigInteger b, BigInteger p)
		{
			BigInteger m = 1;
			BigInteger e = 0;
			while (BigInteger.ModPow(b, m, p) != 1)
			{
				m *= 2;
				e++;
			}

			return e;
		}

		private static BigInteger TwoExp(BigInteger e)
		{
			BigInteger a = 1;

			while (e > 0)
			{
				a *= 2;
				e--;
			}

			return a;
		}

		#endregion Utilities
	}
}
