﻿using System.Linq;

// ReSharper disable once CheckNamespace
namespace System.Numerics
{
	/// <summary>
	/// Provides basic extensions to convert BigIntegers to and from DER encoding
	/// </summary>
	/// <seealso cref="http://luca.ntop.org/Teaching/Appunti/asn1.html"/>
// ReSharper disable once InconsistentNaming
	public static class DEREncodingExtensions
	{
		/// <summary>
		/// Converts a BigInteger to its DER-encoded equivalent
		/// </summary>
		/// <param name="value">The BigInteger to convert</param>
		/// <returns>A byte array which represents the BigInteger</returns>
		/// <exception cref="System.ArgumentOutOfRangeException">
		/// Thrown if the value requires more than 255 bytes to represent
		/// </exception>
		// ReSharper disable once InconsistentNaming
		public static byte[] ToDEREncoding(this BigInteger value)
		{
			var vBytes = value.ToByteArray();

			Array.Reverse(vBytes);

			if (vBytes.Length > byte.MaxValue)
			{
				throw new ArgumentOutOfRangeException(
					"value",
					"Value is to large to encode as a DER integer in this implementation"
				);
			}

			var vLen = (byte)vBytes.Length;

			return new byte[] { 0x02, vLen }.Concat(vBytes).ToArray();
		}

		/// <summary>
		/// Converts a DER-encoded BigInteger representation to a BigInteger
		/// </summary>
		/// <param name="derEncoded">The DER-encoded BigInteger serialization to convert</param>
		/// <param name="strict">
		/// Whether or not to use strict encoding. When false, multiple leading zeroes are allowed
		/// </param>
		/// <returns>The BigInteger represented by the encoding</returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the derEncoded parameter is null
		/// </exception>
		/// <exception cref="System.FormatException">
		/// Thrown if the bytes passed are not a valid DER integer encoding
		/// </exception>
		// ReSharper disable once InconsistentNaming
		public static BigInteger FromDEREncoding(this byte[] derEncoded, bool strict = true)
		{
			if (derEncoded == null)
				throw new ArgumentNullException("derEncoded");

			var formatException = new FormatException("Not a valid DER integer encoding");

			if (derEncoded.Length < 3)
				throw formatException;

			if (derEncoded[0] != 0x02)
				throw formatException;

			var vLen = derEncoded[1];

			// must be exactly the right length
			if (vLen == 0 || vLen + 2 != derEncoded.Length)
				throw formatException;

			// only one leading zero allowed if strict
			if (strict && derEncoded.Skip(2).TakeWhile(v => v == 0).Count() > 1)
				throw formatException;

			return new BigInteger(derEncoded.Skip(2).Reverse().ToArray());
		}
	}
}
