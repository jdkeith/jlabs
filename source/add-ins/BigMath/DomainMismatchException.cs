﻿using System;

namespace PXL.BigMath
{
	/// <summary>
	/// The exception which is thrown when an operation is performed between
	/// two field elements which are in different finite fields
	/// </summary>
	public class DomainMismatchException : Exception
	{
		/// <summary>
		/// Creates a new DomainMismatchException
		/// </summary>
		/// <param name="message">A human-readable error message</param>
		/// <param name="innerException">An exception which is the cause of the exception</param>
		public DomainMismatchException(string message = null, Exception innerException = null)
			: base(message, innerException) { }
	}
}
