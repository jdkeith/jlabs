﻿using System;
using System.IO;
using System.Numerics;

using PXL.Units;
using PXL.Units.Utilities;

namespace PXL.Cryptography.ECC.Specialized.Signatures.Ring
{
	/// <summary>
	/// A signature from a signing ring
	/// </summary>
	public class RingSignature : ISerializable
	{
		#region Factory

		private class RingSignatureDeserializer : IDeserializer<RingSignature>
		{
			private readonly ECCurve _curve;

			public RingSignatureDeserializer(ECCurve curve)
			{
				_curve = curve;
			}

			public RingSignature Deserialize(byte[] serialized)
			{
				if (serialized == null)
					throw new ArgumentNullException("serialized");

				using (var ms = new MemoryStream())
				{
					ms.Write(serialized, 0, serialized.Length);
					ms.Seek(0, SeekOrigin.Begin);
					return ReadFrom(ms);
				}
			}

			public RingSignature ReadFrom(Stream s)
			{
				if (s == null)
					throw new ArgumentNullException("s");

				var rt = s.ReadByteArrayFrom().ToNonNegativeBigInteger();
				var ri = new BigInteger[VarUInt.Factory.ReadFrom(s)];

				for (var i = 0; i < ri.Length; i++)
				{
					ri[i] = s.ReadByteArrayFrom().ToNonNegativeBigInteger();
				}

				var sppk = _curve.ReadFrom(s);

				ECPoint rp = null;

				if( s.ReadByte() > 0 )
					rp = _curve.ReadFrom(s);

				return new RingSignature(rt, ri, sppk, rp);
			}
		}

		/// <summary>
		/// Gets a deserializer for a RingSignature
		/// </summary>
		/// <param name="curve">The curve the signature was generated on</param>
		/// <returns>A deserializer for RingSignatures from the given curve</returns>
		public static IDeserializer<RingSignature> Factory(ECCurve curve)
		{
			if (curve == null)
				throw new ArgumentNullException("curve");

			return new RingSignatureDeserializer(curve);
		}

		#endregion Factory

		#region Constructor

		/// <summary>
		/// Creates a new RingSignature
		/// </summary>
		/// <param name="ringTarget">The final target for the signature</param>
		/// <param name="randomIntermediaries">An array of random intermediaries</param>
		/// <param name="signerPsuedoPublicKey">A psuedonomous public key of the signer</param>
		/// <param name="unlinkedRingPoint">
		/// The unlinked ring point. If null, the RingPoint of the
		/// singing ring is used
		/// </param>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the randomIntermediaries or signerPsuedoPublicKey parametes are null
		/// </exception>
		public RingSignature(
			BigInteger ringTarget,
			BigInteger[] randomIntermediaries,
			ECPoint signerPsuedoPublicKey,
			ECPoint unlinkedRingPoint
		)
		{
			if (randomIntermediaries == null)
				throw new ArgumentNullException("randomIntermediaries");

			if (signerPsuedoPublicKey == null)
				throw new ArgumentNullException("signerPsuedoPublicKey");

			RingTarget = ringTarget;
			RandomIntermediaries = randomIntermediaries;
			SignerPsuedoPublicKey = signerPsuedoPublicKey;
			UnlinkedRingPoint = unlinkedRingPoint;
		}

		#endregion Constructor

		#region Properties

		/// <summary>
		/// The final target for the signature 
		/// </summary>
		public BigInteger RingTarget { get; private set; }

		/// <summary>
		/// An array of random intermediaries
		/// </summary>
		public BigInteger[] RandomIntermediaries { get; private set; }

		/// <summary>
		/// A psuedonomous public key of the signer
		/// </summary>
		/// <remarks>
		/// While the public key cannot be tied to a specific signer,
		/// all signatures by the same signer in the same signing ring
		/// will produce the same SignerPsuedoPublicKey
		/// </remarks>
		public ECPoint SignerPsuedoPublicKey { get; private set; }

		/// <summary>
		/// The unlinked ring point. If null, the RingPoint of the
		/// singing ring is assumed
		/// </summary>
		public ECPoint UnlinkedRingPoint { get; private set; }

		#endregion Properties

		#region Serialization

		/// <summary>
		/// Serializes the given item to a byte array
		/// </summary>
		/// <returns>The serialization of the item</returns>
		public byte[] ToByteArray()
		{
			using (var ms = new MemoryStream())
			{
				WriteTo(ms);
				return ms.ToArray();
			}
		}

		/// <summary>
		/// Writes the item to a stream
		/// </summary>
		/// <param name="s">The stream to write to</param>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the stream parameter is null
		/// </exception>
		public void WriteTo(Stream s)
		{
			if (s == null)
				throw new ArgumentNullException("s");

			RingTarget.ToByteArray().WriteTo(s);
			
			((VarUInt) (uint) RandomIntermediaries.Length).WriteTo(s);

			for (var i = 0; i < RandomIntermediaries.Length; i++)
			{
				RandomIntermediaries[i].ToByteArray().WriteTo(s);
			}

			SignerPsuedoPublicKey.WriteTo(s);

			if (UnlinkedRingPoint == null)
			{
				s.WriteByte(0);
			}
			else
			{
				s.WriteByte(1);
				UnlinkedRingPoint.WriteTo(s);
			}
		}

		#endregion Serialization
	}
}