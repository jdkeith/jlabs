﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Security.Cryptography;

using PXL.BigMath;
using PXL.Units;

namespace PXL.Cryptography.ECC.Specialized.Signatures.Ring
{
	/// <summary>
	/// A set of public keys from which any member can sign a message
	/// without revealing which member was the signer
	/// </summary>
	public class SigningRing : ISerializable
	{
		#region Factory

		private class SigningRingDeserializer : IDeserializer<SigningRing>
		{
			private readonly ECCurve _curve;
			private readonly IRandomHelper _rh;
			private readonly IHasher _hasher;

			public SigningRingDeserializer(ECCurve curve, IRandomHelper rh, IHasher hasher)
			{
				_curve = curve;
				_rh = rh;
				_hasher = hasher;
			}

			public SigningRing Deserialize(byte[] serialized)
			{
				if (serialized == null)
					throw new ArgumentNullException("serialized");

				using (var ms = new MemoryStream())
				{
					ms.Write(serialized, 0, serialized.Length);
					ms.Seek(0, SeekOrigin.Begin);
					return ReadFrom(ms);
				}
			}

			public SigningRing ReadFrom(Stream s)
			{
				if (s == null)
					throw new ArgumentNullException("s");

				var points = new ECPoint[s.ReadByte()];

				for (var i = 0; i < points.Length; i++)
				{
					points[i] = _curve.ReadFrom(s);
				}

				List<ECPoint> usedKeys = null;

				if (s.ReadByte() > 0)
				{
					var usedKeysCount = s.ReadByte();

					usedKeys = new List<ECPoint>();

					for (var i = 0; i < usedKeysCount; i++)
					{
						usedKeys.Add(_curve.ReadFrom(s));
					}
				}

				return new SigningRing(points, usedKeys, _rh, _hasher);
			}
		}

		/// <summary>
		/// Gets a deserializer for a SigningRing
		/// </summary>
		/// <param name="curve">The curve the public keys for the signing ring lie on</param>
		/// <param name="rh">An optional random helper used for internal functions</param>
		/// <param name="hasher">
		/// The hasher to use when computing item or message summaries.
		/// If not specified, SHA256Hash.Factory is used.
		/// </param>
		/// <returns>A deserializer for SigningRing on the given curve</returns>
		public static IDeserializer<SigningRing> Factory(
			ECCurve curve, IRandomHelper rh = null, IHasher hasher = null
		)
		{
			if (curve == null)
				throw new ArgumentNullException("curve");

			return new SigningRingDeserializer(curve, rh, hasher);
		}

		#endregion Factory

		#region Fields

		private readonly ECPoint[] _orderedPublicKeys;
		private readonly List<ECPoint> _usedKeys;
		private readonly IRandomHelper _rh;
		private readonly IHasher _hasher;

		#endregion Fields

		#region Constructors

		/// <summary>
		/// Creates a new SigningRing
		/// </summary>
		/// <param name="publicKeys">
		/// An array of public keys of members who can sign
		/// via this ring. Note that public keys are sorted so the same
		/// public keys will always produce the same ring even if passed
		/// in a different order
		/// </param>
		/// <param name="isLinked">
		/// Whether or not the same signer always produces the same SignerPsuedoPublicKey
		/// </param>
		/// <param name="rh">
		/// An optional random helper used for internal functions
		/// </param>
		/// <param name="hasher">
		/// The hasher to use when computing item or message summaries.
		/// If not specified, SHA256Hash.Factory is used.
		/// </param>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the publicKeys parameter is null
		/// </exception>
		/// <exception cref="System.ArgumentException">
		/// Thrown if
		/// <para>The publicKeys parameter is an empty array</para>
		/// <para>The publicKeys parameter contains more than 255 items</para>
		/// <para>Any of the publicKeys are on different curves from one another</para>
		/// <para>The random helper is specfied and not cryptographically strong</para>
		/// </exception>
		public SigningRing(
			ECPoint[] publicKeys, bool isLinked,
			IRandomHelper rh = null, IHasher hasher = null
		) : this(publicKeys, isLinked ? new List<ECPoint>() : null, rh, hasher)
		{
			if (rh != null && ! rh.IsCryptographicallyStrong)
				throw new ArgumentException("Random helper must be cryptographically strong", "rh");
		}

		private SigningRing(
			ECPoint[] publicKeys, List<ECPoint> usedKeys,
			IRandomHelper rh, IHasher hasher
		)
		{
			if (publicKeys == null)
				throw new ArgumentNullException("publicKeys");

			if (!publicKeys.Any())
				throw new ArgumentException("Public keys cannot be empty", "publicKeys");

			if (publicKeys.Length > byte.MaxValue)
			{
				throw new ArgumentException(
					string.Format("Public keys cannot contain more than {0} items", byte.MaxValue),
					"publicKeys"
				);
			}

			Curve = publicKeys[0].Curve;

			if (publicKeys.Skip(1).Any(p => Curve != p.Curve))
				throw new ArgumentException("Public keys must all be on the same curve", "publicKeys");

			_orderedPublicKeys = publicKeys
				.OrderBy(pk => pk.X.Value)
				.ToArray();

			// ReSharper disable once CoVariantArrayConversion
			Id = SHA256Hash.Factory.Of(publicKeys);

			RingPoint = Curve.G * Id.ToByteArray().ToNonNegativeBigInteger();

			MemberCount = (byte)publicKeys.Length;

			_usedKeys = usedKeys;

			_rh = rh ?? new RandomHelper(new RNGCryptoServiceProvider(), true);

			_hasher = hasher ?? SHA256Hash.Factory;
		}

		#endregion Constructors

		#region Properties

		/// <summary>
		/// The unique id of the ring
		/// </summary>
		public SHA256Hash Id { get; private set; }

		/// <summary>
		/// A public point represented by the ring
		/// </summary>
		public ECPoint RingPoint { get; private set; }

		/// <summary>
		/// The curve the public keys of the ring members lie upon
		/// </summary>
		public ECCurve Curve { get; private set; }

		/// <summary>
		/// The number of members in the ring
		/// </summary>
		public byte MemberCount { get; private set; }

		/// <summary>
		/// The number of members whose signatures have not been seen if the
		/// signing ring is tracked
		/// </summary>
		/// <remarks>
		/// The signing ring must be updated per verification to correctly report usage
		/// </remarks>
		/// <exception cref="System.InvalidOperationException">
		/// Thrown if the signing ring is not linked
		/// </exception>
		public byte SeenMembersCount
		{
			get
			{
				if (! IsLinked )
				{
					throw new InvalidOperationException(
						"Operation not allowed on unlinked SigningRings"
					);
				}

				return (byte) _usedKeys.Count;
			}
		}

		/// <summary>
		/// Whether or not the same signer always produces the same SignerPsuedoPublicKey
		/// </summary>
		public bool IsLinked
		{
			get { return _usedKeys != null; }
		}

		#endregion Properties

		#region Sign

		/// <summary>
		/// Signs an item using the private key of one of the members in the ring
		/// </summary>
		/// <param name="serializable">The item to sign</param>
		/// <param name="privateKey">The private key</param>
		/// <returns>
		/// A ring signature which can be verified by a ring using the same members,
		/// or null if the private key does not have a corresponding public key in the ring
		/// </returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the serializable parameter is null
		/// </exception>
		public RingSignature Sign(ISerializable serializable, BigInteger privateKey)
		{
			if (serializable == null)
				throw new ArgumentNullException("serializable");

			return Sign(_hasher.Of(serializable), privateKey);
		}

		/// <summary>
		/// Signs a message using the private key of one of the members in the ring
		/// </summary>
		/// <param name="message">The message to sign</param>
		/// <param name="privateKey">The private key</param>
		/// <returns>
		/// A ring signature which can be verified by a ring using the same members,
		/// or null if the private key does not have a corresponding public key in the ring
		/// </returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the serializable parameter is null
		/// </exception>
		public RingSignature Sign(byte[] message, BigInteger privateKey)
		{
			if (message == null)
				throw new ArgumentNullException("message");

			return Sign(_hasher.Of(message), privateKey);
		}

		/// <summary>
		/// Signs a message using the private key of one of the members in the ring
		/// </summary>
		/// <param name="hashedMessage">The hash of the message to sign</param>
		/// <param name="privateKey">The private key</param>
		/// <returns>
		/// A ring signature which can be verified by a ring using the same members,
		/// or null if the private key does not have a corresponding public key in the ring
		/// </returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the hashedMessage parameter is null
		/// </exception>
		public RingSignature Sign(IHash hashedMessage, BigInteger privateKey)
		{
			if (hashedMessage == null)
				throw new ArgumentNullException("hashedMessage");

			int? signingKeyIndex = null;

			// find the signing key
			for (var i = 0; i < _orderedPublicKeys.Length; i++)
			{
				if (_orderedPublicKeys[i] != Curve.G * privateKey)
					continue;

				signingKeyIndex = i;
				break;
			}

			// if the signing key is not found, exit
			if (signingKeyIndex == null)
				return null;

			var u = IsLinked
				? new FieldElement(1, Curve.N)
				: GetRandFieldElement();

			var biasedRingPoint = RingPoint * u;

			var yt = biasedRingPoint * privateKey;

			var cs = new FieldElement[MemberCount];
			var ss = new FieldElement[MemberCount];
			var zs = new ECPoint[MemberCount];
			var zss = new ECPoint[MemberCount];

			var r = GetRandFieldElement();

			// start building the ring at the index of the signing index
			cs[(signingKeyIndex.Value + 1) % MemberCount] = GetFieldElement(
				hashedMessage, yt, Curve.G * r, biasedRingPoint * r
			);

			// cycle through the ring
			for (var i = 0; i < MemberCount; i++)
			{
				var ki = (signingKeyIndex.Value + i + 1) % MemberCount;
				var ni = (signingKeyIndex.Value + i + 2) % MemberCount;

				ss[ki] = GetRandFieldElement();

				zs[ki] = Curve.G * ss[ki] + _orderedPublicKeys[ki] * cs[ki];
				zss[ki] = biasedRingPoint * ss[ki] + yt * cs[ki];

				// for some reason, the signer which starts in the first position
				// within the ring, it mustn't be overwritten
				if (ni == 0 && signingKeyIndex.Value == MemberCount - 1)
					break;

				cs[ni] = GetFieldElement(hashedMessage, yt, zs[ki], zss[ki]);
			}

			// subtract out the random value
			ss[signingKeyIndex.Value] = r - (cs[signingKeyIndex.Value] * privateKey);

			return new RingSignature(
				cs[0], ss.Select(fe => fe.Value).ToArray(),
				yt, IsLinked ? null : biasedRingPoint
			);
		}

		#endregion Sign

		#region Verify

		/// <summary>
		/// Verifies a signature made against an equivalent signing ring
		/// </summary>
		/// <param name="serializable">The item to verify</param>
		/// <param name="signature">The signature to verify the message against</param>
		/// <param name="updateUsedSignatureList">
		/// When true, the list of used signatures is updated on successful verifications.
		/// If the IsLinked property of the signing ring is false, this parameter has no effect
		/// </param>
		/// <returns>
		/// True if the message was signed by a
		/// member in the signing ring, false otherwise
		/// </returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if either the serializable or signature parameters are null
		/// </exception>
		public bool Verify(ISerializable serializable, RingSignature signature, bool updateUsedSignatureList = true)
		{
			if (signature == null)
				throw new ArgumentNullException("signature");

			return Verify(_hasher.Of(serializable), signature, updateUsedSignatureList);
		}

		/// <summary>
		/// Verifies a signature made against an equivalent signing ring
		/// </summary>
		/// <param name="message">The message to verify</param>
		/// <param name="signature">The signature to verify the message against</param>
		/// <param name="updateUsedSignatureList">
		/// When true, the list of used signatures is updated on successful verifications.
		/// If the IsLinked property of the signing ring is false, this parameter has no effect
		/// </param>
		/// <returns>
		/// True if the message was signed by a
		/// member in the signing ring, false otherwise
		/// </returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if either the message or signature parameters are null
		/// </exception>
		public bool Verify(byte[] message, RingSignature signature, bool updateUsedSignatureList = true)
		{
			if (message == null)
				throw new ArgumentNullException("message");

			return Verify(_hasher.Of(message), signature, updateUsedSignatureList);
		}

		/// <summary>
		/// Verifies a signature made against an equivalent ring signature
		/// </summary>
		/// <param name="hashedMessage">The hash of the message to verify</param>
		/// <param name="signature">The signature to verify the message against</param>
		/// <param name="updateUsedSignatureList">
		/// When true, the list of used signatures is updated on successful verifications.
		/// If the IsLinked property of the signing ring is false, this parameter has no effect
		/// </param>
		/// <returns>
		/// True if the message was signed by a
		/// member in the signing ring, false otherwise
		/// </returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if either the hashedMessage or signature parameters are null
		/// </exception>
		public bool Verify(IHash hashedMessage, RingSignature signature, bool updateUsedSignatureList = true)
		{
			if (signature == null)
				throw new ArgumentNullException("signature");

			var cs = new BigInteger[MemberCount];
			cs[0] = signature.RingTarget;

			var zs = new ECPoint[MemberCount];
			var zss = new ECPoint[MemberCount];

			var biasedRingPoint = signature.UnlinkedRingPoint ?? RingPoint;

			for (var i = 0; i < _orderedPublicKeys.Length; i++)
			{
				zs[i] = Curve.G * signature.RandomIntermediaries[i] + _orderedPublicKeys[i] * cs[i];
				zss[i] = biasedRingPoint * signature.RandomIntermediaries[i] + signature.SignerPsuedoPublicKey * cs[i];

				if (i >= MemberCount - 1)
					continue;

				cs[i + 1] = GetFieldElement(hashedMessage, signature.SignerPsuedoPublicKey, zs[i], zss[i]);
			}

			var verify = GetFieldElement(
				hashedMessage,
				signature.SignerPsuedoPublicKey,
				zs[MemberCount - 1],
				zss[MemberCount - 1]
			);

			var isValid = cs[0] == verify;

			if (
				isValid && updateUsedSignatureList && IsLinked &&
				! _usedKeys.Contains(signature.SignerPsuedoPublicKey)
			)
			{
				_usedKeys.Add(signature.SignerPsuedoPublicKey);
			}

			return isValid;
		}
		
		#endregion Verify

		#region Membership / Use Test

		/// <summary>
		/// Determines whether a public key is a member of the signing ring
		/// </summary>
		/// <param name="memberPublicKey">The public key of the member to test</param>
		/// <returns>
		/// True if the member is in the ring and the point, false otherwise
		/// </returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the memberPublicKey parameter is null
		/// </exception>
		public bool IsMember(ECPoint memberPublicKey)
		{
			if (memberPublicKey == null)
				throw new ArgumentNullException("memberPublicKey");

			return _orderedPublicKeys.Any(p => p == memberPublicKey);
		}

		/// <summary>
		/// Checks to see if a given member has already signed using the signing ring
		/// </summary>
		/// <param name="signature">The signature to check against the signing ring</param>
		/// <returns>
		/// True if the signature has already been used for the signing ring, false otherwise
		/// </returns>
		/// <remarks>
		/// The signing ring must be updated per verification to correctly report usage
		/// </remarks>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the signature parameter is null
		/// </exception>
		/// <exception cref="System.InvalidOperationException">
		/// Thrown if the signing ring is not linked
		/// </exception>
		public bool IsUsed(RingSignature signature)
		{
			if (signature == null)
				throw new ArgumentNullException("signature");

			return IsUsed(signature.SignerPsuedoPublicKey);
		}

		/// <summary>
		/// Checks to see if a given member has already signed using the signing ring
		/// </summary>
		/// <param name="signerPsuedoPublicKey">The psuedo public key to check against the signing ring</param>
		/// <returns>
		/// True if the signature has already been used for the signing ring, false otherwise
		/// </returns>
		/// <remarks>
		/// The signing ring must be updated per verification to correctly report usage
		/// </remarks>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the signature parameter is null
		/// </exception>
		/// <exception cref="System.InvalidOperationException">
		/// Thrown if the signing ring is not linked
		/// </exception>
		public bool IsUsed(ECPoint signerPsuedoPublicKey)
		{
			if (signerPsuedoPublicKey == null)
				throw new ArgumentNullException("signerPsuedoPublicKey");

			if (!IsLinked)
			{
				throw new InvalidOperationException(
					"Operation not allowed on unlinked SigningRings"
				);
			}

			return _usedKeys.Any(uk => uk == signerPsuedoPublicKey);
		}

		#endregion Membership / Use Test

		#region Serialization

		/// <summary>
		/// Serializes the given item to a byte array
		/// </summary>
		/// <returns>The serialization of the item</returns>
		public byte[] ToByteArray()
		{
			using (var ms = new MemoryStream())
			{
				WriteTo(ms);
				return ms.ToArray();
			}
		}

		/// <summary>
		/// Writes the item to a stream
		/// </summary>
		/// <param name="s">The stream to write to</param>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the stream parameter is null
		/// </exception>
		public void WriteTo(Stream s)
		{
			if (s == null)
				throw new ArgumentNullException("s");

			s.WriteByte(MemberCount);

			for (var i = 0; i < MemberCount; i++)
			{
				_orderedPublicKeys[i].WriteTo(s, true);
			}

			s.WriteByte((byte) (IsLinked ? 1 : 0));

			if (!IsLinked)
				return;

			s.WriteByte((byte) _usedKeys.Count);

			foreach (var usedKey in _usedKeys)
			{
				usedKey.WriteTo(s, true);
			}
		}

		#endregion Serialization

		#region Utilities

		private FieldElement GetRandFieldElement()
		{
			return new FieldElement(_rh.NextBigInteger(1, Curve.N), Curve.N);
		}

		private FieldElement GetFieldElement(
			IHash message, ECPoint yt, ECPoint p1, ECPoint p2
		)
		{
			var h = SHA256Hash.Factory.Of(
				new ISerializable[]
				{
					Id, message,
					yt, p1, p2
				}
			).ToByteArray().ToNonNegativeBigInteger();

			return new FieldElement(h, Curve.N);
		}

		#endregion Utilities
	}
}