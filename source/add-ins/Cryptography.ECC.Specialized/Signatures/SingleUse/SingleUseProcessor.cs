﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Numerics;

using PXL.BigMath;
using PXL.Cryptography.ECC.Signing;
using PXL.Units;

namespace PXL.Cryptography.ECC.Specialized.Signatures.SingleUse
{
	/// <summary>
	/// Signs, verifies, or springs single use address
	/// </summary>
	public static class SingleUseProcessor
	{
		#region Inner Class

		private class FauxRandomHelper : IRandomHelper
		{
			private readonly BigInteger _k;

			public FauxRandomHelper(BigInteger k)
			{
				_k = k;
			}

			public byte[] NextBytes(int count)
			{
				throw new NotImplementedException();
			}

			public void WriteRandomBytesTo(Stream s, int count)
			{
				throw new NotImplementedException();
			}

			public bool NextBool(double trueLikelihood = 0.5)
			{
				throw new NotImplementedException();
			}

			public double NextDouble()
			{
				throw new NotImplementedException();
			}

			public int NextInt(int max, bool isInclusive = false, int quantizeTo = 1)
			{
				throw new NotImplementedException();
			}

			public int NextInt(int min, int max, bool isMaxInclusive = false, int quantizeTo = 1)
			{
				throw new NotImplementedException();
			}

			public BigInteger NextBigInteger(BigInteger max, bool isInclusive = false)
			{
				throw new NotImplementedException();
			}

			public BigInteger NextBigInteger(BigInteger min, BigInteger max, bool isMaxInclusive = false)
			{
				return _k;
			}

			public Guid NextGuid()
			{
				throw new NotImplementedException();
			}

			public T NextEnum<T>()
			{
				throw new NotImplementedException();
			}

			public T NextFlagsEnum<T>(double flagLikelihood = 0.5)
			{
				throw new NotImplementedException();
			}

			public T NextElement<T>(IEnumerable<T> set)
			{
				throw new NotImplementedException();
			}

			public T[] Randomize<T>(ICollection<T> set)
			{
				throw new NotImplementedException();
			}

			public string NextString(int size, string domain = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz")
			{
				throw new NotImplementedException();
			}

			public DateTime NextDate(DateTime min, DateTime max)
			{
				throw new NotImplementedException();
			}

			public DateTime NextDateCenteredAround(DateTime centerDate, int range, bool includeTime = false)
			{
				throw new NotImplementedException();
			}

			public T? NullIfStruct<T>(Func<RandomHelper, T> nonNullGenerator, double nullLikelihood = 0.5) where T : struct
			{
				throw new NotImplementedException();
			}

			public T NullIf<T>(Func<RandomHelper, T> nonNullGenerator, double nullLikelihood = 0.5) where T : class
			{
				throw new NotImplementedException();
			}

			public bool IsCryptographicallyStrong
			{
				get { return true; }
			}
		}

		#endregion Inner Class

		#region Sign

		/// <summary>
		/// Produces a signature for a serializable / privateKey pair
		/// </summary>
		/// <param name="serializable">The item to sign</param>
		/// <param name="address">The single use address to use for signing</param>
		/// <param name="hasher">
		/// The hasher to use to convert the serializable to a value
		/// capable of being signed. If not specified, SHA256Hash.Factory is used.
		/// </param>
		/// <returns>
		/// A digital signature which can be used to verify the
		/// integrity of a message or identity of a signer
		/// </returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the serializable argument is null
		/// </exception>
		/// <exception cref="System.ArgumentOutOfRangeException">
		/// Thrown if the private key is outside of the bounds of
		/// the curve domain given in the construction parameters for the signer
		/// or if it is less than or equal to zero
		/// </exception>
		public static ECDSASignature Sign(
			ISerializable serializable, SingleUseAddress address, IHasher hasher = null
		)
		{
			if (serializable == null)
				throw new ArgumentNullException("serializable");

			return Sign((hasher ?? SHA256Hash.Factory).Of(serializable), address);			
		}

		/// <summary>
		/// Produces a signature for a message item / privateKey pair
		/// </summary>
		/// <param name="message">The message to sign</param>
		/// <param name="address">The single use address to use for signing</param>
		/// <param name="hasher">
		/// The hasher to use to convert the serializable to a value
		/// capable of being signed. If not specified, SHA256Hash.Factory is used.
		/// </param>
		/// <returns>
		/// A digital signature which can be used to verify the
		/// integrity of a message or identity of a signer
		/// </returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the message argument is null
		/// </exception>
		/// <exception cref="System.ArgumentOutOfRangeException">
		/// Thrown if the private key is outside of the bounds of
		/// the curve domain given in the construction parameters for the signer
		/// or if it is less than or equal to zero
		/// </exception>
		public static ECDSASignature Sign(
			byte[] message, SingleUseAddress address, IHasher hasher = null
		)
		{
			if (message == null)
				throw new ArgumentNullException("message");

			return Sign((hasher ?? SHA256Hash.Factory).Of(message), address);
		}

		/// <summary>
		/// Produces a signature for a hashed hashedMessage / privateKey pair
		/// </summary>
		/// <param name="hashedMessage">The hashedMessage to sign</param>
		/// <param name="address">The single use address to use for signing</param>
		/// <returns>
		/// A digital signature which can be used to verify the
		/// integrity of a message or identity of a signer
		/// </returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the hashedMessage argument is null
		/// </exception>
		/// <exception cref="System.ArgumentOutOfRangeException">
		/// Thrown if the private key is outside of the bounds of
		/// the curve domain given in the construction parameters for the signer
		/// or if it is less than or equal to zero
		/// </exception>
		public static ECDSASignature Sign(IHash hashedMessage, SingleUseAddress address)
		{
			if (hashedMessage == null)
				throw new ArgumentNullException("hashedMessage");

			if( address == null )
				throw new ArgumentNullException("address");

			if( ! address.HasPrivateInfo )
				throw new ArgumentException("Address must contain private information to sign", "address");

			var signer = new ECDSASigner(address.PublicKey.Curve, new FauxRandomHelper(address.PrecommitmentK));

			var sig = signer.Sign(hashedMessage, address.PrivateKey);

			return new ECDSASignature(sig.R, sig.S);
		}

		#endregion Sign

		#region Verify

		/// <summary>
		/// Verifies that a given serializable was signed by the private
		/// key associated with the given public key
		/// </summary>
		/// <param name="serializable">The serializable to verify</param>
		/// <param name="signature">The signature of the serializable</param>
		/// <param name="address">
		/// The single use address associated with the signing private key
		/// </param>
		/// <param name="hasher">
		/// The hasher to use to convert the serializable to a value
		/// capable of being signed. If not specified, SHA256Hash.Factory is used.
		/// </param>
		/// <returns>
		/// True if the message was signed by the private key associated
		/// with the given public key and neither the message nor the
		/// signature has been tampered with, almost certainly false otherwise
		/// </returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if any of the parameters are null
		/// </exception>
		/// <exception cref="PointMismatchException">
		/// Thrown if the publicKey parameter is not on the same curve
		/// as the curve specified in the parameters passed on signer construction
		/// </exception>
		public static bool Verify(
			ISerializable serializable, ECDSASignature signature,
			SingleUseAddress address, IHasher hasher = null
		)
		{
			if (serializable == null)
				throw new ArgumentNullException("serializable");

			return Verify(
				(hasher ?? SHA256Hash.Factory).Of(serializable), signature, address
			);
		}

		/// <summary>
		/// Verifies that a given message was signed by the private
		/// key associated with the given public key
		/// </summary>
		/// <param name="message">The serializable to verify</param>
		/// <param name="signature">The signature of the message summary</param>
		/// <param name="address">
		/// The single use address associated with the signing private key
		/// </param>
		/// <param name="hasher">
		/// The hasher to use to convert the serializable to a value
		/// capable of being signed. If not specified, SHA256Hash.Factory is used.
		/// </param>
		/// <returns>
		/// True if the message was signed by the private key associated
		/// with the given public key and neither the message nor the
		/// signature has been tampered with, almost certainly false otherwise
		/// </returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if any of the parameters are null
		/// </exception>
		/// <exception cref="PointMismatchException">
		/// Thrown if the publicKey parameter is not on the same curve
		/// as the curve specified in the parameters passed on signer construction
		/// </exception>
		public static bool Verify(
			byte[] message, ECDSASignature signature,
			SingleUseAddress address, IHasher hasher = null
		)
		{
			if (message == null)
				throw new ArgumentNullException("message");

			return Verify(
				(hasher ?? SHA256Hash.Factory).Of(message), signature, address
			);
		}

		/// <summary>
		/// Verifies that a given hashed message was signed by the private
		/// key associated with the given public key
		/// </summary>
		/// <param name="hashedMessage">The hashed message to verify</param>
		/// <param name="signature">The signature of the message summary</param>
		/// <param name="address">
		/// The single use address associated with the signing private key
		/// </param>
		/// <returns>
		/// True if the message was signed by the private key associated
		/// with the given public key and neither the message nor the
		/// signature has been tampered with, almost certainly false otherwise
		/// </returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if any of the parameters are null
		/// </exception>
		/// <exception cref="PointMismatchException">
		/// Thrown if the publicKey parameter is not on the same curve
		/// as the curve specified in the parameters passed on signer construction
		/// </exception>
		public static bool Verify(IHash hashedMessage, ECDSASignature signature, SingleUseAddress address)
		{
			if (hashedMessage == null)
				throw new ArgumentNullException("hashedMessage");

			if( signature == null )
				throw new ArgumentNullException("signature");

			if( address == null )
				throw new ArgumentNullException("address");

			if (address.PrecommitmentR != SingleUseAddress.ComputeRFromSignature(signature))
				return false;

			var signer = new ECDSASigner(address.PublicKey.Curve, new FauxRandomHelper(0));

			return signer.Verify(hashedMessage, signature, address.PublicKey);
		}

		#endregion Verify

		#region Spring

		/// <summary>
		/// Reveals the private key for a SingleUseAddress given
		/// two different messages and valid signatures
		/// </summary>
		/// <param name="curve">The curve the signature was made on</param>
		/// <param name="serializable1">The first signed item</param>
		/// <param name="signature1">The signature for the first signed item</param>
		/// <param name="serializable2">The second signed item</param>
		/// <param name="signature2">The signature for the second signed item</param>
		/// <param name="hasher">
		/// The hasher to use to convert the serializable to a value
		/// capable of being signed. If not specified, SHA256Hash.Factory is used.
		/// </param>
		/// <returns>
		/// The private key of the SingleUseAddress if the
		/// items and signatures are different and the signatures are valid,
		/// null if the items or signatures are the same,
		/// the signatures do not have the same precommitment output (R) value,
		/// or an arbitrary BigInteger value if either of the signatures is
		/// invalid or the items are not what was signed
		/// </returns>
		/// <exception cref="System.ArgumentException">
		/// Thrown if any of the non-optional parameters are null
		/// </exception>
		public static BigInteger? Spring(
			ECCurve curve,
			ISerializable serializable1, ECDSASignature signature1,
			ISerializable serializable2, ECDSASignature signature2,
			IHasher hasher = null
		)
		{
			if (serializable1 == null)
				throw new ArgumentNullException("serializable1");

			if (serializable2 == null)
				throw new ArgumentNullException("serializable2");

			return Spring(
				curve,
				(hasher ?? SHA256Hash.Factory).Of(serializable1),
				signature1,
				(hasher ?? SHA256Hash.Factory).Of(serializable2),
				signature2
			);
		}

		/// <summary>
		/// Reveals the private key for a SingleUseAddress given
		/// two different messages and valid signatures
		/// </summary>
		/// <param name="curve">The curve the signature was made on</param>
		/// <param name="message1">The first signed message</param>
		/// <param name="signature1">The signature for the first message</param>
		/// <param name="message2">The second signed message</param>
		/// <param name="signature2">The signature for the second message</param>
		/// <param name="hasher">
		/// The hasher to use to convert the messages to a value
		/// capable of being signed. If not specified, SHA256Hash.Factory is used.
		/// </param>
		/// <returns>
		/// The private key of the SingleUseAddress if the
		/// messages and signatures are different and the signatures are valid,
		/// null if the messages or signatures are the same,
		/// the signatures do not have the same precommitment output (R) value,
		/// or an arbitrary BigInteger value if either of the signatures is
		/// invalid or the messages are not what was signed
		/// </returns>
		/// <exception cref="System.ArgumentException">
		/// Thrown if any of the non-optional parameters are null
		/// </exception>
		public static BigInteger? Spring(
			ECCurve curve,
			byte[] message1, ECDSASignature signature1,
			byte[] message2, ECDSASignature signature2,
			IHasher hasher = null
		)
		{
			if (message1 == null)
				throw new ArgumentNullException("message1");

			if (message2 == null)
				throw new ArgumentNullException("message2");

			return Spring(
				curve,
				(hasher ?? SHA256Hash.Factory).Of(message1),
				signature1,
				(hasher ?? SHA256Hash.Factory).Of(message2),
				signature2
			);
		}

		/// <summary>
		/// Reveals the private key for a SingleUseAddress given
		/// two different hashed messages and valid signatures
		/// </summary>
		/// <param name="curve">The curve the signature was made on</param>
		/// <param name="hashedMessage1">The first signed hashed message</param>
		/// <param name="signature1">The signature for the first hashed message</param>
		/// <param name="hashedMessage2">The second hashed message</param>
		/// <param name="signature2">The signature for the second hashed message</param>
		/// <returns>
		/// The private key of the SingleUseAddress if the
		/// hashed message and signatures are different and the signatures are valid,
		/// null if the hashed message or signatures are the same,
		/// the signatures do not have the same precommitment output (R) value,
		/// or an arbitrary BigInteger value if either of the signatures is
		/// invalid or the hashed message are not what was signed
		/// </returns>
		/// <exception cref="System.ArgumentException">
		/// Thrown if any of the non-optional parameters are null
		/// </exception>
		public static BigInteger? Spring(
			ECCurve curve,
			IHash hashedMessage1, ECDSASignature signature1,
			IHash hashedMessage2, ECDSASignature signature2
		)
		{
			if( curve == null )
				throw new ArgumentNullException("curve");

			if( signature1 == null )
				throw new ArgumentNullException("signature1");

			if( signature2 == null )
				throw new ArgumentNullException("signature2");

			if (hashedMessage1.Equals(hashedMessage2))
				return null;

			if (signature1.R != signature2.R)
				return null;

			var e1 = new FieldElement(
				ECDSASigner.CalculateE(curve, hashedMessage1.ToByteArray()),
				curve.N
			);

			var e2 = new FieldElement(
				ECDSASigner.CalculateE(curve, hashedMessage2.ToByteArray()),
				curve.N
			);

			var s1 = new FieldElement(signature1.S, curve.N);
			var s2 = new FieldElement(signature2.S, curve.N);

			var k = (e1 - e2)/(s1 - s2);

			return (s1*k - e1)/signature1.R;
		}

		#endregion Spring
	}
}