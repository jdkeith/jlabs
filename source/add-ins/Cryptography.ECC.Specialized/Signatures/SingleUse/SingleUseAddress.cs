﻿using System;
using System.IO;
using System.Numerics;
using System.Security.Cryptography;

using PXL.Cryptography.ECC.Signing;
using PXL.Units;
using PXL.Units.Utilities;

namespace PXL.Cryptography.ECC.Specialized.Signatures.SingleUse
{
	/// <summary>
	/// An address which can only sign one message without having its public key revealed
	/// </summary>
	public class SingleUseAddress : ISerializable
	{
		#region Factory

		/// <summary>
		/// Gets a factory which deserializes SingleUseAddresses on the given curve
		/// </summary>
		/// <param name="curve">The curve to deserialize the SingleUseAddress on</param>
		/// <returns>A SingleUseAddress deserializer</returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the curve parameter is null
		/// </exception>
		public static IDeserializer<SingleUseAddress> Factory(ECCurve curve)
		{
			if( curve == null )
				throw new ArgumentNullException("curve");

			return new SingleUseAddressFactory(curve);
		}

		private class SingleUseAddressFactory : IDeserializer<SingleUseAddress>
		{
			private readonly ECCurve _curve;

			public SingleUseAddressFactory(ECCurve curve)
			{
				_curve = curve;
			}

			public SingleUseAddress Deserialize(byte[] serialized)
			{
				if( serialized == null )
					throw new ArgumentNullException("serialized");

				using (var ms = new MemoryStream())
				{
					ms.Write(serialized, 0, serialized.Length);
					ms.Seek(0, SeekOrigin.Begin);
					return ReadFrom(ms);
				}
			}

			public SingleUseAddress ReadFrom(Stream stream)
			{
				if (stream == null)
					throw new ArgumentNullException("stream");

				var br = new BinaryReader(stream);

				var hasPrivateInfo = br.ReadBoolean();

				if (hasPrivateInfo)
				{
					var k = new BigInteger(stream.ReadByteArrayFrom());
					var d = new BigInteger(stream.ReadByteArrayFrom());

					return new SingleUseAddress(_curve, d, k);
				}

				var r = SHA256Hash.Factory.ReadFrom(stream);
				var pt = _curve.ReadFrom(stream);

				return new SingleUseAddress(pt, r);
			}
		}

		#endregion Factory

		#region Fields

		private readonly BigInteger? _k;
		private readonly BigInteger? _d;

		#endregion Fields

		#region Constructor

		/// <summary>
		/// Creates a new SingleUseAddress given a curve and private key
		/// </summary>
		/// <param name="curve">The curve to generate the SingleUseAddress on</param>
		/// <param name="privateKey">
		/// The private key of the signing key, this is the value
		/// which will be revealed on multiple uses
		/// </param>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the curve parameter is null
		/// </exception>
		/// <exception cref="System.ArgumentOutOfRangeException">
		/// Thrown if the private key is not positive
		/// </exception>
		public SingleUseAddress(ECCurve curve, BigInteger privateKey)
		{
			if( curve == null )
				throw new ArgumentNullException("curve");

			if( privateKey <= 0 )
				throw new ArgumentOutOfRangeException("privateKey", "Private key must be positive");

			_d = privateKey % curve.N;

			PublicKey = curve.G * _d.Value;

			_k = new RandomHelper(new RNGCryptoServiceProvider(), true).NextBigInteger(1, curve.N);

			PrecommitmentR = ComputeRFromK(curve, _k.Value);
		}

		/// <summary>
		/// Creates a new SingleUseAddress given a curve and private key and a non-random precomittment
		/// </summary>
		/// <param name="curve">The curve to generate the SingleUseAddress on</param>
		/// <param name="privateKey">
		/// The private key of the signing key, this is the value
		/// which will be revealed on multiple uses
		/// </param>
		/// <param name="precommitmentK">
		/// The value to use to generate a precomittment. This is the k value in the ECDSA signature
		/// </param>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the curve parameter is null
		/// </exception>
		/// <exception cref="System.ArgumentOutOfRangeException">
		/// Thrown if the private key is not positive
		/// </exception>
		public SingleUseAddress(ECCurve curve, BigInteger privateKey, BigInteger precommitmentK)
			: this(curve, privateKey)
		{
			if (precommitmentK <= 0)
				throw new ArgumentOutOfRangeException("precommitmentK", "Precommitment must be positive");

			_k = precommitmentK % curve.N;

			PrecommitmentR = ComputeRFromK(curve, _k.Value);
		}

		/// <summary>
		/// Creates a new SingleUseAddress given a curve and private key and a
		/// random precomittment seed using the given random helper
		/// </summary>
		/// <param name="curve">The curve to generate the SingleUseAddress on</param>
		/// <param name="privateKey">
		/// The private key of the signing key, this is the value
		/// which will be revealed on multiple uses
		/// </param>
		/// <param name="rh">
		/// The random helper to use to generate precommitment seeds
		/// </param>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the curve or rh parameters are null
		/// </exception>
		/// <exception cref="System.ArgumentException">
		/// Thrown if the rh parameter is not cryptographically strong
		/// </exception>
		public SingleUseAddress(ECCurve curve, BigInteger privateKey, IRandomHelper rh)
			: this(curve, privateKey)
		{
			if( rh == null )
				throw new ArgumentNullException("rh", "Random helper cannot be null");

			if( ! rh.IsCryptographicallyStrong )
				throw new ArgumentException("Random helper must be cryptographically strong", "rh");

			_k = rh.NextBigInteger(1, curve.N);

			PrecommitmentR = ComputeRFromK(curve, _k.Value);
		}

		/// <summary>
		/// Creates a new SingleUseAddress using the given public key and precommitment
		/// </summary>
		/// <param name="publicKey">The public key the SingleUseAddress uses</param>
		/// <param name="precommitmentR">The precommitment to use for the SingleUseAddress</param>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the publicKey is null
		/// </exception>
		public SingleUseAddress(ECPoint publicKey, SHA256Hash precommitmentR)
		{
			if( publicKey == null )
				throw new ArgumentNullException("publicKey");

			if (publicKey.IsInfinity)
				throw new ArgumentException("Public key cannot be point at infinity", "publicKey");

			PublicKey = publicKey;
			PrecommitmentR = precommitmentR;
		}

		#endregion Constructor

		#region Properties

		/// <summary>
		/// Whether or not the SingleUseAddress instance has
		/// private information in it
		/// </summary>
		public bool HasPrivateInfo
		{
			get { return _d.HasValue && _k.HasValue; }
		}

		/// <summary>
		/// The hash of the public precommitment value
		/// against which signatures are checked
		/// </summary>
		public SHA256Hash PrecommitmentR { get; private set; }

		/// <summary>
		/// The private precommitment (K) value
		/// </summary>
		/// <exception cref="System.NotSupportedException">
		/// Thrown if the SingleUseAddress instance doesn't contain private information
		/// </exception>
		public BigInteger PrecommitmentK
		{
			get
			{
				if (_k.HasValue)
					return _k.Value;

				throw new NotSupportedException("This SingleUseAddress instance doesn't contain private information");
			}
		}

		/// <summary>
		/// The address' public key
		/// </summary>
		public ECPoint PublicKey { get; private set; }

		/// <summary>
		/// The address' private key
		/// </summary>
		/// <exception cref="System.NotSupportedException">
		/// Thrown if the SingleUseAddress instance doesn't contain private information
		/// </exception>
		public BigInteger PrivateKey
		{
			get
			{
				if (_d.HasValue)
					return _d.Value;

				throw new NotSupportedException("This SingleUseAddress instance doesn't contain private information");
			}
		}

		#endregion Properties

		#region Serializable

		/// <summary>
		/// Converts the item to a byte array
		/// </summary>
		/// <returns>A serialized representation of the item</returns>
		public byte[] ToByteArray()
		{
			return ToByteArray(false);
		}

		/// <summary>
		/// Converts the item to a byte array
		/// </summary>
		/// <param name="includePrivateInfo">
		/// Whether or not to include private information in the serialization
		/// </param>
		/// <returns>A serialized representation of the item</returns>
		public byte[] ToByteArray(bool includePrivateInfo)
		{
			using (var ms = new MemoryStream())
			{
				WriteTo(ms, includePrivateInfo);
				return ms.ToArray();
			}
		}

		/// <summary>
		/// Writes the item to a stream
		/// </summary>
		/// <param name="s">The stream to write to</param>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the stream is null
		/// </exception>
		public void WriteTo(Stream s)
		{
			WriteTo(s, false);
		}

		/// <summary>
		/// Writes the item to a stream
		/// </summary>
		/// <param name="s">The stream to write to</param>
		/// <param name="includePrivateInfo">
		/// Whether or not to include private information in the serialization
		/// </param>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the stream is null
		/// </exception>
		public void WriteTo(Stream s, bool includePrivateInfo)
		{
			if( s == null )
				throw new ArgumentNullException("s");

			var bw = new BinaryWriter(s);

			bw.Write(includePrivateInfo);

			if (includePrivateInfo)
			{
				if (! HasPrivateInfo)
				{
					throw new NotSupportedException(
						"This SingleUseAddress instance doesn't contain private information"
					);
				}

				// ReSharper disable once PossibleInvalidOperationException
				_k.Value.ToByteArray().WriteTo(s);

				// ReSharper disable once PossibleInvalidOperationException
				_d.Value.ToByteArray().WriteTo(s);

				return;
			}

			PrecommitmentR.ToByteArray().WriteTo(s);

			var pd = PublicKey.ToByteArray(true);
			s.Write(pd, 0, pd.Length);
		}

		#endregion Serializable

		#region Utilities

		private static SHA256Hash ComputeRFromK(ECCurve curve, BigInteger k)
		{
			return SHA256Hash.Factory.Of(
				(curve.G * k).X.Value.ToFixedSizeByteArray(32, trimLeadingZeros: true)
			);
		}

		/// <summary>
		/// Computes a precommitment hash from an ECDSASignature
		/// </summary>
		/// <param name="signature">The signature to compute the precommitment hash from</param>
		/// <returns>The precommitment hash of the signature</returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the signature parameter is null
		/// </exception>
		public static SHA256Hash ComputeRFromSignature(ECDSASignature signature)
		{
			if( signature == null )
				throw new ArgumentNullException("signature");

			return SHA256Hash.Factory.Of(
				signature.R.ToFixedSizeByteArray(32, trimLeadingZeros: true)
			);
		}

		#endregion Utilities
	}
}
