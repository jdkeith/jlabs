﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace PXL.Core.Storage
{
	/// <summary>
	/// Represents a simple cache with no priority differentiation (except for DoNotStore)
	/// and no automatic deletion to save space (except for expired items)
	/// </summary>
	public class MemoryCache : ICache
	{
		private class CacheItem
		{
			public CacheItem(object value)
				: this(value, DateTime.MaxValue)
			{
			}

			public CacheItem(object value, DateTime expiresOn)
			{
				Value = value;
				ExpiresOn = expiresOn;
			}

			public DateTime ExpiresOn { get; private set; }
			public object Value { get; private set; }
		}

		private readonly Dictionary<string, CacheItem> _backingStore;

		public MemoryCache()
		{
			_backingStore = new Dictionary<string, CacheItem>();
		}

		/// <summary>
		/// Tries to get a value out of cache as the specified type of object
		/// </summary>
		/// <typeparam name="T">The type to get out of the cache</typeparam>
		/// <param name="key">The key of the item to return</param>
		/// <param name="value">The value to fill</param>
		/// <returns>True if the key is not null, the item is present in the cache and can be cast to the specified type, false otherwise</returns>
		/// <remarks>Value will be filled with the default value if the item is not found</remarks>
		public bool TryGetAs<T>(string key, out T value)
		{
			CacheItem result;

			value = default(T);

			if( _backingStore.TryGetValue(key, out result) == false )
				return false;

			if (result.ExpiresOn < DateTime.Now)
				return false;

			if( result is T )
			{
				value = (T)result.Value;
				return true;
			}

			return false;
		}

		/// <summary>
		/// Attempts to add an item to the cache but without overwriting any existing object with that key
		/// </summary>
		/// <param name="key">The key to store the value under</param>
		/// <param name="value">The value to store in the cache</param>
		/// <param name="priority">The priority to cache the item under</param>
		/// <returns>True if the item didn't exist and was overwritten, false otherwise</returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the key argument is null
		/// </exception>
		public bool SafeSet(string key, object value, CachePriority priority = CachePriority.Default)
		{
			return SafeSet(key, value, DateTime.MaxValue, priority);
		}

		/// <summary>
		/// Attempts to add an item to the cache but without overwriting any existing object with that key
		/// </summary>
		/// <param name="key">The key to store the value under</param>
		/// <param name="value">The value to store in the cache</param>
		/// <param name="absoluteExpiration">The date when the item will expire</param>
		/// <param name="priority">The priority to cache the item under</param>
		/// <returns>True if the item didn't exist and was overwritten, false otherwise</returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the key argument is null
		/// </exception>
		public bool SafeSet(string key, object value, DateTime absoluteExpiration, CachePriority priority = CachePriority.Default)
		{
			if (key == null)
				throw new ArgumentNullException("key");

			if (_backingStore.ContainsKey(key) || priority == CachePriority.DoNotCache)
				return false;

			_backingStore[key] = new CacheItem(value, absoluteExpiration);

			return true;
		}

		/// <summary>
		/// Attempts to add an item to the cache but without overwriting any existing object with that key
		/// </summary>
		/// <param name="key">The key to store the value under</param>
		/// <param name="value">The value to store in the cache</param>
		/// <param name="slidingExpiration">The date when the item will expire</param>
		/// <param name="priority">The priority to cache the item under</param>
		/// <returns>True if the item didn't exist and was overwritten, false otherwise</returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the key argument is null
		/// </exception>
		/// <exception cref="System.ArgumentOutOfRangeException">
		/// Thrown if the slidingExpiration argument is equal to or less than TimeSpan.Zero or beyond the range allowed by the cache
		/// </exception>
		public bool SafeSet(string key, object value, TimeSpan slidingExpiration, CachePriority priority = CachePriority.Default)
		{
			if (slidingExpiration.Ticks <= 0)
				throw new ArgumentOutOfRangeException("Sliding expiration must be a positive value");

			return SafeSet(key, value, DateTime.Now.Add(slidingExpiration), priority);
		}

		/// <summary>
		/// Gets or sets an item in the cache
		/// </summary>
		/// <param name="key">The key to store the value under</param>
		/// <returns>The value retrieved if one exists at that key, null otherwise</returns>
		/// <remarks>Any existing value is overwritten</remarks>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the key argument is null
		/// </exception>
		public object this[string key]
		{
			get { return _backingStore[key]; }
			set { _backingStore[key] = new CacheItem(value); }
		}

		/// <summary>
		/// Sets an item in the cache with a custom priority
		/// </summary>
		/// <param name="key">The key to store the value under</param>
		/// <param name="priority">The priority to cache the item under</param>
		/// <returns>Nothing, it's set-only</returns>
		/// <remarks>Any existing value is overwritten</remarks>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the key argument is null
		/// </exception>
		public object this[string key, CachePriority priority = CachePriority.Default]
		{
			set
			{
				if (priority == CachePriority.DoNotCache)
				{
					_backingStore.Remove(key);
					return;
				}

				_backingStore[key] = new CacheItem(value);
			}
		}

		/// <summary>
		/// Sets an item in the cache with an absolute expiration date
		/// </summary>
		/// <param name="key">The key to store the value under</param>
		/// <param name="absoluteExpiration">The date when the item will expire</param>
		/// <param name="priority">The priority to cache the item under</param>
		/// <returns>Nothing, it's set-only</returns>
		/// <remarks>Any existing value is overwritten</remarks>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the key argument is null
		/// </exception>
		public object this[string key, DateTime absoluteExpiration, CachePriority priority = CachePriority.Default]
		{
			set
			{
				if (priority == CachePriority.DoNotCache)
				{
					_backingStore.Remove(key);
					return;
				}

				_backingStore[key] = new CacheItem(value, absoluteExpiration);
			}
		}

		/// <summary>
		/// Sets an item in the cache with an absolute expiration date
		/// </summary>
		/// <param name="key">The key to store the value under</param>
		/// <param name="slidingExpiration">How long to cache the item for</param>
		/// <param name="priority">The priority to cache the item under</param>
		/// <returns>Nothing, it's set-only</returns>
		/// <remarks>Any existing value is overwritten</remarks>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the key argument is null
		/// </exception>
		/// <exception cref="System.ArgumentOutOfRangeException">
		/// Thrown if the slidingExpiration argument is equal to or less than TimeSpan.Zero or beyond the range allowed by the cache
		/// </exception>
		public object this[string key, TimeSpan slidingExpiration, CachePriority priority = CachePriority.Default]
		{
			set
			{
				if (slidingExpiration.Ticks <= 0)
					throw new ArgumentOutOfRangeException("Sliding expiration must be a positive value");

				if (priority == CachePriority.DoNotCache)
				{
					_backingStore.Remove(key);
					return;
				}

				_backingStore[key] = new CacheItem(value, DateTime.Now.Add(slidingExpiration));
			}
		}

		/// <summary>
		/// Removes the item with the specified key if it exists, does nothing otherwise
		/// </summary>
		/// <param name="key">The key of the item to remove</param>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the key argument is null
		/// </exception>
		public void Remove(string key)
		{
			_backingStore.Remove(key);
		}

		/// <summary>
		/// Removes all items from the cache
		/// </summary>
		public void Clear()
		{
			_backingStore.Clear();
		}
		
		/// <summary>
		/// The percentage of memory consumed by the cache
		/// </summary>
		public long EffectivePercentagePhysicalMemoryLimit
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		/// <summary>
		/// The amount of bytes the cache is constrained to
		/// </summary>
		public long EffectivePrivateBytesLimit
		{
			get
			{
				var process = Process.GetCurrentProcess();

				return process.PrivateMemorySize64;
			}
		}
	}
}
