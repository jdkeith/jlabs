﻿using System;

namespace PXL.Core.Storage
{
	/// <summary>
	/// The priority with which to cache an item
	/// </summary>
	/// <remarks>
	/// Items with lower priorities are removed before
	/// items with higher priorities when space is low
	/// </remarks>
	public enum CachePriority : byte
	{
		/// <summary>
		/// The default priority
		/// </summary>
		Default,

		/// <summary>
		/// Do not store the item in cache
		/// </summary>
		DoNotCache,

		/// <summary>
		/// The lowest priority
		/// </summary>
		Low,

		/// <summary>
		/// A priority between normal and low
		/// </summary>
		BelowNormal,

		/// <summary>
		/// A normal priority
		/// </summary>
		Normal,

		/// <summary>
		/// A priority between normal and high
		/// </summary>
		AboveNormal,

		/// <summary>
		/// The highest priority which still allows automatic item removal
		/// </summary>
		High,

		/// <summary>
		/// Specifies that the item is not to be automatically removed from cache
		/// </summary>
		NotRemoveable
	}

	/// <summary>
	/// Represents a cache which can automatically drop items to save space
	/// </summary>
	public interface ICache
	{
		/// <summary>
		/// Tries to get a value out of cache as the specified type of object
		/// </summary>
		/// <typeparam name="T">The type to get out of the cache</typeparam>
		/// <param name="key">The key of the item to return</param>
		/// <param name="value">The value to fill</param>
		/// <returns>True if the key is not null, the item is present in the cache and can be cast to the specified type, false otherwise</returns>
		/// <remarks>Value will be filled with the default value if the item is not found</remarks>
		bool TryGetAs<T>(string key, out T value);

		/// <summary>
		/// Attempts to add an item to the cache but without overwriting any existing object with that key
		/// </summary>
		/// <param name="key">The key to store the value under</param>
		/// <param name="value">The value to store in the cache</param>
		/// <param name="priority">The priority to cache the item under</param>
		/// <returns>True if the item didn't exist and was overwritten, false otherwise</returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the key argument is null
		/// </exception>
		bool SafeSet(string key, object value, CachePriority priority = CachePriority.Default);

		/// <summary>
		/// Attempts to add an item to the cache but without overwriting any existing object with that key
		/// </summary>
		/// <param name="key">The key to store the value under</param>
		/// <param name="value">The value to store in the cache</param>
		/// <param name="absoluteExpiration">The date when the item will expire</param>
		/// <param name="priority">The priority to cache the item under</param>
		/// <returns>True if the item didn't exist and was overwritten, false otherwise</returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the key argument is null
		/// </exception>
		bool SafeSet(string key, object value, DateTime absoluteExpiration, CachePriority priority = CachePriority.Default);

		/// <summary>
		/// Attempts to add an item to the cache but without overwriting any existing object with that key
		/// </summary>
		/// <param name="key">The key to store the value under</param>
		/// <param name="value">The value to store in the cache</param>
		/// <param name="slidingExpiration">The date when the item will expire</param>
		/// <param name="priority">The priority to cache the item under</param>
		/// <returns>True if the item didn't exist and was overwritten, false otherwise</returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the key argument is null
		/// </exception>
		/// <exception cref="System.ArgumentOutOfRangeException">
		/// Thrown if the slidingExpiration argument is equal to or less than TimeSpan.Zero or beyond the range allowed by the cache
		/// </exception>
		bool SafeSet(string key, object value, TimeSpan slidingExpiration, CachePriority priority = CachePriority.Default);

		/// <summary>
		/// Gets or sets an item in the cache
		/// </summary>
		/// <param name="key">The key to store the value under</param>
		/// <returns>The value retrieved if one exists at that key, null otherwise</returns>
		/// <remarks>Any existing value is overwritten</remarks>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the key argument is null
		/// </exception>
		object this[string key] { get; set; }

		/// <summary>
		/// Sets an item in the cache with a custom priority
		/// </summary>
		/// <param name="key">The key to store the value under</param>
		/// <param name="priority">The priority to cache the item under</param>
		/// <returns>Nothing, it's set-only</returns>
		/// <remarks>Any existing value is overwritten</remarks>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the key argument is null
		/// </exception>
		object this[string key, CachePriority priority = CachePriority.Default] { set; }

		/// <summary>
		/// Sets an item in the cache with an absolute expiration date
		/// </summary>
		/// <param name="key">The key to store the value under</param>
		/// <param name="absoluteExpiration">The date when the item will expire</param>
		/// <param name="priority">The priority to cache the item under</param>
		/// <returns>Nothing, it's set-only</returns>
		/// <remarks>Any existing value is overwritten</remarks>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the key argument is null
		/// </exception>
		object this[string key, DateTime absoluteExpiration, CachePriority priority = CachePriority.Default] { set; }

		/// <summary>
		/// Sets an item in the cache with an absolute expiration date
		/// </summary>
		/// <param name="key">The key to store the value under</param>
		/// <param name="slidingExpiration">How long to cache the item for</param>
		/// <param name="priority">The priority to cache the item under</param>
		/// <returns>Nothing, it's set-only</returns>
		/// <remarks>Any existing value is overwritten</remarks>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the key argument is null
		/// </exception>
		/// <exception cref="System.ArgumentOutOfRangeException">
		/// Thrown if the slidingExpiration argument is equal to or less than TimeSpan.Zero or beyond the range allowed by the cache
		/// </exception>
		object this[string key, TimeSpan slidingExpiration, CachePriority priority = CachePriority.Default] { set; }

		/// <summary>
		/// Removes the item with the specified key if it exists, does nothing otherwise
		/// </summary>
		/// <param name="key">The key of the item to remove</param>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the key argument is null
		/// </exception>
		void Remove(string key);

		/// <summary>
		/// Removes all items from the cache
		/// </summary>
		void Clear();
		
		/// <summary>
		/// The percentage of memory consumed by the cache
		/// </summary>
		long EffectivePercentagePhysicalMemoryLimit { get; }

		/// <summary>
		/// The amount of bytes the cache is constrained to
		/// </summary>
		long EffectivePrivateBytesLimit { get; }
	}
}
