﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

// ReSharper disable once CheckNamespace
namespace System.Reflection.Extensions
{
	/// <summary>
	/// Provides methods for filling, mutating, and cloning objects
	/// </summary>
	public static class TypeFiller
	{
		#region Inner Classes and Delegates

		private class ObjectEqualityComparer : IEqualityComparer<object>
		{
			bool IEqualityComparer<object>.Equals(object x, object y)
			{
				if (ReferenceEquals(x, y))
					return true;

				if( ReferenceEquals(x, null) || ReferenceEquals(y, null ) )
					return false;

				return x.Equals(y);
			}

			public int GetHashCode(object obj)
			{
				return obj.GetHashCode();
			}
		}

		/// <summary>
		/// Delegate for converting member names to one or more string keys
		/// </summary>
		/// <param name="member">The member to generate keys for</param>
		/// <returns>An enumeration of string keys representing the member</returns>
		public delegate IEnumerable<string> MemberKeyGenerator(MemberInfo member);

		/// <summary>
		/// Delegate for mapping a custom parser to convert raw string values prior to assigning
		/// them via the given member assignment
		/// </summary>
		/// <param name="destintionMemberAssignment">The member assignment to obtain the custom parser for</param>
		/// <returns>
		/// A custom parser for the given member assignment or null to use the default parser
		/// </returns>
		public delegate IParser<object> ParserMapper(IMemberAssignment destintionMemberAssignment);

		/// <summary>
		/// Predicate used to determine whether a given member assignment will be executed
		/// </summary>
		/// <param name="destinationMemberAssignment">The member assignment to potentially execute</param>
		/// <returns>
		/// True to execute the assignment, false to ignore it
		/// </returns>
		public delegate bool DestinationMemberAssignPredicate(IMemberAssignment destinationMemberAssignment);

		/// <summary>
		/// Predicate used to determine whether a given member assignment will be executed
		/// </summary>
		/// <param name="sourceMember">The source member from which an assignment value is extracted</param>
		/// <param name="sourceValue">The value to assign to a destination member</param>
		/// <param name="destinationMemberAssignment">The member assignment to be executed</param>
		/// <returns>
		/// A successful status containing the value to assign, a failed status
		/// to not perform the assignment
		/// </returns>
		public delegate IStatus<object> SourceDestinationMemberAssignPredicate(
			MemberInfo sourceMember, object sourceValue,
			IMemberAssignment destinationMemberAssignment
		);

		#endregion Inner Classes and Delegates

		#region Clone

		/// <summary>
		/// Creates a shallow copy of an item
		/// </summary>
		/// <summary>
		/// Creates an instance of the destination type with field and property values set from the source item
		/// </summary>
		/// <param name="sourceObject">The item to clone</param>
		/// <param name="includeNonPublicMembers">Whether or not non-public members are set in the clone</param>
		/// <param name="assignmentFilter">Optional filter for ignoring certain member assignments</param>
		/// <param name="useExistingCloneMethodForICloneable">
		/// When true, source objects of type ICloneable will use their default clone behavior
		/// </param>
		/// <returns>An shallow clone of the source item</returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the sourceObject parameter is null
		/// </exception>
		/// <exception cref="System.ArgumentException">
		/// Thrown if the type is lacks a parameterless constructor
		/// </exception>
		/// <exception cref="System.InvalidCastException">
		/// Thrown if the type is ICloneable but did not return a value which is assignable to the source item type
		/// </exception>
		/// <remarks>
		/// If sourceItem implements ICloneable then the includeNonPublicFields and cloneFilter parameters have no effect
		/// if useExistingCloneMethodForICloneable is set to true
		/// </remarks>
		public static object Clone(
			this object sourceObject, bool includeNonPublicMembers = false,
			SourceDestinationMemberAssignPredicate assignmentFilter = null,
			bool useExistingCloneMethodForICloneable = true
		)
		{
			if (sourceObject == null)
				throw new ArgumentNullException("sourceObject");

			var sourceType = sourceObject.GetType();

			var clonableSourceObject = sourceObject as ICloneable;

			if (clonableSourceObject != null && useExistingCloneMethodForICloneable)
			{
				var clone = clonableSourceObject.Clone();

				var destinationType = clone.GetType();

				if (!destinationType.IsAssignableFrom(sourceType))
				{
					throw new InvalidCastException(
						string.Format(
							"Could not cast type {0} to {1} when cloning",
							sourceType.FullName,
							destinationType.FullName
						)
					);
				}

				return clone;
			}

			var ctor = sourceType.GetConstructor(BindingFlags.Instance | BindingFlags.Public, null, new Type[0], null);

			if (ctor == null)
			{
				throw new ArgumentException(
					string.Format(
						"Cannot clone type {0} because it does not have a public parameterless constructor",
						sourceType.FullName
					)
				);
			}

			assignmentFilter = assignmentFilter ?? DefaultAssignMemberFilter;

			var destinationObject = ctor.Invoke(new object[0]);

			var sourceMemberAssignmentsLookup = MemberAssignment.GetLookupFromType(
				sourceObject.GetType(), includeNonPublicMembers
			);

			var filteredMemberAssignmentsAndValues = GetMemberAssignments(
				sourceMemberAssignmentsLookup, sourceMemberAssignmentsLookup,
				assignmentFilter, sourceObject
			);

			FillInternal(destinationObject, filteredMemberAssignmentsAndValues, false);

			return destinationObject;
		}

		/// <summary>
		/// Creates a shallow copy of an item
		/// </summary>
		/// <summary>
		/// Creates an instance of the destination type with field and property values set from the source item
		/// </summary>
		/// <typeparam name="T">The type of item to clone</typeparam>
		/// <param name="sourceObject">The item to clone</param>
		/// <param name="includeNonPublicMembers">Whether or not non-public members are set in the clone</param>
		/// <param name="assignmentFilter">Optional filter for ignoring certain member assignments</param>
		/// <param name="useExistingCloneMethodForICloneable">
		/// When true, source objects of type ICloneable will use their default clone behavior
		/// </param>
		/// <returns>An shallow clone of the source item</returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the sourceObject parameter is null
		/// </exception>
		/// <exception cref="System.ArgumentException">
		/// Thrown if the type is lacks a parameterless constructor
		/// </exception>
		/// <exception cref="System.InvalidCastException">
		/// Thrown if the type is ICloneable but did not return a value which is assignable to the source item type
		/// </exception>
		/// <remarks>
		/// If sourceItem implements ICloneable then the includeNonPublicFields and cloneFilter parameters have no effect
		/// if useExistingCloneMethodForICloneable is true
		/// </remarks>
		public static T Clone<T>(
			this T sourceObject, bool includeNonPublicMembers = false,
			SourceDestinationMemberAssignPredicate assignmentFilter = null,
			bool useExistingCloneMethodForICloneable = true
		) where T : class, new()
		{
			return (T)Clone((object)sourceObject, includeNonPublicMembers, assignmentFilter, useExistingCloneMethodForICloneable);
		}

		#endregion Clone

		#region Mutate

		/// <summary>
		/// Creates an instance of the destination type with field and property values set from the source item
		/// </summary>
		/// <param name="sourceObject">The item to copy values from</param>
		/// <param name="destinationType">The type of the object to create</param>
		/// <param name="includeNonPublicMembers">Whether or not non-public members are set on the target</param>
		/// <param name="validateValues">Whether or not data annotation attributes are respected on assignment</param>
		/// <param name="assignmentFilter">Optional filter for ignoring certain member assignments</param>
		/// <returns>An object of destination type with values pulled from source item</returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the sourceObject or destinationType parameters are null
		/// </exception>
		/// <exception cref="System.ArgumentException">
		/// Thrown if the destination type is abstract or lacks a parameterless constructor
		/// </exception>
		/// <remarks>
		/// Interfaces are supported, in which case the includeNonPublicFields argument has no effect
		/// </remarks>
		public static object Mutate(
			this object sourceObject, Type destinationType,
			bool includeNonPublicMembers = false,
			bool validateValues = false,
			SourceDestinationMemberAssignPredicate assignmentFilter = null
		)
		{
			if (sourceObject == null)
				throw new ArgumentNullException("sourceObject");

			if (destinationType == null)
				throw new ArgumentNullException("destinationType");

			assignmentFilter = assignmentFilter ?? DefaultAssignMemberFilter;

			object destinationInstance;

			ConstructorInfo ctor;

			if (destinationType.IsInterface)
			{
				destinationInstance = InterfaceImplementor.Instantiate(destinationType);
			}
			else if (destinationType.IsAbstract)
			{
				throw new ArgumentException(
					string.Format("Cannot mutate to type {0} because it is an abstract type", destinationType.FullName),
					"destinationType"
				);
			}
			else if ((ctor = destinationType.GetConstructor(BindingFlags.Public | BindingFlags.Instance, null, Type.EmptyTypes, null)) == null)
			{
				throw new ArgumentException(
					string.Format("Cannot instantiate type {0} because it is lacks a public parameterless constructor",
						destinationType.FullName),
					"destinationType"
				);
			}
			else
			{
				destinationInstance = ctor.Invoke(new object[0]);
			}

			var sourceMemberAssignmentsLookup = MemberAssignment.GetLookupFromType(
				sourceObject.GetType(), includeNonPublicMembers
			);

			var destinationMemberAssignmentsLookup = MemberAssignment.GetLookupFromType(
				destinationType, includeNonPublicMembers
			);

			var filteredMemberAssignmentsAndValues = GetMemberAssignments(
				sourceMemberAssignmentsLookup, destinationMemberAssignmentsLookup,
				assignmentFilter, sourceObject
			);

			FillInternal(destinationInstance, filteredMemberAssignmentsAndValues, validateValues);

			return destinationInstance;
		}

		/// <summary>
		/// Creates an instance of the destination type with field and property values set from the source item
		/// </summary>
		/// <typeparam name="T">The type of the object to create</typeparam>
		/// <param name="sourceObject">The item to copy values from</param>
		/// <param name="includeNonPublicMembers">Whether or not non-public members are set on the target</param>
		/// <param name="validateValues">Whether or not data annotation attributes are respected on assignment</param>
		/// <param name="assignmentFilter">Optional filter for ignoring certain member assignments</param>
		/// <returns>An object of destination type with values pulled from source item</returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the sourceObject parameter is null
		/// </exception>
		/// <exception cref="System.ArgumentException">
		/// Thrown if the destination type is abstract or lacks a parameterless constructor
		/// </exception>
		/// <remarks>
		/// Interfaces are supported, in which case the includeNonPublicFields argument has no effect
		/// </remarks>
		public static T Mutate<T>(
			this object sourceObject,
			bool includeNonPublicMembers = false,
			bool validateValues = false,
			SourceDestinationMemberAssignPredicate assignmentFilter = null
		) where T : class, new()
		{
			return (T)Mutate(sourceObject, typeof(T), includeNonPublicMembers, validateValues, assignmentFilter);
		}

		#endregion Mutate

		#region Fill

		/// <summary>
		/// Fills a target item with field and property values set from a source item
		/// </summary>
		/// <param name="sourceObject">The item to copy values from</param>
		/// <param name="destinationObject">The item to copy values to</param>
		/// <param name="fillNonPublicMembers">Whether or not non-public members are set on the target</param>
		/// <param name="assignmentFilter">Optional filter for ignoring certain member assignments</param>
		/// <returns>An object of destination type with values pulled from source item</returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the sourceObject or destinationObject parameters are null
		/// </exception>
		public static void Fill(
			this object sourceObject, object destinationObject,
			bool fillNonPublicMembers = false,
			SourceDestinationMemberAssignPredicate assignmentFilter = null
		)
		{
			if( sourceObject == null )
				throw new ArgumentNullException("sourceObject");

			if( destinationObject == null )
				throw new ArgumentNullException("destinationObject");

			assignmentFilter = assignmentFilter ?? DefaultAssignMemberFilter;

			var sourceMemberAssignmentsLookup = MemberAssignment.GetLookupFromType(
				sourceObject.GetType(), true
			);

			var destinationMemberAssignmentsLookup = MemberAssignment.GetLookupFromType(
				destinationObject.GetType(), fillNonPublicMembers
			);

			var filteredMemberAssignmentsAndValues = GetMemberAssignments(
				sourceMemberAssignmentsLookup, destinationMemberAssignmentsLookup,
				assignmentFilter, sourceObject
			);

			FillInternal(destinationObject, filteredMemberAssignmentsAndValues, false);
		}

		/// <summary>
		/// Fills a target item with parsed raw values from a source dictionary
		/// </summary>
		/// <param name="destinationObject">The item to copy values to</param>
		/// <param name="sourceRawValues">The dictionary to copy and parse raw values from</param>
		/// <param name="fillNonPublicMembers">Whether or not non-public members are set on the target</param>
		/// <param name="validateValues">Whether or not data annotation attributes are respected on assignment</param>
		/// <param name="memberKeyGenerator">Assigns a member to one or more keys in the sourceRawValues dictionary</param>
		/// <param name="assignmentFilter">Optional filter for ignoring certain member assignments</param>
		/// <param name="parserMapper">Optional delegate to map assign custom parsers of raw values for certain members</param>
		/// <param name="exposedAsType">The type to get validators from if not the same as the instance's type</param>
		/// <returns>An object of destination type with values pulled from source item</returns>
		/// <remarks>
		/// If the destination property is an array, all elements in a value within sourceRawValues will
		/// attempt to be assigned. If the destination property is not an array, only the first value in the
		/// array is used.
		/// </remarks>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the sourceObject or destinationObject parameters are null
		/// </exception>
		public static void FillRaw(
			this object destinationObject,
			Dictionary<string, string[]> sourceRawValues,
			bool fillNonPublicMembers = false,
			bool validateValues = false,
			MemberKeyGenerator memberKeyGenerator = null,
			DestinationMemberAssignPredicate assignmentFilter = null,
			ParserMapper parserMapper = null,
			Type exposedAsType = null
		)
		{
			if( destinationObject == null )
				throw new ArgumentNullException("destinationObject");

			if( sourceRawValues == null )
				throw new ArgumentNullException("sourceRawValues");

			parserMapper = parserMapper ?? DefaultParserMapper;

			var memberAssignmentsLookup = MemberAssignment.GetLookupFromType(
				destinationObject.GetType(), fillNonPublicMembers, memberKeyGenerator, assignmentFilter, exposedAsType
			);

			var groupedMemberAssignments = GroupedMemberAssignment.Group(memberAssignmentsLookup);

			var convertedValues = new List<Tuple<MemberAssignment, object>>();

			foreach (var grouped in groupedMemberAssignments)
			{
				var raws = (
					from srv in sourceRawValues
					join k in grouped.Keys
						on srv.Key equals k
					select srv.Value
				).Distinct().ToArray();
				// todo I can't run a distinct here - it needs to be a sequence equals

				if (raws.Length > 1)
					throw new AmbiguousAssignmentException(grouped.Assignment);

				var rawArray = raws.FirstOrDefault();

				if (grouped.Assignment.IsRequired && validateValues)
				{
					var reqEx = new RequiredAssignmentMissingException(grouped.Assignment);

					if (rawArray == null || ! rawArray.Any())
						throw reqEx;

					if ((grouped.Assignment.IsEnumerable && rawArray.Any(string.IsNullOrWhiteSpace)) || string.IsNullOrWhiteSpace(rawArray.FirstOrDefault()))
					{
						throw reqEx;
					}
				}

				if (rawArray == null)
					continue;

				var parser = parserMapper(grouped.Assignment) ?? ParserFactory.Create(grouped.Assignment.ParserType).Data;
				
				if (parser == null)
				{
					if (grouped.Assignment.IsRequired && validateValues)
						throw new RequiredAssignmentMissingException(grouped.Assignment);

					continue;
				}

				try
				{
					var isNullable = grouped.Assignment.WrapperType.HasFlag(TypeExtensions.WrapperType.Nullable);
					Func<string, object> nullablize = r => string.IsNullOrWhiteSpace(r) ? null : parser.Parse(r);

					object convertedValue;

					if (grouped.Assignment.IsEnumerable)
					{
						convertedValue = CreateArrayOfValues(
							grouped.Assignment,
							rawArray.Select(isNullable ? nullablize : parser.Parse).ToArray()
						);
					}
					else
					{
						convertedValue = isNullable ? nullablize(rawArray.First()) : parser.Parse(rawArray.First());
					}

					convertedValues.Add(new Tuple<MemberAssignment, object>(grouped.Assignment, convertedValue));
				}
				catch (FormatException)
				{
					throw new FormatException(
						string.Format(
							"Could not parse value {0} into type {1} on field {2} in type {3}",
							string.Join(",", rawArray), grouped.Assignment.ParserType,
							grouped.Assignment.MemberInfo.Name,

							// ReSharper disable once PossibleNullReferenceException
							grouped.Assignment.MemberInfo.DeclaringType.FullName
						)
					);
				}
			}

			FillInternal(destinationObject, convertedValues, validateValues);
		}

		/// <summary>
		/// Fills a target item with parsed raw values from a source dictionary
		/// </summary>
		/// <param name="destinationObject">The item to copy values to</param>
		/// <param name="sourceValues">The dictionary to copy values from</param>
		/// <param name="fillNonPublicMembers">Whether or not non-public members are set on the target</param>
		/// <param name="validateValues">Whether or not data annotation attributes are respected on assignment</param>
		/// <param name="memberKeyGenerator">Assigns a member to one or more keys in the sourceRawValues dictionary</param>
		/// <param name="assignmentFilter">Optional filter for ignoring certain member assignments</param>
		/// <param name="exposedAsType">The type to get validators from if not the same as the instance's type</param>
		/// <returns>An object of destination type with values pulled from source item</returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the sourceObject or destinationObject parameters are null
		/// </exception>
		public static void Fill(
			this object destinationObject,
			Dictionary<string, object> sourceValues,
			bool fillNonPublicMembers = false,
			bool validateValues = false,
			MemberKeyGenerator memberKeyGenerator = null,
			DestinationMemberAssignPredicate assignmentFilter = null,
			Type exposedAsType = null
		)
		{
			if (destinationObject == null)
				throw new ArgumentNullException("destinationObject");

			if (sourceValues == null)
				throw new ArgumentNullException("sourceValues");

			var memberAssignmentsLookup = MemberAssignment.GetLookupFromType(
				destinationObject.GetType(), fillNonPublicMembers, memberKeyGenerator, assignmentFilter
			);

			var groupedMembers = GroupedMemberAssignment.Group(memberAssignmentsLookup);

			var convertedValues = new List<Tuple<MemberAssignment, object>>();

			var eqCompare = new ObjectEqualityComparer();

			foreach (var grouped in groupedMembers)
			{
				var converteds = (
					from rv in sourceValues
					join k in grouped.Keys
						on rv.Key equals k
					select rv.Value
				)
				.Distinct(eqCompare)
				.ToArray();

				if (converteds.Length > 1)
					throw new AmbiguousAssignmentException(grouped.Assignment);

				if (!converteds.Any())
				{
					if( validateValues && grouped.Assignment.IsRequired)
						throw new RequiredAssignmentMissingException(grouped.Assignment);

					continue;
				}
					
				convertedValues.Add(
					new Tuple<MemberAssignment, object>(grouped.Assignment, converteds.First())
				);
			}

			FillInternal(destinationObject, convertedValues, validateValues);
		}

		private static void FillInternal(
			object destinationObject,
			IEnumerable<Tuple<MemberAssignment, object>> membersAndValues,
			bool validateValues
		)
		{
			foreach (var mv in membersAndValues)
			{
				// validate
				if (validateValues)
				{
					foreach (var validator in mv.Item1.Validators)
					{
						if (! validator.IsValid(mv.Item2))
						{
							throw new ValidationException(
								string.Format(
									"Value {0} failed validator {1} for field {2} on type {3}",
									mv.Item2, validator, mv.Item1.MemberInfo.Name,
									// ReSharper disable once PossibleNullReferenceException
									mv.Item1.MemberInfo.DeclaringType.FullName
								), validator, mv.Item2
							);
						}
					}
				}

				mv.Item1.SetValue(destinationObject, mv.Item2);
			}
		}

		private static Status<object> DefaultAssignMemberFilter(
			MemberInfo sourceMember, object sourceValue,
			IMemberAssignment destinationMemberAssigmnent
		)
		{
			return Status.Succeed.WithData(sourceValue);
		}

		private static IParser<object> DefaultParserMapper(IMemberAssignment destinationMemberAssignment)
		{
			return null;
		}

		private static Array CreateArrayOfValues(IMemberAssignment memberAssignment, object[] values)
		{
			var arrayElementType = memberAssignment.ParserType;

			if (memberAssignment.WrapperType.HasFlag(TypeExtensions.WrapperType.Nullable))
				arrayElementType = typeof (Nullable<>).MakeGenericType(arrayElementType);

			var array = Array.CreateInstance(arrayElementType, values.Length);

			for (var i = 0; i < array.Length; i++)
			{
				array.SetValue(values[i], i);
			}

			return array;
		}

		#endregion Fill

		#region Utilities

		private static IEnumerable<Tuple<MemberAssignment, object>> GetMemberAssignments(
			IEnumerable<KeyValuePair<string, MemberAssignment>> sourceMemberAssignments,
			IEnumerable<KeyValuePair<string, MemberAssignment>> destinationMemberAssignments,
			SourceDestinationMemberAssignPredicate assignmentFilter,
			object sourceObject
		)
		{

			var memberAssignments = (
				from sma in sourceMemberAssignments
				join dma in destinationMemberAssignments
					on sma.Key equals dma.Key
				select new {Source = sma.Value, Destination = dma.Value}
			);

			var memberAssignmentsAndValues = new List<Tuple<MemberAssignment, object>>();

			foreach (var memberAssignment in memberAssignments)
			{
				object sourceValue;

				if (!memberAssignment.Source.GetValue(sourceObject, out sourceValue))
					continue;

				var assignmentStatus = assignmentFilter(
					memberAssignment.Source.MemberInfo, sourceValue,
					memberAssignment.Destination
				);

				if (! assignmentStatus.IsSuccess )
					continue;

				memberAssignmentsAndValues.Add(
					new Tuple<MemberAssignment, object>(memberAssignment.Destination, assignmentStatus.Data)
				);
			}

			return memberAssignmentsAndValues;
		}

		#endregion Utilities
	}
}