﻿// ReSharper disable once CheckNamespace
namespace System.Reflection.Extensions
{
	/// <summary>
	/// Thrown when attempting to assign a null or empty value to
	/// a property or field marked with the Required data annotation
	/// </summary>
	public class RequiredAssignmentMissingException : Exception
	{
		private readonly string _message;

		/// <summary>
		/// Creates a new RequiredAssignmentMissingException
		/// </summary>
		/// <param name="assignment">The assignment which failed></param>
		public RequiredAssignmentMissingException(IMemberAssignment assignment)
		{
			if (assignment == null)
				throw new ArgumentNullException("assignment");

			if (assignment.MemberInfo == null)
				throw new ArgumentException("Assignment cannot have a null MemberInfo property", "assignment");

			Assignment = assignment;

			var typeName = assignment.MemberInfo.DeclaringType != null
				? assignment.MemberInfo.DeclaringType.FullName
				: "Unknown type";

			_message = string.Format("Assignment to required member {0} on type {1} failed", assignment.MemberInfo.Name, typeName);
		}

		/// <summary>
		/// The assignment which failed
		/// </summary>
		public IMemberAssignment Assignment { get; private set; }

		/// <summary>
		/// Gets a message that describes the current exception
		/// </summary>
		public override string Message { get { return _message; } }
	}

	/// <summary>
	/// Thrown when multiple assignments exist for a member and
	/// those assignments contain differing values
	/// </summary>
	public class AmbiguousAssignmentException : Exception
	{
		private readonly string _message;

		/// <summary>
		/// Creates a new AmbiguousAssignmentException
		/// </summary>
		/// <param name="assignment">The assignment which failed</param>
		public AmbiguousAssignmentException(IMemberAssignment assignment)
		{
			if( assignment == null )
				throw new ArgumentNullException("assignment");

			if( assignment.MemberInfo == null )
				throw new ArgumentException("Assignment cannot have a null MemberInfo property", "assignment");

			Assignment = assignment;

			var typeName = assignment.MemberInfo.DeclaringType != null
				? assignment.MemberInfo.DeclaringType.FullName
				: "Unknown type";

			_message = string.Format(
				"Assignment to member {0} on type {1} failed failed because multiple differing values were mapped to it",
				assignment.MemberInfo.Name, typeName
			);
		}

		/// <summary>
		/// The assignment which failed
		/// </summary>
		public IMemberAssignment Assignment { get; private set; }

		/// <summary>
		/// Gets a message that describes the current exception
		/// </summary>
		public override string Message { get { return _message; } }
	}
}