﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection.Emit;
using System.Security.Cryptography;
using System.Text;
using PXL.Core.String;

// ReSharper disable once CheckNamespace
namespace System.Reflection.Extensions
{
	/// <summary>
	/// Creates concrete versions of interfaces for mocking, configuration reading, etc.
	/// </summary>
	/// <remarks>
	/// <para>Only properties are functional. Methods will all throw a NotImplementedException</para>
	/// <para>Generic methods using type parameters defined on the actual method are not supported</para>
	/// <para>Events are not supported yet</para>
	/// <para>
	///		Dynamic dlls are created in the directory of the executing assembly. Changes to the interface signature
	///		will require manual clearing of compiled dynamic dlls.
	/// </para>
	/// </remarks>

	// todo support generic methods, auto-detect non-compliance with interface and clear/rebuild DLL

	public static class InterfaceImplementor
	{
		#region Inner Classes

		private class EventEqualityComparer : IEqualityComparer<EventInfo>
		{
			public bool Equals(EventInfo x, EventInfo y)
			{
				if (ReferenceEquals(x, null))
					return ReferenceEquals(y, null);

				if (ReferenceEquals(y, null))
					return false;

				if( x.Name != y.Name )
					return false;

				if (x.EventHandlerType == y.EventHandlerType)
					return true;

				string xdtn = null;
				string ydtn = null;

				if (x.DeclaringType != null)
					xdtn = x.DeclaringType.Name;

				if (y.DeclaringType != null)
					ydtn = y.DeclaringType.Name;

				throw new InvalidCastException(
					string.Format(
						"Events {0} {1}.{2} and {3} {4}.{5} have same name but different delegate types",
						x.EventHandlerType.Name, xdtn, x.Name,
						y.EventHandlerType.Name, ydtn, y.Name
					)
				);
			}

			public int GetHashCode(EventInfo obj)
			{
				return obj.Name.GetHashCode();
			}
		}

		private class MethodEqualityComparer : IEqualityComparer<MethodInfo>
		{
			public bool Equals(MethodInfo x, MethodInfo y)
			{
				if (ReferenceEquals(x, null))
					return ReferenceEquals(y, null);

				if (ReferenceEquals(y, null))
					return false;

				if (x.Name != y.Name)
					return false;

				var xParameters = x.GetParameters().Select(p => p.ParameterType);
				var yParameters = y.GetParameters().Select(p => p.ParameterType);

				if (!xParameters.SequenceEqual(yParameters))
					return false;

				if (!x.GetGenericArguments().SequenceEqual(y.GetGenericArguments()))
					return false;

				if (x.ReturnType == y.ReturnType)
					return true;

				string xdtn = null;
				string ydtn = null;

				if (x.DeclaringType != null)
					xdtn = x.DeclaringType.Name;

				if (y.DeclaringType != null)
					ydtn = y.DeclaringType.Name;

				throw new InvalidCastException(
					string.Format(
						"Methods {0} {1}.{2} and {3} {4}.{5} have same name and parameters but different return types",
						x.ReturnType.Name, xdtn, x.Name,
						y.ReturnType.Name, ydtn, y.Name
					)
				);
			}

			public int GetHashCode(MethodInfo obj)
			{
				return obj.Name.GetHashCode();
			}
		}

		private class PropertyEqualityComparer : IEqualityComparer<PropertyInfo>
		{
			public bool Equals(PropertyInfo x, PropertyInfo y)
			{
				if (ReferenceEquals(x, null))
					return ReferenceEquals(y, null);

				if (ReferenceEquals(y, null))
					return false;

				if (x.Name != y.Name)
					return false;

				var xParameters = x.GetIndexParameters().Select(p => p.ParameterType);
				var yParameters = y.GetIndexParameters().Select(p => p.ParameterType);

				if (!xParameters.SequenceEqual(yParameters))
					return false;

				if (x.PropertyType == y.PropertyType)
					return true;

				throw new InvalidCastException(
					string.Format(
						"Properties {0} {1} and {2} {3} have same name but different types",
						x.PropertyType.Name, x.Name,
						y.PropertyType.Name, y.Name
					)
				);
			}

			public int GetHashCode(PropertyInfo obj)
			{
				return obj.Name.GetHashCode();
			}
		}

		#endregion Inner Classes

		#region Fields

		private const string AssemblyFileNamePrefix = "DynamicImplementations-";

		private static readonly Dictionary<Guid, Type> _cachedImplementations =
			new Dictionary<Guid, Type>();

		#endregion Fields

		#region Public Methods

		/// <summary>
		/// Removes all cached dynamic types and their assembly files
		/// </summary>
		public static void ClearCache()
		{
			_cachedImplementations.Clear();

			var selfDirectory = new DirectoryInfo(PathExtensions.GetExecutingBaseDirectory());

			if (! selfDirectory.Exists)
				return;

			var filesToDelete = selfDirectory.GetFiles(AssemblyFileNamePrefix + "*.dll");

			foreach (var fileToDelete in filesToDelete)
			{
				fileToDelete.Delete();
			}
		}

		/// <summary>
		/// Implements a basic version of the given interface
		/// </summary>
		/// <param name="interfaceTypes">The interfaces to implement</param>
		/// <returns>
		/// A basic implementation of the interface, with functional properties and
		/// methods which throw NotImplementedException
		/// </returns>
		/// <exception cref="System.ArgumentNullException">Thrown if the interfaceTypes parameter is null</exception>
		/// <exception cref="System.ArgumentException">
		/// <para>Thrown if the interfaceTypes parameter contains no items</para>
		/// <para>Thrown if the interfaceTypes parameter contains any non-interface type</para>
		/// </exception>
		public static Type Implement(params Type[] interfaceTypes)
		{
			if (interfaceTypes == null)
				throw new ArgumentNullException("interfaceTypes");

			if (! interfaceTypes.Any())
				throw new ArgumentException("At least one interface must be defined", "interfaceTypes");

			if( ! interfaceTypes.All( t => t.IsInterface ))
				throw new ArgumentException("Type must be an interface", "interfaceTypes");

			var dynamicType = GetOrCreateImplementationFor(interfaceTypes);

			return dynamicType;
		}

		/// <summary>
		/// Implements a basic version of the given interface
		/// </summary>
		/// <typeparam name="T">The interface to implement</typeparam>
		/// <returns>
		/// A basic implementation of the interface, with functional properties and
		/// methods which throw NotImplementedException
		/// </returns>
		/// <exception cref="System.ArgumentNullException">Thrown if the interfaceType parameter is null</exception>
		/// <exception cref="System.ArgumentException">Thrown if the interfaceType parameter is not an interface type</exception>
		public static Type Implement<T>() where T : class
		{
			return Implement(typeof (T));
		}

		/// <summary>
		/// Creates an empty instance of the given interface
		/// </summary>
		/// <param name="interfaceTypes">The interfaces to implement</param>
		/// <returns>
		/// An instance basic implementation of the interface, with functional properties and
		/// methods which throw NotImplementedException
		/// </returns>
		/// <exception cref="System.ArgumentNullException">Thrown if the interfaceTypes parameter is null</exception>
		/// <exception cref="System.ArgumentException">
		/// <para>Thrown if the interfaceTypes parameter contains no items</para>
		/// <para>Thrown if the interfaceTypes parameter contains any non-interface type</para>
		/// </exception>
		public static object Instantiate(params Type[] interfaceTypes)
		{
			var dynamicType = Implement(interfaceTypes);

			var instance = Activator.CreateInstance(dynamicType, new object[0]);

			return instance;
		}

		/// <summary>
		/// Creates an empty instance of the given interface
		/// </summary>
		/// <typeparam name="T">The interface to implement</typeparam>
		/// <returns>
		/// An instance basic implementation of the interface, with functional properties and
		/// methods which throw NotImplementedException
		/// </returns>
		/// <exception cref="System.ArgumentNullException">Thrown if the interfaceType parameter is null</exception>
		/// <exception cref="System.ArgumentException">Thrown if the interfaceType parameter is not an interface type</exception>
		public static T Instantiate<T>() where T : class
		{
			return (T) Instantiate(typeof(T));
		}

		#endregion Public Methods

		#region Builders

		private static void DefineProperties(TypeBuilder typeBuilder, Type[] interfaceTypes)
		{
			const BindingFlags flags = BindingFlags.Instance | BindingFlags.Public;

			var pec = new PropertyEqualityComparer();

			var distinctPropertyNamesAndTypes = interfaceTypes
				.SelectMany(it => it.GetProperties(flags))
				.Distinct(pec)
				.Select(
					p => new
					{
						p.PropertyType,
						p.Name,
						IndexParameterTypes = p.GetIndexParameters().Select(v => v.ParameterType).ToArray()
					}
				);

			foreach (var propertyNameAndType in distinctPropertyNamesAndTypes)
			{
				var canRead = interfaceTypes.Any(
					it =>
					{
						var p = it.GetProperty(propertyNameAndType.Name, flags);
						return p != null && p.CanRead;
					}
				);

				var canWrite = interfaceTypes.Any(
					it =>
					{
						var p = it.GetProperty(propertyNameAndType.Name, flags);
						return p != null && p.CanWrite;
					}
				);

				// create a backing field
				var bfn = propertyNameAndType.Name.SplitCamelOrPascal().CamelCombine();
				var backingField = typeBuilder.DefineField("_" + bfn, propertyNameAndType.PropertyType, FieldAttributes.Private);

				// create a getter, only public if the interface property has a getter
				var getterMethod = typeBuilder.DefineMethod(
					"get_" + propertyNameAndType.Name,
					MethodAttributes.SpecialName | MethodAttributes.HideBySig | MethodAttributes.Virtual | (canRead ? MethodAttributes.Public : MethodAttributes.Private),
					propertyNameAndType.PropertyType,
					propertyNameAndType.IndexParameterTypes
				);

				var getterGenerator = getterMethod.GetILGenerator();

				getterGenerator.Emit(OpCodes.Ldarg_0); // this
				getterGenerator.Emit(OpCodes.Ldfld, backingField);
				getterGenerator.Emit(OpCodes.Ret);

				// create a setter, only public if the interface property has a setter
				var setterMethod = typeBuilder.DefineMethod(
					"set_" + propertyNameAndType.Name,
					MethodAttributes.SpecialName | MethodAttributes.HideBySig | MethodAttributes.Virtual | (canWrite ? MethodAttributes.Public : MethodAttributes.Private),
					typeof(void),
					propertyNameAndType.IndexParameterTypes.Concat(new[] { propertyNameAndType.PropertyType }).ToArray()
				);

				var setterGenerator = setterMethod.GetILGenerator();
				
				// value is the last argument
				var valueArgIndex = propertyNameAndType.IndexParameterTypes.Length + 1;

				setterGenerator.Emit(OpCodes.Ldarg_0); // this
				setterGenerator.Emit(OpCodes.Ldarg, valueArgIndex); // value
				setterGenerator.Emit(OpCodes.Stfld, backingField);
				setterGenerator.Emit(OpCodes.Ret);

				var newProperty = typeBuilder.DefineProperty(
					propertyNameAndType.Name,
					PropertyAttributes.HasDefault,
					CallingConventions.HasThis,
					propertyNameAndType.PropertyType,
					new Type[0]
				);

				newProperty.SetGetMethod(getterMethod);
				newProperty.SetSetMethod(setterMethod);
			}
		}

		private static void DefineMethods(Guid key, TypeBuilder typeBuilder, IEnumerable<Type> interfaceTypes)
		{
			var mec = new MethodEqualityComparer();

			var distinctMethods = interfaceTypes
				.SelectMany(it => it.GetMethods(BindingFlags.Instance | BindingFlags.Public))
				.Distinct(mec);

			foreach (var methodInfo in distinctMethods)
			{
				if (methodInfo.IsSpecialName)
					continue;

				// ToString will be handled manually
				if (methodInfo.Name == "ToString" && ! methodInfo.IsGenericMethod && ! methodInfo.GetParameters().Any())
					continue;

				// create a getter, only public if the interface property has a getter
				var stubMethod = typeBuilder.DefineMethod(
					methodInfo.Name,
					MethodAttributes.SpecialName | MethodAttributes.HideBySig | MethodAttributes.Virtual | MethodAttributes.Public,
					methodInfo.ReturnType,
					methodInfo.GetParameters().Select(p => p.ParameterType).ToArray()
				);

				if (methodInfo.IsGenericMethod)
				{
					var gParams = methodInfo.GetGenericArguments();

					var tgParams = stubMethod.DefineGenericParameters(gParams.Select(gp => gp.Name).ToArray());

					for (var i = 0; i < gParams.Length; i++)
					{
						tgParams[i].SetGenericParameterAttributes(gParams[i].GenericParameterAttributes);

						var constraints = gParams[i].GetGenericParameterConstraints();

						tgParams[i].SetInterfaceConstraints(constraints.Where(c => c.IsInterface).ToArray());

						var baseTypeConstraint = constraints.FirstOrDefault(c => ! c.IsInterface);

						if (baseTypeConstraint != null)
							tgParams[i].SetBaseTypeConstraint(baseTypeConstraint);
					}
				}		

				var exCtor = typeof (NotImplementedException).GetConstructor(new[] {typeof (string)});

				var stubGenerator = stubMethod.GetILGenerator();

				stubGenerator.Emit(OpCodes.Ldarg_0); // this

				stubGenerator.Emit(
					OpCodes.Ldstr,
					string.Format("Dynamic implementation does not contain body for {0}", methodInfo.Name)
				);

				// ReSharper disable once AssignNullToNotNullAttribute
				stubGenerator.Emit(OpCodes.Newobj, exCtor);

				stubGenerator.Emit(OpCodes.Throw);
			}

			var toStringMethod = typeBuilder.DefineMethod(
				"ToString",
				MethodAttributes.Public | MethodAttributes.Virtual | MethodAttributes.ReuseSlot,
				typeof(string),
				Type.EmptyTypes
			);

			var toStringGenerator = toStringMethod.GetILGenerator();

			toStringGenerator.Emit(OpCodes.Ldstr, string.Format("Dynamic implementation {0}", key));
			toStringGenerator.Emit(OpCodes.Ret);
		}

		private static void DefineEvents(TypeBuilder typeBuilder, IEnumerable<Type> interfaceTypes)
		{
			const MethodAttributes eventMethodFlags =
				MethodAttributes.SpecialName | MethodAttributes.HideBySig | MethodAttributes.Virtual | MethodAttributes.NewSlot |
				MethodAttributes.Public | MethodAttributes.Final;

			var eec = new EventEqualityComparer();

			var distinctEvents = interfaceTypes
				.SelectMany(it => it.GetEvents(BindingFlags.Instance | BindingFlags.Public))
				.Distinct(eec);

			foreach (var eventInfo in distinctEvents)
			{
				// create a getter, only public if the interface property has a getter
				var stubEvent = typeBuilder.DefineEvent(
					eventInfo.Name, EventAttributes.None, eventInfo.EventHandlerType
				);

				var addMethod = typeBuilder.DefineMethod(
					"add_" + eventInfo.Name,
					eventMethodFlags,
					typeof (void), new[] {eventInfo.EventHandlerType}
				);

				var addMethodGenerator = addMethod.GetILGenerator();

				addMethodGenerator.Emit(OpCodes.Ret);

				stubEvent.SetAddOnMethod(addMethod);

				var removeMethod = typeBuilder.DefineMethod(
					"remove_" + eventInfo.Name,
					eventMethodFlags,
					typeof(void), new[] { eventInfo.EventHandlerType }
				);

				var removeMethodGenerator = removeMethod.GetILGenerator();

				removeMethodGenerator.Emit(OpCodes.Ret);

				stubEvent.SetRemoveOnMethod(removeMethod);
			}
		}

		#endregion Builders

		#region Loaders

		private static Type GetOrCreateImplementationFor(Type[] interfaceTypes)
		{
			var key = GetUIDByInterfaces(interfaceTypes);

			Type implementedType;

			if (_cachedImplementations.TryGetValue(key, out implementedType))
				return implementedType;

			// try to load the assembly
			var assemblyFileName = AssemblyFileNamePrefix + key + ".dll";

			var assemblyPath = PathExtensions.GetExecutingBaseDirectory() + "\\" + assemblyFileName;

			if (File.Exists(assemblyPath))
			{
				var assemblyBytes = File.ReadAllBytes(assemblyPath);

				var assembly = Assembly.Load(assemblyBytes);

				// if there is a problem loading the assembly, then the code probably
				// changed and should be blown away
				try
				{
					return assembly.GetTypes().First(
						t => interfaceTypes.All(i => i.IsAssignableFrom(t))
					);
				}
				catch (ReflectionTypeLoadException)
				{
					File.Delete(assemblyPath);
				}
			}

			var assemblyName =
				"System.Reflection.DynamicallyGeneratedInterfaceImplementations." +
			    key.ToString().Replace("-", "");

			var assemblyBuilder = AppDomain.CurrentDomain.DefineDynamicAssembly(
				new AssemblyName(assemblyName), AssemblyBuilderAccess.RunAndSave
			);

			var moduleBuilder = assemblyBuilder.DefineDynamicModule(assemblyName, assemblyFileName);

			var typeBuilder = moduleBuilder.DefineType(
				key + "DynamicImplementation",
				TypeAttributes.Class,
				typeof(object),
				interfaceTypes
			);

			DefineProperties(typeBuilder, interfaceTypes);

			DefineMethods(key, typeBuilder, interfaceTypes);

			DefineEvents(typeBuilder, interfaceTypes);

			implementedType = typeBuilder.CreateType();

			assemblyBuilder.Save(assemblyFileName);

			_cachedImplementations[key] = implementedType;

			return implementedType;
		}

		#endregion Loaders

		#region Utilities

		// ReSharper disable once InconsistentNaming
		private static Guid GetUIDByInterfaces(IEnumerable<Type> interfaces)
		{
			var fullNames = interfaces
				.Select(i => i.FullName)
				.OrderBy(fn => fn);

			var clearBytes = Encoding.UTF8.GetBytes(
				string.Join(",", fullNames)
			);

			var hashBytes = new SHA256Managed().ComputeHash(clearBytes);

			return new Guid(hashBytes.Take(16).ToArray());
		}

		#endregion Utilities
	}
}