﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection.Emit;

// ReSharper disable once CheckNamespace
namespace System.Reflection.Extensions
{
	/// <summary>
	/// Class used to create wrapped instances of types or interfaces
	/// which can have propertie, method, and event watchers
	/// </summary>
    public static class TypeWrapper
    {
        #region Private Classes

	    private class ProxyTypeCreateException : Exception
	    {
	    }

	    private class BuilderHelper
	    {
			static BuilderHelper()
		    {
				// general
				ParameterInfoKeyValuePairCtor = typeof(KeyValuePair<ParameterInfo, object>).GetConstructors().First();
				GuidParseMethod = typeof(Guid).GetMethod("Parse");

				// properties
			    ActivatorPropertyLookupField = typeof (Activator).GetField("PropertyLookup");
				
			    DictionaryPropertyGetInfoMethod = typeof (Dictionary<Guid, PropertyInfo>).GetMethod("get_Item");
				PropertyInfoGetGetterMethod = typeof (PropertyInfo).GetMethod("GetGetMethod", new Type[0]);
				PropertyInfoGetSetterMethod = typeof (PropertyInfo).GetMethod("GetSetMethod", new Type[0]);
	
				PAEventArgsCtor = typeof(PropertyAccessEventArgs).GetConstructors().First();
				PAEventHandlerInvoke = typeof(EventHandler<PropertyAccessEventArgs>).GetMethod("Invoke");

				// methods
				ActivatorMethodLookupField = typeof(Activator).GetField("MethodLookup");

				DictionaryMethodGetInfoMethod = typeof(Dictionary<Guid, MemberInfo>).GetMethod("get_Item");
				MethodInfoGetParameters = typeof(MethodInfo).GetMethod("GetParameters");

				MCEventArgsCtor = typeof(MethodCallEventArgs).GetConstructors().First();
				MCEventHandlerInvoke = typeof(EventHandler<MethodCallEventArgs>).GetMethod("Invoke");
		    }

			// ReSharper disable InconsistentNaming

		    public FieldInfo WrapperField;
		    public FieldInfo ActivatorField;
		    public FieldInfo PGRaiseField;
		    public FieldInfo PSRaiseField;
		    public FieldInfo MCRaiseField;

		    // general
			public static readonly ConstructorInfo ParameterInfoKeyValuePairCtor;
			public static readonly MethodInfo GuidParseMethod;

			// properties
			public static readonly FieldInfo ActivatorPropertyLookupField;
			
			public static readonly MethodInfo DictionaryPropertyGetInfoMethod;
		    public static readonly MethodInfo PropertyInfoGetGetterMethod;
		    public static readonly MethodInfo PropertyInfoGetSetterMethod;

		    public static readonly ConstructorInfo PAEventArgsCtor;
		    public static readonly MethodInfo PAEventHandlerInvoke;

			// methods
		    public static readonly FieldInfo ActivatorMethodLookupField;
			
			public static readonly MethodInfo DictionaryMethodGetInfoMethod;
			public static readonly MethodInfo MethodInfoGetParameters;

			public static readonly ConstructorInfo MCEventArgsCtor;
		    public static readonly MethodInfo MCEventHandlerInvoke;

			// ReSharper restore InconsistentNaming
	    }

		/// <summary>
		/// Helper class for activating wrapped objets
		/// </summary>
		/// <remarks>
		/// Currently this class has to be public
		/// </remarks>
	    public class Activator
	    {
			/// <summary>
			/// Looks up a property by unique identifier
			/// </summary>
		    public Dictionary<Guid, PropertyInfo> PropertyLookup;

			/// <summary>
			/// Looks up a method by unique identifier
			/// </summary>
			public Dictionary<Guid, MethodInfo> MethodLookup;

			/// <summary>
			/// Looks up an event by unique identifier
			/// </summary>
		    public Dictionary<Guid, EventInfo> EventLookup;

			/// <summary>
			/// The constructor for a proxy type
			/// </summary>
		    public ConstructorInfo ProxyTypeConstructor;

			/// <summary>
			/// The proxy type the activator makes
			/// </summary>
		    public Type ProxyType;
	    }

        private class Wrapping<T> : IWrappedType<T>
        {
	        private readonly Activator _activator;

	        public Wrapping(Type[] interfaces, Activator activator)
	        {
		        _activator = activator;
				Interfaces = interfaces;
	        }

            public object Wrap(T instance)
            {
	            if (ReferenceEquals(instance, null))
		            throw new ArgumentNullException("instance");

				var pgDelegate = new EventHandler<PropertyAccessEventArgs>(RaisePropertyGet);
				var psDelegate = new EventHandler<PropertyAccessEventArgs>(RaisePropertySet);
				var mcDelegate = new EventHandler<MethodCallEventArgs>(RaiseMethodCall);

	            return _activator.ProxyTypeConstructor.Invoke(
					new object[]
					{
						instance,
						_activator,
						pgDelegate,
						psDelegate,
						mcDelegate
					}
				);
            }

	        public Type ContainedType
	        {
		        get { return typeof(T); }
	        }

            public Type[] Interfaces { get; private set; }

			private void RaisePropertyGet(object sender, PropertyAccessEventArgs e)
	        {
		        if (OnPropertyGet != null)
			        OnPropertyGet(sender, e);
	        }

			private void RaisePropertySet(object sender, PropertyAccessEventArgs e)
			{
				if (OnPropertySet != null)
					OnPropertySet(sender, e);
			}

			private void RaiseMethodCall(object sender, MethodCallEventArgs e)
			{
				if (OnMethodCall != null)
					OnMethodCall(sender, e);
			}

			public event EventHandler<PropertyAccessEventArgs> OnPropertyGet;
			public event EventHandler<PropertyAccessEventArgs> OnPropertySet;
			public event EventHandler<MethodCallEventArgs> OnMethodCall;
		}

        #endregion Private Classes

        #region Fields

        private static readonly Dictionary<Guid, Activator> _cachedActivators =
			new Dictionary<Guid, Activator>();

        #endregion Fields

        // todo dictionary

        #region Public Methods

		/// <summary>
		/// Creates a wrapper for a given type
		/// </summary>
		/// <typeparam name="T">The type to wrap</typeparam>
		/// <param name="interfaces">The interface(s) the wrapper will implement / expose</param>
		/// <returns>A wrapper meeting the given parameters</returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the interfaces parameter is null
		/// </exception>
		/// <exception cref="System.ArgumentException">
		/// Thrown if no interfaces are specified
		/// </exception>
        public static IWrappedType<T> CreateWrapper<T>(params Type[] interfaces)
        {
            return (IWrappedType<T>) CreateWrapper(typeof(T), interfaces);
        }

        private static object CreateWrapper(Type containedType, params Type[] interfaces)
        {
	        if (containedType == null)
		        throw new ArgumentNullException("containedType");

	        if (interfaces == null)
		        throw new ArgumentNullException("interfaces");

	        if (! interfaces.Any())
		        throw new ArgumentException("At least one interface must be specified", "interfaces");

	        var typeKeys = new[] {containedType}.Concat(interfaces);

	        var lookup = typeKeys.ToUID();

	        Activator activator;

	        if (! _cachedActivators.TryGetValue(lookup, out activator))
	        {
		        _cachedActivators[
			        LoadOrCreateImplementation(containedType, interfaces, out activator)
			    ] = activator;
	        }

	        var t = typeof (Wrapping<>).MakeGenericType(new[] {containedType});
	        
			var ctor = t.GetConstructor(
				new[] { typeof (Type[]), typeof (Activator) }
			);

	        if (ctor == null)
		        throw new Exception("todo jdk"); // todo jdk

	        return ctor.Invoke(new object[] {interfaces, activator});
        }

        #endregion Public Methods

        #region Builders

		private static BuilderHelper DefineWrapping(TypeBuilder typeBuilder, Type wrappedType)
		{
			var result = new BuilderHelper
			{
				WrapperField = typeBuilder.DefineField(
					"_wrapped", wrappedType, FieldAttributes.InitOnly | FieldAttributes.Private
				),

				ActivatorField = typeBuilder.DefineField(
					"_activator", typeof (Activator), FieldAttributes.InitOnly | FieldAttributes.Private
				),

				PGRaiseField = typeBuilder.DefineField(
					"_pgRaise", typeof (EventHandler<PropertyAccessEventArgs>), FieldAttributes.InitOnly | FieldAttributes.Private
				),

				PSRaiseField = typeBuilder.DefineField(
					"_psRaise", typeof (EventHandler<PropertyAccessEventArgs>), FieldAttributes.InitOnly | FieldAttributes.Private
				),

				MCRaiseField = typeBuilder.DefineField(
					"_mcRaise", typeof (EventHandler<MethodCallEventArgs>), FieldAttributes.InitOnly | FieldAttributes.Private
				)
			};

			var ctor = typeBuilder.DefineConstructor(
                MethodAttributes.Private, CallingConventions.Standard,
				new[]
				{
					wrappedType,
					typeof(Activator),
					typeof(EventHandler<PropertyAccessEventArgs>),
					typeof(EventHandler<PropertyAccessEventArgs>),
					typeof(EventHandler<MethodCallEventArgs>)
				}
            );

            var ctorGenerator = ctor.GetILGenerator();

            ctorGenerator.Emit(OpCodes.Ldarg_0); // this
			// ReSharper disable once AssignNullToNotNullAttribute
	        ctorGenerator.Emit(OpCodes.Call, typeof (object).GetConstructor(new Type[0])); // call base constructor

			ctorGenerator.Emit(OpCodes.Ldarg_0); // this
			ctorGenerator.Emit(OpCodes.Ldarg_1); // wrapped type
            ctorGenerator.Emit(OpCodes.Stfld, result.WrapperField);
			
			ctorGenerator.Emit(OpCodes.Ldarg_0); // this
	        ctorGenerator.Emit(OpCodes.Ldarg_2); // activator
			ctorGenerator.Emit(OpCodes.Stfld, result.ActivatorField);
			
			ctorGenerator.Emit(OpCodes.Ldarg_0); // this
			ctorGenerator.Emit(OpCodes.Ldarg_3); // _pgRaise
			ctorGenerator.Emit(OpCodes.Stfld, result.PGRaiseField);
			
			ctorGenerator.Emit(OpCodes.Ldarg_0); // this
			ctorGenerator.Emit(OpCodes.Ldarg, 4); // _psRaise
			ctorGenerator.Emit(OpCodes.Stfld, result.PSRaiseField);
			
			ctorGenerator.Emit(OpCodes.Ldarg_0); // this
			ctorGenerator.Emit(OpCodes.Ldarg, 5); // _mcRaise
			ctorGenerator.Emit(OpCodes.Stfld, result.MCRaiseField);
            
			ctorGenerator.Emit(OpCodes.Ret);

			return result;
        }

        private static void DefineProperties(
			TypeBuilder typeBuilder,
			Dictionary<Guid, PropertyInfo> intersectProperties,
			BuilderHelper builderHelper
		)
        {
            foreach (var kvp in intersectProperties)
            {
	            var key = kvp.Key;
	            var property = kvp.Value;

                var indexParameterTypes = property.GetIndexParameters().Select(p => p.ParameterType).ToArray();

                var newProperty = typeBuilder.DefineProperty(
                    property.Name, PropertyAttributes.None, CallingConventions.HasThis,
                    property.PropertyType,
					indexParameterTypes
                );

				var canRead = property.CanRead && property.GetGetMethod(false) != null;

				if (canRead)
                {
                    var getterMethod = typeBuilder.DefineMethod(
                        "get_" + property.Name,
                        MethodAttributes.SpecialName | MethodAttributes.HideBySig | MethodAttributes.Virtual | MethodAttributes.Public,
                        newProperty.PropertyType,
						indexParameterTypes
                    );

	                var getterGenerator = getterMethod.GetILGenerator(3); // todo maxstack

					GeneratePropertyOrMethodBody(
						getterGenerator, builderHelper, property,
						key, false, indexParameterTypes, property.PropertyType
					);

                    newProperty.SetGetMethod(getterMethod);
                }

				var canWrite = property.CanWrite && property.GetSetMethod(false) != null;

	            if (! canWrite)
					continue;

	            var setIndexParameterTypes = indexParameterTypes.Concat(new[] { property.PropertyType }).ToArray();

	            var setterMethod = typeBuilder.DefineMethod(
		            "set_" + property.Name,
		            MethodAttributes.SpecialName | MethodAttributes.HideBySig | MethodAttributes.Virtual | MethodAttributes.Public,
		            typeof (void),
		            setIndexParameterTypes
				);

	            var setterGenerator = setterMethod.GetILGenerator(); // todo maxstack

	            GeneratePropertyOrMethodBody(
		            setterGenerator, builderHelper, property,
		            key, true, setIndexParameterTypes, property.PropertyType
		            );

	            newProperty.SetSetMethod(setterMethod);
            }
        }

		private static void DefineMethods(
			TypeBuilder typeBuilder, Dictionary<Guid, MethodInfo> intersectMethods, BuilderHelper builderHelper
		)
		{
			foreach (var kvp in intersectMethods)
			{
				var key = kvp.Key;
				var method = kvp.Value;

				var parameterTypes = method.GetParameters().Select(p => p.ParameterType).ToArray();

				var newMethod = typeBuilder.DefineMethod(
					method.Name,
					MethodAttributes.Virtual | MethodAttributes.Public,
					method.ReturnType,
					parameterTypes
				);

				if (method.IsGenericMethod)
				{
					var gParams = method.GetGenericArguments();

					var tgParams = newMethod.DefineGenericParameters(gParams.Select(gp => gp.Name).ToArray());

					for (var i = 0; i < gParams.Length; i++)
					{
						tgParams[i].SetGenericParameterAttributes(gParams[i].GenericParameterAttributes);

						var constraints = gParams[i].GetGenericParameterConstraints();

						tgParams[i].SetInterfaceConstraints(constraints.Where(c => c.IsInterface).ToArray());

						var baseTypeConstraint = constraints.FirstOrDefault(c => !c.IsInterface);

						if (baseTypeConstraint != null)
							tgParams[i].SetBaseTypeConstraint(baseTypeConstraint);
					}
				}

				var methodGenerator = newMethod.GetILGenerator();

				GeneratePropertyOrMethodBody(
					methodGenerator, builderHelper, method,
					key, false, parameterTypes, method.ReturnType
				);
			}
		}

        private static void DefineMethods(TypeBuilder typeBuilder, Type interfaceType)
        {
            foreach (var methodInfo in interfaceType.GetMethods(BindingFlags.Instance | BindingFlags.Public))
            {
                // ToString will be handled manually
                if (methodInfo.Name == "ToString" && ! methodInfo.IsGenericMethod && ! methodInfo.GetParameters().Any())
                    continue;

                // create a getter, only public if the interface property has a getter
                var stubMethod = typeBuilder.DefineMethod(
                    methodInfo.Name,
                    MethodAttributes.SpecialName | MethodAttributes.HideBySig | MethodAttributes.Virtual | MethodAttributes.Public,
                    methodInfo.ReturnType,
                    methodInfo.GetParameters().Select(p => p.ParameterType).ToArray()
                );

                var exCtor = typeof (NotImplementedException).GetConstructor(new[] {typeof (string)});

                var stubGenerator = stubMethod.GetILGenerator();

                stubGenerator.Emit(OpCodes.Ldarg_0); // this

                stubGenerator.Emit(
                    OpCodes.Ldstr,
                    string.Format("Dynamic implementation of {0} does not contain body for {1}", interfaceType.FullName, methodInfo.Name)
                );

                // ReSharper disable once AssignNullToNotNullAttribute
                stubGenerator.Emit(OpCodes.Newobj, exCtor);

                stubGenerator.Emit(OpCodes.Throw);
            }

            var toStringMethod = typeBuilder.DefineMethod(
                "ToString",
                MethodAttributes.Public | MethodAttributes.Virtual | MethodAttributes.ReuseSlot,
                typeof(string),
                Type.EmptyTypes
            );

            var toStringGenerator = toStringMethod.GetILGenerator();

            toStringGenerator.Emit(OpCodes.Ldstr, string.Format("Dynamic implementation of {0}", interfaceType.FullName));
            toStringGenerator.Emit(OpCodes.Ret);
        }

        #endregion Builders

        #region Loaders

		private static Guid LoadOrCreateImplementation(Type wrappedType, Type[] interfaceTypes, out Activator activator)
		{
			var result = new[] {wrappedType}.Concat(interfaceTypes).ToUID();
			var typeName = "Wrapper_" + result.ToString().Replace("-", "");

			Dictionary<Guid, PropertyInfo> intersectProperties;
			Dictionary<Guid, MethodInfo> intersectMethods;
			Dictionary<Guid, EventInfo> intersectEvents;

			ExtractMembers(wrappedType, interfaceTypes, out intersectProperties, out intersectMethods, out intersectEvents);

			activator = new Activator
			{
				PropertyLookup = intersectProperties,
				MethodLookup = intersectMethods,
				EventLookup = intersectEvents
			};

            // try to load the assembly
			var assemblyFileName = "DynamicImplementations-" + typeName + ".dll";

			var assemblyPath = PathExtensions.GetExecutingBaseDirectory() + "\\" + assemblyFileName;

			if (File.Exists(assemblyPath))
			{
				var assemblyBytes = File.ReadAllBytes(assemblyPath);

				var assembly = Assembly.Load(assemblyBytes);

				// if there is a problem loading the assembly, then the code probably
				// changed and should be blown away
				try
				{
					var proxyType = assembly.GetTypes().FirstOrDefault(t => t.Name == typeName);

					if (proxyType == null)
						throw new ProxyTypeCreateException();

					if (! proxyType.GetInterfaces().All(interfaceTypes.Contains))
						throw new ProxyTypeCreateException();

					activator.ProxyType = proxyType;
				}
				catch (ProxyTypeCreateException)
				{
					File.Delete(assemblyPath);
				}
				catch (ReflectionTypeLoadException)
				{
					File.Delete(assemblyPath);
				}
			}
			
			if( activator.ProxyType == null )
			{
				var assemblyName = "System.Reflection.DynamicallyGeneratedTypeWrapper." + typeName;

				var assemblyBuilder = AppDomain.CurrentDomain.DefineDynamicAssembly(
					new AssemblyName(assemblyName), AssemblyBuilderAccess.RunAndSave
				);

				var moduleBuilder = assemblyBuilder.DefineDynamicModule(assemblyName, assemblyFileName);

				var typeBuilder = moduleBuilder.DefineType(
					typeName,
					TypeAttributes.Class,
					typeof (object),
					interfaceTypes
				);

				var fields = DefineWrapping(typeBuilder, wrappedType);

				DefineProperties(typeBuilder, intersectProperties, fields);
				
				DefineMethods(typeBuilder, intersectMethods, fields);

				//    DefineEvents(typeBuilder, events, wrappedField);



				activator.ProxyType = typeBuilder.CreateType();

				assemblyBuilder.Save(assemblyFileName);
			}

			activator.ProxyTypeConstructor = activator.ProxyType.GetConstructor(
				BindingFlags.Instance | BindingFlags.NonPublic,
				null,
				new[]
				{
					wrappedType,
					typeof(Activator),
					typeof(EventHandler<PropertyAccessEventArgs>),
					typeof(EventHandler<PropertyAccessEventArgs>),
					typeof(EventHandler<MethodCallEventArgs>)
				},
				null
			);

			return result;
        }

        #endregion Loaders

        #region Utilities

	    private static int GetMaxStack(
		    MemberInfo member, bool isPropertySet,
		    Type[] parameterTypes, Type returnOrAssignType
		)
	    {
		    return 0;
	    }

	    private static void GeneratePropertyOrMethodBody(
			ILGenerator generator, BuilderHelper bh,
			MemberInfo member, Guid memberKey,
			bool isPropertySet, IList<Type> parameterTypes, Type returnOrAssignType
		)
	    {
			#region Setup

			var isProperty = member is PropertyInfo;

		    var hasValueLocal =
			    (isProperty && ! isPropertySet) ||
			    (! isProperty && returnOrAssignType != typeof (void));

		    MethodInfo methodToCall;

		    if (isProperty && isPropertySet)
			    methodToCall = ((PropertyInfo) member).GetSetMethod();
		    else if (isProperty)
			    methodToCall = ((PropertyInfo) member).GetGetMethod();
		    else
				methodToCall = (MethodInfo)member;

			#endregion Setup

			// locals are:
			//	0: $member PropertyInfo or MethodInfo
			//	1: $parameters ParameterInfo[]
			//	2: $parameterMappings KeyValuePair<ParameterInfo, object>[]
			//  3: $ea PropertyAccessEventArgs or MethodCallEventArgs
			//	4: $valueOrReturn <returnOrAssignType> optional

			#region Locals

			generator.DeclareLocal(
				isProperty ? typeof (PropertyInfo) : typeof (MethodInfo)
			);

		    generator.DeclareLocal(typeof(ParameterInfo[])); // $parameters
			generator.DeclareLocal(typeof(KeyValuePair<ParameterInfo, object>[])); // $parameterMappings

		    generator.DeclareLocal(
				isProperty ? typeof (PropertyAccessEventArgs) : typeof (MethodCallEventArgs)
			);

		    if (hasValueLocal)
			    generator.DeclareLocal(returnOrAssignType);

			#endregion Locals

			#region Call the Inner Method

			generator.Emit(OpCodes.Ldarg_0); // this

			generator.Emit(OpCodes.Ldfld, bh.WrapperField); // _wrapped

			for (var i = 0; i < parameterTypes.Count; i++)
			{
				generator.Emit(OpCodes.Ldarg, i + 1); // $parameters / $value    
			}

			generator.Emit(OpCodes.Callvirt, methodToCall); // _wrapped.<method>

		    if (hasValueLocal)
			   generator.Emit(OpCodes.Stloc, 4); // $valueOrReturn

			#endregion Call the Inner Method

			#region Look up the Member

			generator.Emit(OpCodes.Ldarg_0); // this
			generator.Emit(OpCodes.Ldfld, bh.ActivatorField); // _activator

			generator.Emit(
				OpCodes.Ldfld,
				isProperty
					? BuilderHelper.ActivatorPropertyLookupField // .PropertyLookup
					: BuilderHelper.ActivatorMethodLookupField
			); 

			generator.Emit(OpCodes.Ldstr, memberKey.ToString());	// "GUID"
			generator.Emit(OpCodes.Call, BuilderHelper.GuidParseMethod); // Guid.Parse

			generator.Emit(
				OpCodes.Callvirt,
				isProperty
					? BuilderHelper.DictionaryPropertyGetInfoMethod // .PropertyLookup[v]
					: BuilderHelper.DictionaryMethodGetInfoMethod // .MethodLookup[v]
			); 

			generator.Emit(OpCodes.Stloc_0); // $member

			#endregion Look up the Member

			#region Load the Parameters

			generator.Emit(OpCodes.Ldloc_0); // $member

		    if (isProperty && isPropertySet)
				generator.Emit(OpCodes.Call, BuilderHelper.PropertyInfoGetSetterMethod); // $member.GetSetterMethod()    
			else if (isProperty)
				generator.Emit(OpCodes.Call, BuilderHelper.PropertyInfoGetGetterMethod); // $member.GetGetterMethod()
			// if it's a method the $member variable is already all set
			
			generator.Emit(OpCodes.Callvirt, BuilderHelper.MethodInfoGetParameters); // $member....GetParameters()
			generator.Emit(OpCodes.Stloc_1); // $parameters

			#endregion Load the Parameters

			#region Fill the Parameter Map

		    var pln = isProperty && isPropertySet
			    ? parameterTypes.Count - 1
			    : parameterTypes.Count;

			generator.Emit(OpCodes.Ldc_I4, pln); // array length
			generator.Emit(OpCodes.Newarr, typeof(KeyValuePair<ParameterInfo, object>)); // $parameterMappings
			generator.Emit(OpCodes.Stloc_2); // $parameterMappings

			for (var i = 0; i < pln; i++)
			{
				generator.Emit(OpCodes.Ldloc_2);		// $parameterMappings
				generator.Emit(OpCodes.Ldc_I4, i); // $i

				generator.Emit(OpCodes.Ldloc_1);		// $parameters
				generator.Emit(OpCodes.Ldc_I4, i); // $i
				generator.Emit(OpCodes.Ldelem, typeof(ParameterInfo)); // $parameterInfo[$i]

				generator.Emit(OpCodes.Ldarg, i + 1);	// indexed arg

				// value types must be boxed
				if (parameterTypes[i].IsValueType)
				{
					generator.Emit(OpCodes.Box, parameterTypes[i].UnderlyingSystemType);
				}

				generator.Emit(OpCodes.Newobj, BuilderHelper.ParameterInfoKeyValuePairCtor); // new KeyValuePair<ParameterInfo, object>
				generator.Emit(OpCodes.Stelem, typeof(KeyValuePair<ParameterInfo, object>));	// $parameterMappings[$i]
			}

			#endregion Fill the Parameter Map

			#region Create the Event Args

			generator.Emit(OpCodes.Ldloc_0); // $member
			generator.Emit(OpCodes.Ldloc_2); // $parameterMappings

		    if (isProperty && isPropertySet)
			{
			    generator.Emit(OpCodes.Ldarg, pln+1); // "$value"

				// value types must be boxed
				if (returnOrAssignType.IsValueType)
				{
					generator.Emit(OpCodes.Box, returnOrAssignType);
				}
			}
			else if (hasValueLocal)
			{
				generator.Emit(OpCodes.Ldloc, 4); // $valueOrReturn

				// value types must be boxed
				if( returnOrAssignType.IsValueType )
				{
					generator.Emit(OpCodes.Box, returnOrAssignType);
				}
			}
			else
			{
				generator.Emit(OpCodes.Ldnull); // null
			}

		    generator.Emit(
				OpCodes.Newobj,
				isProperty
					? BuilderHelper.PAEventArgsCtor // $ea = new PropertyAccessEventArgs($member, $parameterMappings, $valueOrReturn)
					: BuilderHelper.MCEventArgsCtor // $ea = new MethodCallEventArgs($member, $parameterMappings, $valueOrReturn)
			);

			generator.Emit(OpCodes.Stloc_3); // $ea

			#endregion Create the Event Args

			#region Raise the Event

			generator.Emit(OpCodes.Ldarg_0); // this

			if( isProperty && isPropertySet )
				generator.Emit(OpCodes.Ldfld, bh.PSRaiseField); // _psRaise
			else if( isProperty )
				generator.Emit(OpCodes.Ldfld, bh.PGRaiseField); // _pgRaise
			else
				generator.Emit(OpCodes.Ldfld, bh.MCRaiseField); // _mcRaise

			generator.Emit(OpCodes.Ldarg_0); // this
			generator.Emit(OpCodes.Ldfld, bh.WrapperField); // _wrapped

			generator.Emit(OpCodes.Ldloc_3); // $ea

		    generator.Emit(
				OpCodes.Callvirt,
			    isProperty
					? BuilderHelper.PAEventHandlerInvoke // _pgRaise.Invoke($member, $ea) or _psRaise.Invoke($member, $ea)
					: BuilderHelper.MCEventHandlerInvoke // _mcRaise.Invoke($member, $ea)
			);

		    #endregion Raise the Event

		    if (hasValueLocal && returnOrAssignType != typeof (void))
			    generator.Emit(OpCodes.Ldloc, 4); // $valueOrReturn

			generator.Emit(OpCodes.Ret);
	    }

        private static void ExtractMembers(
			Type wrappedType, Type[] interfaces,
            out Dictionary<Guid, PropertyInfo> intersectProperties,
			out Dictionary<Guid, MethodInfo> intersectMethods,
			out Dictionary<Guid, EventInfo> intersectEvents
        )
        {
            const BindingFlags flags = BindingFlags.Instance | BindingFlags.Public;

	        var pic = new PropertyInfoComparer();
	        var mic = new MethodInfoComparer();
	        var eic = new EventInfoComparer();

			var distinctInterfaceProperties = interfaces
				.SelectMany(i => i.GetProperties(flags))
				.Where(m => !m.IsSpecialName)
				.Distinct(pic)
				.Select(m => new { m, k = pic.GetUID(m, true) });

	        var distinctInterfaceMethods = interfaces
		        .SelectMany(i => i.GetMethods(flags))
				.Where(m => ! m.IsSpecialName)
		        .Distinct(mic)
				.Select(m => new { m, k = mic.GetUID(m) });

	        var distinctInterfaceEvents = interfaces
		        .SelectMany(i => i.GetEvents(flags))
				.Where(m => !m.IsSpecialName)
		        .Distinct(eic)
				.Select(m => new { m, k = eic.GetUID(m) });

			var distinctTypeProperties = wrappedType.GetProperties(flags)
				.Where(m => !m.IsSpecialName)
				.Select(m => new { m, k = pic.GetUID(m, true) });

			var distinctTypeMethods = wrappedType.GetMethods(flags)
				.Where(m => !m.IsSpecialName)
				.Select(m => new { m, k = mic.GetUID(m) });

			var distinctTypeEvents = wrappedType.GetEvents(flags)
				.Where(m => !m.IsSpecialName)
				.Select(m => new { m, k = eic.GetUID(m) });

	        intersectProperties = (
		        from dip in distinctInterfaceProperties
		        join dtp in distinctTypeProperties
			        on dip.k equals dtp.k
		        select dtp
		    ).ToDictionary(km => km.k, km => km.m);

			intersectMethods = (
				from dim in distinctInterfaceMethods
				join dtm in distinctTypeMethods
					on dim.k equals dtm.k
				select dtm
			).ToDictionary(km => km.k, km => km.m);

			intersectEvents = (
				from die in distinctInterfaceEvents
				join dte in distinctTypeEvents
					on die.k equals dte.k
				select dte
			).ToDictionary(km => km.k, km => km.m);
		}

		#endregion Utilities
	}
}