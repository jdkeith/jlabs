﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

// ReSharper disable once CheckNamespace

namespace System.Reflection.Extensions
{
	public interface IMemberUIDGenerator<T> where T : MemberInfo
	{
		Guid GetUID(T member);
	}

	public static class TypeUIDGenerator
	{
		public static Guid ToUID(this IEnumerable<Type> types)
		{
			if (types == null)
				throw new ArgumentNullException("types");

			types = types.ToArray();

			if (! types.Any())
				return Guid.Empty;

			var clearBytes = Encoding.UTF8.GetBytes(
				string.Join("", types.Select(t => t.FullName).Distinct().OrderBy(n => n))
			);

			var hashBytes = new SHA256Managed().ComputeHash(clearBytes);

			return new Guid(hashBytes.Take(16).ToArray());
		}
	}

	public class PropertyInfoComparer : IEqualityComparer<PropertyInfo>, IMemberUIDGenerator<PropertyInfo>
	{
		public bool Equals(PropertyInfo x, PropertyInfo y)
		{
			if (ReferenceEquals(x, null))
				return ReferenceEquals(y, null);

			return ! ReferenceEquals(y, null) && ToString(x, false).Equals(ToString(y, false));
		}

		public int GetHashCode(PropertyInfo obj)
		{
			return ToString(obj, false).GetHashCode();
		}

		private static string ToString(PropertyInfo p, bool ignoreReadWrite)
		{
			return string.Format(
				"{0}::{1}{2}->{3}",
				p.Name,
				! ignoreReadWrite && p.CanRead ? 'r' : '\0',
				! ignoreReadWrite && p.CanWrite ? 'w' : '\0',
				p.PropertyType.FullName
			);
		}

		public Guid GetUID(PropertyInfo member)
		{
			return GetUID(member, false);
		}

		public Guid GetUID(PropertyInfo member, bool ignoreReadWrite)
		{
			if (member == null)
				throw new ArgumentNullException("member");

			// todo use SHA256Hash
			var clearBytes = Encoding.UTF8.GetBytes(ToString(member, ignoreReadWrite));
			var hashBytes = new SHA256Managed().ComputeHash(clearBytes);
			return new Guid(hashBytes.Take(16).ToArray());
		}
	}

	public class MethodInfoComparer : IEqualityComparer<MethodInfo>, IMemberUIDGenerator<MethodInfo>
	{
		public bool Equals(MethodInfo x, MethodInfo y)
		{
			if (ReferenceEquals(x, null))
				return ReferenceEquals(y, null);

			return !ReferenceEquals(y, null) && ToString(x).Equals(ToString(y));
		}

		public int GetHashCode(MethodInfo obj)
		{
			return ToString(obj).GetHashCode();
		}

		private static string ToString(MethodInfo m)
		{
			var sb = new StringBuilder();

			sb.Append(m.ReturnType.FullName);
			sb.Append(' ');
			sb.Append(m.Name);

			var generics = m.GetGenericArguments();

			if (generics.Any())
			{
				sb.Append('<');
				sb.Append(string.Join(",", generics.Select(t => t.FullName)));
				sb.Append('>');
			}

			sb.Append('(');

			foreach (var parm in m.GetParameters())
			{
				if (parm.IsOptional)
					sb.Append("opt ");

				if (parm.IsOut)
					sb.Append("out ");

				if (parm.IsRetval)
					sb.Append("ret ");

				sb.Append(parm.ParameterType.FullName);
			}

			sb.Append(')');

			return sb.ToString();
		}

		public Guid GetUID(MethodInfo member)
		{
			if (member == null)
				throw new ArgumentNullException("member");

			// todo use SHA256Hash
			var clearBytes = Encoding.UTF8.GetBytes(ToString(member));
			var hashBytes = new SHA256Managed().ComputeHash(clearBytes);
			return new Guid(hashBytes.Take(16).ToArray());
		}
	}

	public class EventInfoComparer : IEqualityComparer<EventInfo>, IMemberUIDGenerator<EventInfo>
	{
		public bool Equals(EventInfo x, EventInfo y)
		{
			if (ReferenceEquals(x, null))
				return ReferenceEquals(y, null);

			return !ReferenceEquals(y, null) && ToString(x).Equals(ToString(y));
		}

		public int GetHashCode(EventInfo obj)
		{
			return ToString(obj).GetHashCode();
		}

		private static string ToString(EventInfo e)
		{
			return string.Format(
				"{0}<-{1}", e.EventHandlerType.FullName, e.Name
			);
		}

		public Guid GetUID(EventInfo member)
		{
			if (member == null)
				throw new ArgumentNullException("member");

			// todo use SHA256Hash
			var clearBytes = Encoding.UTF8.GetBytes(ToString(member));
			var hashBytes = new SHA256Managed().ComputeHash(clearBytes);
			return new Guid(hashBytes.Take(16).ToArray());
		}
	}
}