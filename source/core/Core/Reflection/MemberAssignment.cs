﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

// ReSharper disable once CheckNamespace
namespace System.Reflection.Extensions
{
	/// <summary>
	/// Represents an assignment of a value into an object's property or field
	/// </summary>
	public interface IMemberAssignment
	{
		/// <summary>
		/// Whether or not the member is marked with a Required data annotation
		/// </summary>
		bool IsRequired { get; }

		/// <summary>
		/// An array of ValidationAttribute data annotations
		/// </summary>
		IEnumerable<ValidationAttribute> Validators { get; }

		/// <summary>
		/// A reference to the property or field
		/// </summary>
		MemberInfo MemberInfo { get; }

		/// <summary>
		/// The type stored in the property or field
		/// </summary>
		Type MemberType { get; }

		/// <summary>
		/// The parser-safe type stored in the property or field
		/// </summary>
		Type ParserType { get; }

		/// <summary>
		/// Whether the type stored in the property or field is an array
		/// </summary>
		TypeExtensions.WrapperType WrapperType { get; }

		/// <summary>
		/// Whether the type stored in the property or field is enumerable
		/// </summary>
		bool IsEnumerable { get; }

		/// <summary>
		/// Assigns a value to an object through the member assignment
		/// </summary>
		/// <param name="invocant">The object to assign the value on</param>
		/// <param name="value">The value to assign</param>
		void SetValue(object invocant, object value);
	}

	internal class GroupedMemberAssignment
	{
		public static IEnumerable<GroupedMemberAssignment> Group(Dictionary<string, MemberAssignment> memberAssignmentsLookup)
		{
			return memberAssignmentsLookup
				.GroupBy(kvp => kvp.Value.MemberInfo)
				.Select(g => new GroupedMemberAssignment
				{
					Assignment = g.Select(kvp => kvp.Value).First(),
					Keys = g.Select(kvp => kvp.Key).ToArray()
				})
			.ToArray();
		}

		private GroupedMemberAssignment()
		{
		}

		public MemberAssignment Assignment { get; private set; }
		public string[] Keys { get; private set; }
	}

	internal class MemberAssignment : IMemberAssignment
	{
		#region Factories

		internal static Dictionary<string, MemberAssignment> GetLookupFromType(
			Type type, bool includeNonPublicMembers,
			TypeFiller.MemberKeyGenerator memberKeyGenerator = null,
			TypeFiller.DestinationMemberAssignPredicate assignmentFilter = null,
			Type exposedAsType = null
		)
		{
			var flags = BindingFlags.Instance | BindingFlags.Public  | BindingFlags.FlattenHierarchy;

			memberKeyGenerator = memberKeyGenerator ?? DefaultMemberKeyGenerator;
			assignmentFilter = assignmentFilter ?? DefaultAssignmentFilter;

			if (includeNonPublicMembers)
				flags |= BindingFlags.NonPublic;

			Func<MemberInfo, MemberAssignment> selectFunc;

			if (exposedAsType == null)
				selectFunc = FromMemberInfo;
			else
				selectFunc = m => FromMemberInfo(m, exposedAsType);

			var fields = type
				.GetFields(flags)
				.Where(f => !f.IsInitOnly)
				.Select(selectFunc)
				.Where(fma => assignmentFilter(fma));

			var properties = type
				.GetProperties(flags)
				.Where(p => p.GetSetMethod(includeNonPublicMembers) != null)
				.Select(selectFunc)
				.Where(pma => assignmentFilter(pma));

			var result = new Dictionary<string, MemberAssignment>();

			// properties win
			foreach (var member in fields.Concat(properties))
			{
				var keys = memberKeyGenerator(member.MemberInfo);

				foreach (var key in keys)
				{
					result[key] = member;	
				}
			}

			return result;
		}

		internal static MemberAssignment FromMemberInfo(MemberInfo member)
		{
			var result = new MemberAssignment
			{
				IsRequired = member.GetCustomAttributes(typeof (RequiredAttribute), true).Any(),
				Validators = (ValidationAttribute[]) member.GetCustomAttributes(typeof (ValidationAttribute), true),
				MemberInfo = member
			};

			switch (member.MemberType)
			{
				case MemberTypes.Field:
					result.MemberType = ((FieldInfo) member).FieldType;
					break;

				case MemberTypes.Property:
					result.MemberType = ((PropertyInfo)member).PropertyType;
					break;

				default:
					throw new NotSupportedException(string.Format("Member type {0} is not supported", member.MemberType));
			}

			TypeExtensions.WrapperType wrapperType;

			result.ParserType = result.MemberType.UnwrapType(out wrapperType);

			result.WrapperType = wrapperType;

			return result;
		}

		internal static MemberAssignment FromMemberInfo(MemberInfo member, Type exposedAsType)
		{
			var result = FromMemberInfo(member);

			var exposedMember = FindMatchingMemberInfo(member, exposedAsType);

			if (exposedMember != null)
			{
				result.IsRequired = exposedMember.GetCustomAttributes(typeof(RequiredAttribute), true).Any();
				result.Validators = (ValidationAttribute[])exposedMember.GetCustomAttributes(typeof(ValidationAttribute), true);
			}
			else
			{
				result.IsRequired = false;
				result.Validators = Enumerable.Empty<ValidationAttribute>();
			}

			return result;
		}

		#endregion Factories

		#region Properties

		public bool IsRequired { get; private set; }
		public IEnumerable<ValidationAttribute> Validators { get; private set; }
		public MemberInfo MemberInfo { get; private set; }
		public Type MemberType { get; private  set; }
		public Type ParserType { get; private set; }
		public TypeExtensions.WrapperType WrapperType { get; private set; }

		public bool IsEnumerable
		{
			get
			{
				return WrapperType.HasFlag(TypeExtensions.WrapperType.Array) ||
				       WrapperType.HasFlag(TypeExtensions.WrapperType.NonArrayEnumerable);
			}
		}

		#endregion Properties

		#region Public Methods

		public bool GetValue(object invocant, out object value)
		{
			if (MemberInfo == null)
				throw new InvalidOperationException("MemberInfo property has not been set yet");

			switch (MemberInfo.MemberType)
			{
				case MemberTypes.Field:
					value = ((FieldInfo)MemberInfo).GetValue(invocant);
					return true;

				case MemberTypes.Property:
					var pi = (PropertyInfo) MemberInfo;
					
					if (! pi.CanRead)
					{
						value = null;
						return false;
					}

					value = pi.GetValue(invocant, new object[0]);
					return true;

				default:
					throw new NotSupportedException(string.Format("Member type {0} is not supported", MemberInfo.MemberType));
			}
		}

		public void SetValue(object invocant, object value)
		{
			if (MemberInfo == null)
				throw new InvalidOperationException("MemberInfo property has not been set yet");

			switch (MemberInfo.MemberType)
			{
				case MemberTypes.Field:
					((FieldInfo) MemberInfo).SetValue(invocant, value);
					break;

				case MemberTypes.Property:
					((PropertyInfo) MemberInfo).SetValue(invocant, value, new object[0]);
					break;

				default:
					throw new NotSupportedException(string.Format("Member type {0} is not supported", MemberInfo.MemberType));
			}
		}

		#endregion Public Methods

		#region Utilities

		private static MemberInfo FindMatchingMemberInfo(MemberInfo sourceMember, Type exposedAsType)
		{
			var candidateMembers = exposedAsType.GetMember(
				sourceMember.Name, sourceMember.MemberType,
				BindingFlags.Instance | BindingFlags.FlattenHierarchy | BindingFlags.Public | BindingFlags.NonPublic
			);

			if (sourceMember.MemberType == MemberTypes.Field)
			{
				var sourceField = (FieldInfo) sourceMember;

				return candidateMembers
					.Where(cm => cm is FieldInfo)
					.Cast<FieldInfo>()
					.FirstOrDefault(cf => cf.FieldType.IsAssignableFrom(sourceField.FieldType));
			}

			if (sourceMember.MemberType != MemberTypes.Property)
				return null;

			var sourceProperty = (PropertyInfo) sourceMember;

			return candidateMembers
				.Where(cm => cm is PropertyInfo)
				.Cast<PropertyInfo>()
				.FirstOrDefault(cp => cp.PropertyType.IsAssignableFrom(sourceProperty.PropertyType));
		}

		private static IEnumerable<string> DefaultMemberKeyGenerator(MemberInfo member)
		{
			return new[] { member.Name };
		}

		private static bool DefaultAssignmentFilter(IMemberAssignment destinationMemberAssignment)
		{
			return true;
		}

		public override string ToString()
		{
			if (MemberInfo == null || MemberInfo.DeclaringType == null)
				return "Unknown assignment";

			return string.Format(
				"{0} on {1}", MemberInfo.Name, MemberInfo.DeclaringType.FullName
			);
		}

		#endregion Utilities
	}
}