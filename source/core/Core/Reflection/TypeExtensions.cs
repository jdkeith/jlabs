﻿using System.Collections.Generic;
using System.Linq;

// ReSharper disable once CheckNamespace
namespace System.Reflection.Extensions
{
	/// <summary>
	/// Various type enhancements
	/// </summary>
	public static class TypeExtensions
	{
		/// <summary>
		/// The type of wrapper around a primitive type
		/// </summary>
		[Flags]
		public enum WrapperType
		{
			/// <summary>
			/// None, the type is either already primitive or is not wrapper
			/// </summary>
			None = 0,

			/// <summary>
			/// The primitive is the type parameter of a nullable
			/// </summary>
			Nullable = 1,

			/// <summary>
			/// The primitive is the element type of an array
			/// </summary>
			Array = 2,

			/// <summary>
			/// The primitive is the element type of a non-array enumerable such as List
			/// </summary>
			NonArrayEnumerable = 4
		}

		/// <summary>
		/// Determines whether a type is a data primitive, that is a type of data which
		/// commonly storable by databases and expressible by JSON
		/// </summary>
		/// <param name="type">The type to check</param>
		/// <returns>
		/// True if the type parameter is one of the following:
		/// <para>String</para>
		/// <para>Character</para>
		/// <para>Built in number type (uints 8-64, ints 8-64, float, double, decimal)</para>
		/// <para>Boolean</para>
		/// <para>Enumeration</para>
		/// <para>DateTime</para>
		/// <para>Guid</para>
		/// <para>Nullable version of any of the above</para>
		/// </returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the type parameter is null
		/// </exception>
		public static bool IsDataPrimitive(this Type type)
		{
			WrapperType wrapper;

			var innerType = type.UnwrapType(out wrapper);

			if (wrapper != WrapperType.None && wrapper != WrapperType.Nullable)
				return false;

			return innerType.IsPrimitive || innerType.IsEnum ||
				   innerType == typeof(string) ||
				   innerType == typeof(decimal) || innerType == typeof(Guid) ||
				   innerType == typeof(DateTime);
		}

		/// <summary>
		/// Extracts the wrapped type from types such as nullables, arrays, or enumerables
		/// </summary>
		/// <param name="type">The type to extract the inner type from</param>
		/// <param name="wrapper">The type(s) of wrapper around the type</param>
		/// <returns>
		/// The inner wrapped type or the type itself if it's not wrapped
		/// </returns>
		public static Type UnwrapType(this Type type, out WrapperType wrapper)
		{
			if (type == null)
				throw new ArgumentNullException("type");

			wrapper = WrapperType.None;

			if (type.IsArray)
			{
				wrapper = wrapper | WrapperType.Array;
				type = type.GetElementType();
			}
			else if (type.IsGenericType && type.GetInterfaces().Any(i => i.IsGenericType && i.GetGenericTypeDefinition() == typeof(IEnumerable<>)))
			{
				wrapper = wrapper | WrapperType.NonArrayEnumerable;
				type = type.GetGenericArguments().First();
			}

			if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>))
			{
				wrapper = wrapper | WrapperType.Nullable;
				type = type.GetGenericArguments().First();
			}

			return type;
		}

		/// <summary>
		/// Checks whether one can create an instance of a type of interface without constructor parameters
		/// </summary>
		/// <typeparam name="T">The type to check creation capabilities for</typeparam>
		/// <returns>
		/// A successful status if the type can be created without parameters,
		/// a failed status containing an exception otherwise
		/// </returns>
		public static Status CanCreateParameterlessInstanceOf<T>()
		{
			return CanCreateParameterlessInstanceOf(typeof(T));
		}

		/// <summary>
		/// Checks whether one can create an instance of a type of interface without constructor parameters
		/// </summary>
		/// <param name="type">The type to check creation capabilities for</param>
		/// <returns>
		/// A successful status if the type can be created without parameters,
		/// a failed status containing an exception otherwise
		/// </returns>
		public static Status CanCreateParameterlessInstanceOf(Type type)
		{
			if (type == null)
				throw new ArgumentNullException("type");

			try
			{
				CheckCanCreateParameterlessType(type);
				return true;
			}
			catch (ArgumentException ex)
			{
				return ex;
			}
		}

		private static ConstructorInfo CheckCanCreateParameterlessType(Type type)
		{
			if (type == null)
				throw new ArgumentNullException("type");

			if (type.IsInterface)
				return null;

			if (type.IsAbstract)
			{
				throw new ArgumentException(
					string.Format(
						"Cannot create instance of type {0} because it is abstract",
						type.FullName
					)
				);
			}

			var ctor = type.GetConstructor(BindingFlags.Public | BindingFlags.Instance, null, new Type[0], null);

			if (ctor == null)
			{
				throw new ArgumentException(
					string.Format(
						"Cannot create instance of type {0} because it doesn't have a parameterless constructor",
						type.FullName
					)
				);
			}

			return ctor;
		}

		/// <summary>
		/// Attempts to create an instance of a type or dynamically create an implementation of an interface
		/// </summary>
		/// <typeparam name="T">The type to create an instance of</typeparam>
		/// <returns>
		/// An status whose data property is instance of the created type
		/// or whose exception property contains the reason why the creation failed
		/// </returns>
		public static IStatus<T> TryCreateInstanceOf<T>()
		{
			var type = typeof (T);

			try
			{
				var instance = (T) InnerTryCreateInstanceOf(type);

				return Status.Succeed.WithData(instance);
			}
			catch (Exception ex)
			{
				return Status.FailWithException<T>(ex);
			}
		}

		/// <summary>
		/// Attempts to create an instance of a type or dynamically create an implementation of an interface
		/// </summary>
		/// <param name="type">The type to create an instance of</param>
		/// <returns>
		/// An status whose data property is instance of the created type
		/// or whose exception property contains the reason why the creation failed
		/// </returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the type parameter is null
		/// </exception>
		public static IStatus<object> TryCreateInstanceOf(Type type)
		{
			if (type == null)
				throw new ArgumentNullException("type");

			try
			{
				return Status.Succeed.WithData(InnerTryCreateInstanceOf(type));
			}
			catch (Exception ex)
			{
				return Status.FailWithException<object>(ex);
			}
		}

		private static object InnerTryCreateInstanceOf(Type type)
		{
			var ctor = CheckCanCreateParameterlessType(type);

			if (ctor != null)
				return ctor.Invoke(new object[0]);

			if (type.IsInterface)
				return InterfaceImplementor.Instantiate(type);

			throw new NotSupportedException(
				"Could not find parameterless constructor or type is abstract"
			);
		}

		/// <summary>
		/// Tests a type to see if it is derived from the given generic definition
		/// </summary>
		/// <example>IsDerivedFromGenericDefinition(typeof(int?), typeof(Nullable&lt;&gt;)</example>
		/// <param name="definition">
		/// The generic definition to check derivision from
		/// </param>
		/// <param name="testType">The type to check</param>
		/// <returns>
		/// True if the test type can be derived from the given generic definition
		/// </returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if either parameter is null
		/// </exception>
		/// <exception cref="System.ArgumentException">
		/// Thrown if the definition parameter is not a generic type definition
		/// </exception>
		
		// todo is this even necessary ?

		public static bool IsDerivedFromGenericDefinition(this Type testType, Type definition)
		{
			if (testType == null)
				throw new ArgumentNullException("testType");

			if (definition == null)
				throw new ArgumentNullException("definition");

			if( definition.IsGenericTypeDefinition == false )
				throw new ArgumentException("Expected a generic definition", "definition");

			while (testType != null && testType != typeof(object))
			{
				if (testType.IsGenericType && definition.IsAssignableFrom(testType.GetGenericTypeDefinition()))
					return true;

				testType = testType.BaseType;
			}

			return false;
		}
	}
}
