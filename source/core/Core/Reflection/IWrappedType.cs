﻿using System.Collections.Generic;

// ReSharper disable once CheckNamespace
namespace System.Reflection.Extensions
{
	public class PropertyAccessEventArgs : EventArgs
	{
		public PropertyAccessEventArgs(
			PropertyInfo propertyInfo,
			KeyValuePair<ParameterInfo,object>[] parameters,
			object assignedOrReturnedValue
		)
		{
			PropertyInfo = propertyInfo;
			Parameters = parameters;
			AssignedOrReturnedValue = assignedOrReturnedValue;
		}

		public PropertyInfo PropertyInfo { get; private set; }
		public KeyValuePair<ParameterInfo, object>[] Parameters;
		public object AssignedOrReturnedValue { get; private set; }
	}

	public class MethodCallEventArgs : EventArgs
	{
		public MethodCallEventArgs(
			MethodInfo methodInfo,
			IEnumerable<KeyValuePair<ParameterInfo, object>> parameters,
			object returnValue
		)
		{
			MethodInfo = methodInfo;
			Parameters = parameters;
			ReturnValue = returnValue;
		}

		public MethodInfo MethodInfo { get; private set; }
		public IEnumerable<KeyValuePair<ParameterInfo, object>> Parameters;
		public object ReturnValue { get; private set; }
	}

	public interface IWrappedType<in T>
	{
		object Wrap(T instance);
		Type ContainedType { get; }
		Type[] Interfaces { get; }

		event EventHandler<PropertyAccessEventArgs> OnPropertyGet;
		event EventHandler<PropertyAccessEventArgs> OnPropertySet;
		event EventHandler<MethodCallEventArgs> OnMethodCall;
		//event EventHandler<EventRaiseEventArgs> OnEventRaise;
	}
}