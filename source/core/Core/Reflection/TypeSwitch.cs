﻿using System.Collections.Generic;
using System.Collections.Generic.Extensions;
using System.Linq;

// ReSharper disable once CheckNamespace
namespace System
{
	/// <summary>
	/// Tests a variable for type and casts it to the type expected
	/// by the first matching when clause, executing the when clause
	/// and optionally returning a value
	/// </summary>
	public class TypeSwitch : TypeSwitch<object>
	{
		/// <summary>
		/// Creates a new TypeSwitch
		/// </summary>
		/// <param name="autoSortTypesBySpecificity">
		/// Whether casts are automatically sorted by specificity
		/// as soon as a GetWhen or DoWhen is added. When false
		/// the first delegate which can handle the type will be called.
		/// When true, the delegate with the most closely matching type will be called.
		/// </param>
		public TypeSwitch(bool autoSortTypesBySpecificity = true)
			: base(autoSortTypesBySpecificity)
		{
		}
	}

	/// <summary>
	/// Tests a variable for type and casts it to the type expected
	/// by the first matching when clause, executing the when clause
	/// and optionally returning a value
	/// </summary>
	/// <typeparam name="TR">
	/// The type that functions are expected to return
	/// </typeparam>
	public class TypeSwitch<TR>
	{
		#region Inner Class

		private class CastActionTypeDepthComparer : IComparer<CastAction>
		{
			public int Compare(CastAction x, CastAction y)
			{
				return GetTypeDepth(x.TestType).CompareTo(GetTypeDepth(y.TestType));
			}

			private static int GetTypeDepth(Type t)
			{
				var result = 0;

				while (t != null && t != typeof(object))
				{
					result++;
					t = t.BaseType;
				}

				return result;
			}
		}

		private class CastAction
	    {
		    public bool HasReturnValue;
		    public Delegate DoOrGetDelegate;
			public Delegate AndThenDo;
		    public Type TestType;

			public T GetAndDo<T>(object value)
			{
				Func<Delegate, int> pln = d => d.Method.GetParameters().Length;

				T final;

				if (HasReturnValue)
				{
					var ln = pln(DoOrGetDelegate);

					final = ln == 1
						? (T) DoOrGetDelegate.DynamicInvoke(value)
						: (T) DoOrGetDelegate.DynamicInvoke();
				}
				else
				{
					final = default(T);
				}

				if (! HasReturnValue || AndThenDo == null)
					return final;

				if (pln(AndThenDo) == 1)
					AndThenDo.DynamicInvoke(final);
				else
					AndThenDo.DynamicInvoke(value, final);

				return final;
			}
	    }

		#endregion Inner Class

		#region Fields

		private readonly bool _autoSortTypesBySpecificity;
		private readonly List<CastAction> _castActions;
		private CastAction _whenNull;
		private CastAction _default;

		#endregion Fields

		#region Constructor

		/// <summary>
		/// Creates a new TypeSwitch
		/// </summary>
		/// <param name="autoSortTypesBySpecificity">
		/// Whether casts are automatically sorted by specificity
		/// as soon as a GetWhen or DoWhen is added. When false
		/// the first delegate which can handle the type will be called.
		/// When true, the delegate with the most closely matching type will be called.
		/// </param>
		public TypeSwitch(bool autoSortTypesBySpecificity = true)
		{
			_autoSortTypesBySpecificity = autoSortTypesBySpecificity;
			_castActions = new List<CastAction>();
	    }

		#endregion Constructor

		#region Clauses

		/// <summary>
		/// Adds a when casting clause
		/// </summary>
		/// <typeparam name="T">The type which the value must be assignable from</typeparam>
		/// <param name="action">The action to execute</param>
		/// <returns>The TypeSwitch object</returns>
		/// <remarks>
		/// <para>Only the first matching clause is executed</para>
		/// <para>Actions, returning void, cause the TypeSwitch to return the default value of type TR</para>
		/// </remarks>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the action parameter is null
		/// </exception>
		/// <exception cref="System.Collections.Generic.Extensions">
		/// Thrown if type T is already bound in another casting clause
		/// </exception>
		public TypeSwitch<TR> DoWhen<T>(Action<T> action)
		{
			AddCastClause(action, null, false, typeof (T));
			return this;
	    }

		/// <summary>
		/// Adds a when casting clause
		/// </summary>
		/// <typeparam name="T">The type which the value must be assignable from</typeparam>
		/// <param name="function">The function to execute</param>
		/// <param name="andThenDo">
		/// Optional action to execute with the result returned from the function parameter
		/// </param>
		/// <returns>The TypeSwitch object</returns>
		/// <remarks>
		/// <para>Only the first matching clause is executed</para>
		/// </remarks>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the function parameter is null
		/// </exception>
		/// <exception cref="System.Collections.Generic.Extensions">
		/// Thrown if type T is already bound in another casting clause
		/// </exception>
		public TypeSwitch<TR> GetWhen<T>(Func<T, TR> function, Action<T,TR> andThenDo = null)
		{
			AddCastClause(function, andThenDo, true, typeof(T));
			return this;
		}

		/// <summary>
		/// Adds a null casting clause
		/// </summary>
		/// <param name="action">The action to execute when the value is null</param>
		/// <returns>The TypeSwitch object</returns>
		/// <remarks>
		/// <para>Only one null clause is allowed</para>
		/// <para>Actions, returning void, cause the TypeSwitch to return the default value of type TR</para>
		/// </remarks>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the action parameter is null
		/// </exception>
		/// <exception cref="System.InvalidOperationException">
		/// Thrown if a null clause is already bound
		/// </exception>
		public TypeSwitch<TR> DoWhenNull(Action action)
		{
			AddSingletonClause(action, null, false, ref _whenNull);
			return this;
	    }

		/// <summary>
		/// Adds a null casting clause
		/// </summary>
		/// <param name="function">The function to execute when the value is null</param>
		/// <param name="andThenDo">
		/// Optional action to execute with the result returned from the function parameter
		/// </param>
		/// <returns>The TypeSwitch object</returns>
		/// <remarks>
		/// <para>Only one null clause is allowed</para>
		/// </remarks>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the function parameter is null
		/// </exception>
		/// <exception cref="System.InvalidOperationException">
		/// Thrown if a null clause is already bound
		/// </exception>
		public TypeSwitch<TR> GetWhenNull(Func<TR> function, Action<TR> andThenDo = null)
	    {
			AddSingletonClause(function, andThenDo, true, ref _whenNull);
			return this;
	    }

		/// <summary>
		/// Adds a null casting clause
		/// </summary>
		/// <param name="defaultValue">The default value to return</param>
		/// <param name="andThenDo">
		/// Optional action to execute with the result returned from the function parameter
		/// </param>
		/// <returns>The TypeSwitch object</returns>
		/// <remarks>
		/// <para>Only one null clause is allowed</para>
		/// </remarks>
		/// <exception cref="System.InvalidOperationException">
		/// Thrown if a null clause is already bound
		/// </exception>
		public TypeSwitch<TR> GetWhenNull(TR defaultValue, Action<TR> andThenDo = null)
		{
			Func<TR> function = () => defaultValue;
				
			AddSingletonClause(function, andThenDo, true, ref _whenNull);
			return this;
		}

		/// <summary>
		/// Adds a catchall non-null clause
		/// </summary>
		/// <param name="action">
		/// The action to execute the value doesn't match any type clauses and is non-null
		/// </param>
		/// <returns>The TypeSwitch object</returns>
		/// <remarks>
		/// <para>Only one catchall non-null clause is allowed</para>
		/// <para>Actions, returning void, cause the TypeSwitch to return the default value of type TR</para>
		/// </remarks>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the action parameter is null
		/// </exception>
		/// <exception cref="System.InvalidOperationException">
		/// Thrown if a catchall non-null clause is already bound
		/// </exception>
		public TypeSwitch<TR> DoWhenDefaultNotNull(Action<object> action)
	    {
			AddSingletonClause(action, null, false, ref _default);
			return this;
	    }

		/// <summary>
		/// Adds a catchall non-null clause
		/// </summary>
		/// <param name="function">
		/// The function to execute the value doesn't match any type clauses and is non-null
		/// </param>
		/// <param name="andThenDo">
		/// Optional action to execute with the result returned from the function parameter
		/// </param>
		/// <returns>The TypeSwitch object</returns>
		/// <remarks>
		/// <para>Only one catchall non-null clause is allowed</para>
		/// </remarks>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the function parameter is null
		/// </exception>
		/// <exception cref="System.InvalidOperationException">
		/// Thrown if a catchall non-null clause is already bound
		/// </exception>
		public TypeSwitch<TR> GetWhenDefaultNotNull(Func<object, TR> function, Action<object, TR> andThenDo = null)
		{
			AddSingletonClause(function, andThenDo, true, ref _default);
			return this;
		}

		#endregion Clauses

		#region Return Value

		/// <summary>
		/// Gets the return value of an input value by executing the appropriate clause
		/// </summary>
		/// <param name="value">The input value to run through the clauses</param>
		/// <returns>
		/// A return value produced by one of the clause functions. If the first matching
		/// clause is an action, the return value will be the default value for type TR
		/// </returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the value parameter is null and there is no bound null clause
		/// </exception>
		public TR Execute(object value)
		{
			CastAction castAction;

		    if (ReferenceEquals(value, null))
		    {
			    if (_whenNull == null)
				    throw new ArgumentNullException("value");

			    castAction = _whenNull;
		    }
		    else
		    {
				var valueType = value.GetType();
			    castAction = _castActions.FirstOrDefault(c => c.TestType.IsAssignableFrom(valueType));
		    }

			if (castAction == null && _default != null)
			{
				castAction = _default;
		    }

			return castAction == null 
				? default(TR)
				: castAction.GetAndDo<TR>(value);
		}

		#endregion Return Value

		#region Utilities

		private void AddCastClause(Delegate doOrGet, Delegate andThenDo, bool hasReturnValue, Type testType)
		{
			if (doOrGet == null)
				throw new ArgumentNullException();

			if( _castActions.Any(c => c.TestType == testType) )
			    throw new DuplicateKeyException(testType);

			_castActions.Add(
			    new CastAction
			    {
				    TestType = testType,
					DoOrGetDelegate = doOrGet,
					AndThenDo = andThenDo,
				    HasReturnValue = hasReturnValue
			    }
			);

			if (! _autoSortTypesBySpecificity)
				return;

			// order the casts by type depth, most specific first
			var catdc = new CastActionTypeDepthComparer();
			_castActions.Sort(catdc);
			_castActions.Reverse();
		}

		private static void AddSingletonClause(
			Delegate doOrGet, Delegate andThenDo, bool hasReturnValue, ref CastAction cast
		)
		{
			if (doOrGet == null)
				throw new ArgumentNullException();

			if (cast != null )
				throw new InvalidOperationException("Clause already exists");

			cast = new CastAction
			{
				DoOrGetDelegate = doOrGet,
				AndThenDo = andThenDo,
				HasReturnValue = hasReturnValue
			};
		}

		#endregion Utilities
	}
}