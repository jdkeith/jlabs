﻿using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

// ReSharper disable once CheckNamespace
namespace System
{
	public class SafeAccessBuilderResult
	{
		internal SafeAccessBuilderResult(Delegate nullSafeFunction, IEnumerable<MemberInfo> nonArgumentMemberCalls)
		{
			NullSafeFunction = nullSafeFunction;
			NonArgumentMemberCalls = nonArgumentMemberCalls;
		}

		public Delegate NullSafeFunction { get; private set; }
		public IEnumerable<MemberInfo> NonArgumentMemberCalls { get; private set; }
	}

	public class SafeAccessBuilderResult<TIn, TOut> : SafeAccessBuilderResult
	{
		internal SafeAccessBuilderResult(
			Func<TIn, TOut> nullSafeFunction, IEnumerable<MemberInfo> nonArgumentMemberCalls
		) : base(nullSafeFunction, nonArgumentMemberCalls)
		{
		}

		public new Func<TIn, TOut> NullSafeFunction
		{
			get { return (Func<TIn, TOut>) base.NullSafeFunction;  }
		}
	}

	/// <summary>
	/// Provides functions for building null-dereference-safe property and method call functions
	/// </summary>
	public static class SafeAccessBuilder
	{
		#region Inner Class

		private class NullSafeExpressionWalker : ExpressionVisitor
		{
			#region Fields

			private readonly ParameterExpression _p;
			private readonly LabelTarget _returnNullTarget;
			private readonly Type _tout;

			private int _varCount;
			private bool _isInMethodCall;

			#endregion Fields

			#region Constructor

			public NullSafeExpressionWalker(Type tout, ParameterExpression p, LabelTarget returnNullTarget)
			{
				_tout = tout;
				_p = p;
				_returnNullTarget = returnNullTarget;

				Variables = new List<ParameterExpression>();
				AccessMembers = new List<MemberInfo>();
			}

			#endregion Constructor

			#region Properties

			public List<ParameterExpression> Variables { get; private set; }
			
			public List<MemberInfo> AccessMembers { get; private set; }

			#endregion Properties	

			#region Visitor Overrides

			protected override Expression VisitParameter(ParameterExpression node)
			{
				return _p;
			}

			protected override Expression VisitMember(MemberExpression node)
			{
				// items called from static, non-parameter contexts, or 
				// as method call arguments are not null checked
				if (node.Expression == null || _isInMethodCall)
					return base.VisitMember(node);

				var t = GetMemberType(node);
				var v = Expression.Variable(t, "v" + _varCount++);
				var va = Expression.Assign(v, base.VisitMember(node));

				Variables.Add(v);
				AccessMembers.Add(node.Member);

				if (t.IsValueType)
					return va;

				var vrc = CheckNull(v);

				return Expression.Block(va, vrc, va);
			}

			protected override Expression VisitMethodCall(MethodCallExpression node)
			{
				var invocant = node.Object == null
					? null
					: base.Visit(node.Object);

				if (_isInMethodCall)
				{
					var result = base.VisitMethodCall(node);
									
					return result;
				}

				_isInMethodCall = true;

				var args = node.Arguments.Select(a => base.Visit(a));
					
				var mc = Expression.Call(invocant, node.Method, args);

				var t = node.Method.ReturnType;
				var v = Expression.Variable(t, "v" + _varCount++);
				var va = Expression.Assign(v, mc);

				AccessMembers.Add(node.Method);
				Variables.Add(v);

				_isInMethodCall = false;

				if (t.IsValueType)
					return va;

				var vrc = CheckNull(v);

				return Expression.Block(va, vrc, va);
			}

			#endregion Visitor Overrides

			#region Utilities

			private static Type GetMemberType(MemberExpression node)
			{
				Type t;

				switch (node.Member.MemberType)
				{
					case MemberTypes.Property:
						t = ((PropertyInfo)node.Member).PropertyType;
						break;

					case MemberTypes.Field:
						t = ((FieldInfo)node.Member).FieldType;
						break;

					case MemberTypes.Method:
						t = ((MethodInfo)node.Member).ReturnType;
						break;

					default:
						throw new NotSupportedException(
							string.Format("Cannot create null reference checker for member of type {0}", node.Member.MemberType)
						);
				}

				return t;
			}

			public Expression CheckNull(ParameterExpression node)
			{
				if (node.Type.IsValueType)
					return Expression.Empty();

				return Expression.IfThen(
					Expression.ReferenceEqual(node, Expression.Constant(null, node.Type)),
					Expression.Return(_returnNullTarget, Expression.Constant(null, _tout), _tout)
				);
			}

			#endregion Utilities
		}

		#endregion Inner Class

		#region Public Functions

		/// <summary>
		/// Creates a null-dereference-safe function for a reference type result
		/// </summary>
		/// <typeparam name="TIn">The type of the input parameter</typeparam>
		/// <typeparam name="TOut">The type of the output parameter</typeparam>
		/// <param name="expression">A property, field, or method call expression</param>
		/// <returns>A result containing the null-dereference-safe function and other properties</returns>
		public static SafeAccessBuilderResult<TIn, TOut> BuildReferenceTypeAccess<TIn, TOut>(
			Expression<Func<TIn, TOut>> expression
		) where TOut : class
		{
			MemberInfo[] accessMembers;

			var lambda = (Expression<Func<TIn, TOut>>)BuildTypeAccessInternal(
				typeof(TIn), typeof(TOut), expression, out accessMembers
			);

			return new SafeAccessBuilderResult<TIn, TOut>(
				lambda.Compile(), accessMembers
			);
		}

		/// <summary>
		/// Creates a null-dereference-safe function for a value type result
		/// </summary>
		/// <typeparam name="TIn">The type of the input parameter</typeparam>
		/// <typeparam name="TOut">The type of the output parameter</typeparam>
		/// <param name="expression">A property, field, or method call expression</param>
		/// <returns>A result containing the null-dereference-safe function and other properties</returns>
		public static SafeAccessBuilderResult<TIn, TOut?> BuildValueTypeAccess<TIn, TOut>(
			Expression<Func<TIn, TOut>> expression
		) where TOut : struct
		{
			MemberInfo[] accessMembers;

			var lambda = (Expression<Func<TIn, TOut?>>)BuildTypeAccessInternal(
				typeof(TIn), typeof(TOut), expression, out accessMembers
			);

			return new SafeAccessBuilderResult<TIn, TOut?>(
				lambda.Compile(), accessMembers
			);
		}

		/// <summary>
		/// Creates a null-dereference-safe function
		/// </summary>
		/// <param name="inputType">The type of the input parameter</param>
		/// <param name="outputType">The type of the output parameter</param>
		/// <param name="expression">A property, field, or method call expression</param>
		/// <returns>A result containing the null-dereference-safe function and other properties</returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if any of the input parameters are null
		/// </exception>
		/// <exception cref="System.ArgumentException">
		/// Thrown if the expression is not a function accepting a
		/// parameter of inputType and returning a value of outputType or if
		/// the input or output types are Void
		/// </exception>
		public static SafeAccessBuilderResult BuildTypeAccess(
			Type inputType, Type outputType, LambdaExpression expression
		)
		{
			if( inputType == null )
				throw new ArgumentNullException("inputType");

			if( outputType == null )
				throw new ArgumentNullException("outputType");

			if( expression == null )
				throw new ArgumentNullException("expression");

			if( inputType == typeof(void) )
				throw new ArgumentException("Input type cannot be of type void", "inputType");

			if (outputType == typeof(void))
				throw new ArgumentException("Output type cannot be of type void", "outputType");

			var funcType = typeof (Func<,>).MakeGenericType(inputType, outputType);
			var lambdaType = typeof (Expression<>).MakeGenericType(funcType);

			if (! expression.GetType().IsAssignableFrom(lambdaType))
			{
				throw new ArgumentException(
					string.Format(
						"Cannot coerce expression of type {0} to expected type Expression<Func<{1},{2}>>",
						expression.GetType().FullName,
						inputType.FullName,
						outputType.FullName
					)
				);
			}

			MemberInfo[] accessMembers;

			var lambda = (LambdaExpression)BuildTypeAccessInternal(
				inputType, outputType, expression, out accessMembers
			);

			return new SafeAccessBuilderResult(lambda.Compile(), accessMembers);
		}

		#endregion Public Functions

		#region Inner Expression Builder

		private static Expression BuildTypeAccessInternal(
			Type tin, Type tout, LambdaExpression lambda, out MemberInfo[] accessMembers
		)
		{
			var p = Expression.Parameter(tin, "p");

			if (tout.IsValueType)
				tout = typeof (Nullable<>).MakeGenericType(tout);

			var returnTarget = Expression.Label(tout, "result");

			var walker = new NullSafeExpressionWalker(tout, p, returnTarget);

			var checkPNullBlock = walker.CheckNull(p);

			var outBody = walker.Visit(lambda.Body);

			var outBlock = Expression.Block(
				walker.Variables,
				new[]
				{
					checkPNullBlock,
					outBody,
					Expression.Return(returnTarget, Expression.Convert(walker.Variables.LastOrDefault() ?? p, tout), tout),
					Expression.Label(returnTarget, Expression.Constant(null, tout))
				}
			);

			accessMembers = walker.AccessMembers.ToArray();

			return Expression.Lambda(outBlock, p);
		}

		#endregion Inner Expression Builder
	}
}