﻿using System;
using System.Collections.Generic;
using System.Collections.Generic.Extensions;
using System.Text;

namespace PXL.Core.String
{
	/// <summary>
	/// Provides various string helper methods
	/// </summary>
	public static class StringExtensions
	{
		/// <summary>
		/// Combines multiple strings into a camel-cased string
		/// </summary>
		/// <param name="strings">An enumerable of strings to combine</param>
		/// <returns>A camel-cased string combined from the input values</returns>
		/// <example>["foo", "bar", "baz", "", null, "555"] would be converted to fooBarBaz555</example>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the strings argument is null
		/// </exception>
		public static string CamelCombine(this IEnumerable<string> strings)
		{
			return PascalOrCamelCombine(strings, false);
		}

		/// <summary>
		/// Combines multiple strings into a pascal-cased string
		/// </summary>
		/// <param name="strings">An enumerable of strings to combine</param>
		/// <returns>A pascal-cased string combined from the input values</returns>
		/// <example>["foo", "bar", "baz", "", null, "555"] would be converted to FooBarBaz555</example>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the strings argument is null
		/// </exception>
		public static string PascalCombine(this IEnumerable<string> strings)
		{
			return PascalOrCamelCombine(strings, true);
		}

		private static string PascalOrCamelCombine(IEnumerable<string> strings, bool ucFirst)
		{
			if (strings == null)
				throw new ArgumentNullException("strings");

			var builder = new StringBuilder();

			foreach (var str in strings.ToIndexed())
			{
				if (string.IsNullOrEmpty(str))
					continue;

				var fStr = str.Value.Trim();

				if (! str.IsFirst || ucFirst)
					builder.Append(fStr.Substring(0, 1).ToUpper());
				else
					builder.Append(fStr.Substring(0, 1).ToLower());
				
				builder.Append(fStr.Substring(1));
			}

			return builder.ToString();
		}

		/// <summary>
		/// Converts a camel- or pascal-cased string to separate strings
		/// </summary>
		/// <param name="camelOrPascal">The string to convert</param>
		/// <returns>A string split on all lower-to-upper case changes</returns>
		/// <example>IIdentityDIjector3 would be converted to ["IIdentity", "DIjector3"]</example>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the camelOrPascal argument is null
		/// </exception>
		public static IEnumerable<string> SplitCamelOrPascal(this string camelOrPascal)
		{
			if (camelOrPascal == null)
				throw new ArgumentNullException("camelOrPascal");

			var builder = new StringBuilder();

			for (var i = 0; i < camelOrPascal.Length; i++)
			{
				var c = camelOrPascal[i];

				if (i > 0 && char.IsUpper(c) && !char.IsUpper(camelOrPascal[i - 1]))
				{
					yield return builder.ToString();

					builder.Clear();
					builder.Append(c);
				}
				else
				{
					builder.Append(c);
				}
			}

			if (builder.Length > 0)
				yield return builder.ToString();
		}

		/// <summary>
		/// Chops a string to a maximum length.
		/// If the length is less than the maximum, the original string is returned.
		/// </summary>
		/// <param name="value">The value to chop</param>
		/// <param name="maxLength">The maximum length the string may be</param>
		/// <returns>
		/// The chopped string if the value's length is greater than the maxLength parameter,
		/// the original value if the value's length is equal to or less than the maxLength parameter,
		/// null if the value parameter is null
		/// </returns>
		/// <exception cref="System.ArgumentOutOfRangeException">
		/// Thrown if the maxLength parameter is negative
		/// </exception>
		public static string Chop(this string value, int maxLength)
		{
			if( maxLength < 0)
				throw new ArgumentOutOfRangeException("maxLength");

			if (value == null)
				return null;

			return value.Length >= maxLength
				? value
				: value.Substring(0, maxLength);
		}

		/// <summary>
		/// Returns the portion of a string before a certain pattern
		/// If the pattern is not found, the original value is returned
		/// </summary>
		/// <param name="value">The value to chop</param>
		/// <param name="splitAt">The pattern to return the string before</param>
		/// <param name="splitAtLast">
		/// Whether to split at the last or first instance of the splitAt
		/// </param>
		/// <param name="comparison">
		/// The string comparison to use when searching for the splitAt parameter
		/// </param>
		/// <returns>
		/// The the portion of a string before the given pattern if the pattern is found,
		/// the original value if the pattern is not found,
		/// null if the original value is null
		/// </returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the splitAt parameter is null
		/// </exception>
		public static string Before(
			this string value,
			string splitAt,
			bool splitAtLast = false,
			StringComparison comparison = StringComparison.CurrentCulture
		)
		{
			if( splitAt == null )
				throw new ArgumentNullException("splitAt");

			if (value == null)
				return null;

			var indexOf = splitAtLast
				? value.LastIndexOf(splitAt, comparison)
				: value.IndexOf(splitAt, comparison);

			return indexOf == -1 
				? value
				: value.Substring(0, indexOf);
		}
	}
}
