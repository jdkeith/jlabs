﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Security.Cryptography;

// ReSharper disable once CheckNamespace
namespace System
{
	/// <summary>
	/// Wraps a random or strong random and provides methods for getting random objects
	/// </summary>
	/// <remarks>
	/// This class can be used to generate a known sequence of objects if the random
	/// object passed to the constructor has a determined seed
	/// </remarks>
	public class RandomHelper : IRandomHelper
	{
		#region Fields and Constants

		/// <summary>
		/// An alphanumeric string domain (ASCII A-Z, a-z, 0-9)
		/// </summary>
		public const string AlphaNumeric = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

		/// <summary>
		/// An alpha string domain (ASCII A-Z, a-z)
		/// </summary>
		public const string Alpha = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

		/// <summary>
		/// A numeric string domain (0-9)
		/// </summary>
		public const string Numeric = "0123456789";

		/// <summary>
		/// A base 58 string domain (Same as alphanumeric domain without l, I, 0, and O)
		/// </summary>
		public const string Base58 = "123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz";

		/// <summary>
		/// A base 64 string domain (Same as alphanumeric domain with / and +)
		/// </summary>
		public const string Base64 = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz/+";

		private static readonly Type[] _signedEnumBaseTypes;

		private readonly Func<int, byte[]> _byteGetter;

		#endregion Fields and Constants

		#region Factories

		/// <summary>
		/// Gets a new, cryptographically weak RandomHelper
		/// </summary>
		public static RandomHelper Weak
		{
			get { return new RandomHelper(new Random()); }
		}

		/// <summary>
		/// Gets a new, cryptographically strong RandomHelper
		/// </summary>
		public static RandomHelper Strong
		{
			get { return new RandomHelper(new RNGCryptoServiceProvider(), true); }
		}

		#endregion Factories

		#region Constructors

		static RandomHelper()
		{
			_signedEnumBaseTypes = new[]
			{
				typeof (sbyte), typeof (short), typeof (int), typeof (long)
			};
		}

		/// <summary>
		/// Creates a new RandomHelper wrapping a random object
		/// </summary>
		/// <param name="r">The random object to wrap</param>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the r parameter is null
		/// </exception>
		public RandomHelper(Random r)
		{
			if (r == null)
				throw new ArgumentNullException("r");

			_byteGetter = i =>
			{
				var b = new byte[i];
				r.NextBytes(b);
				return b;
			};
		}

		/// <summary>
		/// Creates a new RandomHelper wrapping a function which gets random bytes
		/// </summary>
		/// <param name="byteGetter">A function taking an int (n) and returning n random bytes</param>
		/// <param name="isCryptographicallyStrong">
		/// Whether the underlying random number generator
		/// is suitable for cryptographic uses
		/// </param>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the byteGetter parameter is null
		/// </exception>
		public RandomHelper(Func<int, byte[]> byteGetter, bool isCryptographicallyStrong)
		{
			if (byteGetter == null)
				throw new ArgumentNullException("byteGetter");

			_byteGetter = byteGetter;
			IsCryptographicallyStrong = isCryptographicallyStrong;
		}

		/// <summary>
		/// Creates a new RandomHelper wrapping a strong random object
		/// </summary>
		/// <param name="rng">The strong random object to wrap</param>
		/// <param name="isCryptographicallyStrong">
		/// Whether the underlying random number generator
		/// is suitable for cryptographic uses
		/// </param>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the rng parameter is null
		/// </exception>
		public RandomHelper(RandomNumberGenerator rng, bool isCryptographicallyStrong)
		{
			if (rng == null)
				throw new ArgumentNullException("rng");

			_byteGetter = i =>
			{
				var b = new byte[i];
				rng.GetBytes(b);
				return b;
			};

			IsCryptographicallyStrong = isCryptographicallyStrong;
		}

		#endregion Constructors

		#region Bytes

		/// <summary>
		/// Gets an array of random bytes
		/// </summary>
		/// <param name="count">The amount of bytes to return</param>
		/// <returns>An array of random bytes</returns>
		/// <exception cref="System.ArgumentOutOfRangeException">
		/// Thrown if the count parameter is less than 0
		/// </exception>
		public byte[] NextBytes(int count)
		{
			if (count < 0)
				throw new ArgumentOutOfRangeException("count", "Count must not be a negative number");

			return _byteGetter(count);
		}

		/// <summary>
		/// Writes random bytes to a stream
		/// </summary>
		/// <param name="s">The stream to write random bytes to</param>
		/// <param name="count">The number of byte to write</param>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the s parameter is null
		/// </exception>
		/// <exception cref="System.ArgumentOutOfRangeException">
		/// Thrown if the count parameter is less than 0
		/// </exception>
		public void WriteRandomBytesTo(Stream s, int count)
		{
			if (s == null)
				throw new ArgumentNullException("s");

			s.Write(NextBytes(count), 0, count);
		}

		#endregion Bytes

		#region Bool

		/// <summary>
		/// Gets a random boolean value
		/// </summary>
		/// <param name="trueLikelihood">
		/// The likelihood that the returned value should be true, between 0 and 1
		/// </param>
		/// <returns>A random boolean</returns>
		/// <exception cref="System.ArgumentOutOfRangeException">
		/// Thrown if the trueLikelihood parameter is less than 0 or greater than 1
		/// </exception>
		public bool NextBool(double trueLikelihood = 0.5)
		{
			if (trueLikelihood < 0)
				throw new ArgumentOutOfRangeException("trueLikelihood", "True likelihood must be non-negative");

			if (trueLikelihood > 1)
				throw new ArgumentOutOfRangeException("trueLikelihood", "True likelihood must be less than or equal to 1");

			if (trueLikelihood.Equals(0))
				return false;

			if (trueLikelihood.Equals(1))
				return true;

			double d;

			do
			{
				d = NextDouble();
			} while (d.Equals(1.0));

			return d < trueLikelihood;
		}

		#endregion Bool

		#region Double

		/// <summary>
		/// Gets a random double precision floating point value between 0.0 and 1.0 inclusive
		/// </summary>
		public double NextDouble()
		{
			var ul = BitConverter.ToUInt64(_byteGetter(8), 0);

			return (double) ul / ulong.MaxValue;
		}

		#endregion Double

		#region Int

		private BigInteger DiscardToRandomInt(BigInteger exclusiveRange)
		{
			if (exclusiveRange == BigInteger.One)
				return BigInteger.Zero;

			const int bitsPerByte = 8;

			// only get the bytes which are needed

			var rangeBitCount = (int) Math.Ceiling(BigInteger.Log(exclusiveRange, 2));
			var rangeByteCount = (int) Math.Ceiling((double) rangeBitCount / bitsPerByte);

			byte finalMask = 255;

			var bitBias = 8 - (rangeBitCount % 8);

			// filter out any bits which aren't needed
			if (bitBias != 8)
				finalMask >>= bitBias;

			BigInteger result;

			do
			{
				// add one more byte for the 0
				var rbytes = _byteGetter(rangeByteCount + 1);

				rbytes[rangeByteCount - 1] &= finalMask;
				rbytes[rangeByteCount] = 0;

				result = new BigInteger(rbytes);

				// exclude results which are too high so that the values are evenly distributed
			} while (result >= exclusiveRange);

			return result;
		}

		/// <summary>
		/// Gets a random signed 32 bit integer value
		/// between the inclusive minimum and exclusive maximum
		/// </summary>
		/// <param name="max">The maximum value</param>
		/// <param name="isInclusive">Whether the max value is inclusive</param>
		/// <param name="quantizeTo">Value will be snapped to a the nearest multiple of this number</param>
		/// <returns>A random signed 32 bit integer value</returns>
		/// <exception cref="System.ArgumentOutOfRangeException">
		/// Thrown if the maximum value is less than 2 exclusive, or less than 1 inclusive
		/// </exception>
		public int NextInt(int max, bool isInclusive = false, int quantizeTo = 1)
		{
			if (quantizeTo < 1)
			{
				throw new ArgumentOutOfRangeException(
					"quantizeTo", "Quantize to must be at least 1"
				);
			}

			if (isInclusive)
				max++;

			if (max <= 0)
			{
				throw new ArgumentOutOfRangeException(
					"max",
					"Max must be greater than 2 exclusive, or greater than 1 inclusive"
				);
			}

			var result = NextInt(0, max);

			if (quantizeTo == 1)
				return result;

			return (int) Math.Round((double)result / quantizeTo) * quantizeTo;
		}

		/// <summary>
		/// Gets a random signed 32 bit integer value
		/// between the inclusive minimum and exclusive maximum
		/// </summary>
		/// <param name="min">The inclusive minimum value</param>
		/// <param name="max">The exclusive maximum value</param>
		/// <param name="isMaxInclusive">Whether the max value is inclusive</param>
		/// <param name="quantizeTo">Value will be snapped to a the nearest multiple of this number</param>
		/// <returns>A random signed 32 bit integer value</returns>
		/// <exception cref="System.ArgumentOutOfRangeException">
		/// Thrown if the maximum value is less than the minimum value
		/// </exception>
		public int NextInt(int min, int max, bool isMaxInclusive = false, int quantizeTo = 1)
		{
			if (quantizeTo < 1)
			{
				throw new ArgumentOutOfRangeException(
					"quantizeTo", "Quantize to must be at least 1"
				);
			}

			if (isMaxInclusive)
				max++;

			if (max < min)
				throw new ArgumentOutOfRangeException("max", "Max parameter must be greater than or equal to min parameter");

			var random = (int) DiscardToRandomInt(max - min);

			var result = min + random;

			if (quantizeTo == 1)
				return result;

			return (int) Math.Round((double)result / quantizeTo) * quantizeTo;
		}

		/// <summary>
		/// Gets a random signed BigInteger value
		/// between the inclusive minimum and exclusive maximum
		/// </summary>
		/// <param name="max">The exclusive maximum value</param>
		/// <param name="isInclusive">Whether the max value is inclusive</param>
		/// <returns>A random signed 32 bit integer value</returns>
		/// <exception cref="System.ArgumentOutOfRangeException">
		/// Thrown if the maximum value is less than 2 exclusive, or less than 1 inclusive
		/// </exception>
		public BigInteger NextBigInteger(BigInteger max, bool isInclusive = false)
		{
			if (isInclusive)
				max++;

			if (max <= 0)
			{
				throw new ArgumentOutOfRangeException(
					"max",
					"Max must be greater than 2 exclusive, or greater than 1 inclusive"
				);
			}

			return NextBigInteger(0, max);
		}

		/// <summary>
		/// Gets a random signed BigInteger value
		/// between the inclusive minimum and inclusive maximum
		/// </summary>
		/// <param name="min">The inclusive minimum value</param>
		/// <param name="max">The inclusive maximum value</param>
		/// <param name="isMaxInclusive">Whether the max value is inclusive</param>
		/// <returns>A random signed 32 bit integer value</returns>
		/// <exception cref="System.ArgumentOutOfRangeException">
		/// Thrown if the maximum value is less than the minimum value
		/// </exception>
		public BigInteger NextBigInteger(BigInteger min, BigInteger max, bool isMaxInclusive = false)
		{
			if (isMaxInclusive)
				max++;

			if (max < min)
				throw new ArgumentOutOfRangeException("max", "Max parameter must be greater than or equal to min parameter");

			var random = DiscardToRandomInt(max - min);

			return min + random;
		}

		#endregion Int

		#region Guid

		/// <summary>
		/// Gets a random guid
		/// </summary>
		/// <returns>A random guid</returns>
		public Guid NextGuid()
		{
			return new Guid(_byteGetter(16));
		}

		#endregion Guid

		#region Enum

		/// <summary>
		/// Gets a random single enumeration value from the defined values in the enumeration type
		/// </summary>
		/// <typeparam name="T">The type of enumeration</typeparam>
		/// <returns>A single enumeration value of the given type</returns>
		/// <remarks>
		/// Unless a type of value 0 is explicitly defined, it will not be returned.
		/// </remarks>
		public T NextEnum<T>()
		{
			if (!typeof(T).IsEnum)
				throw new ArgumentException("Type parameter T must be an enumeration type");

			var values = (T[])Enum.GetValues(typeof(T));

			return NextElement(values);
		}

		/// <summary>
		/// Gets a random combination of enumeration values
		/// </summary>
		/// <typeparam name="T">The type of enumertion</typeparam>
		/// <param name="flagLikelihood">How likely it is that any given flag will be set</param>
		/// <returns>
		/// A random combination of enumeration values, including a potential "0 value"
		/// </returns>
		/// <exception cref="System.ArgumentOutOfRangeException">
		/// Thrown if the flagLikelihood parameter is less than 0 or greater than 1
		/// </exception>
		public T NextFlagsEnum<T>(double flagLikelihood = 0.5)
		{
			if (flagLikelihood < 0)
				throw new ArgumentOutOfRangeException("flagLikelihood", "Flag likelihood must be non-negative");

			if (flagLikelihood > 1)
				throw new ArgumentOutOfRangeException("flagLikelihood", "Flag likelihood must be less than or equal to 1");

			var isSigned = _signedEnumBaseTypes.Contains(Enum.GetUnderlyingType(typeof(T)));

			var values = (T[])Enum.GetValues(typeof(T));

			object result;

			if (isSigned)
			{
				result = values.Aggregate(
					0L,
					(v, t) => NextBool(flagLikelihood) ? v | (long)(object)t : v
				);
			}
			else
			{
				result = values.Aggregate(
					0UL,
					(v, t) => NextBool(flagLikelihood) ? v | (ulong)(object)t : v
				);
			}

			return (T)Enum.ToObject(typeof(T), result);
		}

		#endregion Enum

		#region Element

		/// <summary>
		/// Returns a random element from a set of objects
		/// </summary>
		/// <typeparam name="T">The type of objects in the set</typeparam>
		/// <param name="set">The set of objects to choose from</param>
		/// <returns>A randomly-selected item</returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the set parameter is null
		/// </exception>
		public T NextElement<T>(IEnumerable<T> set)
		{
			if (set == null)
				throw new ArgumentNullException("set");

			var setArray = set.ToArray();

			var pos = (int) DiscardToRandomInt(setArray.Length);

			return setArray[pos];
		}

		/// <summary>
		/// Randomizes a collection of items and returns a copy
		/// </summary>
		/// <typeparam name="T">The type of item in the collection</typeparam>
		/// <param name="set">The items to randomize</param>
		/// <returns>A shallow copy of the original set in random order</returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the set parameter is null
		/// </exception>
		public T[] Randomize<T>(ICollection<T> set)
		{
			if( set == null )
				throw new ArgumentNullException("set");

			var result = set.ToArray();

			for (var i = 0; i < set.Count; i++)
			{
				var swapIndex = NextInt(i, set.Count);
				var temp = result[i];
				result[i] = result[swapIndex];
				result[swapIndex] = temp;
			}

			return result;
		}

		#endregion Element

		#region String

		/// <summary>
		/// Returns a random string of the specified length with characters from the given domain
		/// </summary>
		/// <param name="size">The length of the string to be returned</param>
		/// <param name="domain">The domain of characters from which to build the string</param>
		/// <returns>A randomly-generated string</returns>
		/// <exception cref="System.ArgumentOutOfRangeException">
		/// Thrown if the size parameter is less than 0
		/// </exception>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the domain parameter is null
		/// </exception>
		/// <exception cref="System.ArgumentException">
		/// Thrown if the domain parameter is an empty string
		/// </exception>
		public string NextString(int size, string domain = AlphaNumeric)
		{
			if (size < 0)
				throw new ArgumentOutOfRangeException("size", "Size must not be a negative number");

			if (domain == null)
				throw new ArgumentNullException("domain");

			if (domain == string.Empty)
				throw new ArgumentException("Domain parameter cannot be an empty string", "domain");

			if (size == 0)
				return "";

			var buffer = new char[size];

			for (var i = 0; i < size; i++)
			{
				var p = NextInt(size);
				buffer[i] = domain[p];
			}

			return new string(buffer);
		}

		#endregion String

		#region Date

		/// <summary>
		/// Gets a random signed date between the inclusive minimum and exclusive maximum
		/// </summary>
		/// <param name="min">The inclusive minimum value</param>
		/// <param name="max">The exclusive maximum value</param>
		/// <returns>A randomly-generated date with a precision of 1 second</returns>
		/// <exception cref="System.ArgumentOutOfRangeException">
		/// Thrown if the maximum value is less than or equal to the minimum value
		/// </exception>
		public DateTime NextDate(DateTime min, DateTime max)
		{
			if (max <= min)
				throw new ArgumentOutOfRangeException("max", "Max parameter must be greater than min parameter");

			var minSeconds = min.Ticks / TimeSpan.TicksPerSecond;
			var maxSeconds = max.Ticks / TimeSpan.TicksPerSecond;

			var random = (int) DiscardToRandomInt(maxSeconds - minSeconds);

			return min.AddSeconds(random);
		}

		/// <summary>
		/// Gets a random signed date between the inclusive minimum and exclusive maximum
		/// </summary>
		/// <param name="centerDate">The date to center the return value around</param>
		/// <param name="range">The number of days, inclusive, around the center date the return date can be</param>
		/// <param name="includeTime">Whether or not random time is included in the returned date</param>
		/// <returns>A randomly-generated date with a precision of 1 second</returns>
		/// <exception cref="System.ArgumentOutOfRangeException">
		/// Thrown if the range value is negative
		/// </exception>
		public DateTime NextDateCenteredAround(DateTime centerDate, int range, bool includeTime = false)
		{
			// todo add a weight parameter

			if (range < 0)
				throw new ArgumentOutOfRangeException("range", "Range parameter must be a positive number");

			var days = Math.Abs(NextDouble()) % range;
			days -= days / 2;

			var seconds = days * 24 * 3600;

			var result = centerDate.AddSeconds(seconds);

			return includeTime
				? result.Date
				: result;
		}

		#endregion Date

		#region Nulls

		/// <summary>
		/// Returns an optional null value
		/// </summary>
		/// <typeparam name="T">The non-null type of object to return</typeparam>
		/// <param name="nonNullGenerator">A generator for the non-null return value</param>
		/// <param name="nullLikelihood">How likely a null value is</param>
		/// <returns>Null or the generated object</returns>
		/// <remarks>
		/// If a null is returned, the nonNullGenerator function is never called
		/// </remarks>
		public T? NullIfStruct<T>(Func<RandomHelper, T> nonNullGenerator, double nullLikelihood = 0.5) where T : struct
		{
			// ReSharper disable once InconsistentNaming
			Func<RandomHelper, T?> fixedUpNNG = rh => nonNullGenerator(rh);

			return NullIfInternal(fixedUpNNG, nullLikelihood);
		}

		/// <summary>
		/// Returns an optional null value
		/// </summary>
		/// <typeparam name="T">The non-null type of object to return</typeparam>
		/// <param name="nonNullGenerator">A generator for the non-null return value</param>
		/// <param name="nullLikelihood">How likely a null value is</param>
		/// <returns>Null or the generated object</returns>
		/// <remarks>
		/// If a null is returned, the nonNullGenerator function is never called
		/// </remarks>
		public T NullIf<T>(Func<RandomHelper, T> nonNullGenerator, double nullLikelihood = 0.5) where T : class
		{
			return NullIfInternal(nonNullGenerator, nullLikelihood);
		}

		private T NullIfInternal<T>(Func<RandomHelper, T> nonNullGenerator, double nullLikelihood = 0.5)
		{
			if (nonNullGenerator == null)
				throw new ArgumentNullException("nonNullGenerator");

			if (nullLikelihood < 0)
				throw new ArgumentOutOfRangeException("nullLikelihood", "Null likelihood must be non-negative");

			if (nullLikelihood > 1)
				throw new ArgumentOutOfRangeException("nullLikelihood", "Null likelihood must be less than or equal to 1");

			return NextBool(nullLikelihood)
				? default(T)
				: nonNullGenerator(this);
		}

		#endregion Nulls

		#region Other Properties

		/// <summary>
		/// Whether or not the random helper is suitable for cryptographic uses
		/// </summary>
		public bool IsCryptographicallyStrong { get; private set; }

		#endregion Other Properties
	}
}