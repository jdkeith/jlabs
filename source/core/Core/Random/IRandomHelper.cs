﻿using System.Collections.Generic;
using System.IO;
using System.Numerics;

// ReSharper disable once CheckNamespace
namespace System
{
	/// <summary>
	/// Interface for a generator of strong random values
	/// </summary>
	public interface IRandomHelper
	{
		#region Bytes

		/// <summary>
		/// Gets an array of random bytes
		/// </summary>
		/// <param name="count">The amount of bytes to return</param>
		/// <returns>An array of random bytes</returns>
		/// <exception cref="System.ArgumentOutOfRangeException">
		/// Thrown if the count parameter is less than 0
		/// </exception>
		byte[] NextBytes(int count);

		/// <summary>
		/// Writes random bytes to a stream
		/// </summary>
		/// <param name="s">The stream to write random bytes to</param>
		/// <param name="count">The number of byte to write</param>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the s parameter is null
		/// </exception>
		/// <exception cref="System.ArgumentOutOfRangeException">
		/// Thrown if the count parameter is less than 0
		/// </exception>
		void WriteRandomBytesTo(Stream s, int count);

		#endregion Bytes

		#region Bool

		/// <summary>
		/// Gets a random boolean value
		/// </summary>
		/// <param name="trueLikelihood">
		/// The likelihood that the returned value should be true, between 0 and 1
		/// </param>
		/// <returns>A random boolean</returns>
		/// <exception cref="System.ArgumentOutOfRangeException">
		/// Thrown if the trueLikelihood parameter is less than 0 or greater than 1
		/// </exception>
		bool NextBool(double trueLikelihood = 0.5);

		#endregion Bool

		#region Double

		/// <summary>
		/// Gets a random double precision floating point value between 0.0 and 1.0 inclusive
		/// </summary>
		double NextDouble();

		#endregion Double

		#region Int

		/// <summary>
		/// Gets a random signed 32 bit integer value
		/// between the inclusive minimum and exclusive maximum
		/// </summary>
		/// <param name="max">The maximum value</param>
		/// <param name="isInclusive">Whether the max value is inclusive</param>
		/// <param name="quantizeTo">Value will be snapped to a the nearest multiple of this number</param>
		/// <returns>A random signed 32 bit integer value</returns>
		/// <exception cref="System.ArgumentOutOfRangeException">
		/// Thrown if the maximum value is less than 2 exclusive, or less than 1 inclusive
		/// or if quantizeTo is less than 1
		/// </exception>
		int NextInt(int max, bool isInclusive = false, int quantizeTo = 1);

		/// <summary>
		/// Gets a random signed 32 bit integer value
		/// between the inclusive minimum and exclusive maximum
		/// </summary>
		/// <param name="min">The inclusive minimum value</param>
		/// <param name="max">The exclusive maximum value</param>
		/// <param name="isMaxInclusive">Whether the max value is inclusive</param>
		/// <param name="quantizeTo">Value will be snapped to a the nearest multiple of this number</param>
		/// <returns>A random signed 32 bit integer value</returns>
		/// <exception cref="System.ArgumentOutOfRangeException">
		/// Thrown if the maximum value is less than the minimum value
		/// or if quantizeTo is less than 1
		/// </exception>
		int NextInt(int min, int max, bool isMaxInclusive = false, int quantizeTo = 1);

		/// <summary>
		/// Gets a random signed BigInteger value
		/// between the inclusive minimum and exclusive maximum
		/// </summary>
		/// <param name="max">The exclusive maximum value</param>
		/// <param name="isInclusive">Whether the max value is inclusive</param>
		/// <returns>A random signed 32 bit integer value</returns>
		/// <exception cref="System.ArgumentOutOfRangeException">
		/// Thrown if the maximum value is less than 2 exclusive, or less than 1 inclusive
		/// </exception>
		BigInteger NextBigInteger(BigInteger max, bool isInclusive = false);

		/// <summary>
		/// Gets a random signed BigInteger value
		/// between the inclusive minimum and inclusive maximum
		/// </summary>
		/// <param name="min">The inclusive minimum value</param>
		/// <param name="max">The inclusive maximum value</param>
		/// <param name="isMaxInclusive">Whether the max value is inclusive</param>
		/// <returns>A random signed 32 bit integer value</returns>
		/// <exception cref="System.ArgumentOutOfRangeException">
		/// Thrown if the maximum value is less than the minimum value
		/// </exception>
		BigInteger NextBigInteger(BigInteger min, BigInteger max, bool isMaxInclusive = false);

		#endregion Int

		#region Guid

		/// <summary>
		/// Gets a random guid
		/// </summary>
		/// <returns>A random guid</returns>
		Guid NextGuid();

		#endregion Guid

		#region Enum

		/// <summary>
		/// Gets a random single enumeration value from the defined values in the enumeration type
		/// </summary>
		/// <typeparam name="T">The type of enumeration</typeparam>
		/// <returns>A single enumeration value of the given type</returns>
		/// <remarks>
		/// Unless a type of value 0 is explicitly defined, it will not be returned.
		/// </remarks>
		T NextEnum<T>();

		/// <summary>
		/// Gets a random combination of enumeration values
		/// </summary>
		/// <typeparam name="T">The type of enumertion</typeparam>
		/// <param name="flagLikelihood">How likely it is that any given flag will be set</param>
		/// <returns>
		/// A random combination of enumeration values, including a potential "0 value"
		/// </returns>
		/// <exception cref="System.ArgumentOutOfRangeException">
		/// Thrown if the flagLikelihood parameter is less than 0 or greater than 1
		/// </exception>
		T NextFlagsEnum<T>(double flagLikelihood = 0.5);

		#endregion Enum

		#region Element

		/// <summary>
		/// Returns a random element from a set of objects
		/// </summary>
		/// <typeparam name="T">The type of objects in the set</typeparam>
		/// <param name="set">The set of objects to choose from</param>
		/// <returns>A randomly-selected item</returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the set parameter is null
		/// </exception>
		T NextElement<T>(IEnumerable<T> set);

		/// <summary>
		/// Randomizes a collection of items and returns a copy
		/// </summary>
		/// <typeparam name="T">The type of item in the collection</typeparam>
		/// <param name="set">The items to randomize</param>
		/// <returns>A shallow copy of the original set in random order</returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the set parameter is null
		/// </exception>
		T[] Randomize<T>(ICollection<T> set);

		#endregion Element

		#region String

		/// <summary>
		/// Returns a random string of the specified length with characters from the given domain
		/// </summary>
		/// <param name="size">The length of the string to be returned</param>
		/// <param name="domain">The domain of characters from which to build the string</param>
		/// <returns>A randomly-generated string</returns>
		/// <exception cref="System.ArgumentOutOfRangeException">
		/// Thrown if the size parameter is less than 0
		/// </exception>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the domain parameter is null
		/// </exception>
		/// <exception cref="System.ArgumentException">
		/// Thrown if the domain parameter is an empty string
		/// </exception>
		string NextString(int size, string domain = RandomHelper.AlphaNumeric);

		#endregion String

		#region Date

		/// <summary>
		/// Gets a random signed date between the inclusive minimum and exclusive maximum
		/// </summary>
		/// <param name="min">The inclusive minimum value</param>
		/// <param name="max">The exclusive maximum value</param>
		/// <returns>A randomly-generated date with a precision of 1 second</returns>
		/// <exception cref="System.ArgumentOutOfRangeException">
		/// Thrown if the maximum value is less than or equal to the minimum value
		/// </exception>
		DateTime NextDate(DateTime min, DateTime max);

		/// <summary>
		/// Gets a random signed date between the inclusive minimum and exclusive maximum
		/// </summary>
		/// <param name="centerDate">The date to center the return value around</param>
		/// <param name="range">The number of days, inclusive, around the center date the return date can be</param>
		/// <param name="includeTime">Whether or not random time is included in the returned date</param>
		/// <returns>A randomly-generated date with a precision of 1 second</returns>
		/// <exception cref="System.ArgumentOutOfRangeException">
		/// Thrown if the range value is negative
		/// </exception>
		DateTime NextDateCenteredAround(DateTime centerDate, int range, bool includeTime = false);

		#endregion Date

		#region Nulls

		/// <summary>
		/// Returns an optional null value
		/// </summary>
		/// <typeparam name="T">The non-null type of object to return</typeparam>
		/// <param name="nonNullGenerator">A generator for the non-null return value</param>
		/// <param name="nullLikelihood">How likely a null value is</param>
		/// <returns>Null or the generated object</returns>
		/// <remarks>
		/// If a null is returned, the nonNullGenerator function is never called
		/// </remarks>
		T? NullIfStruct<T>(Func<RandomHelper, T> nonNullGenerator, double nullLikelihood = 0.5) where T : struct;

		/// <summary>
		/// Returns an optional null value
		/// </summary>
		/// <typeparam name="T">The non-null type of object to return</typeparam>
		/// <param name="nonNullGenerator">A generator for the non-null return value</param>
		/// <param name="nullLikelihood">How likely a null value is</param>
		/// <returns>Null or the generated object</returns>
		/// <remarks>
		/// If a null is returned, the nonNullGenerator function is never called
		/// </remarks>
		T NullIf<T>(Func<RandomHelper, T> nonNullGenerator, double nullLikelihood = 0.5) where T : class;

		#endregion Nulls

		#region Other Properties

		/// <summary>
		/// Whether or not the random helper is suitable for cryptographic uses
		/// </summary>
		bool IsCryptographicallyStrong { get; }

		#endregion Other Properties
	}
}