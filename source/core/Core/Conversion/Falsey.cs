﻿using System.Collections;

// ReSharper disable once CheckNamespace
namespace System.Conversion
{
	/// <summary>
	/// Converts non-boolean values to boolean representations
	/// </summary>
	public static class Falsey
	{
		/// <summary>
		/// Converts an object into its boolean representation using
		/// JavaScript-like rules for falsehood
		/// </summary>
		/// <param name="o">The object to test</param>
		/// <returns>
		/// False if
		/// <para>o is false</para>
		/// <para>o is an empty string</para>
		/// <para>o is zero</para>
		/// <para>o is the null character \0</para>
		/// <para>o is an empty enumeration</para>
		/// <para>o is a failed status</para>
		/// <para>o is not a number (NaN)</para>
		/// <para>True otherwise</para>
 		/// </returns>
		public static bool If(this object o)
		{
			if (ReferenceEquals(o, null))
				return false;

			if (o.Equals(0) || o.Equals(false) || o.Equals("") || o.Equals(Guid.Empty) || o.Equals('\0'))
				return false;

			var i = o as IEnumerable;

			if (i != null && ! i.GetEnumerator().MoveNext())
				return false;

			var s = o as IStatus;

			if (s != null)
				return s.IsSuccess;

			if (o is double)
			{
				var d = (double)o;

				if (d.Equals(0.0) || double.IsNaN(d))
					return false;
			}
			else if (o is float)
			{
				var f = (float)o;

				if (f.Equals(0f) || float.IsNaN(f))
					return false;
			}
			else if (o is decimal)
			{
				var d = (decimal) o;

				if (d.Equals(0m))
					return false;
			}

			return true;
		}

		/// <summary>
		/// Tests an object for truthiness and returns the whenTrue parameter
		/// if truthy, and the optional whenFalse parameter if falsey
		/// </summary>
		/// <param name="test">The object to test</param>
		/// <param name="whenTrue">The object to return when test is truthy</param>
		/// <param name="whenFalse">The object to return when test is falsey</param>
		/// <returns>The whenTrue or whenFalse parameter depending on test</returns>
		public static object If(this object test, object whenTrue, object whenFalse = null)
		{
			return If(test)
				? whenTrue
				: whenFalse;
		}

		/// <summary>
		/// Tests an object for truthiness and returns the test object if it's
		/// truthy, or the whenFalse parameter if falsey
		/// </summary>
		/// <param name="test">The object to test</param>
		/// <param name="whenFalse">The object to return when test is falsey</param>
		/// <returns>The test parameter or whenFalse parameter depending on test</returns>
		public static object Unless(this object test, object whenFalse)
		{
			return If(test)
				? test
				: whenFalse;
		}
	}
}
