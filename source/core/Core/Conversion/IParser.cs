﻿// ReSharper disable once CheckNamespace
namespace System
{
	/// <summary>
	/// An interface for parsing string representations of objects
	/// </summary>
	/// <typeparam name="T">The type of object to be parsed</typeparam>
	public interface IParser<out T>
	{
		/// <summary>
		/// Extracts an item from a string representation
		/// </summary>
		/// <param name="value">The string to parse</param>
		/// <returns>The item represented by the string</returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the string parameter is null
		/// </exception>
		/// <exception cref="System.FormatException">
		/// Thrown if the string parameter does not represent an item of the return type
		/// </exception>
		T Parse(string value);

		/// <summary>
		/// Attempts to extract an item from a string representation
		/// </summary>
		/// <param name="value">The string to parse</param>
		/// <returns>An extraction status whose data value is the parsed value</returns>
		/// <remarks>
		/// If the parse fails, the status will contain an exception
		/// </remarks>
		IStatus<T> TryParse(string value);
	}
}
