﻿using System.Collections.Generic;
using System.Reflection;

// ReSharper disable once CheckNamespace
namespace System
{
	/// <summary>
	/// Creates parsers for existing types
	/// </summary>
	public static class ParserFactory
	{
		#region Fields

		private static readonly Dictionary<Type, object> _cachedParsers;

		#endregion Fields

		#region Constructor

		static ParserFactory()
		{
			_cachedParsers = new Dictionary<Type, object>();
		}

		#endregion Constructor

		#region Inner Class

		private class StringParser : IParser<string>
		{
			public string Parse(string value)
			{
				return value;
			}

			public IStatus<string> TryParse(string value)
			{
				return Status.Succeed.WithData(value);
			}
		}

		private class BuiltParser<T> : IParser<T>
		{
			private readonly MethodInfo _parse;
			private readonly MethodInfo _tryParse;
			private readonly Type _realType;

			public BuiltParser(MethodInfo parse, MethodInfo tryParse, Type realType)
			{
				_parse = parse;
				_tryParse = tryParse;
				_realType = realType;
			}

			public T Parse(string value)
			{
				bool success;
				object result;

				Extract(value, out success, out result);

				if (success) return (T)result;

				throw new FormatException(
					string.Format(
						"Value '{0}' could not be parsed to type {1}",
						value,
						_realType.FullName
					)
				);
			}

			public IStatus<T> TryParse(string value)
			{
				try
				{
					return Status.Succeed.WithData(Parse(value));
				}
				catch (FormatException fex)
				{
					return Status.FailWithException(fex).WithDefaultData<T>();
				}
			}

			private void Extract(string input, out bool success, out object result)
			{
				object[] @parms;

				if (_tryParse != null)
				{
					@parms = new object[] { input, null };

					success = (bool)_tryParse.Invoke(null, @parms);

					result = @parms[1];
				}
				else if (_realType.IsEnum)
				{
					try
					{
						@parms = new object[] { _realType, input, true };
						result = _parse.Invoke(null, @parms);
						success = true;
					}
					catch (Exception)
					{
						result = _realType.IsValueType ? Activator.CreateInstance(_realType) : null;
						success = false;
					}
				}
				else
				{
					try
					{
						@parms = new object[] { input };
						result = _parse.Invoke(null, @parms);
						success = true;
					}
					catch (Exception)
					{
						result = _realType.IsValueType ? Activator.CreateInstance(_realType) : null;
						success = false;
					}
				}
			}

			public override string ToString()
			{
				return "Built parser for " + _realType.FullName;
			}
		}

		#endregion Inner Class

		#region Public Methods

		/// <summary>
		/// Creates a parser for a given type if possible
		/// </summary>
		/// <typeparam name="T">The type of parser to generate</typeparam>
		/// <returns>
		/// A successful status containing the parser as a data argument if
		/// a parser for the type could be created, failed otherwise
		/// </returns>
		public static Status<IParser<T>> Create<T>()
		{
			var type = typeof (T);

			object parser;

			return CreateInner(type, out parser, false)
				? Status.Succeed.WithData((IParser<T>)parser)
				: Status.Fail.WithDefaultData<IParser<T>>();
		}

		/// <summary>
		/// Creates a parser for a given type if possible
		/// </summary>
		/// <param name="type">The type to build the parser for</param>
		/// <returns>
		/// A successful status containing the parser as a data argument if
		/// a parser for the type could be created, failed otherwise
		/// </returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the type parameter is null
		/// </exception>
		public static Status<IParser<object>> Create(Type type)
		{
			object parser;

			return CreateInner(type, out parser, type.IsValueType)
				? Status.Succeed.WithData((IParser<object>) parser)
				: Status.Fail.WithDefaultData<IParser<object>>();
		}

		private static bool CreateInner(Type type, out object parser, bool castAsObject)
		{
			if( type == null )
				throw new ArgumentNullException("type");

			MethodInfo parse;
			MethodInfo tryParse;

			if (_cachedParsers.TryGetValue(type, out parser))
				return parser != null;

			if (type == typeof (string))
			{
				parser = new StringParser();
			}
			else if (!ExtractParseMethods(type, out parse, out tryParse))
			{
				_cachedParsers[type] = null;
				return false;
			}
			else
			{
				var ctor = typeof (BuiltParser<>)
					.MakeGenericType(castAsObject ? typeof(object) : type)
					.GetConstructor(new[] {typeof (MethodInfo), typeof (MethodInfo), typeof(Type)});

				if (ctor == null)
				{
					_cachedParsers[type] = null;
					return false;
				}

				parser = ctor.Invoke(new object[] {parse, tryParse, type});
			}

			_cachedParsers[type] = parser;

			return Status.Succeed.WithData(parser);
		}

		private static bool ExtractParseMethods(Type type, out MethodInfo parse, out MethodInfo tryParse)
		{
			const BindingFlags flags = BindingFlags.Static | BindingFlags.Public;

			if (type.IsEnum)
			{
				parse = typeof(Enum).GetMethod(
					"Parse", flags,
					null, CallingConventions.Standard,
					new[] { typeof(Type), typeof(string), typeof(bool) },
					new ParameterModifier[0]
				);

				tryParse = null;
			}
			else
			{
				parse = type.GetMethod(
					"Parse", flags,
					null, CallingConventions.Standard,
					new[] { typeof(string) },
					new ParameterModifier[0]
				);

				tryParse = type.GetMethod(
					"TryParse", flags,
					null, CallingConventions.Standard,
					new[] { typeof(string), type.MakeByRefType() },
					new ParameterModifier[0]
				);
			}

			return parse != null || tryParse != null;
		}

		#endregion Public Methods
	}
}