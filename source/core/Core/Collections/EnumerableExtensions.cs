﻿using System.Linq;

// ReSharper disable once CheckNamespace
namespace System.Collections.Generic.Extensions
{
	/// <summary>
	/// A helper which extends enumerations
	/// </summary>
	public static class EnumerableExtensions
	{
		#region To Indexed Item

		/// <summary>
		/// Makes an indexed enumerable from one or more items
		/// </summary>
		/// <typeparam name="T">The type of item to make an indexed enumerable from</typeparam>
		/// <param name="first">The first item in the set</param>
		/// <param name="others">Additional items in the set</param>
		/// <returns>An indexed enumerable from the given parameters</returns>
		public static IEnumerable<IndexedItem<T>> Enumerate<T>(this T first, params T[] others)
		{
			var result = new T[others.Length + 1];
			result[0] = first;
			Array.Copy(others, 0, result, 1, others.Length);

			return result.ToIndexed();
		}

		/// <summary>
		/// Converts a list into a IndexedItem list
		/// </summary>
		/// <typeparam name="T">The type of item in the list</typeparam>
		/// <param name="input">The list to perform actions on</param>
		/// <returns>A list where all items have been converted to IndexedItem</returns>
		/// <remarks>
		/// This result remains lazy and is only computed as needed
		/// </remarks>
		public static IEnumerable<IndexedItem<T>> ToIndexed<T>(this IEnumerable<T> input)
		{
			var enumerator = input.GetEnumerator();

			var isFirst = true;

			if (!enumerator.MoveNext())
				yield break;

			var current = enumerator.Current;

			var index = 0;

			while (true)
			{
				var isLast = !enumerator.MoveNext();

				yield return new IndexedItem<T>(current, isFirst, isLast, index++);

				isFirst = false;

				if (isLast)
					yield break;

				current = enumerator.Current;
			}
		}

		/// <summary>
		/// A noop added to prevent accidental nesting
		/// </summary>
		/// <typeparam name="T">The type of item in the list</typeparam>
		/// <param name="input">The list to perform actions on</param>
		/// <returns>The input parameter</returns>
		public static IEnumerable<IndexedItem<T>> ToIndexed<T>(this IEnumerable<IndexedItem<T>> input)
		{
			return input;
		}

		#endregion To Indexed Item

		#region ForEach

		/// <summary>
		/// Executes an action once for each item in an enumeration
		/// </summary>
		/// <typeparam name="T">The type of item in the enumeration</typeparam>
		/// <param name="source">The enumeration to execute over</param>
		/// <param name="action">The action to execute</param>
		public static void ForEach<T>(this IEnumerable<T> source, Action<T> action)
		{
			ForEachUntil(source, (v, i, f, l) => { action(v); return false; });
		}

		/// <summary>
		/// Executes an action once for each item in an enumeration, with optional early exit
		/// </summary>
		/// <typeparam name="T">The type of item in the enumeration</typeparam>
		/// <param name="source">The enumeration to execute over</param>
		/// <param name="action">The action to execute</param>
		/// <remarks>Returning true from the action stops further execution</remarks>
		public static void ForEachUntil<T>(this IEnumerable<T> source, Func<T, bool> action)
		{
			ForEach(source, (v, i, f, l) => action(v));
		}

		/// <summary>
		/// Executes an action once for each item in an enumeration
		/// </summary>
		/// <typeparam name="T">The type of item in the enumeration</typeparam>
		/// <param name="source">The enumeration to execute over</param>
		/// <param name="action">The action to execute, passed the zero-based index of the item</param>
		public static void ForEach<T>(this IEnumerable<T> source, Action<T, int> action)
		{
			ForEachUntil(source, (v, i, f, l) => { action(v, i); return false; });
		}

		/// <summary>
		/// Executes an action once for each item in an enumeration, with optional early exit
		/// </summary>
		/// <typeparam name="T">The type of item in the enumeration</typeparam>
		/// <param name="source">The enumeration to execute over</param>
		/// <param name="action">The action to execute, passed the zero-based index of the item</param>
		/// <remarks>Returning true from the action stops further execution</remarks>
		public static void ForEachUntil<T>(this IEnumerable<T> source, Func<T, int, bool> action)
		{
			ForEach(source, (v, i, f, l) => action(v, i));
		}

		/// <summary>
		/// Executes an action once for each item in an enumeration
		/// </summary>
		/// <typeparam name="T">The type of item in the enumeration</typeparam>
		/// <param name="source">The enumeration to execute over</param>
		/// <param name="action">
		/// The action to execute, passed a boolean indicating whether or not the item is the last item
		/// </param>
		public static void ForEach<T>(this IEnumerable<T> source, Action<T, bool> action)
		{
			ForEachUntil(source, (v, i, f, l) => { action(v, l); return false; });
		}

		/// <summary>
		/// Executes an action once for each item in an enumeration, with optional early exit
		/// </summary>
		/// <typeparam name="T">The type of item in the enumeration</typeparam>
		/// <param name="source">The enumeration to execute over</param>
		/// <param name="action">
		/// The action to execute, passed a boolean indicating whether or not the item is the last item
		/// </param>
		/// <remarks>Returning true from the action stops further execution</remarks>
		public static void ForEach<T>(this IEnumerable<T> source, Func<T, bool, bool> action)
		{
			ForEachUntil(source, (v, i, f, l) => action(v, l));
		}

		/// <summary>
		/// Executes an action once for each item in an enumeration
		/// </summary>
		/// <typeparam name="T">The type of item in the enumeration</typeparam>
		/// <param name="source">The enumeration to execute over</param>
		/// <param name="action">
		/// The action to execute, passed the zero-based index of the item,
		/// and whether the item is the first item and/or the last item
		/// </param>
		public static void ForEach<T>(this IEnumerable<T> source, Action<T, int, bool, bool> action)
		{
			ForEachUntil(source, (v, i, f, l) => { action(v, i, f, l); return false; });
		}

		/// <summary>
		/// Executes an action once for each item in an enumeration, with optional early exit
		/// </summary>
		/// <typeparam name="T">The type of item in the enumeration</typeparam>
		/// <param name="source">The enumeration to execute over</param>
		/// <param name="action">
		/// The action to execute, passed the zero-based index of the item,
		/// and whether the item is the first item and/or the last item
		/// </param>
		/// <remarks>Returning true from the action stops further execution</remarks>
		public static void ForEachUntil<T>(this IEnumerable<T> source, Func<T, int, bool, bool, bool> action)
		{
			if (source == null)
				throw new ArgumentNullException("source");

			if (action == null)
				throw new ArgumentNullException("action");

			var indexed = source.ToIndexed();

			// ReSharper disable once LoopCanBeConvertedToQuery
			foreach (var b in indexed.Select(s => action(s.Value, s.Index, s.IsFirst, s.IsLast)))
			{
				if (b) { return;}
			}
		}

		#endregion ForEach

		#region Nth

		/// <summary>
		/// Gets the item at the wrapped position. Useful if you want to round robin a list of unknown size.
		/// </summary>
		/// <typeparam name="T">The type of item in the enumerable</typeparam>
		/// <param name="items">The enumerable of items to extract a value from</param>
		/// <param name="position">The position, modulus the length of the items enumeration to return</param>
		/// <returns>The item at position modulus items length</returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the items parameter is null
		/// </exception>
		/// <exception cref="System.ArgumentException">
		/// Thrown if the items parameter is an empty enumeration
		/// </exception>
		/// <exception cref="System.ArgumentOutOfRangeException">
		/// Thrown if the position parameter is negative
		/// </exception>
		public static T GetNthModLength<T>(this IEnumerable<T> items, int position)
		{
			if (items == null)
				throw new ArgumentNullException("items");

			if (position < 0)
				throw new ArgumentOutOfRangeException("position", "Position parameter cannot be a negative number");

			int length;

			Func<int, int> getIndex = ln =>
				position >= 0 ? position % ln : position % ln + (ln - 1);

			var emptyEx = new ArgumentException("Items set must contain items", "items");

			if (items is IList<T>)
			{
				var list = (IList<T>)items;
				length = list.Count;

				if (length == 0)
					throw emptyEx;

				return list[getIndex(length)];
			}

			var built = new List<T>();

			var enumerator = items.GetEnumerator();

			var index = 0;

			while (enumerator.MoveNext())
			{
				if (index == position)
					return enumerator.Current;

				built.Add(enumerator.Current);

				index++;
			}

			length = built.Count;

			if (length == 0)
				throw emptyEx;

			return built[getIndex(length)];
		}

		#endregion Nth

		#region Slicing

		/// <summary>
		/// Returns a subset of an enumeration
		/// </summary>
		/// <typeparam name="T">The type of item contained in the enumeration</typeparam>
		/// <param name="source">The set to obtain a subset from</param>
		/// <param name="start">The index, inclusive, of the first item to return in the subset</param>
		/// <param name="count">The number of items to return in the subset</param>
		/// <returns>
		/// A subset between the specified bounds, or the the largest subset possible if
		/// one or both start and count parameters are out of bounds
		/// </returns>
		/// <remarks>
		/// The source enumeration is not modified
		/// </remarks>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the source parameter is null
		/// </exception>
		/// <exception cref="System.ArgumentOutOfRangeException">
		/// Thrown if the start or count parameters are less than 0
		/// </exception>
		public static IEnumerable<T> Slice<T>(this IEnumerable<T> source, int start, int count)
		{
			if (source == null)
				throw new ArgumentNullException("source");

			if (start < 0)
				throw new ArgumentOutOfRangeException("start", "Start parameter cannot be a negative number");

			if (count < 0)
				throw new ArgumentOutOfRangeException("count", "Count parameter cannot be a negative number");

			return source.Skip(start).Take(count);
		}

		/// <summary>
		/// Returns a modified set of an enumeration with items removed and added
		/// </summary>
		/// <typeparam name="T">The type of item contained in the enumeration</typeparam>
		/// <param name="source">The set to obtain a modified set from</param>
		/// <param name="start">The index, inclusive, of the first item to return in the subset</param>
		/// <param name="removeCount">
		/// The number of items from the start index to not include in the result.
		/// If unspecified, no items are skipped.
		/// </param>
		/// <param name="insert">
		/// Optional items to insert at the position specified by the start parameter.
		/// If unspecified, no items are added.
		/// </param>
		/// <returns>
		/// A modified set with items removed and added as specified
		/// </returns>
		/// <remarks>
		/// The source enumeration is not modified
		/// </remarks>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the source parameter is null
		/// </exception>
		/// <exception cref="System.ArgumentOutOfRangeException">
		/// Thrown if the start or removeCount parameters are less than 0
		/// </exception>
		public static IEnumerable<T> Splice<T>(
			this IEnumerable<T> source,
			int start, int removeCount = 0,
			IEnumerable<T> insert = null
		)
		{
			if (source == null)
				throw new ArgumentNullException("source");

			if (start < 0)
				throw new ArgumentOutOfRangeException("start", "Start parameter cannot be a negative number");

			if (removeCount < 0)
				throw new ArgumentOutOfRangeException("removeCount", "Remove count parameter cannot be a negative number");

			var sourceIndex = -1;

			var sourceEnumerator = source.GetEnumerator();

			var wasInserted = false;

			while (sourceEnumerator.MoveNext())
			{
				sourceIndex++;

				// handles before and after start, but not index start
				if (sourceIndex < start || sourceIndex > start + removeCount)
				{
					yield return sourceEnumerator.Current;
					continue;
				}

				if (insert != null)
				{
					// ReSharper disable once PossibleMultipleEnumeration
					foreach (var inserted in insert)
					{
						yield return inserted;
					}

					wasInserted = true;
				}

				if (removeCount == 0)
				{
					yield return sourceEnumerator.Current;
					continue;
				}

				for (var i = 0; i < removeCount - 1; i++)
				{
					sourceEnumerator.MoveNext();
					sourceIndex++;
				}

				sourceIndex++;
			}

			if (wasInserted || insert == null)
				yield break;

			// ReSharper disable once PossibleMultipleEnumeration
			foreach (var inserted in insert)
			{
				yield return inserted;
			}
		}

		#endregion Slicing

		#region Paging

		// todo make a lazy version as well?
		/// <summary>
		/// Breaks an enumeration into chunks of a specified size
		/// </summary>
		/// <typeparam name="T">The type of item contained in the enumeration</typeparam>
		/// <param name="source">The enumeration to page</param>
		/// <param name="pageSize">The number of items per page</param>
		/// <returns>An enumeration of arrays of items</returns>
		/// <remarks>
		/// The final page in the enumeration may contain less than pageSize items
		/// </remarks>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the source parameter is null
		/// </exception>
		/// <exception cref="System.ArgumentOutOfRangeException">
		/// Thrown if the pageSize parameter is less than or equal to 0
		/// </exception>
		public static IEnumerable<T[]> Page<T>(this IEnumerable<T> source, int pageSize)
		{
			if (source == null)
				throw new ArgumentNullException("source");

			if (pageSize <= 0)
				throw new ArgumentOutOfRangeException("pageSize", "Page size parameter must be a positive number");

			var sourceEnumerator = source.GetEnumerator();

			var accumulation = new T[pageSize];

			var sourceIndex = -1;

			while (sourceEnumerator.MoveNext())
			{
				sourceIndex++;


				if (sourceIndex != 0 && sourceIndex % pageSize == 0)
				{
					yield return accumulation;
					accumulation = new T[pageSize];
				}

				accumulation[sourceIndex % pageSize] = sourceEnumerator.Current;
			}

			var remainderLength = (sourceIndex + 1) % pageSize;

			if (sourceIndex == -1)
				yield break;

			if (remainderLength == 0)
			{
				yield return accumulation;
				yield break;
			}

			var remainder = new T[remainderLength];

			Array.Copy(accumulation, remainder, remainderLength);

			yield return remainder;
		}

		#endregion Paging
	}
}