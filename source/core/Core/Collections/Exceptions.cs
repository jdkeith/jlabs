﻿// ReSharper disable once CheckNamespace
namespace System.Collections.Generic.Extensions
{
	/// <summary>
	/// Thrown if an item with the given key already exists
	/// </summary>
	public class DuplicateKeyException : Exception
	{
		/// <summary>
		/// Creates a new DuplicateKeyException
		/// </summary>
		/// <param name="key">The key which already exists</param>
		/// <param name="message">An optional message providing details about the exception</param>
		/// <param name="innerException">The exception which caused this exception</param>
		public DuplicateKeyException(
			object key, string message = null, Exception innerException = null
		) : base(
			message ?? string.Format("An item with the key {0} already exists", key), innerException	
		)
		{
			Key = key;
		}

		/// <summary>
		/// The key which already exists
		/// </summary>
		public object Key { get; private set; }
	}
}