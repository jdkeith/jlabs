﻿// ReSharper disable once CheckNamespace
namespace System.Collections.Generic.Extensions
{
	/// <summary>
	/// An item which contains data about its position in an enumeration
	/// </summary>
	/// <typeparam name="T">The type of data this class is extending</typeparam>
	/// <remarks>This is currently not variant</remarks>
	public class IndexedItem<T>
	{
		/// <summary>
		/// Convets an IndexedItem to its containing type
		/// </summary>
		/// <param name="me">The IndexedItem to convert</param>
		/// <returns>The Value property of the item</returns>
		public static implicit operator T(IndexedItem<T> me)
		{
			return me == null ? default(T) : me.Value;
		}

		/// <summary>
		/// Creates a new IndexedItem
		/// </summary>
		/// <param name="value">The value of the item</param>
		/// <param name="isFirst">Whether or not the item is the first in a list</param>
		/// <param name="isLast">Whether or not the item is the last in the list</param>
		/// <param name="index">The position of the item in the list</param>
		public IndexedItem(T value, bool isFirst, bool isLast, int index)
		{
			Value = value;
			IsFirst = isFirst;
			IsLast = isLast;
			Index = index;
		}

		/// <summary>The value of the item</summary>
		public T Value { get; private set; }

		/// <summary>Whether or not the item is the first in the list</summary>
		public bool IsFirst { get; private set; }

		/// <summary>Whether or not the item is the last in the list</summary>
		public bool IsLast { get; private set; }

		/// <summary>The position of the item in the list</summary>
		public int Index { get; private set; }

		/// <summary>
		/// Converts the Value contained in this instance to a string
		/// </summary>
		/// <returns>The string value of the Value property or null if the Value is null</returns>
		public override string ToString()
		{
			if (ReferenceEquals(null, Value))
				return "";

			return Value.ToString();
		}

		/// <summary>
		/// Determines whether or not two items are equal
		/// </summary>
		/// <param name="obj">The value to check against</param>
		/// <returns>True if the other object or value is of the same type and the Value property equals the invocant's Value property</returns>
		public override bool Equals(object obj)
		{
			if (obj is T)
			{
				return ! ReferenceEquals(null, Value) && Value.Equals(obj);
			}

			var fleObj = obj as IndexedItem<T>;

			return ! ReferenceEquals(null, fleObj) && Value.Equals(fleObj.Value);
		}

		/// <summary>
		/// Gets the hashcode suitable for storing the item in a dictionary
		/// </summary>
		/// <returns>
		/// A hash code based on the Value property or the
		/// reference hashcode if the Value property is null
		/// </returns>
		public override int GetHashCode()
		{
			return ReferenceEquals(null, Value)
				? string.Format("{0}=null", typeof (T).FullName).GetHashCode()
				: Value.GetHashCode();
		}
	}
}