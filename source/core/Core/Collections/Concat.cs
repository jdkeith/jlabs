﻿using System.Linq;

// ReSharper disable once CheckNamespace
namespace System.Collections.Generic.Extensions
{
	/// <summary>
	/// Provides additional concatenation options
	/// </summary>
	public static class Concat
	{
		public static IEnumerable<T> Make<T>(params T[] items)
		{
			return items ?? Enumerable.Empty<T>();
		}

		public static IEnumerable<T> Concatenate<T>(this T item, params T[] items)
		{
			return new[] {item}.Concat(items);
		}

		public static IEnumerable<T> Concatenate<T>(this IEnumerable<T> source, params T[] items)
		{
			return source.Concat(items);
		}

		public static IEnumerable<T> Concatenate<T>(this T item, IEnumerable<T> items)
		{
			return new[] { item }.Concat(items);
		}
	}
}