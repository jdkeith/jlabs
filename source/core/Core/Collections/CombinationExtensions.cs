﻿using System.Linq;

// ReSharper disable once CheckNamespace
namespace System.Collections.Generic.Extensions
{
	/// <summary>
	/// Provides combination and permutation support for enumerables
	/// </summary>
	public static class CombinationExtensions
	{
		#region Public Extensions

		/// <summary>
		/// Gets all distinct combinations in an enumerable
		/// </summary>
		/// <typeparam name="T">The type of items to return</typeparam>
		/// <param name="items">An enumeration of items to search through</param>
		/// <param name="selectionSize">The number of items chosen</param>
		/// <returns>An enumeration of arrays containing unique combinations</returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the items parameter is null
		/// </exception>
		/// <exception cref="System.ArgumentOutOfRangeException">
		/// Thrown if the selectionSize parameter is less than one
		/// </exception>
		/// <remarks>
		/// <para>
		/// If the selection size is less than the number of items in the source enumeration,
		/// a single array containing the source enumeration as an array is returned
		/// </para>
		/// <para>
		/// If the selection size is greater than 1, then the source enumeration is coerced to an array,
		/// otherwise it is lazily evaluated
		/// </para>
		/// </remarks>
		public static IEnumerable<T[]> GetCombinations<T>(this IEnumerable<T> items, int selectionSize = 2)
		{
			if (items == null)
				throw new ArgumentNullException("items");

			if (selectionSize < 1)
				throw new ArgumentOutOfRangeException("selectionSize", "Selection size must be greater than or equal to 1");

			// lazy evaluation is allowed here
			if (selectionSize == 1)
			{
				foreach (var item in items)
					yield return new[] { item };

				yield break;
			}

			// hard convert (for now)
			var asArray = items.ToArray();

			if (selectionSize >= asArray.Length)
			{
				yield return asArray;
				yield break;
			}

			var comboEnumerator = new CombinationIndexEnumerator(asArray.Length, selectionSize);

			while(comboEnumerator.MoveNext())
			{
				yield return comboEnumerator.Current.Select(fidx => asArray[fidx]).ToArray();
			}
		}

		/// <summary>
		/// Gets all non-distinct arrangements in an enumerable
		/// </summary>
		/// <typeparam name="T">The type of items to return</typeparam>
		/// <param name="items">An enumeration of items to search through</param>
		/// <param name="domainSize">The number of items chosen</param>
		/// <returns>
		/// An enumeration of arrays containing all possible permutations of the domain size
		/// which may contain duplicate values
		/// </returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the items parameter is null
		/// </exception>
		/// <exception cref="System.ArgumentOutOfRangeException">
		/// Thrown if the domainSize parameter is less than one
		/// </exception>
		/// <remarks>
		/// <para>
		/// If the domain size is less than the number of items in the source enumeration,
		/// a single array containing the source enumeration as an array is returned
		/// </para>
		/// <para>
		/// If the domain size is greater than 1, then the source enumeration is coerced to an array,
		/// otherwise it is lazily evaluated
		/// </para>
		/// </remarks>
		public static IEnumerable<T[]> GetPemutations<T>(this IEnumerable<T> items, int domainSize = 2)
		{
			if (items == null)
				throw new ArgumentNullException("items");

			if (domainSize < 1)
				throw new ArgumentOutOfRangeException("domainSize", "Domain size must be greater than or equal to 1");

			// hard convert (for now)
			var sourceArray = items.ToArray();

			domainSize = Math.Min(sourceArray.Length, domainSize);

			// lazy evaluation is allowed here
			if (domainSize == 1)
			{
				return new[] {sourceArray};
			}

			var accumulator = new List<T[]>();

			RecursePermutation(sourceArray, 0, domainSize, accumulator);

			return accumulator.ToArray();
		}

		private static void RecursePermutation<T>(T[] items, int sourceIndex, int domainSize, ICollection<T[]> accumulator)
		{
			if (sourceIndex >= domainSize)
			{
				accumulator.Add(items.Take(domainSize).ToArray());
				return;
			}

			for (var i = sourceIndex; i < items.Length; i++)
			{
				var temp = items[sourceIndex];
				items[sourceIndex] = items[i];
				items[i] = temp;			

				RecursePermutation(items, sourceIndex + 1, domainSize, accumulator);

				temp = items[sourceIndex];
				items[sourceIndex] = items[i];
				items[i] = temp;
			}
		}

		#endregion Public Extensions
	}

	/// <summary>
	/// A combination enumerator, providing unique combinations of integers
	/// with a given item domain and selection size
	/// </summary>
	public class CombinationIndexEnumerator : IEnumerator<int[]>
	{
		private int[] _frame;
		private bool _isStarted;
		private bool _overEnd;

		/// <summary>
		/// Creates a new CombinationEnumerator
		/// </summary>
		/// <param name="numberOfItems">The number of unique items / domain size</param>
		/// <param name="selectionSize">The number of items to choose</param>
		/// <exception cref="System.ArgumentOutOfRangeException">
		/// Thrown if the numberOfItems parameter is negative or the selectionSize parameter is less than one
		/// </exception>
		public CombinationIndexEnumerator(int numberOfItems, int selectionSize)
		{
			if( numberOfItems < 0 )
				throw new ArgumentOutOfRangeException("numberOfItems", "Number of items parameter must be positive");

			if (selectionSize < 1)
				throw new ArgumentOutOfRangeException("selectionSize", "Selection size must be at least 1");

			NumberOfItems = numberOfItems;
			SelectionSize = Math.Min(numberOfItems, selectionSize);
		}

		/// <summary>
		/// The number of unique items / domain size
		/// </summary>
		public int NumberOfItems { get; private set; }

		/// <summary>
		/// The number of items to choose
		/// </summary>
		public int SelectionSize { get; private set; }

		public void Dispose()
		{
		}

		public int[] Current
		{
			get
			{
				if (_overEnd || ! _isStarted)
					throw new InvalidOperationException();

				return _frame;
			}
		}

		object IEnumerator.Current
		{
			get { return Current; }
		}

		public bool MoveNext()
		{
			if (NumberOfItems == 0)
			{
				_overEnd = true;
				return false;
			}

			if (_frame == null)
			{
				Reset();
				
				_isStarted = true;

				return true;
			}

			_isStarted = true;

			var fixupAt = _frame.Length;

			_frame[_frame.Length - 1]++;

			for (var i = _frame.Length - 1; i >= 0; i--)
			{
				if (_frame[i] < NumberOfItems - (_frame.Length - i - 1))
					break;

				if (i == 0)
				{
					_overEnd = true;
					return false;
				}

				_frame[i - 1]++;
				_frame[i] = 0;
				fixupAt = i;
			}

			for (var i = fixupAt; i < _frame.Length; i++)
			{
				_frame[i] = _frame[i - 1] + 1;
			}

			return true;
		}

		public void Reset()
		{
			_frame = Enumerable.Range(0, SelectionSize).ToArray();
			_isStarted = false;
			_overEnd = false;
		}
	}

	/// <summary>
	/// A permutation enumerator, providing unique permutations of integers
	/// with a given item domain and selection size
	/// </summary>
	public class PermutationIndexEnumerator : IEnumerator<int[]>
	{
		// this isn't a great solution
		private List<int[]> _accumulator;
		private int _pos = -1;

		/// <summary>
		/// Creates a new PermutationIndexEnumerator
		/// </summary>
		/// <param name="numberOfItems">The number of unique items / domain size</param>
		/// <param name="selectionSize">The number of items to choose</param>
		/// <exception cref="System.ArgumentOutOfRangeException">
		/// Thrown if the numberOfItems parameter is negative or the selectionSize parameter is less than one
		/// </exception>
		public PermutationIndexEnumerator(int numberOfItems, int selectionSize)
		{
			if (numberOfItems < 0)
				throw new ArgumentOutOfRangeException("numberOfItems", "Number of items parameter must be positive");

			if (selectionSize < 1)
				throw new ArgumentOutOfRangeException("selectionSize", "Selection size must be at least 1");

			NumberOfItems = numberOfItems;
			SelectionSize = Math.Min(numberOfItems, selectionSize);
		}

		/// <summary>
		/// The number of unique items / domain size
		/// </summary>
		public int NumberOfItems { get; private set; }

		/// <summary>
		/// The number of items to choose
		/// </summary>
		public int SelectionSize { get; private set; }

		public void Dispose()
		{
		}

		public int[] Current
		{
			get
			{
				if (_pos < 0 || _pos >= _accumulator.Count)
					throw new InvalidOperationException();

				return _accumulator[_pos];
			}
		}

		object IEnumerator.Current
		{
			get { return Current; }
		}

		public bool MoveNext()
		{
			if (NumberOfItems == 0)
				return false;

			if (_accumulator == null)
				Reset();

			// ReSharper disable once PossibleNullReferenceException
			return ++_pos < _accumulator.Count;
		}

		public void Reset()
		{
			var frame = Enumerable.Range(0, NumberOfItems).ToArray();

			_accumulator = new List<int[]>();
			_pos = -1;

			RecursePermutation(frame, 0, _accumulator);
		}

		private void RecursePermutation<T>(T[] items, int sourceIndex, ICollection<T[]> accumulator)
		{
			if (sourceIndex >= SelectionSize)
			{
				accumulator.Add(items.Take(SelectionSize).ToArray());
				return;
			}

			for (var i = sourceIndex; i < items.Length; i++)
			{
				var temp = items[sourceIndex];
				items[sourceIndex] = items[i];
				items[i] = temp;

				RecursePermutation(items, sourceIndex + 1, accumulator);

				temp = items[sourceIndex];
				items[sourceIndex] = items[i];
				items[i] = temp;
			}
		}
	}
}