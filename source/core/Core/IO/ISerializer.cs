﻿// ReSharper disable once CheckNamespace
namespace System.IO
{
	/// <summary>
	/// An interface for a class which can serialize and deserialize objects
	/// </summary>
	/// <typeparam name="T">
	/// The type of item which will be serialized or deserialized
	/// </typeparam>
	public interface ISerializer<T>
	{
		/// <summary>
		/// Serializes the given item to a byte array
		/// </summary>
		/// <returns>The serialized representation of the item</returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the item parameter is null
		/// </exception>
		byte[] ToByteArray(T item);

		/// <summary>
		/// Writes the item to a stream
		/// </summary>
		/// <param name="stream">The stream to write the item to</param>
		/// <param name="item">The item to write to the stream</param>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if either parameter is null
		/// </exception>
		void WriteTo(Stream stream, T item);

		/// <summary>
		/// Deserializes an item from a given byte array
		/// </summary>
		/// <param name="serialized">
		/// The byte array to deserialize an item from
		/// </param>
		/// <returns>The deserialized item</returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the serialized parameter is null
		/// </exception>
		/// <exception cref="System.FormatException">
		/// Thrown if the serialized bytes do not represent a valid item
		/// </exception>
		T Deserialize(byte[] serialized);

		/// <summary>
		/// Writes the item to a stream
		/// </summary>
		/// <param name="stream">The stream to write the item to</param>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if either parameter is null
		/// </exception>
		/// <exception cref="System.FormatException">
		/// Thrown if the stream does not contain a valid item
		/// </exception>
		T ReadFrom(Stream stream);
	}
}
