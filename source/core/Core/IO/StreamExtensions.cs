﻿using System;
using System.IO;

namespace PXL.Core.IO
{
	/// <summary>
	/// Extension methods for manipulating stream data
	/// </summary>
	public static class StreamExtensions
	{
		#region Insert

		// todo figure out how to get this from the system
		private const int MaxInsertSizeBeforeUsingFile = 10 * 1024 * 1024; // 10 mb

		/// <summary>
		/// Inserts data into a stream at the specified position
		/// </summary>
		/// <param name="targetStream">The stream to insert data in</param>
		/// <param name="data">The data to insert</param>
		/// <param name="insertPosition">
		/// The position, relative to the targetStream's current position,
		/// to insert new data.
		/// </param>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the targetStream or data parameters are null
		/// </exception>
		/// <exception cref="System.InvalidOperationException">
		/// Thrown if the targetStream does not support writing or seeking
		/// </exception>
		/// <exception cref="System.ArgumentOutOfRangeException">
		/// Thrown if:
		/// <para>The insertPosition parameter is beyond the bounds of the targetStream relative to its current position</para>
		/// </exception>
		/// <remarks>
		/// <para>
		/// The algorithm is lazy when it can be: if an insert is 0 width this method does nothing,
		/// if an insert is at the end of the targetStream a straight buffered copy is performed
		/// </para>
		/// <para>
		/// A temporary stream is used to insert new data before the end. If the new data is more than 10 megabytes,
		/// a temporary file is created to facilitate the move, otherwise a MemoryStream is used
		/// </para>
		/// <para>
		/// The targetStream's position is reset to where it was before this method was called
		/// </para>
		/// </remarks>
		public static void InsertInfo(this Stream targetStream, byte[] data, long insertPosition)
		{
			if( data == null )
				throw new ArgumentNullException("data");

			using (var sourceStream = new MemoryStream())
			{
				sourceStream.Write(data, 0, data.Length);
				sourceStream.Seek(0, SeekOrigin.Begin);
				InsertIntoInternal(targetStream, sourceStream, insertPosition, data.Length);
			}
		}

		/// <summary>
		/// Inserts data into a stream at the specified position
		/// </summary>
		/// <param name="targetStream">The stream to insert data in</param>
		/// <param name="sourceStream">The stream to insert data from</param>
		/// <param name="insertPosition">
		/// The position, relative to the targetStream's current position,
		/// to insert new data.
		/// </param>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the targetStream or sourceStream parameters are null
		/// </exception>
		/// <exception cref="System.InvalidOperationException">
		/// Thrown if the targetStream does not support writing or seeking
		/// or if the sourceStream does not support reading
		/// </exception>
		/// <exception cref="System.ArgumentOutOfRangeException">
		/// Thrown if:
		/// <para>The insertPosition parameter is beyond the bounds of the targetStream relative to its current position</para>
		/// </exception>
		/// <remarks>
		/// <para>
		/// The algorithm is lazy when it can be: if an insert is 0 width this method does nothing,
		/// if an insert is at the end of the targetStream a straight buffered copy is performed
		/// </para>
		/// <para>
		/// A temporary stream is used to insert new data before the end. If the new data is more than 10 megabytes,
		/// a temporary file is created to facilitate the move, otherwise a MemoryStream is used
		/// </para>
		/// <para>
		/// The targetStream's position is reset to where it was before this method was called
		/// </para>
		/// </remarks>
		public static void InsertInto(this Stream targetStream, Stream sourceStream, long insertPosition)
		{
			InsertIntoInternal(targetStream, sourceStream, insertPosition, null);
		}

		/// <summary>
		/// Inserts data into a stream at the specified position
		/// </summary>
		/// <param name="targetStream">The stream to insert data in</param>
		/// <param name="sourceStream">The stream to insert data from</param>
		/// <param name="insertPosition">
		/// The position, relative to the targetStream's current position,
		/// to insert new data.
		/// </param>
		/// <param name="insertLength">
		/// The number of bytes to insert from the sourceStream
		/// </param>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the targetStream or sourceStream parameters are null
		/// </exception>
		/// <exception cref="System.InvalidOperationException">
		/// Thrown if the targetStream does not support writing or seeking
		/// or if the sourceStream does not support reading
		/// </exception>
		/// <exception cref="System.ArgumentOutOfRangeException">
		/// Thrown if:
		/// <para>The insertPosition parameter is beyond the bounds of the targetStream relative to its current position</para>
		/// <para>The insertLength parameter is negative</para>
		/// <para>The insertLength parameter is beyond the bounds of the sourceStream relative to its current position</para>
		/// </exception>
		/// <remarks>
		/// <para>
		/// The algorithm is lazy when it can be: if an insert is 0 width this method does nothing,
		/// if an insert is at the end of the targetStream a straight buffered copy is performed
		/// </para>
		/// <para>
		/// A temporary stream is used to insert new data before the end. If the new data is more than 10 megabytes,
		/// a temporary file is created to facilitate the move, otherwise a MemoryStream is used
		/// </para>
		/// <para>
		/// The targetStream's position is reset to where it was before this method was called
		/// </para>
		/// </remarks>
		public static void InsertInto(this Stream targetStream, Stream sourceStream, long insertPosition, long insertLength)
		{
			InsertIntoInternal(targetStream, sourceStream, insertPosition, insertLength);
		}

		private static void InsertIntoInternal(
			Stream targetStream, Stream sourceStream, long insertPosition, long? insertLength
		)
		{
			if( targetStream == null )
				throw new ArgumentNullException("targetStream");

			if( ! targetStream.CanWrite )
				throw new InvalidOperationException("Cannot write to target stream");

			if (!targetStream.CanSeek)
				throw new InvalidOperationException("Cannot seek within target stream");

			if (sourceStream == null)
				throw new ArgumentNullException("sourceStream");

			if (!sourceStream.CanRead)
				throw new InvalidOperationException("Cannot read from source stream");

			if( targetStream.Position - insertPosition < 0 )
				throw new ArgumentOutOfRangeException("insertPosition", "Insert position beyond the start of the target stream");

			if( insertPosition > targetStream.Length - targetStream.Position )
				throw new ArgumentOutOfRangeException("insertPosition", "Insert position beyond remaining bounds of target stream");

			if (insertLength < 0)
				throw new ArgumentOutOfRangeException("insertLength", "Insert length cannot be negative");

			if (insertLength > sourceStream.Length - sourceStream.Position)
				throw new ArgumentOutOfRangeException("insertLength", "Insert position beyond remaining bounds of source stream");

			insertLength = insertLength ?? sourceStream.Length;

			// nothing to do
			if (insertLength == 0)
				return;

			// retain the position to reset to
			var resetPosition = targetStream.Position;

			// seek to the insertion point
			targetStream.Seek(insertPosition, SeekOrigin.Current);

			FileInfo tempFi = null;

			try
			{
				Stream tempStream = null;

				// if the insert position is at the end,
				// a normal copy onto the end will suffice
				// otherwise a third stream is necessary
				if (insertPosition + resetPosition < targetStream.Length)
				{
					if (insertLength > MaxInsertSizeBeforeUsingFile)
					{
						tempFi = new FileInfo(Path.GetTempFileName());
						tempStream = tempFi.Open(FileMode.Create, FileAccess.ReadWrite);
					}
					else
					{
						tempStream = new MemoryStream();
					}

					// copy the portion starting from the insert point
					targetStream.CopyTo(tempStream);

					// reset to the insert position
					targetStream.Seek(resetPosition + insertPosition, SeekOrigin.Begin);
				}

				// copy the new data in
				var toRead = insertLength.Value;
				var buffer = new byte[ushort.MaxValue];

				while (toRead > 0)
				{
					var read = (int) Math.Min(buffer.Length, toRead);

					sourceStream.Read(buffer, 0, read);
					targetStream.Write(buffer, 0, read);

					toRead -= read;
				}

				if (tempStream != null)
				{
					// write the remaining existing portion on
					tempStream.Seek(0, SeekOrigin.Begin);

					tempStream.CopyTo(targetStream);

					tempStream.Dispose();
				}

				// put the target stream back in position
				targetStream.Seek(resetPosition, SeekOrigin.Begin);
			}
			finally
			{
				if( tempFi != null )
					tempFi.Delete();
			}
		}

		#endregion Insert
	}
}
