﻿// ReSharper disable once CheckNamespace
namespace System.IO
{
	/// <summary>
	/// An interface for deserializing objects from byte arrays or streams
	/// </summary>
	/// <typeparam name="T">The type of object to be deserialized</typeparam>
	public interface IDeserializer<out T>
	{
		/// <summary>
		/// Deserializes an object from a byte array
		/// </summary>
		/// <param name="serialized">The byte array to deserialize an object from</param>
		/// <returns>The object represented by the byte array</returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the serialized parameter is null
		/// </exception>
		/// <exception cref="System.FormatException">
		/// Thrown if the serialized bytes do not represent a valid item
		/// </exception>
		T Deserialize(byte[] serialized);

		/// <summary>
		/// Reads an object from a steam
		/// </summary>
		/// <param name="stream">The stream to read the object from</param>
		/// <returns>The object within the stream</returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the stream parameter is null
		/// </exception>
		/// <exception cref="System.FormatException">
		/// Thrown if the stream does not contain a valid item
		/// </exception>
		T ReadFrom(Stream stream);
	}
}
