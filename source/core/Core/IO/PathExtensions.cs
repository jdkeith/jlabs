﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;

// ReSharper disable once CheckNamespace
namespace System.IO
{
	/// <summary>
	/// Provides enhancements for dealing with file paths
	/// </summary>
	public static class PathExtensions
	{
		private const char WindowsPathSeparatorChar = '\\';
		private static readonly char[] _pathSeparatorChars = {'\\', '/'};

		/// <summary>
		/// Computes an absolute target path from applying a relative path to an absolute base path
		/// </summary>
		/// <param name="relativeOrAbsoluteBiasPath">The path to start from</param>
		/// <param name="baseAbsolutePath">The root to start from</param>
		/// <returns>An absolute path by rebasing relativeOrAbsoluteBiasPath to baseAbsolutePath</returns>
		/// <exception cref="System.ArgumentNullException">Thrown if either parameter is null</exception>
		/// <exception cref="System.FormatException">Thrown if the format of either parameter is invalid</exception>
		/// <remarks>Windows path separators are used if either argument contains backslashes</remarks>
		public static string ToAbsolutePath(this string relativeOrAbsoluteBiasPath, string baseAbsolutePath)
		{
			if (relativeOrAbsoluteBiasPath == null)
				throw new ArgumentNullException("relativeOrAbsoluteBiasPath");

			if (baseAbsolutePath == null)
				throw new ArgumentNullException("baseAbsolutePath");

			baseAbsolutePath = baseAbsolutePath.Trim().TrimEnd(_pathSeparatorChars);
			relativeOrAbsoluteBiasPath = relativeOrAbsoluteBiasPath.Trim().TrimEnd(_pathSeparatorChars);

			var invariant = CultureInfo.InvariantCulture;

			var baseParts = baseAbsolutePath.Trim().Split(_pathSeparatorChars);
			var relParts = relativeOrAbsoluteBiasPath.Trim().Split(_pathSeparatorChars);

			if (relParts.Any() && (relParts.First().Contains(':') || relParts.First() == ""))
				return string.Join(WindowsPathSeparatorChar.ToString(invariant), relParts);

			var takeCount = baseParts.Count();
			var skipIgnored = relParts.TakeWhile(r => r == "~" || r == ".").ToArray();
			var skipBack = relParts.Skip(skipIgnored.Count()).TakeWhile(r => r == "..").ToArray();
			var remainder = relParts.Skip(skipIgnored.Count() + skipBack.Count()).ToArray();

			// error checking
			if (baseParts.Any(bp => bp == ".." || bp == "."))
				throw new FormatException("Format of the base parameter is not valid");

			if (remainder.Any(sb => sb == ".." || sb == "~" || sb == "."))
				throw new FormatException("Format of the relativeOrAbsolute parameter is not valid");

			takeCount -= skipBack.Count();

			if (takeCount < 0)
				throw new FormatException("relativeOrAbsolute parameter drills up beyond root of base parameter");

			return string.Join(WindowsPathSeparatorChar.ToString(invariant), baseParts.Take(takeCount).Concat(remainder));
		}

		/// <summary>
		/// Computes a relative path which, starting from the source absolute path will arrive at the target absolute path
		/// </summary>
		/// <param name="sourceAbsolutePath">The path to start from</param>
		/// <param name="targetAbsolutePath">The path to reach</param>
		/// <returns>A relative path used to transform sourceAbsolutePath into targetAbsolutePath</returns>
		/// <exception cref="System.ArgumentNullException">Thrown if either parameter is null</exception>
		/// <exception cref="System.FormatException">Thrown if the format of either parameter is invalid</exception>
		/// <remarks>Windows path separators are used if either argument contains backslashes</remarks>
		public static string ToRelativePath(this string sourceAbsolutePath, string targetAbsolutePath)
		{
			if (sourceAbsolutePath == null)
				throw new ArgumentNullException("sourceAbsolutePath");

			if (targetAbsolutePath == null)
				throw new ArgumentNullException("targetAbsolutePath");

			sourceAbsolutePath = sourceAbsolutePath.Trim().TrimEnd(_pathSeparatorChars);
			targetAbsolutePath = targetAbsolutePath.Trim().TrimEnd(_pathSeparatorChars);

			var sourceParts = sourceAbsolutePath.Trim().Split(_pathSeparatorChars);
			var targetParts = targetAbsolutePath.Trim().Split(_pathSeparatorChars);

			// errors
			if (sourceParts.Any(sp => sp == ".." || sp == "." || sp == "~"))
				throw new FormatException("Format of source path is not valid");

			if (targetParts.Any(tp => tp == ".." || tp == "." || tp == "~"))
				throw new FormatException("Format of target path is not valid");

			// if the either path is empty or the first parameter isn't the same, return the target path
			if (! sourceParts.Any() || ! targetParts.Any() || sourceParts.First() != targetParts.First())
				return targetAbsolutePath;

			var result = new List<string>();

			int i;

			for (i = 0; i < Math.Min(sourceParts.Count(), targetParts.Count()); i++)
			{
				if (sourceParts[i] == targetParts[i])
					continue;

				result.AddRange(
					Enumerable.Repeat("..", sourceParts.Length - i)
				);

				break;
			}

			result.AddRange(targetParts.Skip(i));

			return string.Join(WindowsPathSeparatorChar.ToString(CultureInfo.InvariantCulture), result);
		}

		/// <summary>
		/// Removes the file portion of a complete path to a file, giving the full path of the directory the file is in
		/// </summary>
		/// <param name="filePath">The complete path to the file</param>
		/// <returns>The path which the file should be within</returns>
		/// <exception cref="System.ArgumentNullException">Thrown if filePath is null</exception>
		/// <exception cref="System.FormatException">Thrown if filePath doesn't appear to be the path of a file</exception>
		/// <remarks>Windows path separators are used if filePath contains backslashes</remarks>
		public static string GetPathOfFile(this string filePath)
		{
			if (filePath == null)
				throw new ArgumentNullException("filePath");

			var pathParts = filePath.Split(_pathSeparatorChars);

			var invariant = CultureInfo.InvariantCulture;

			var isInvalid =
				pathParts.Length < 2 ||
				pathParts.All(string.IsNullOrWhiteSpace) ||
				_pathSeparatorChars.Any(c => filePath.EndsWith(c.ToString(invariant)));

			if (isInvalid)
				throw new FormatException("File path does not appear to be valid");

			return string.Join(WindowsPathSeparatorChar.ToString(invariant), pathParts.Take(pathParts.Length - 1));
		}

		/// <summary>
		/// Converts a string containing standard file query wildcards into a regular expression
		/// </summary>
		/// <param name="filePattern">The file pattern to generate a regular expression from</param>
		/// <returns>A regular expression computed from the filePattern parameter if not null, null otherwise</returns>
		public static Regex FilePatternToRegex(string filePattern)
		{
			if (filePattern == null)
				return null;

			var patternBuilder = new StringBuilder();

			foreach (var c in filePattern)
			{
				string r;

				switch (c)
				{
					case '*':
						r = ".*";
						break;

					case '?':
						r = ".";
						break;

					case '.':
						r = "\\.";
						break;

					default:
						r = c.ToString(CultureInfo.InvariantCulture);
						break;
				}

				patternBuilder.Append(r);
			}

			return new Regex(patternBuilder.ToString());
		}

		/// <summary>
		/// Gets the absolute base directory for the currently-executing assembly
		/// </summary>
		/// <returns>The directory path of the currently-executing assembly</returns>
		public static string GetExecutingBaseDirectory()
		{
			return Assembly.GetExecutingAssembly().GetExecutingBaseDirectory();
		}

		/// <summary>
		/// Gets the absolute base directory for the specified assembly
		/// </summary>
		/// <param name="assembly">The assembly to get the directory of</param>
		/// <returns>The directory path of the currently-executing assembly</returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the assembly parameter is null
		/// </exception>
		public static string GetExecutingBaseDirectory(this Assembly assembly)
		{
			if( assembly == null )
				throw new ArgumentNullException("assembly");

			var source = assembly.CodeBase.Replace("file:///", "");

			return new FileInfo(source).DirectoryName;
		}
	}
}