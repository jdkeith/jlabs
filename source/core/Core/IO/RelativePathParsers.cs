﻿// ReSharper disable once CheckNamespace
namespace System.IO
{
	/// <summary>
	/// A parser for converting relative path values into FileInfo
	/// objects relative to a given base path
	/// </summary>
	public class RelativeFilePathParser : IParser<FileInfo>
	{
		#region Constructor

		/// <summary>
		/// Creates a new RelativeFilePathParser with a specified base
		/// </summary>
		/// <param name="basePath">
		/// The base path all parse values will be relative to
		/// </param>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the basePath parameter is null
		/// </exception>
		public RelativeFilePathParser(string basePath)
		{
			if (basePath == null)
				throw new ArgumentNullException("basePath");

			if (string.IsNullOrWhiteSpace(basePath))
				basePath = "";

			BasePath = basePath.Trim().TrimEnd(new[] {'/', '\\'});
		}

		#endregion Constructor

		#region Property

		/// <summary>
		/// The base path all parse values will be relative to
		/// </summary>
		public string BasePath { get; private set; }

		#endregion Property

		#region Parse Methods

		/// <summary>
		/// Converts the string representation of an absolute or relative path
		/// to a FileInfo relative to the base path value used in the constructor
		/// </summary>
		/// <param name="value">The value to parse</param>
		/// <returns>
		/// A FileInfo with the given path relative to
		/// the base path value used in the constructor
		/// </returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the value parameter is null
		/// </exception>
		public FileInfo Parse(string value)
		{
			if (value == null)
				throw new ArgumentNullException("value");

			var abs = value.ToAbsolutePath(BasePath);

			return new FileInfo(abs);
		}

		/// <summary>
		/// Converts the string representation of an absolute or relative path
		/// to a FileInfo relative to the base path value used in the constructor.
		/// The return status indicates whether the operation was successful.
		/// </summary>
		/// <param name="value">The value to parse</param>
		/// <returns>
		/// A status containing a FileInfo with the given path relative to
		/// the base path value used in the constructor, or a null FileInfo
		/// if the conversion failed
		/// </returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the value parameter is null
		/// </exception>
		public IStatus<FileInfo> TryParse(string value)
		{
			try
			{
				return Status.Succeed.WithData(Parse(value));
			}
			catch (Exception ex)
			{
				return Status.FailWithException<FileInfo>(ex);
			}
		}

		#endregion Parse Methods

		#region Conversion

		public override string ToString()
		{
			return string.Format("RelativeFilePathParser for {0}", BasePath);
		}

		#endregion Conversion
	}

	/// <summary>
	/// A parser for converting relative path values into DiretoryInfo
	/// objects relative to a given base path
	/// </summary>
	public class RelativeDirectoryPathParser : IParser<DirectoryInfo>
	{
		#region Constructor

		/// <summary>
		/// Creates a new RelativeDirectoryPathParser with a specified base
		/// </summary>
		/// <param name="basePath">
		/// The base path all parse values will be relative to
		/// </param>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the basePath parameter is null
		/// </exception>
		public RelativeDirectoryPathParser(string basePath)
		{
			if (basePath == null)
				throw new ArgumentNullException("basePath");

			if (string.IsNullOrWhiteSpace(basePath))
				basePath = "";

			BasePath = basePath.Trim().TrimEnd(new[] {'/', '\\'});
		}

		#endregion Constructor

		#region Property

		/// <summary>
		/// The base path all parse values will be relative to
		/// </summary>
		public string BasePath { get; private set; }

		#endregion Property

		#region Parse Methods

		/// <summary>
		/// Converts the string representation of an absolute or relative path
		/// to a DirectoryInfo relative to the base path value used in the constructor
		/// </summary>
		/// <param name="value">The value to parse</param>
		/// <returns>
		/// A DirectoryInfo with the given path relative to
		/// the base path value used in the constructor
		/// </returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the value parameter is null
		/// </exception>
		public DirectoryInfo Parse(string value)
		{
			if (value == null)
				throw new ArgumentNullException("value");

			var abs = value.ToAbsolutePath(BasePath);

			return new DirectoryInfo(abs);
		}

		/// <summary>
		/// Converts the string representation of an absolute or relative path
		/// to a DirectoryInfo relative to the base path value used in the constructor.
		/// The return status indicates whether the operation was successful.
		/// </summary>
		/// <param name="value">The value to parse</param>
		/// <returns>
		/// A status containing a DirectoryInfo with the given path relative to
		/// the base path value used in the constructor, or a null FileInfo
		/// if the conversion failed
		/// </returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the value parameter is null
		/// </exception>
		public IStatus<DirectoryInfo> TryParse(string value)
		{
			try
			{
				return Status.Succeed.WithData(Parse(value));
			}
			catch (Exception ex)
			{
				return Status.FailWithException<DirectoryInfo>(ex);
			}
		}

		#endregion Parse Methods

		#region Conversion

		public override string ToString()
		{
			return string.Format("RelativeDirectoryPathParser for {0}", BasePath);
		}

		#endregion Conversion
	}
}