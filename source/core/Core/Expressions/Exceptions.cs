﻿// ReSharper disable once CheckNamespace
namespace System.Linq.Expressions.Extensions
{
	/// <summary>
	/// Exception that is thrown when an expression is not cacheable
	/// </summary>
	public class NotCacheableException : Exception
	{
		/// <summary>
		/// Creates a new NotCacheableException
		/// </summary>
		public NotCacheableException()
		{
		}

		/// <summary>
		/// Creates a new NotCacheableException
		/// </summary>
		/// <param name="message">
		/// A message that describes the current exception
		/// </param>
		public NotCacheableException(string message) : base(message)
		{
		}
	}
}