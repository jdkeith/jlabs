﻿using System.Collections.Generic;
using System.Reflection.Extensions;

// ReSharper disable once CheckNamespace
namespace System.Linq.Expressions.Extensions
{
	/// <summary>
	/// Visits a linq expression and returns a unique key
	/// </summary>
	public static class KeyExpressionGenerator
	{
		/// <summary>
		/// Converts a linq expression to a unique key which is agnostic of parameter names
		/// </summary>
		/// <param name="expression">The expression to visit</param>
		/// <returns>A unique key for the expression</returns>
		public static string ToKey(this Expression expression)
		{
			if (expression == null)
				throw new ArgumentNullException("expression");

			var visitor = new KeyCacheableExpressionVisitor();

			try
			{
				visitor.Visit(expression);

				return visitor.Key;
			}
			catch (NotCacheableException)
			{
				return null;
			}
		}

		private class KeyCacheableExpressionVisitor : ExpressionVisitor
		{
			public KeyCacheableExpressionVisitor()
			{
				Key = "";
				_parametersLookup = new Dictionary<string, string>();
			}

			private readonly Dictionary<string, string> _parametersLookup;
			public string Key { get; private set; }

			protected override Expression VisitBinary(BinaryExpression node)
			{
				Key += node.Type + "(";

				Visit(node.Left);

				Key += string.Format(",{0},", node.Method);

				Visit(node.Right);

				Key += ")";

				return node;
			}

			protected override Expression VisitConditional(ConditionalExpression node)
			{
				Key += "Conditional(";

				base.Visit(node.Test);

				Key += "?";

				base.Visit(node.IfTrue);

				Key += ":";

				base.Visit(node.IfFalse);

				Key += ")";

				return node;
			}

			protected override Expression VisitConstant(ConstantExpression node)
			{
				// hoisted variables will show up here

				// if it's a wrapped primitive it's no good
				if (node.Type.Name.StartsWith("<>"))
				{
					// no need to keep processing
					throw new NotCacheableException();
				}

				// if it's not a primative, it's no good
				if (node.Type.IsDataPrimitive() == false)
				{
					// no need to keep processing
					throw new NotCacheableException();
				}

				Key += "Constant(";

				Key += node.Type;

				Key += "," + (node.Value ?? "null");

				var result = base.VisitConstant(node);

				Key += ")";

				return result;
			}

			protected override Expression VisitDefault(DefaultExpression node)
			{
				Key += "Default(";

				Key += node.Type;

				Key += ")";

				return node;
			}

			protected override Expression VisitDynamic(DynamicExpression node)
			{
				Key += "Dynamic(";

				foreach (var arg in node.Arguments)
				{
					Visit(arg);
					Key += ",";
				}

				Key += ")";

				return node;
			}

			protected override ElementInit VisitElementInit(ElementInit node)
			{
				Key += "ElementInit(";

				foreach (var arg in node.Arguments)
				{
					Visit(arg);
					Key += ",";
				}

				Key += ")";

				return node;
			}

			protected override Expression VisitIndex(IndexExpression node)
			{
				Key += "Index(";

				Visit(node.Object);

				Key += string.Format(",{0},", node.Indexer);

				foreach (var arg in node.Arguments)
				{
					Visit(arg);
					Key += ",";
				}

				Key += ")";

				return node;
			}

			protected override Expression VisitLambda<T>(Expression<T> node)
			{
				Key += string.Format("Lambda({0},", typeof(T));

				foreach (var arg in node.Parameters)
				{
					_parametersLookup[arg.Name] = string.Format("$p({0}:{1})", _parametersLookup.Count, arg.Type);

					Visit(arg);
					Key += ",";
				}

				Key += ":";

				Visit(node.Body);

				Key += ")";

				return node;
			}

			protected override Expression VisitListInit(ListInitExpression node)
			{
				Key += "ListInit(";

				Visit(node.NewExpression);

				Key += ":";

				foreach (var init in node.Initializers)
				{
					foreach (var arg in init.Arguments)
					{
						Visit(arg);
					}

					Key += ",";
				}

				Key += ")";

				return node;
			}

			protected override Expression VisitMember(MemberExpression node)
			{
				Key += "VisitMember(";

				Visit(node.Expression);

				Key += "," + node.Member;

				return base.VisitMember(node);
			}

			protected override MemberAssignment VisitMemberAssignment(MemberAssignment node)
			{
				Key += "MemberAssignment(";

				Key += node.Member;

				Key += ":";

				Visit(node.Expression);

				Key += ")";

				return node;
			}

			protected override MemberBinding VisitMemberBinding(MemberBinding node)
			{
				Key += "MemberBinding(";

				Key += node.Member;

				Key += ")";

				return node;
			}

			protected override Expression VisitMemberInit(MemberInitExpression node)
			{
				Key += "MemberInit(";

				Visit(node.NewExpression);

				Key += ":";

				foreach (var binding in node.Bindings)
				{
					Key += binding;
					Key += ",";
				}

				Key += ")";

				return node;
			}

			protected override MemberListBinding VisitMemberListBinding(MemberListBinding node)
			{
				Key += "ListBinding(";

				Key += node.Member;

				Key += ":";

				foreach (var init in node.Initializers)
				{
					foreach (var arg in init.Arguments)
					{
						Visit(arg);
					}

					Key += ",";
				}

				Key += ")";

				return node;
			}

			protected override MemberMemberBinding VisitMemberMemberBinding(MemberMemberBinding node)
			{
				Key += "MemberBinding(";

				Key += node.Member;

				Key += ":";

				foreach (var binding in node.Bindings)
				{
					Key += binding;
					Key += ",";
				}

				Key += ")";

				return node;
			}

			protected override Expression VisitMethodCall(MethodCallExpression node)
			{
				Key += "Call(";

				Key += node.Method;

				Key += ":";

				Visit(node.Object);

				Key += ":";

				foreach (var arg in node.Arguments)
				{
					Visit(arg);
					Key += ",";
				}

				Key += ")";

				return node;
			}

			protected override Expression VisitNew(NewExpression node)
			{
				Key += "New(";

				Key += node.Constructor;

				Key += ",";

				foreach (var arg in node.Arguments)
				{
					Visit(arg);
					Key += ",";
				}

				Key += ")";

				return node;
			}

			protected override Expression VisitNewArray(NewArrayExpression node)
			{
				Key += "NewArray(";

				foreach (var exp in node.Expressions)
				{
					Visit(exp);
					Key += ",";
				}

				Key += ")";

				return node;
			}

			protected override Expression VisitParameter(ParameterExpression node)
			{
				Key += _parametersLookup[node.Name];

				return base.VisitParameter(node);
			}

			protected override Expression VisitTypeBinary(TypeBinaryExpression node)
			{
				Key += "TypeBinary(";

				Visit(node.Expression);

				Key += ",";

				Key += string.Format("{0})", node.TypeOperand);

				return node;
			}

			protected override Expression VisitUnary(UnaryExpression node)
			{
				Key += "Unary(";

				Key += string.Format("{0},", node.Method);

				Visit(node.Operand);

				Key += ")";

				return node;
			}
		}
	}
}
