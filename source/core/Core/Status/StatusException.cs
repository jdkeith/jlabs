﻿// ReSharper disable once CheckNamespace
namespace System
{
	/// <summary>
	/// Exception which is thrown when calling ThrowIfException on
	/// a Status with a non-null exception
	/// </summary>
	/// <remarks>
	/// <para>Used to retain original stack trace</para>
	/// <para>Use InnerException to get the original exception</para>
	/// </remarks>
	public class StatusException : Exception
	{
		internal StatusException(
			Exception innerException, string[] originalStackTrace
		) : base(null, innerException)
		{
			OriginalStackTrace = originalStackTrace;
		}

		/// <summary>
		/// A stack trace for the original exception
		/// </summary>
		/// <remarks>
		/// This is approximate
		/// </remarks>
		public string[] OriginalStackTrace { get; private set; }
	}
}