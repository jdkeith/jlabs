﻿// ReSharper disable once CheckNamespace
namespace System
{
	/// <summary>
	/// Represents the result of an operation without specific return data
	/// </summary>
	public interface IStatus
	{
		/// <summary>
		/// A message or state stored with the status
		/// </summary>
		object StateOrMessage { get; }

		/// <summary>
		/// Whether or not the status represents a successful operation
		/// </summary>
		bool IsSuccess { get; }

		/// <summary>
		/// An exception thrown by the operation
		/// </summary>
		Exception Exception { get; }

		/// <summary>
		/// Throws the exception stored with the status if one exists
		/// </summary>
		void ThrowIfException();
	}

	/// <summary>
	/// Represents the result of an operation with specific return data
	/// </summary>
	/// <typeparam name="TD">The type of return data</typeparam>
	public interface IStatus<out TD> : IStatus
	{
		/// <summary>
		/// Operational result data stored with the status
		/// </summary>
		TD Data { get; }
	}
}
