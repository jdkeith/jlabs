﻿// ReSharper disable once CheckNamespace
namespace System
{
	/// <summary>
	/// Marks a value in an enumeration as an unsuccessful state
	/// </summary>
	[AttributeUsage(AttributeTargets.Field)]
	public class UnsuccessfulStateAttribute : Attribute
	{
	}
}