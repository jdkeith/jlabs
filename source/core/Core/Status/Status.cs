﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

// ReSharper disable once CheckNamespace

namespace System
{
	/// <summary>
	/// The result of an operation without specific return data
	/// </summary>
	public class Status : IStatus
	{
		#region Fields

		private readonly StatusException _statusException;

		#endregion Fields

		#region Inner Class

		private class EnumHelper
		{
			#region Fields

			private static readonly Type[] _signedIntegralTypes;

			private readonly Type _enumType;
			private readonly bool _isLongConvertible;

			private Tuple<object, bool>[] _fields;

			#endregion Fields

			#region Constructors

			static EnumHelper()
			{
				_signedIntegralTypes = new[]
				{
					typeof (sbyte),
					typeof (short),
					typeof (int),
					typeof (long)
				};
			}

			public EnumHelper(Type enumType)
			{
				_enumType = enumType;

				var underlyingType = Enum.GetUnderlyingType(enumType);

				_isLongConvertible = _signedIntegralTypes.Any(t => t == underlyingType);
				IsFlags = _enumType.GetCustomAttributes(typeof (FlagsAttribute), false).Any();
			}

			#endregion Constructors

			#region Properties

			public bool IsFlags { get; private set; }

			public IEnumerable<Enum> Values
			{
				get { return Enum.GetValues(_enumType).Cast<Enum>(); }
			}

			public bool IsNegative(Enum value)
			{
				if (!_isLongConvertible)
					return false;

				return Convert.ToInt64(value) < 0;
			}

			public bool IsZero(Enum value)
			{
				if (_isLongConvertible)
					return Convert.ToUInt64(value) == 0;

				return Convert.ToUInt64(value) == 0;
			}

			public bool HasUnsuccessfulStausFlag(Enum value)
			{
				if (_fields == null)
				{
					var i = Activator.CreateInstance(_enumType);

					_fields = (
						from f in _enumType.GetFields()
						let v = f.GetValue(i)
						where v is Enum
						let husa = f.GetCustomAttributes(typeof (UnsuccessfulStateAttribute), false).Any()
						select new Tuple<object, bool>(v, husa)
						).ToArray();
				}

				return _fields.Any(f => f.Item1.Equals(value) && f.Item2);
			}

			#endregion Properties
		}

		#endregion Inner Class

		#region Factories

		/// <summary>
		/// Creates a new successful status
		/// </summary>
		public static Status Succeed
		{
			get { return true; }
		}

		/// <summary>
		/// Creates a new successful status with a specific message in the state property
		/// </summary>
		/// <param name="message">The message to store with the status</param>
		/// <returns>A successful status</returns>
		public static Status SucceedWithMessage(object message)
		{
			return new Status(true, message);
		}

		/// <summary>
		/// Creates a new unsuccessful status
		/// </summary>
		public static Status Fail
		{
			get { return false; }
		}

		/// <summary>
		/// Creates a new unsuccessful status with a specific message in the state property
		/// </summary>
		/// <param name="message">The message to store with the status</param>
		/// <returns>An unsuccessful status</returns>
		public static Status FailWithMessage(object message)
		{
			return new Status(false, message);
		}

		/// <summary>
		/// Creates a status from an exception
		/// </summary>
		/// <param name="exception">The exception to create the status from</param>
		/// <param name="stateOrMessage">An optional message or state to store with the status</param>
		/// <returns>An unsuccessful status with the given exception</returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the exception parameter is null
		/// </exception>
		public static Status FailWithException(Exception exception, object stateOrMessage = null)
		{
			if (exception == null)
				throw new ArgumentNullException("exception");

			return new Status(exception, stateOrMessage, 3);
		}

		/// <summary>
		/// Creates a status from an exception with a default data value
		/// </summary>
		/// <typeparam name="TD">The type of data to return</typeparam>
		/// <param name="exception">The exception to create the status from</param>
		/// <param name="stateOrMessage">An optional message or state to store with the status</param>
		/// <returns>
		/// An unsuccessful status with the given exception and the default data value
		/// </returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the exception parameter is null
		/// </exception>
		public static Status<TD> FailWithException<TD>(Exception exception, object stateOrMessage = null)
		{
			if( exception == null )
				throw new ArgumentNullException("exception");

			var statusException = GetStatusException(exception, 1);

			return new Status<TD>(default(TD), false, statusException, stateOrMessage);
		}

		/// <summary>
		/// Creates a status whose success value is specified
		/// </summary>
		/// <param name="isSuccess">Whether or not the status is successful</param>
		/// <param name="stateOrMessage">An optional message or state to store with the status</param>
		/// <returns>
		/// A status which is successful if the isSuccess parameter is true
		/// or an unsuccessful status if the isSuccess parameter is false
		/// </returns>
		public static Status FromBoolean(bool isSuccess, object stateOrMessage = null)
		{
			return new Status(isSuccess, stateOrMessage);
		}

		/// <summary>
		/// Creates a status whose success value comes from a state
		/// </summary>
		/// <param name="state">
		/// The state to store as a message and to determine success value from
		/// </param>
		/// <returns>
		/// A status which is successful if the state parameter is non-negative
		/// or an unsuccessful status if the state parameter is negative
		/// </returns>
		public static Status FromState(long state)
		{
			return new Status(state >= 0, state);
		}

		/// <summary>
		/// Creates a status whose success value comes from a state
		/// </summary>
		/// <param name="state">
		/// The state to store as a message and to determine success value from
		/// </param>
		/// <returns>
		/// A status which is successful if the state parameter is non-negative
		/// or an unsuccessful status if the state parameter is negative or is
		/// marked with an UnsuccessfulState attribute, or if any flag
		/// is marked with an UnsuccessfulState attribute and the enumeration type
		/// is marked with a FlagsAttribute
		/// </returns>
		public static Status FromState(Enum state)
		{
			var enumHelper = new EnumHelper(state.GetType());

			bool success;

			if (! enumHelper.IsFlags)
			{
				success =
					!enumHelper.IsNegative(state) &&
					!enumHelper.HasUnsuccessfulStausFlag(state);
			}
			else
			{
				success = true;

				foreach (var value in enumHelper.Values)
				{
					if (enumHelper.IsNegative(value))
					{
						throw new NotSupportedException(
							"Cannot get status from state if it is a flags enum with defined negative values"
							);
					}

					if (enumHelper.IsZero(value) && ! state.Equals(value))
						continue;

					if (! state.HasFlag(value))
						continue;

					// check to make sure it doesn't have the flag defined
					if (!enumHelper.HasUnsuccessfulStausFlag(value))
						continue;

					success = false;
					break;
				}
			}

			return new Status(success, state);
		}

		#endregion Factories

		#region Constructors

		internal Status(Exception exception, object stateOrMessage, int skipFrames)
		{
			_statusException = GetStatusException(exception, skipFrames);
			StateOrMessage = stateOrMessage;
		}

		internal Status(bool isSuccess, object stateOrMessage)
		{
			IsSuccess = isSuccess;
			StateOrMessage = stateOrMessage;
		}

		#endregion Constructors

		#region Properties

		/// <summary>
		/// A message or state stored with the status
		/// </summary>
		public object StateOrMessage { get; private set; }

		/// <summary>
		/// Whether or not the status represents a successful operation
		/// </summary>
		public bool IsSuccess { get; private set; }

		/// <summary>
		/// An exception thrown by the operation
		/// </summary>
		public Exception Exception
		{
			get
			{
				return _statusException == null
					? null
					: _statusException.InnerException;
			}
		}

		#endregion Properties

		#region Methods

		/// <summary>
		/// Throws the exception stored with the status if one exists
		/// </summary>
		public void ThrowIfException()
		{
			if (_statusException != null)
				throw _statusException;
		}

		/// <summary>
		/// Adds operation result data to the status
		/// </summary>
		/// <typeparam name="TD">The type of data to add to the status</typeparam>
		/// <param name="data">The data to add to the status</param>
		/// <returns>A version of the status containing the given data</returns>
		public Status<TD> WithData<TD>(TD data)
		{
			return new Status<TD>(data, IsSuccess, _statusException, StateOrMessage);
		}

		/// <summary>
		/// Adds a default operation result data to the status
		/// </summary>
		/// <typeparam name="TD">The type of data to add to the status</typeparam>
		/// <returns>A version of the status containing the default value of the given data type</returns>
		public Status<TD> WithDefaultData<TD>()
		{
			return new Status<TD>(default(TD), IsSuccess, _statusException, StateOrMessage);
		}

		#endregion Methods

		#region Conversions

		/// <summary>
		/// Converts an exception to a status
		/// </summary>
		/// <param name="ex">The exception to convert into a status</param>
		/// <returns>
		/// A failed status containing the given exception if the exception is not null,
		/// a successful status otherwise
		/// </returns>
		public static implicit operator Status(Exception ex)
		{
			return FailWithException(ex);
		}

		/// <summary>
		/// Converts a boolean value to a status
		/// </summary>
		/// <param name="isSuccess">
		/// Whether or not the status is successful
		/// </param>
		/// <returns>
		/// A successful status if the isSuccess parameter is true,
		/// an unsuccessful status otherwise
		/// </returns>
		public static implicit operator Status(bool isSuccess)
		{
			return FromBoolean(isSuccess);
		}

		/// <summary>
		/// Converts a status to a boolean value
		/// </summary>
		/// <param name="status">The status to convert to a boolean value</param>
		/// <returns>
		/// True if the given status is not null and is successful, false otherwise
		/// </returns>
		public static implicit operator bool(Status status)
		{
			return status != null && status.IsSuccess;
		}

		/// <summary>
		/// Provides a string representation of the status
		/// </summary>
		/// <returns>A string representation of the status</returns>
		public override string ToString()
		{
			return IsSuccess ? "[Successful]" : "[Failed]";
		}

		#endregion Conversions

		#region Utilities

		private static StatusException GetStatusException(Exception innerException, int framesToSkip)
		{
			var trace = new StackTrace(framesToSkip, true);

			var items = new List<string>();

			for (var i = 0; i < trace.FrameCount; i++)
			{
				var frame = trace.GetFrame(i);

				var fileName = frame.GetFileName();

				if (string.IsNullOrEmpty(fileName))
					break;

				items.Add(
					string.Format(
						"at {0},{1} in {2} in {3}",
						frame.GetFileLineNumber(), frame.GetFileColumnNumber(),
						frame.GetMethod().Name, frame.GetFileName()
					)
				);
			}

			return new StatusException(innerException, items.ToArray());
		}

		#endregion Utilities
	}

	/// <summary>
	/// The result of an operation with specific return data
	/// </summary>
	/// <typeparam name="TD">The type of return data</typeparam>
	public class Status<TD> : IStatus<TD>
	{
		#region Fields

		private readonly StatusException _statusException;

		#endregion Fields

		#region Constructor

		internal Status(TD data, bool isSuccess, StatusException statusException, object stateOrMessage)
		{
			_statusException = statusException;

			Data = data;
			IsSuccess = isSuccess;
			StateOrMessage = stateOrMessage;
		}

		#endregion Constructor

		#region Properties

		/// <summary>
		/// Operational result data stored with the status
		/// </summary>
		public TD Data { get; private set; }

		/// <summary>
		/// A message or state stored with the status
		/// </summary>
		public object StateOrMessage { get; private set; }

		/// <summary>
		/// Whether or not the status represents a successful operation
		/// </summary>
		public bool IsSuccess { get; private set; }

		/// <summary>
		/// An exception thrown by the operation
		/// </summary>
		public Exception Exception
		{
			get
			{
				return _statusException == null
					? null
					: _statusException.InnerException;
			}
		}

		#endregion Properties

		#region Methods

		/// <summary>
		/// Throws the exception stored with the status if one exists
		/// </summary>
		public void ThrowIfException()
		{
			if (_statusException != null)
				throw _statusException;
		}

		#endregion Methods

		#region Conversions

		/// <summary>
		/// Converts a typed status to an untyped status
		/// </summary>
		/// <param name="status">The typed status to downconvert</param>
		/// <returns>
		/// Null if the typed status is null, an untyped status otherwise
		/// </returns>
		public static implicit operator Status(Status<TD> status)
		{
			if (status == null)
				return null;

			return status.Exception != null
				? new Status(status.Exception, status.StateOrMessage, 3)
				: new Status(status.IsSuccess, status.StateOrMessage);
		}

		/// <summary>
		/// Converts a status to a boolean value
		/// </summary>
		/// <param name="status">The status to convert to a boolean value</param>
		/// <returns>
		/// True if the given status is not null and is successful, false otherwise
		/// </returns>
		public static implicit operator bool(Status<TD> status)
		{
			return status != null && status.IsSuccess;
		}

		/// <summary>
		/// Provides a string representation of the status
		/// </summary>
		/// <returns>A string representation of the status</returns>
		public override string ToString()
		{
			return string.Format(
				"[{0} = {1}]",
				IsSuccess ? "Successful" : "Failed",
				Data
				);
		}

		#endregion Conversions
	}
}