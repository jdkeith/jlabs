﻿using System.Collections.Generic;
using System.Collections.Generic.Extensions;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Reflection.Extensions;
using System.Text.RegularExpressions;

// ReSharper disable once CheckNamespace
namespace System.Configuration.Extensions
{
	/// <summary>
	/// Parses command line arguments
	/// </summary>
	/// <remarks>
	/// The format for command-line arguments is:
	/// <para>({command} ({val}*) ({key} {val}*)*)</para>
	/// <para>Command: any word starting with an ascii letter, containing only ascii letters, numbers, hyphens or underscores</para>
	/// <para>Key: any letter prefixed by a single or double hyphen, or word prefixed by a double hyphen</para>
	/// <para>Value: any quoted string, number, string starting with a number, or string starting with equal sign</para>
	/// <para>sample-command -f --inferred-with-numbers 9 10 --connected=hello --separated=bye --quoted-value "inferred"</para>
	/// <para>arguments belonging to no command are put into the "default" command</para>
	/// <para>values belonging to no key are put into an unnamed (null) key</para>
	/// </remarks>
	public static class CommandLineArgumentsParser
	{
		#region Fields

		private const string IgnoredCommandRunnerTypeNameSuffix = "CommandRunner";
		private const string DefaultCommandRunnerName = "default";

		private static readonly Regex _commandMatcher;
		private static readonly Regex _flagLetterMatcher;
		private static readonly Regex _flagWordMatcher;
		private static readonly Regex _quotedValueMatcher;
		private static readonly Regex _unquotedValueMatcher;

		#endregion Fields

		#region Constructor

		static CommandLineArgumentsParser()
		{
			_commandMatcher = new Regex(@"^[A-Za-z][A-Za-z0-9]*$");
			_flagLetterMatcher = new Regex(@"^-[A-Za-z]$");
			_flagWordMatcher = new Regex(@"^--[A-Za-z](-?[A-Za-z0-9]+)*$");
			_quotedValueMatcher = new Regex(@"^=?"".*(?<!\\)""$");
			_unquotedValueMatcher = new Regex(@"^=?[^-]+[\w+\[\]{},.<>!@#$%\^&*():;/?\\|\-]*$");
		}

		#endregion Constructor

		#region Public Methods

		/// <summary>
		/// Parses command line arguments and returns an array of commands
		/// </summary>
		/// <param name="args">The command line arguments to parse</param>
		/// <returns>An array of command objects</returns>
		/// <remarks>The args parameter is string joined by space before processing</remarks>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the args parameter is null
		/// </exception>
		/// <exception cref="System.FormatException">
		/// Thrown when there is a problem with parsing command line
		/// </exception>
		public static Command[] Parse(string[] args)
		{
			if (args == null)
				throw new ArgumentNullException("args");

			return Parse(string.Join("", args));
		}

		/// <summary>
		/// Parses command line arguments and returns an array of commands
		/// </summary>
		/// <param name="flattenedArgs">The command line arguments to parse</param>
		/// <returns>An array of command objects</returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the flattenedArgs parameter is null
		/// </exception>
		/// <exception cref="System.FormatException">
		/// Thrown when there is a problem with parsing command line
		/// </exception>
		public static Command[] Parse(string flattenedArgs)
		{
			if (flattenedArgs == null)
				throw new ArgumentNullException("flattenedArgs");

			return Tokenize(flattenedArgs).ToArray();
		}

		/// <summary>
		/// Parses command line arguments and instantiates the appropriate command runner objects
		/// </summary>
		/// <param name="args">The command line arguments to parse</param>
		/// <param name="ignoreMissingCommandRunners">
		/// When true commands which can't be mapped to command runner are ignored,
		/// when false an exception is thrown in such a case
		/// </param>
		/// <param name="searchAssemblies">
		/// Zero or more assemblies to search for command runners within
		/// </param>
		/// <returns>An array of command runners with their properties filled</returns>
		/// <remarks>
		/// <para>If no search assemblies are specified, the currently executing assembly is searched</para>
		/// <para>The args parameter is string joined by space before processing</para>
		/// </remarks>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the args parameter is null
		/// </exception>
		/// <exception cref="System.FormatException">
		/// Thrown when there is a problem with parsing command line
		/// </exception>
		/// <exception cref="JLabs.Core.CommandLine.CommandRunnerSetupException">
		/// Thrown when there is a problem with setting up a command runner to execute the command
		/// </exception>
		public static CommandRunner[] GetRunnersFor(
			string[] args, bool ignoreMissingCommandRunners = false,
			params Assembly[] searchAssemblies
		)
		{
			if (args == null)
				throw new ArgumentNullException("args");

			return GetRunnersFor(string.Join("", args), ignoreMissingCommandRunners, searchAssemblies);
		}

		/// <summary>
		/// Parses command line arguments and instantiates the appropriate command runner objects
		/// </summary>
		/// <param name="flattenedArgs">The command line arguments to parse</param>
		/// <param name="ignoreMissingCommandRunners">
		/// When true commands which can't be mapped to command runner are ignored,
		/// when false an exception is thrown in such a case
		/// </param>
		/// <param name="searchAssemblies">
		/// Zero or more assemblies to search for command runners within
		/// </param>
		/// <returns>An array of command runners with their properties filled</returns>
		/// <remarks>
		/// If no search assemblies are specified, the currently executing assembly is searched
		/// </remarks>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the flattenedArgs parameter is null
		/// </exception>
		/// <exception cref="System.FormatException">
		/// Thrown when there is a problem with parsing command line
		/// </exception>
		/// <exception cref="JLabs.Core.CommandLine.CommandRunnerSetupException">
		/// Thrown when there is a problem with setting up a command runner to execute the command
		/// </exception>
		public static CommandRunner[] GetRunnersFor(
			string flattenedArgs, bool ignoreMissingCommandRunners = false, params Assembly[] searchAssemblies
		)
		{
			if (flattenedArgs == null)
				throw new ArgumentNullException("flattenedArgs");

			return Parse(flattenedArgs).GetRunnersFor(ignoreMissingCommandRunners, searchAssemblies);
		}

		/// <summary>
		/// Instantiates command runners from the given commands
		/// </summary>
		/// <param name="commands">The commands to map to runners</param>
		/// <param name="ignoreMissingCommandRunners">
		/// When true commands which can't be mapped to command runner are ignored,
		/// when false an exception is thrown in such a case
		/// </param>
		/// <param name="searchAssemblies">
		/// Zero or more assemblies to search for command runners within
		/// </param>
		/// <returns>An array of command runners with their properties filled</returns>
		/// <remarks>
		/// If no search assemblies are specified, the currently executing assembly is searched
		/// </remarks>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the commands parameter is null
		/// </exception>
		/// <exception cref="JLabs.Core.CommandLine.CommandRunnerSetupException">
		/// Thrown when there is a problem with setting up a command runner to execute the command
		/// </exception>
		public static CommandRunner[] GetRunnersFor(
			this IEnumerable<Command> commands, bool ignoreMissingCommandRunners = false, params Assembly[] searchAssemblies
		)
		{
			if (commands == null)
				throw new ArgumentNullException("commands");

			var commandRunnerType = typeof (CommandRunner);
			searchAssemblies = searchAssemblies ?? new Assembly[0];

			if (! searchAssemblies.Any())
				searchAssemblies = new [] {Assembly.GetEntryAssembly()};

			var allRunnersTypes = searchAssemblies
				.SelectMany(a => a.GetTypes())
				.Where(commandRunnerType.IsAssignableFrom)
				.ToDictionary(t => t.Name.Replace(IgnoredCommandRunnerTypeNameSuffix, "").ToLower());

			// create aliases
			foreach (var runnerType in allRunnersTypes.Values.ToArray())
			{
				var aliases = (CommandRunnerAliasAttribute[])runnerType.GetCustomAttributes(typeof(CommandRunnerAliasAttribute), false);

				var closureSafeRunnerType = runnerType;
				aliases.ForEach(a => allRunnersTypes[a.Alias.ToLower()] = closureSafeRunnerType);

				var isDefault = runnerType.GetCustomAttributes(typeof (DefaultCommandRunnerAttribute), false).Any();

				if (isDefault)
					allRunnersTypes[DefaultCommandRunnerName] = runnerType;
			}

			var results = new List<CommandRunner>();

			// search for the given commands
			foreach (var command in commands)
			{
				Type runnerType;

				if (! allRunnersTypes.TryGetValue(command.Name.ToLower(), out runnerType))
				{
					if (ignoreMissingCommandRunners) continue;

					throw new CommandRunnerNotFoundException(command.Name, null);
				}

				var ctor = runnerType.GetConstructor(new[] {typeof (IEnumerable<Argument>)});

				if (ctor == null)
					throw new CommandInitializationException(runnerType, command.Name, null);

				var destinationIntance = (CommandRunner) ctor.Invoke(new object[] { command.Arguments });

				var commandRawValues = command.Arguments.ToDictionary(
					a => a.Name.Replace("_", "").Replace("-", ""), a => a.Values.ToArray()
				);

				destinationIntance.FillRaw(
					commandRawValues, true, true,
					MemberKeyGenerator, IgnoreIgnoredMembers, ParserMapper
				);

				results.Add(destinationIntance);
			}

			return results.ToArray();
		}

		#endregion Public Methods

		#region Parse

		private static List<Command> Tokenize(string args)
		{
			var commands = new List<Command>();

			var wsSplit = args.Split(new[] {'\t', ' ', '\n', '\r', '\v'}, StringSplitOptions.RemoveEmptyEntries).ToList();

			var preProcessed = new List<string>();

			// handle "tight" equals
			foreach (var token in wsSplit)
			{
				if (token.Contains("=") && ! token.StartsWith("=") && ! _quotedValueMatcher.IsMatch(token))
				{
					var eqSplit = token.Split(new[] {'='});
					preProcessed.Add(eqSplit[0]);
					preProcessed.Add('=' + eqSplit[1]);
					continue;
				}

				preProcessed.Add(token);
			}

			foreach (var token in preProcessed)
			{
				// go down the line
				if (_commandMatcher.IsMatch(token))
				{
					commands.Add(new Command(token));
					continue;
				}

				if (_flagLetterMatcher.IsMatch(token))
				{
					if (!commands.Any())
						commands.Add(new Command(DefaultCommandRunnerName));

					commands.Last().AddArgument(new Argument(token.Replace("-", "")));

					continue;
				}

				if (_flagWordMatcher.IsMatch(token))
				{
					if (!commands.Any())
						commands.Add(new Command(DefaultCommandRunnerName));

					commands.Last().AddArgument(new Argument(token.Replace("--", "")));

					continue;
				}

				if (_quotedValueMatcher.IsMatch(token))
				{
					if (!commands.Any())
						commands.Add(new Command(DefaultCommandRunnerName));

					if (!commands.Last().Arguments.Any())
						commands.Last().AddArgument(new Argument(null));

					var sToken = token.StartsWith("=") ? token.Substring(1) : token;
					sToken = sToken.Substring(1); // remove the leading "
					sToken = sToken.Substring(0, sToken.Length - 1); // remove the trailing "

					commands.Last().Arguments.Last().AddValue(sToken);

					continue;
				}

				if (_unquotedValueMatcher.IsMatch(token))
				{
					if (!commands.Any())
						commands.Add(new Command(DefaultCommandRunnerName));

					if (!commands.Last().Arguments.Any())
						commands.Last().AddArgument(new Argument(null));

					commands.Last().Arguments.Last().AddValue(token.StartsWith("=") ? token.Substring(1) : token);

					continue;
				}

				throw new FormatException(string.Format("Cannot parse command line arguments. Error near {0}", token));
			}

			// all commands need at least one argument value
			// add a "true" to any arguments missing values
			commands
				.SelectMany(c => c.Arguments)
				.Where(a => ! a.Values.Any())
				.ForEach(a => a.AddValue("true"));

			return commands;
		}

		#endregion Parse

		#region Utilities

		private static IEnumerable<string> MemberKeyGenerator(MemberInfo member)
		{
			var result = new List<string>
			{
				member.Name,
				member.Name.ToLower(),
				member.Name.Replace("_", "").Replace("-", "").ToLower()
			};

			var sf = (ShortFlagAttribute) member.GetCustomAttributes(typeof(ShortFlagAttribute), true).FirstOrDefault();

			if (sf != null)
				result.Add(sf.ShortFlag.ToString(CultureInfo.InvariantCulture));

			return result;
		}

		private static IParser<object> ParserMapper(IMemberAssignment memberAssignment)
		{
			if (memberAssignment.ParserType == typeof (FileInfo))
				return new RelativeFilePathParser(PathExtensions.GetExecutingBaseDirectory());

			if (memberAssignment.ParserType == typeof (DirectoryInfo))
				return new RelativeDirectoryPathParser(PathExtensions.GetExecutingBaseDirectory());

			return null;
		}

		private static bool IgnoreIgnoredMembers(IMemberAssignment memberAssignment)
		{
			return ! memberAssignment.MemberInfo.GetCustomAttributes(typeof (IgnoredCommandRunnerFieldAttribute), true).Any();
		}

		#endregion Utilities
	}
}