﻿using System.Collections.Generic;

// ReSharper disable once CheckNamespace
namespace System.Configuration.Extensions
{
	/// <summary>
	/// Represents a class which can run a command line command
	/// </summary>
	/// <remarks>
	/// Use named fields in the class with data annotations to validate values automatically;
	/// the values passed to the Execute method are not validated
	/// </remarks>
	public abstract class CommandRunner
	{
		protected readonly IEnumerable<Argument> UnvalidatedArguments;

		/// <summary>
		/// Creates a new command runner with the given unvalidated arguments
		/// </summary>
		/// <param name="unvalidatedArguments">The set of unvalidated arguments</param>
		protected CommandRunner(IEnumerable<Argument> unvalidatedArguments)
		{
			UnvalidatedArguments = unvalidatedArguments;
		}

		/// <summary>
		/// Executes the command represented by the implementation
		/// </summary>
		/// <returns>A status indicating whether or not the operation was successful</returns>
		public abstract Status Execute();
	}
}