﻿// ReSharper disable once CheckNamespace
namespace System.Configuration.Extensions
{
	/// <summary>
	/// Provides an alias mapping from command name to command runner class
	/// </summary>
	/// <remarks>
	/// Has no effect unless decorating a class which derives from CommandRunner
	/// </remarks>
	[AttributeUsage(AttributeTargets.Class, AllowMultiple=true, Inherited=false)]
	public class CommandRunnerAliasAttribute : Attribute
	{
		/// <summary>
		/// Creates a new CommandRunnerAliasAttribute
		/// </summary>
		/// <param name="alias">The command name alias</param>
		public CommandRunnerAliasAttribute(string alias)
		{
			if(string.IsNullOrWhiteSpace(alias))
				throw new ArgumentException("Alias must be specified", "alias");

			Alias = alias;
		}

		/// <summary>
		/// The command name alias
		/// </summary>
		public string Alias { get; private set; }
	}
}