﻿// ReSharper disable once CheckNamespace
namespace System.Configuration.Extensions
{
	/// <summary>
	/// Indicates that a member should not be filled regardless of whether
	/// or not it matches a command argument
	/// </summary>
	[AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, AllowMultiple=true, Inherited=true)]
	public class IgnoredCommandRunnerFieldAttribute : Attribute
	{
	}
}