﻿using System.Collections.Generic;
using System.Linq;

// ReSharper disable once CheckNamespace
namespace System.Configuration.Extensions
{
	/// <summary>
	/// Represents a command with associated arguments and values
	/// </summary>
	public class Command
	{
		private readonly List<Argument> _arguments; 

		internal Command(string name)
		{
			Name = name;
			_arguments = new List<Argument>();
		}

		/// <summary>
		/// The name of the command
		/// </summary>
		public string Name { get; private set; }

		/// <summary>
		/// Arguments associated with the command
		/// </summary>
		public IEnumerable<Argument> Arguments
		{
			get { return _arguments; }
		}

		internal void AddArgument(Argument argument)
		{
			_arguments.Add(argument);
		}

		/// <summary>
		/// Provides a string representation of the command and associated arguments and values
		/// </summary>
		/// <returns>A string representation of the command</returns>
		public override string ToString()
		{
			return string.Format(
				"@{0}({1})",
				Name,
				string.Join(", ", _arguments.Select(a => string.Format(":{0}({1})", a.Name, a.Values.Count())))
			);
		}
	}

	/// <summary>
	/// Represents an argument associated with a command
	/// </summary>
	public class Argument
	{
		private readonly List<string> _values; 

		internal Argument(string name)
		{
			Name = name;
			_values = new List<string>();
		}

		/// <summary>
		/// The name of the argument
		/// </summary>
		public string Name { get; private set; }

		/// <summary>
		/// Values associated with the argument
		/// </summary>
		public IEnumerable<string> Values
		{
			get { return _values; }
		}

		internal void AddValue(string value)
		{
			_values.Add(value);
		}

		/// <summary>
		/// Provides a string representation of the argument and associated values
		/// </summary>
		/// <returns>A string representation of the argument</returns>
		public override string ToString()
		{
			return string.Format(":{0}({1})", Name, Values.Count());
		}
	}
}