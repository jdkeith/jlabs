﻿// ReSharper disable once CheckNamespace
namespace System.Configuration.Extensions
{
	/// <summary>
	/// Indicates that the decorated class is the default command runner
	/// </summary>
	/// <remarks>
	/// Has no effect unless decorating a class which is an instance of CommandRunner
	/// </remarks>
	[AttributeUsage(AttributeTargets.Class, AllowMultiple=false, Inherited=false)]
	public class DefaultCommandRunnerAttribute : Attribute
	{
	}
}