﻿// ReSharper disable once CheckNamespace
namespace System.Configuration.Extensions
{
	/// <summary>
	/// Indicates a short flag, case sensitive argument name association with a member
	/// </summary>
	/// <example>
	/// Mark a member with [ShortFlag('L')] to have it receive values from -L
	/// </example>
	[AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, AllowMultiple=true, Inherited=true)]
	public class ShortFlagAttribute : Attribute
	{
		/// <summary>
		/// Creates a new ShortFlagAttribute
		/// </summary>
		/// <param name="shortFlag">The argument name to associate with the decorated member</param>
		public ShortFlagAttribute(char shortFlag)
		{
			ShortFlag = shortFlag;
		}

		/// <summary>
		/// The argument name to associate with the decorated member
		/// </summary>
		public char ShortFlag { get; private set; }
	}
}