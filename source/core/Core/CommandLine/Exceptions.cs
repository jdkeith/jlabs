﻿using System.ComponentModel.DataAnnotations;
using System.Reflection;

// ReSharper disable once CheckNamespace
namespace System.Configuration.Extensions
{
	/// <summary>
	/// Base exception which is thrown when command line arguments can't be parsed or are structurally invalid
	/// </summary>
	public class CommandFormatException : FormatException
	{
		/// <summary>
		/// Creates a new CommandFormatException
		/// </summary>
		/// <param name="characterIndex">The position where the parse error occured</param>
		/// <param name="failToken">The token which caused the parse failure</param>
		/// <param name="innerException">An optional inner exception</param>
		public CommandFormatException(int characterIndex, string failToken, Exception innerException) :
			base(string.Format("Parse error. Unexpected token {0} at character {1}", failToken, characterIndex), innerException)
		{
			Character = characterIndex;
			FailToken = failToken;
		}

		/// <summary>
		/// The position where the parse error occured
		/// </summary>
		public int Character { get; private set; }

		/// <summary>
		/// The token which caused the parse failure
		/// </summary>
		public string FailToken { get; private set; }
	}

	/// <summary>
	/// Base exception which is thrown when commands runners cannot be instantiated from commands
	/// </summary>
	public class CommandRunnerSetupException : Exception
	{
		/// <summary>
		/// Creates a new CommandRunnerSetupException
		/// </summary>
		/// <param name="message">The message to pass in the exception</param>
		protected CommandRunnerSetupException(string message) : base(message)
		{
		}

		/// <summary>
		/// Creates a new CommandRunnerSetupException
		/// </summary>
		/// <param name="message">The message to pass in the exception</param>
		/// <param name="innerException">An optional inner exception</param>
		protected CommandRunnerSetupException(string message, Exception innerException)
			: base(message, innerException)
		{
		}
	}

	/// <summary>
	/// Thrown when a command runner cannot be found to process a command
	/// </summary>
	public class CommandRunnerNotFoundException : CommandRunnerSetupException
	{
		/// <summary>
		/// Creates a new CommandRunnerNotFoundException
		/// </summary>
		/// <param name="commandName">The name of the command to run</param>
		/// <param name="innerException">An optional inner exception</param>
		public CommandRunnerNotFoundException(string commandName, Exception innerException) :
			base(string.Format("Command {0} not found.", commandName), innerException)
		{
			CommandName = commandName;
		}

		/// <summary>
		/// The name of the command to run
		/// </summary>
		public string CommandName { get; private set;}
	}

	/// <summary>
	/// Thrown when a command runner is found but could not be created to process a command
	/// </summary>
	public class CommandInitializationException : CommandRunnerSetupException
	{
		/// <summary>
		/// Creates a new CommandInitializationException
		/// </summary>
		/// <param name="commandRunnerType">The type of the command runner which failed to initialize</param>
		/// <param name="commandName">The name of the command the runner was to process</param>
		/// <param name="innerException">An optional inner exception</param>
		public CommandInitializationException(Type commandRunnerType, string commandName, Exception innerException) :
			base(string.Format("An instance of {0} to run command {1} could not be created.", commandRunnerType.Name, commandName), innerException)
		{
			CommandName = commandName;
			CommandRunnerType = commandRunnerType;
		}

		/// <summary>
		/// The type of the command runner which failed to initialize
		/// </summary>
		public string CommandName { get; private set; }

		/// <summary>
		/// The name of the command the runner was to process
		/// </summary>
		public Type CommandRunnerType { get; private set; }
	}

	/// <summary>
	/// Thrown when a command runner is found but a required value has not been supplied in the command's argments
	/// </summary>
	public class CommandArgumentRequiredButMissingException : CommandRunnerSetupException
	{
		/// <summary>
		/// Creates a new CommandArgumentRequiredButMissingException
		/// </summary>
		/// <param name="commandName">The name of the command to run</param>
		/// <param name="argumentName">The required argument name for which a value is missing</param>
		/// <param name="member">The field or property on which has the required validator</param>
		/// <param name="innerException">An optional inner exception</param>
		public CommandArgumentRequiredButMissingException(
			string commandName, string argumentName, MemberInfo member, Exception innerException
		) : base(string.Format(
			"Required argument {0} in command {1} is missing but required on member {2}.", argumentName, commandName, member.Name
		), innerException)
		{
			CommandName = commandName;
			ArgumentName = argumentName;
			Member = member;
		}

		/// <summary>
		/// The name of the command to run
		/// </summary>
		public string CommandName { get; private set;}

		/// <summary>
		/// The required argument name for which a value is missing
		/// </summary>
		public string ArgumentName { get; private set; }

		/// <summary>
		/// The field or property on which has the required validator
		/// </summary>
		public MemberInfo Member { get; private set; }
	}

	/// <summary>
	/// Thrown when a command runner is found but a value has been supplied fails a validator
	/// </summary>
	public class CommandArgumentInvalidException : CommandRunnerSetupException
	{
		/// <summary>
		/// Creates a new CommandArgumentInvalidException
		/// </summary>
		/// <param name="commandName">The name of the command to run</param>
		/// <param name="argumentName">The required argument name for which a value is missing</param>
		/// <param name="argumentValue">The converted argument value to validate and assign to the member</param>
		/// <param name="member">The field or property on which has the required validator</param>
		/// <param name="validator">The validator which the argument value failed</param>
		public CommandArgumentInvalidException(
			string commandName, string argumentName, object argumentValue, MemberInfo member, ValidationAttribute validator
		) : base(string.Format(
			"Argument {0} with value {1} in command {2} fails validation of type {3} on member {4}.",
			argumentName, argumentValue, commandName, validator.GetType().Name, member.Name
		), new ValidationException(validator.ErrorMessage))
		{
			CommandName = commandName;
			ArgumentName = argumentName;
			ArgumentValue = argumentValue;
			Validator = validator;
			Member = member;
		}

		/// <summary>
		/// The name of the command to run
		/// </summary>
		public string CommandName { get; private set;}

		/// <summary>
		/// The required argument name for which a value is missing
		/// </summary>
		public string ArgumentName { get; private set; }
		
		/// <summary>
		/// The converted argument value to validate and assign to the member
		/// </summary>
		public object ArgumentValue { get; private set; }
		
		/// <summary>
		/// The field or property on which has the required validator
		/// </summary>
		public MemberInfo Member { get; private set; }
		
		/// <summary>
		/// The validator which the argument value failed
		/// </summary>
		public ValidationAttribute Validator { get; private set; }
	}

	/// <summary>
	/// Thrown when a command runner is found but a raw value cannot be coerced to the type
	/// required by the member to which it is to be assigned
	/// </summary>
	public class CommandArgumentCoercionFailureException : CommandRunnerSetupException
	{
		private readonly string _message;

		/// <summary>
		/// Creates a new CommandArgumentCoercionFailureException
		/// </summary>
		/// <param name="commandName">The name of the command to run</param>
		/// <param name="argumentName">The required argument name for which a value is missing</param>
		/// <param name="argumentRawValue">The raw argument value to failed coercion</param>
		/// <param name="member">The field or property on which has the required validator</param>
		/// <param name="innerException">An optional inner exception</param>
		public CommandArgumentCoercionFailureException(
			string commandName, string argumentName, string argumentRawValue, MemberInfo member, Exception innerException
		) : base(null, innerException)
		{
			CommandName = commandName;
			ArgumentName = argumentName;
			ArgumentRawValue = argumentRawValue;
			Member = member;

			var memberType = member.MemberType == MemberTypes.Field
				? ((FieldInfo) member).FieldType
				: ((PropertyInfo) member).PropertyType;

			_message = string.Format(
				"Argument {0} with raw value {1} in command {2} could not be coerced to type {3} on member {4}.",
				argumentName, argumentRawValue, commandName, memberType.Name, member.Name
			);
		}

		/// <summary>
		/// A message which describes the current exception
		/// </summary>
		public override string Message { get { return _message; } }

		/// <summary>
		/// The name of the command to run
		/// </summary>
		public string CommandName { get; private set;}

		/// <summary>
		/// The required argument name for which a value is missing
		/// </summary>
		public string ArgumentName { get; private set; }

		/// <summary>
		/// The raw argument value to failed coercion
		/// </summary>
		public string ArgumentRawValue { get; private set; }

		/// <summary>
		/// The field or property on which has the required validator
		/// </summary>
		public MemberInfo Member { get; private set; }
	}
}