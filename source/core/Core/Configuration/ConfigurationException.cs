﻿// ReSharper disable once CheckNamespace
namespace System.Configuration.Extensions
{
	/// <summary>
	/// Thrown when a configuration file cannot be read
	/// </summary>
	public class ConfigurationException : Exception
	{
		/// <summary>
		/// Creates a new ConfigurationException
		/// </summary>
		/// <param name="key">The name of the parameter which caused the exception or which was expected</param>
		/// <param name="value">The value of the parameter or null if it is not present</param>
		/// <param name="message">The message for the exception</param>
		/// <param name="innerException">Any inner exception</param>
		public ConfigurationException(string key, object value, string message = null, Exception innerException = null)
			: base(message, innerException)
		{
			Key = key;
			Value = value;
		}

		/// <summary>
		/// The name of the parameter which caused the exception or which was expected
		/// </summary>
		public string Key { get; private set; }

		/// <summary>
		/// The value of the parameter or null if it is not present
		/// </summary>
		public object Value { get; private set; }
	}

	/// <summary>
	/// Thrown then a required configuration parameter is not supplied
	/// </summary>
	public class ConfigurationValueRequiredException : ConfigurationException
	{
		/// <summary>
		/// Creates a new ConfigurationException
		/// </summary>
		/// <param name="key">The name of the parameter which caused the exception or which was expected</param>
		/// <param name="innerException">Any inner exception</param>
		public ConfigurationValueRequiredException(
			string key, Exception innerException = null
		) : base(key, null, string.Format("Configuration for key {0} is required but not present", key), innerException)
		{
		}
	}

	/// <summary>
	/// Thrown when a configuration parameter is specified but fails to pass one of validators on a field or property
	/// </summary>
	public class ConfigurationValueValidationException : ConfigurationException
	{
		/// <summary>
		/// Creates a new ConfigurationException
		/// </summary>
		/// <param name="key">The name of the parameter which caused the exception or which was expected</param>
		/// <param name="value">The value of the parameter or null if it is not present</param>
		/// <param name="validatorType">The type of validator which the value failed to pass</param>
		/// <param name="innerException">Any inner exception</param>
		public ConfigurationValueValidationException(
			string key, object value, Type validatorType, Exception innerException = null
		)
			: base(key, value, string.Format("Configuration value {0} for key {1} fails validator {2}", value, key, validatorType.Name), innerException)
		{
		}
	}
}
