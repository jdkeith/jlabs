﻿using System.Collections.Generic;
using System.IO;
using System.Reflection.Extensions;
using System.Linq;
using System.Reflection;
using System.Xml.Linq;
using System.Xml.XPath;

// ReSharper disable once CheckNamespace
namespace System.Configuration.Extensions
{
	/// <summary>
	/// Provides the configuration file-reading properties that Microsoft "forgot"
	/// Supports DataAnnotations
	/// </summary>
	/// <remarks>
	/// Only single-level complex object filling is supported at this time
	/// </remarks>
	public static class ConfigurationExtensions
	{
		#region App Settings

		/// <summary>
		/// Creates an instance of an object with properties and fields filled
		/// from app settings values in the given configuration file
		/// </summary>
		/// <param name="config">The configuration file to get app settings from</param>
		/// <param name="type">The type of object to create</param>
		/// <returns>
		/// An object of the given type or implementing the given interface filled with
		/// the app setting values from the configuration file
		/// </returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if either parameter is null
		/// </exception>
		/// <remarks>
		/// Only interfaces, or non-abstract types with a parameterless constructor may be created
		/// </remarks>
		public static object CreateFromAppSettings(this Configuration config, Type type)
		{
			if( config == null )
				throw new ArgumentNullException("config");

			if(type == null)
				throw new ArgumentNullException("type");

			var status = TypeExtensions.TryCreateInstanceOf(type);

			status.ThrowIfException();

			FillFromAppSettingsInternal(config, status.Data, type);

			return status.Data;
		}

		/// <summary>
		/// Creates an instance of an object with properties and fields filled
		/// from app settings values in the given configuration file
		/// </summary>
		/// <typeparam name="T">The type of object to create</typeparam>
		/// <param name="config">The configuration file to get app settings from</param>
		/// <returns>
		/// An object of the given type or implementing the given interface filled with
		/// the app setting values from the configuration file
		/// </returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the configuration paramter is null
		/// </exception>
		/// <remarks>
		/// Only interfaces, or non-abstract types with a parameterless constructor may be created
		/// </remarks>
		public static T CreateFromAppSettings<T>(this Configuration config)
		{
			return (T) CreateFromAppSettings(config, typeof (T));
		}

		/// <summary>
		/// Fills an instance of an object with properties and fields filled
		/// from app settings values in the given configuration file
		/// </summary>
		/// <typeparam name="T">The type of object to fill</typeparam>
		/// <param name="config">The configuration file to get app settings from</param>
		/// <param name="destinationItem">The item to fill</param>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if either parameter is null
		/// </exception>
		/// <remarks>
		/// Only interfaces, or non-abstract types with a parameterless constructor may be created
		/// </remarks>
		public static void FillFromAppSettings<T>(this Configuration config, T destinationItem)
		{
			FillFromAppSettingsInternal(config, destinationItem, typeof(T));
		}

		private static void FillFromAppSettingsInternal(Configuration config, object destinationItem, Type exposedAsType)
		{
			if (ReferenceEquals(null, destinationItem))
				throw new ArgumentNullException("destinationItem");

			var rawAppSettings = new Dictionary<string, string[]>();

			foreach (var key in config.AppSettings.Settings.AllKeys)
			{
				rawAppSettings[key.ToLower()] = ToConfigValue(config.AppSettings.Settings[key].Value);
			}

			destinationItem.FillRaw(rawAppSettings, true, true, MemberKeyGenerator, parserMapper: ParserMapper, exposedAsType: exposedAsType);
		}

		#endregion App Settings

		#region Config Sections

		/// <summary>
		/// Creates an instance of an object with properties and fields filled
		/// from the specified section in the given configuration file
		/// </summary>
		/// <param name="config">The configuration file to get section settings from</param>
		/// <param name="type">The type of object to create</param>
		/// <param name="sectionName">
		/// The case-insensitive name of the section to pull fill values from.
		/// If no section name is specified, the correct section will be inferred from
		/// the type associated with the section. In the case of more than one such
		/// section, the first one will be used.
		/// </param>
		/// <returns>
		/// An object of the given type or implementing the given interface filled with
		/// the section values as specified if one is found, null otherwise.
		/// </returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the config or type parameters are null
		/// </exception>
		/// <remarks>
		/// Only interfaces, or non-abstract types with a parameterless constructor may be created
		/// </remarks>
		public static object CreateFromSection(this Configuration config, Type type, string sectionName = null)
		{
			if (config == null)
				throw new ArgumentNullException("config");

			if (type == null)
				throw new ArgumentNullException("type");

			var status = TypeExtensions.TryCreateInstanceOf(type);

			status.ThrowIfException();

			FillFromSectionInternal(config, status.Data, type, sectionName);

			return status.Data;
		}

		/// <summary>
		/// Creates an instance of an object with properties and fields filled
		/// from the specified section in the given configuration file
		/// </summary>
		/// <typeparam name="T">The type of object to create</typeparam>
		/// <param name="config">The configuration file to get section settings from</param>
		/// <param name="sectionName">
		/// The case-insensitive name of the section to pull fill values from.
		/// If no section name is specified, the correct section will be inferred from
		/// the type associated with the section. In the case of more than one such
		/// section, the first one will be used.
		/// </param>
		/// <returns>
		/// An object of the given type or implementing the given interface filled with
		/// the section values as specified if one is found, null otherwise.
		/// </returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the config or type parameters are null
		/// </exception>
		/// <remarks>
		/// Only interfaces, or non-abstract types with a parameterless constructor may be created
		/// </remarks>
		public static T CreateFromSection<T>(this Configuration config, string sectionName = null) where T : class
		{
			return (T) CreateFromSection(config, typeof (T), sectionName);
		}

		/// <summary>
		/// Fills an instance of an object with properties and fields filled
		/// from the specified section in the given configuration file
		/// </summary>
		/// <typeparam name="T">The type of object to fill</typeparam>
		/// <param name="config">The configuration file to get section settings from</param>
		/// <param name="destinationInstance">The item to fill</param>
		/// <param name="sectionName">
		/// The case-insensitive name of the section to pull fill values from.
		/// If no section name is specified, the correct section will be inferred from
		/// the type associated with the section. In the case of more than one such
		/// section, the first one will be used.
		/// </param>
		/// <returns>
		/// An object of the given type or implementing the given interface filled with
		/// the section values as specified if one is found, null otherwise.
		/// </returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the config or item parameters are null
		/// </exception>
		/// <remarks>
		/// Only interfaces, or non-abstract types with a parameterless constructor may be created
		/// </remarks>
		public static void FillFromSection<T>(this Configuration config, T destinationInstance, string sectionName = null)
		{
			FillFromSectionInternal(config, destinationInstance, typeof(T), sectionName);
		}

		private static void FillFromSectionInternal(
			this Configuration config, object destinationInstance, Type exposedAsType, string sectionName = null
		)
		{
			if (ReferenceEquals(null, destinationInstance))
				throw new ArgumentNullException("destinationInstance");

			var sectionElement = string.IsNullOrWhiteSpace(sectionName)
				? GetSectionElement(config, exposedAsType)
				: GetSectionElement(config, sectionName);

			if (sectionElement == null)
				return;

			var memberAssignments = MemberAssignment.GetLookupFromType(
				destinationInstance.GetType(), true, MemberKeyGenerator
			);

			var rawSectionValues = GetRawSectionValues(sectionElement, memberAssignments);

			destinationInstance.FillRaw(rawSectionValues, true, true, MemberKeyGenerator, parserMapper: ParserMapper, exposedAsType: exposedAsType);
		}

		#endregion Config Sections

		#region Create, Fill, and Validate

		private static IEnumerable<string> MemberKeyGenerator(MemberInfo member)
		{
			return new[]
			{
				member.Name,
				member.Name.ToLower(),
				member.Name.Replace("_", "").ToLower()
			};
		}

		private static IParser<object> ParserMapper(IMemberAssignment memberAssignment)
		{
			if (memberAssignment.ParserType == typeof (FileInfo))
				return new RelativeFilePathParser(PathExtensions.GetExecutingBaseDirectory());

			if (memberAssignment.ParserType == typeof (DirectoryInfo))
				return new RelativeDirectoryPathParser(PathExtensions.GetExecutingBaseDirectory());

			return null;
		}

		private static string[] ToConfigValue(string original)
		{
			return string.IsNullOrWhiteSpace(original)
				? new string[0]
				: original.Trim().Split(',');
		}

		#endregion Create, Fill, and Validate

		#region Configuration Walker

		private static XElement GetSectionElement(Configuration config, string sectionName)
		{
			var doc = XDocument.Load(config.FilePath);

			if (doc.Root == null)
				return null;

			return doc.Root.Elements().FirstOrDefault(e => string.Equals(e.Name.ToString(), sectionName.Trim(), StringComparison.CurrentCultureIgnoreCase));
		}

		private static XElement GetSectionElement(Configuration config, Type exposedAsType)
		{
			var doc = XDocument.Load(config.FilePath);

			if (doc.Root == null || doc.Root.Name.ToString().ToLower() != "configuration")
				return null;

			var sectionDefinitionsGrouped = doc.Root.XPathSelectElements("/configuration/configSections/sectionGroup/section");
			var sectionDefinitionsLoose = doc.Root.XPathSelectElements("/configuration/configSections/section");

			var combined = sectionDefinitionsGrouped.Concat(sectionDefinitionsLoose).ToArray();

			Predicate<string> matchesTypeName = t =>
				t.Trim().ToLower().StartsWith(exposedAsType.Name.ToLower()) ||
				t.Trim().ToLower().StartsWith(exposedAsType.FullName.ToLower());

			var section = combined
				.Where(c => c.Attributes().Any(a => a.Name == "type" && matchesTypeName(a.Value)))
				.Join(
					// ReSharper disable once PossibleNullReferenceException
					doc.Root.Elements(),
					c => c.Attributes().Where(a => a.Name == "name").Select(a => a.Value.ToLower()).FirstOrDefault(),
					s => s.Name.ToString().ToLower(),
					(c,s) => s
				)
				.FirstOrDefault();

			return section;
		}

		private static Dictionary<string, string[]> GetRawSectionValues(
			XElement sectionElement,
			Dictionary<string, MemberAssignment> memberAssignments
		)
		{
			var result = new Dictionary<string, string[]>();

			foreach (var kvp in memberAssignments)
			{
				var element = sectionElement.Elements().FirstOrDefault(e => e.Name == kvp.Key);

				if (element == null)
					continue;

				var values = kvp.Value.IsEnumerable
					? element.Elements().Select(e => e.Value.Trim()).ToArray()
					: ToConfigValue(element.Value);

				result[kvp.Key] = values;
			}

			return result;
		}

		#endregion Configuration Walker
	}
}