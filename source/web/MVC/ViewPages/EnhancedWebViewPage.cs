﻿using System;
using System.Collections.Generic;
using System.Collections.Generic.Extensions;
using System.Linq;
using System.Globalization;
using System.Linq.Expressions;
using System.Resources;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.WebPages;

using PXL.Web.Mvc.Utilities;
using PXL.Web.Mvc.ViewHelpers;


// ReSharper disable once CheckNamespace
namespace PXL.Web.Mvc.Html
{
	/// <summary>
	/// Creates a new EnhancedWebViewPage
	/// </summary>
	/// <typeparam name="TModel">The type of ViewModel</typeparam>
	/// <typeparam name="TAccount">The type of User model</typeparam>
	public abstract class EnhancedWebViewPage<TModel, TAccount> : WebViewPage<TModel>
		where TModel : class
		where TAccount : class, IIdentity		
	{
		/// <summary>
		/// A helper which handles Html5-related functionality
		/// </summary>
		public Html5Helper<TModel> Html5 { get; private set; }

		/// <summary>
		/// A helper which handles deferred XMLHttpRequest calls
		/// </summary>
		public DeferredHelper<TModel> Defer { get; private set; }

		/// <summary>
		/// A helper which handles javascript functionality
		/// </summary>
		public JavascriptHelper<TModel> Js { get; private set; }

		/// <summary>
		/// Initializes the helpers
		/// </summary>
		public override void InitHelpers()
		{
			base.InitHelpers();
			Html5 = new Html5Helper<TModel>(base.ViewContext, this);
			Defer = null; //todo new DeferredHelper<TModel>(base.ViewContext, this);
			Js = new JavascriptHelper<TModel>(base.ViewContext, this);
		}

		#region Authentication

		/// <summary>
		/// True if the user is logged in, false otherwise
		/// </summary>
		public bool IsLoggedIn
		{
			get { return User != null; }
		}

		/// <summary>
		///	Determines whether the current principal belongs to the specified role.
		/// </summary>
		/// <param name="role">
		/// The name of the role for which to check membership.
		/// </param>
		/// <returns>
		///	true if the current principal is a member of the specified role; otherwise, false.
		///	</returns>
		public bool IsInRole(string role)
		{
			var user = ViewContext.HttpContext.User;

			if (user == null)
				return false;

			return user.IsInRole(role);
		}

		/// <summary>
		/// Returns the current user or null if no user is logged in
		/// </summary>
		public new TAccount User
		{
			get
			{
				var user = ViewContext.HttpContext.User;

				if (user == null)
					return null;

				return user.Identity as TAccount;
			}
		}

		#endregion Authentication

		#region -- old -- Conditionals

		/// <summary>
		/// Returns a string depending on whether or not an item is null
		/// </summary>
		/// <typeparam name="TS">A nullable type</typeparam>
		/// <param name="nullable">The item to check for nullity</param>
		/// <param name="whenIsNotNull">A function which receives a non-null structure and returns a string</param>
		/// <param name="whenIsNull">The string to display when the item is null, default is empty string</param>
		/// <returns>The string computed from whenIsNotNull or whenIsNull as appropriate</returns>
		public string IfNN<TS>(TS? nullable, Func<TS, string> whenIsNotNull, string whenIsNull = "") where TS : struct
		{
			if (!nullable.HasValue)
				return whenIsNull;

			return whenIsNotNull(nullable.Value);
		}

		/// <summary>
		/// Returns a string depending on whether or not an item is null
		/// </summary>
		/// <typeparam name="TS">A nullable type</typeparam>
		/// <param name="nullable">The item to check for nullity</param>
		/// <param name="whenIsNotNull">A string to display when the item is not null</param>
		/// <param name="whenIsNull">The string to display when the item is null, default is empty string</param>
		/// <returns>The string computed from whenIsNotNull or whenIsNull as appropriate</returns>
		public string IfNN<TS>(TS? nullable, string whenIsNotNull, string whenIsNull = "") where TS : struct
		{
			if (!nullable.HasValue)
				return whenIsNull;

			return whenIsNotNull;
		}

		/// <summary>
		/// Returns a string depending on whether or not an item is null
		/// </summary>
		/// <typeparam name="TC">A nullable type</typeparam>
		/// <param name="nullable">The item to check for nullity</param>
		/// <param name="whenIsNotNull">A function which receives a the non-null reference and returns a string</param>
		/// <param name="whenIsNull">The string to display when the item is null, default is empty string</param>
		/// <returns>The string computed from whenIsNotNull or whenIsNull as appropriate</returns>
		public string IfNN<TC>(TC nullable, Func<TC, string> whenIsNotNull, string whenIsNull = "") where TC : class
		{
			if (nullable == null)
				return whenIsNull;

			return whenIsNotNull(nullable);
		}

		/// <summary>
		/// Returns a string depending on whether or not an item is null
		/// </summary>
		/// <typeparam name="TC">A nullable type</typeparam>
		/// <param name="nullable">The item to check for nullity</param>
		/// <param name="whenIsNotNull">A string to display when the item is not null</param>
		/// <param name="whenIsNull">The string to display when the item is null, default is empty string</param>
		/// <returns>The string computed from whenIsNotNull or whenIsNull as appropriate</returns>
		public string IfNN<TC>(TC nullable, string whenIsNotNull, string whenIsNull = "") where TC : class
		{
			return nullable != null ? whenIsNotNull : whenIsNull;
		}

		/// <summary>
		/// Returns a string depending on whether a condition is true
		/// </summary>
		/// <param name="condition">A condition to test for truth</param>
		/// <param name="whenTrue">A string to display when the condition is true</param>
		/// <param name="whenFalse">The string to display when the condition is false, default is empty string</param>
		/// <returns>The string computed from whenTrue or whenFalse as appropriate</returns>
		public string If(bool condition, string whenTrue, string whenFalse = "")
		{
			return condition ? whenTrue : whenFalse;
		}

		#endregion Conditionals

		#region Values

		/// <summary>
		/// Gets the value of a Model-accessing expression in a way which results in null if any
		/// of the items in the expression's tail are null
		/// </summary>
		/// <typeparam name="T">The value type to return</typeparam>
		/// <param name="expression">The unsafe expression</param>
		/// <returns>The resulting value of the expression or null if any of the invocants are null</returns>
		/// <remarks>
		/// This does NOT shield against NullReferenceExceptions or NullArgumentExceptions in method calls
		/// </remarks>
		public T? VN<T>(Expression<Func<TModel, T>> expression)
			where T : struct
		{
			var func = new ExpressionSafetyVisitor().MakeSafeNullable(expression);

			return func(ViewData.Model);
		}

		/// <summary>
		/// Gets the value of a Model-accessing expression in a way which results in null if any
		/// of the items in the expression's tail are null
		/// </summary>
		/// <typeparam name="T">The value type to return</typeparam>
		/// <param name="expression">The unsafe expression</param>
		/// <returns>The resulting value of the expression or null if any of the invocants are null</returns>
		/// <remarks>
		/// This does NOT shield against NullReferenceExceptions or NullArgumentExceptions in method calls
		/// </remarks>
		public T V<T>(Expression<Func<TModel, T>> expression)
			where T : class
		{
			var func = new ExpressionSafetyVisitor().MakeSafe(expression);

			return func(ViewData.Model);
		}

		#endregion Values

		#region Translations

		/// <summary>
		/// Gets the appropriate resource manager for on-page translation functionality
		/// </summary>
		/// <param name="culture">The culture of the resource reader to get</param>
		/// <returns>The resource reader appropriate for the current culture, null otherwise</returns>
		protected abstract ResourceReader GetResourceReader(string culture);

		/// <summary>
		/// Gets the current culture
		/// </summary>
		/// <returns>The string name of the current culture</returns>
		public string T()
		{
			return CultureInfo.CurrentUICulture.Name;
		}	

		/// <summary>
		/// Translates a given key
		/// </summary>
		/// <param name="key">The key to translate</param>
		/// <param name="values">Any objects which are used to fill placeholders in the value returned by the key</param>
		/// <returns>A fully translated key with values placed into placeholders</returns>
		/// <remarks>
		/// If the key is not found, it is filled with given values and returned
		/// </remarks>
		public string T(string key, params object[] values)
		{
			var reader = GetResourceReader(T());

			try
			{
				if (reader == null)
				{
					return string.Format(key, values);
				}

				using (reader)
				{
					byte[] data;
					string type;

					reader.GetResourceData(key, out type, out data);

					return string.Format(System.Text.Encoding.UTF8.GetString(data), values);
				}
			}
			catch (MissingManifestResourceException)
			{
				return string.Format(key, values);
			}
		}

		#endregion Translations

		#region Lists

		/// <summary>
		/// Gets an empty enumerable of type TL
		/// </summary>
		/// <typeparam name="TL">The type of enumerable to return</typeparam>
		/// <returns>An empty enumerable of type TL</returns>
		public IEnumerable<TL> L<TL>()
		{
			return Enumerable.Empty<TL>();
		}

		/// <summary>
		/// Captures and returns a FirstLastEnumerable enumerable, to prevent double-wrapping
		/// </summary>
		/// <typeparam name="TL">The type of FirstLastEnumerable item</typeparam>
		/// <param name="input">The list to wrap</param>
		/// <returns>The exact same list as input</returns>
		public IEnumerable<IndexedItem<TL>> L<TL>(IEnumerable<IndexedItem<TL>> input)
		{
			return input;
		}

		/// <summary>
		/// Converts a generic enumerable into a FirstLastEnumerable enumerable
		/// </summary>
		/// <typeparam name="TL">The type of list item</typeparam>
		/// <param name="input">The list to wrap</param>
		/// <returns>A wrapped list</returns>
		public IEnumerable<IndexedItem<TL>> L<TL>(IEnumerable<TL> input)
		{
			return input.ToIndexed();
		}

		/// <summary>
		/// Renders content for each item in a list
		/// </summary>
		/// <typeparam name="TL">The type of list item</typeparam>
		/// <param name="input">The list to wrap and output</param>
		/// <param name="template">A Razor template which produces output</param>
		/// <returns>A HelperResult for rendering content</returns>
		/// <remarks>
		/// The passed list is wrapped before passing it to the template
		/// </remarks>
		public HelperResult L<TL>(IEnumerable<TL> input, Func<IndexedItem<TL>, HelperResult> template)
		{
			return new HelperResult(writer =>
			{
				foreach (var item in input.ToIndexed())
				{
					template(item).WriteTo(writer);
				}
			});
		}

		/// <summary>
		/// Renders content for each item in a list, but doesn't double-wrap it
		/// </summary>
		/// <typeparam name="TL">The type of FirstLastEnumerable list item</typeparam>
		/// <param name="input">The list to use</param>
		/// <param name="template">A Razor template which produces output</param>
		/// <returns>A HelperResult for rendering content</returns>
		public HelperResult L<TL>(IEnumerable<IndexedItem<TL>> input, Func<IndexedItem<TL>, HelperResult> template)
		{
			return new HelperResult( writer => {
				foreach( var item in input )
				{
					template(item).WriteTo(writer);
				}
			});
		}

		#endregion Lists

		#region Debug

		/// <summary>
		/// Renders content if debugging is enabled
		/// </summary>
		/// <param name="obj">The object to render</param>
		/// <returns>
		/// The content returned as a string or the string [NULL] if obj is null
		/// wrapped in a classed span
		/// if debugging is enabled, null otherwise
		/// </returns>
		/// <remarks>The css class of the span is "debug"</remarks>
		public IHtmlString Debug(object obj)
		{
			if (HttpContext.Current.IsDebuggingEnabled == false)
				return null;

			return new MvcHtmlString("<span class=\"debug\">" + (obj ?? "[NULL]").ToString() + "</span>");
		}

		#endregion Debug

		#region Todo

		/// <summary>
		/// Puts a note in a page which Resharper and other tools can find
		/// </summary>
		/// <param name="message">The message to display on the page</param>
		/// <returns>The todo message in a classed span</returns>
		/// <remarks>The css class of the span is "todo"</remarks>
		[Obsolete("There are web page elements which are not complete")]
		public IHtmlString Todo(string message)
		{
			return new MvcHtmlString("<span class=\"todo\">" + message + "</span>");
		}

		/// <summary>
		/// Puts a note in a page which Resharper and other tools can find
		/// by wrapping an HTML section
		/// </summary>
		/// <param name="template">The content to wrap</param>
		/// <returns>The content wrapped in a classed div</returns>
		/// <remarks>
		/// <para>The css class of the span is "todo"</para>
		/// <para>
		///		The item value passed to the content template is
		///		a boolean indicating whether web debuggin is enabled
		///	</para>
		/// </remarks>
		[Obsolete("There are web page elements which are not complete")]
		public HelperResult Todo(Func<bool,HelperResult> template)
		{
			return new HelperResult(writer =>
			{
				writer.Write("<div class=\"todo\">");
				template(HttpContext.Current.IsDebuggingEnabled).WriteTo(writer);
				writer.Write("</div>");
			});
		}

		#endregion Todo
	}
}