﻿using JLabs.Web.Mvc.Utilities;

// ReSharper disable once CheckNamespace
namespace System.Web.Mvc
{
	/// <summary>
	/// An action which redirects to the URL which the request came from
	/// </summary>
	public class RedirectToReferrerResult : ActionResult
	{
		/// <summary>
		/// Executes the action result
		/// </summary>
		/// <param name="context">The context the action result is executed within</param>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the context argument is null
		/// </exception>
		public override void ExecuteResult(ControllerContext context)
		{
			if (context == null)
				throw new ArgumentNullException("context");

			var urlReferrer = context.RequestContext.HttpContext.Request.UrlReferrer;

			if (urlReferrer == null)
				return;

			var url = urlReferrer.ToString();

			if (context.Controller.ViewData.ModelState.IsValid == false)
				context.Controller.TempData[Constants.TempDataViewDataKey] = context.Controller.ViewData;

			new RedirectResult(url).ExecuteResult(context);
		}
	}
}
