﻿using System.Linq.Expressions;
using System.Linq.Expressions.Extensions;

using PXL.Web.Mvc.Utilities;
using PXL.Web.Utilities;

// ReSharper disable once CheckNamespace
namespace System.Web.Mvc
{
	/// <summary>
	/// An action which redirects to the URL defined by the Linq expression
	/// </summary>
	public class RedirectToExpressionResult : ActionResult
	{
		/// <summary>
		/// Creates a new RedirectToExpressionResult
		/// </summary>
		/// <param name="expression">A linq expression which will be converted to a RouteValues dictionary</param>
		/// <param name="values">The values to use as parameters to the expression</param>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the expression argument is null
		/// </exception>
		public RedirectToExpressionResult(LambdaExpression expression, params object[] values)
		{
			if (expression == null)
				throw new ArgumentNullException("expression");

			Expression = expression;
			Values = values;
		}
	
		/// <summary>
		/// A linq expression which will be converted to a RouteValues dictionary
		/// </summary>
		public LambdaExpression Expression { get; private set; }

		/// <summary>
		/// The values to use as parameters to the expression
		/// </summary>
		public object[] Values { get; private set; }

		/// <summary>
		/// Executes the action result
		/// </summary>
		/// <param name="context">The context the action result is executed within</param>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the context argument is null
		/// </exception>
		public override void ExecuteResult(ControllerContext context)
		{
			if (context == null)
				throw new ArgumentNullException("context");

			var key = RouteExpressionConverter.FKey(Expression.ToKey());

			ParRouteDictionary prd;

			if (!context.HttpContext.Cache.TryGetAs(key, out prd))
			{
				var expressionConverter = new RouteExpressionConverter();

				prd = expressionConverter.GetRouteValues(Expression);

				if (key != null)
				{
					context.HttpContext.Cache[key] = prd;
				}
			}

			var rvd = prd.Complete(Values);

			new RedirectToRouteResult(rvd).ExecuteResult(context);
		}
	}
}
