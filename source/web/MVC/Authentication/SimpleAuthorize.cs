﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;
using JLabs.Web.Mvc;

namespace PXL.Web.Mvc.Authentication
{
	public enum AuthorizationFailureAction
	{
		RedirectToLogin,
		Http404
	}

	/// <summary>
	/// Provides simple role-based authorization filtering and supports enum-based roles
	/// </summary>
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false, Inherited = false)]
	public class SimpleAuthorizeAttribute : System.Web.Mvc.ActionFilterAttribute
	{
		public partial class RequireAllAttribute { }
		public partial class RequireAnyAttribute { }

		/// <summary>
		/// Creates a new SimpleAuthorizeAttribute
		/// </summary>
		/// <param name="requiredRoles">The roles - all of which the user must be in</param>
		public SimpleAuthorizeAttribute(params string[] requiredRoles)
		{
			Roles = requiredRoles ?? Enumerable.Empty<string>();

			// just in case someone went all comma-happy
			Roles = Roles.SelectMany(r => r.Split(','));
		}

		/// <summary>
		/// Creates a new SimpleAuthorizeAttribute
		/// </summary>
		/// <param name="enumRequiredRoles">The enumeration value of the roles - all of which the user must be in</param>
		public SimpleAuthorizeAttribute(object enumRequiredRoles)
		{
			var et = enumRequiredRoles.GetType();

			if (et.IsEnum == false)
				throw new ArgumentException("enumRoles argument must be an enumeration type", "enumRoles");

			Roles = EnumRolesPrincipal.RolesFromEnum((Enum)enumRequiredRoles);
		}

		public AuthorizationFailureAction FailAction { get; set; }

		/// <summary>
		/// A list of roles all of which a visiting user must meet to be granted access
		/// </summary>
		public new IEnumerable<string> Roles { get; private set; }

		public override void OnActionExecuting(ActionExecutingContext filterContext)
		{
			bool isAuthorized;

			var principal = filterContext.HttpContext.User;

			// it's allowed if there's an AllowAnonymous
			if (filterContext.Controller.ControllerContext.GetAttributes<AllowAnonymousAttribute>().Any())
			{
				isAuthorized = true;
			}
			else if (principal == null || principal.Identity == null || !principal.Identity.IsAuthenticated)
			{
				isAuthorized = false;
			}
			else
			{
				isAuthorized = Roles.All(principal.IsInRole);
			}

			if (!isAuthorized && FailAction == AuthorizationFailureAction.Http404)
			{
				filterContext.Result = new HttpNotFoundResult();
				return;
			}
			else if (!isAuthorized && FailAction == AuthorizationFailureAction.RedirectToLogin)
			{
				var currentUrl = filterContext.HttpContext.Request.Url.PathAndQuery;
				currentUrl = filterContext.HttpContext.Server.UrlEncode(currentUrl);
				currentUrl = FormsAuthentication.LoginUrl + "?redirectTo=" + currentUrl;

				filterContext.Result = new RedirectResult(currentUrl);
				return;
			}

			base.OnActionExecuting(filterContext);
		}
	}
}