﻿//using System;
//using System.Security.Cryptography;
//using System.Web.Routing;
//using JLabs.Service.Authentication;

//// ReSharper disable CheckNamespace
//namespace JLabs.Web.Mvc.Authentication
//// ReSharper restore CheckNamespace
//{
//    /// <summary>
//    /// Base implementation of forms authentication service which handles unsuccessful login lockouts
//    /// </summary>
//    /// <typeparam name="TU">The type which this authentication service authenticates</typeparam>
//    public abstract class LockoutFormAuthenticationWrapperBase<TU> : FormAuthenticationWrapperBase<TU> where TU : class, ITrackedLogin
//    {
//        private ushort _lockoutSeconds;
//        private byte _maxAttempts;

//        /// <summary>
//        /// Creates a new authentication wrapper
//        /// </summary>
//        /// <param name="requestContext">The request context of the current call</param>
//        /// <param name="hashAlgorithm">
//        /// The algorithm used to compute the password hash. If unspecified or null, SHA256 is used
//        /// </param>
//        /// <param name="cookieDurationMinutes">The cookie timeout duration in minutes</param>
//        /// <param name="saltLength">The length of the salt, default is 8</param>
//        /// <param name="maxAttempts">The maximum number of unsuccessful login attempts before locking the user out</param>
//        /// <param name="lockoutSeconds">The duration for the lockout, in seconds</param>
//        /// <remarks>
//        /// If not specified, the cookie duration uses the web.config settings
//        /// </remarks>
//        /// <exception cref="System.ArgumentNullException">
//        /// Thrown if the requestContext argument is null
//        /// </exception>
//        protected LockoutFormAuthenticationWrapperBase(
//            RequestContext requestContext, HashAlgorithm hashAlgorithm = null,
//            int? cookieDurationMinutes = null, int saltLength = 8,
//            byte maxAttempts = (byte) 3, ushort lockoutSeconds = (byte) 180
//        ) : base(requestContext, hashAlgorithm, cookieDurationMinutes, saltLength)
//        {
//            _maxAttempts = maxAttempts;
//            _lockoutSeconds = lockoutSeconds;
//        }

//        /// <summary>
//        /// Authenticates a user AND signs the user in if authenticated
//        /// </summary>
//        /// <param name="user">The user to authenticate</param>
//        /// <param name="password">The password to authenticate with</param>
//        /// <param name="isPersistent">Whether or not the user is authenticating persistently</param>
//        /// <returns>True if the user authenticates, false otherwise</returns>
//        public override bool AuthenticateSignIn(TU user, string password, bool isPersistent)
//        {
//            if (user == null)
//                return false;

//            if (IsPassword(user, password))
//            {
//                user.LastSuccessfulLogin = DateTime.Now;
//                SignIn(user, isPersistent);

//                SaveChanges(user);

//                return true;
//            }

//            // lockout reset if beyond this time
//            var lockoutReset = user.LastUnsuccessfulLogin.HasValue && DateTime.Now.AddSeconds(-1 * _lockoutSeconds) > user.LastUnsuccessfulLogin.Value;

//            user.LastUnsuccessfulLogin = DateTime.Now;
//            user.UnsuccessfulLoginAttempts++;

//            if (user.UnsuccessfulLoginAttempts < _maxAttempts)
//            {
//                SaveChanges(user);
//                return false;
//            }

//            if (lockoutReset)
//            {
//                user.UnsuccessfulLoginAttempts = 1;
//                SaveChanges(user);

//                return false;
//            }

//            SaveChanges(user);

//            throw new UserLockedOutException(user, _lockoutSeconds);
//        }
//    }
//}