﻿using System;
using System.Security.Cryptography;
using System.Security.Principal;
using System.Text;
using System.Web;
using System.Web.Configuration;
using System.Web.Routing;
using System.Web.Security;

using PXL.Service.Authentication;

// ReSharper disable CheckNamespace
namespace JLabs.Web.Mvc.Authentication
// ReSharper restore CheckNamespace
{
	/// <summary>
	/// Base implementation of forms authentication service
	/// </summary>
	/// <typeparam name="TU">The type which this authentication service authenticates</typeparam>
	public abstract class FormAuthenticationWrapperBase<TU> : IAuthenticationService<TU> where TU : class, ILogin
	{
		private class TicketCache
		{
			public bool IsPersistent { get; set; }
			public TU CurrentUser { get; set; }
		}

		/// <summary>
		/// The request context of the current call
		/// </summary>
		protected readonly RequestContext RequestContext;

		private int? _cookieDurationMinutes;
		private HashAlgorithm _hashAlgorithm;
		private readonly int _saltLength;

		private TicketCache _cache;

		/// <summary>
		/// Creates a new authentication wrapper
		/// </summary>
		/// <param name="requestContext">The request context of the current call</param>
		/// <param name="hashAlgorithm">
		/// The algorithm used to compute the password hash. If unspecified or null, SHA256 is used
		/// </param>
		/// <param name="cookieDurationMinutes">The cookie timeout duration in minutes</param>
		/// <param name="saltLength">The length of the salt, default is 8</param>
		/// <remarks>
		/// If not specified, the cookie duration uses the web.config settings
		/// </remarks>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the requestContext argument is null
		/// </exception>
		protected FormAuthenticationWrapperBase(RequestContext requestContext, HashAlgorithm hashAlgorithm = null, int? cookieDurationMinutes = null, int saltLength = 8)
		{
			if (requestContext == null)
				throw new ArgumentNullException("requestContext");

			RequestContext = requestContext;
			_cookieDurationMinutes = cookieDurationMinutes;
			_hashAlgorithm = _hashAlgorithm ?? SHA256.Create();
			_saltLength = saltLength;
		}

		/// <summary>
		/// Changes the password of the specified user but DOES NOT update the cookie or save the object
		/// Password validation is also not performed
		/// </summary>
		/// <param name="user">The user to change the password on</param>
		/// <param name="clearTextPassword">The new password</param>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if either the user or clearTextPassword arguments are null
		/// </exception>
		public virtual void SetAuthenticationInfo(TU user, string clearTextPassword)
		{
			if (user == null)
				throw new ArgumentNullException("user");

			if (clearTextPassword == null)
				throw new ArgumentNullException("clearTextPassword");

			user.Salt = user.Salt ?? GenerateSalt();
			user.EncryptedPassword = EncryptPassword(clearTextPassword, user.Salt);
		}

		/// <summary>
		/// Tests the password of the specified user
		/// </summary>
		/// <param name="user">The user to test the password on</param>
		/// <param name="clearTextPassword">The password to try</param>
		/// <returns>True if the user is not null and has the password specified, false otherwise</returns>
		public virtual bool IsPassword(TU user, string clearTextPassword)
		{
			if (user == null)
				return false;

			return user.EncryptedPassword.ToLower() == EncryptPassword(clearTextPassword, user.Salt).ToLower();
		}

		private string EncryptPassword(string clearTextPassword, string salt)
		{
			var saltedPassword = clearTextPassword + salt;

			var hashBytes = _hashAlgorithm.ComputeHash(Encoding.UTF8.GetBytes(saltedPassword));

			return BitConverter.ToString(hashBytes).Replace("-", "");
		}

		/// <summary>
		/// Generates a random salt
		/// </summary>
		/// <returns>A random salt using only lower alpha characters</returns>
		protected virtual string GenerateSalt()
		{
			return GenerateSalt(_saltLength, 'a', 'z');
		}

		/// <summary>
		/// Generates a random salt
		/// </summary>
		/// <param name="saltLength">The length of the salt to return</param>
		/// <param name="lowerDomain">The inclusive lower bound of characters to use</param>
		/// <param name="upperDomain">The inclusive upper bound of characters to use</param>
		/// <returns>A random salt using only characters within the specified bounds</returns>
		protected virtual string GenerateSalt(int saltLength, char lowerDomain, char upperDomain)
		{
			var r = new Random();

			var builder = new StringBuilder();

			while (builder.Length < saltLength)
			{
				builder.Append((char)r.Next(lowerDomain, upperDomain + 1));
			}

			return builder.ToString();
		}

		/// <summary>
		/// Authenticates a user AND signs the user in if authenticated
		/// </summary>
		/// <param name="user">The user to authenticate</param>
		/// <param name="password">The password to authenticate with</param>
		/// <param name="isPersistent">Whether or not the user is authenticating persistently</param>
		/// <returns>True if the user authenticates, false otherwise</returns>
		public virtual bool AuthenticateSignIn(TU user, string password, bool isPersistent)
		{
			if (user == null)
				return false;

			if (IsPassword(user, password) == false)
				return false;

			SignIn(user, isPersistent);

			return true;
		}

		/// <summary>
		/// Signs a user in without authentication
		/// </summary>
		/// <param name="user">The user to sign in</param>
		/// <param name="isPersistent">
		/// Whether or not the user is authenticating persistently.
		/// When null, will try to use the previous setting otherwise will be set to false
		/// </param>
		/// <remarks>
		/// If the user is null, no cookie is created and any previous cookie is removed
		/// </remarks>
		/// <exception cref="System.InvalidOperationException">
		/// Thrown if the user parameter has a session id set
		/// </exception>
		public virtual void SignIn(TU user, bool? isPersistent=null)
		{
			if (user == null)
			{
				SignOut();
				return;
			}

			if (user.SessionId.HasValue)
				throw new InvalidOperationException("Cannot sign in user with an existing session id");

			user.SessionId = (isPersistent ?? false) ? (Guid?) null : Guid.NewGuid();

			if (_cookieDurationMinutes == null)
			{
				var configuration = WebConfigurationManager.OpenWebConfiguration(null);
				var authenticationSection = (AuthenticationSection)configuration.GetSection("system.web/authentication");

				_cookieDurationMinutes = Convert.ToInt32(authenticationSection.Forms.Timeout.TotalMinutes);
			}

			if (!isPersistent.HasValue)
			{
				var previousTicket = GetTicket();

				isPersistent = previousTicket == null ? false : previousTicket.IsPersistent;
			}
			else
			{
				isPersistent = isPersistent ?? false;
			}

			var rawTicket = new FormsAuthenticationTicket(
				1,
				ToUserName(user),
				DateTime.Now,
				DateTime.Now.AddMinutes(_cookieDurationMinutes.Value),
				isPersistent.Value,
				ToUserData(user)
			);

			_cache = new TicketCache
			{
				IsPersistent = rawTicket.IsPersistent,
				CurrentUser = FromTicket(rawTicket.Name, rawTicket.UserData)
			};

			// Encrypt the ticket.
			var encTicket = FormsAuthentication.Encrypt(rawTicket);

			// Create the cookie.
			RequestContext.HttpContext.Response.Cookies.Add(new HttpCookie(FormsAuthentication.FormsCookieName, encTicket));

			RequestContext.HttpContext.User = GetPrincipalFromUser(user);
		}

		/// <summary>
		/// Signs the current user out and removes the cookie
		/// </summary>
		public virtual void SignOut()
		{
			FormsAuthentication.SignOut();

			// overwrite the cached fields
			_cache = null;
			RequestContext.HttpContext.User = GetPrincipalFromUser(null);
		}

		private FormsAuthenticationTicket GetTicket()
		{
			// get the cookie.
			var encTicket = RequestContext.HttpContext.Request.Cookies[FormsAuthentication.FormsCookieName];

			if (encTicket == null || string.IsNullOrEmpty(encTicket.Value))
				return null;

			return FormsAuthentication.Decrypt(encTicket.Value);
		}

		/// <summary>
		/// The current user or null if there is no current user
		/// </summary>
		public TU CurrentUser
		{
			get
			{
				if (_cache != null)
					return _cache.CurrentUser;

				var ticket = GetTicket();

				if (ticket != null)
				{
					_cache = new TicketCache
					{
						IsPersistent = ticket.IsPersistent,
						CurrentUser = FromTicket(ticket.Name, ticket.UserData)
					};

					return _cache.CurrentUser;
				}

				return null;
			}
		}

		/// <summary>
		/// Whether or not the current user is in the specified role
		/// </summary>
		/// <param name="role">The role which the user must be in</param>
		/// <returns>True if the user is not  null and is in the specified role, false otherwise</returns>
		public virtual bool IsInRole(string role)
		{
			var cu = CurrentUser;

			if (cu == null)
				return false;

			return GetPrincipalFromUser(cu).IsInRole(role);
		}

		/// <summary>
		/// Whether or not the current user is in the specified role
		/// </summary>
		/// <param name="roles">The roles which the user must be in</param>
		/// <returns>True if the user is not null and is in ALL of the specified roles, false otherwise</returns>
		public virtual bool IsInAllRoles(params string[] roles)
		{
			var cu = CurrentUser;

			if (cu == null)
				return false;

			var p = GetPrincipalFromUser(cu);
				
			foreach( var role in roles )
			{
				if( ! p.IsInRole(role) )
					return false;
			}

			return true;
		}

		/// <summary>
		/// Whether or not the current user is in the specified role
		/// </summary>
		/// <param name="roles">The roles which the user must be in</param>
		/// <returns>True if the user is not null and is in ANY of the specified roles, false otherwise</returns>
		public virtual bool IsInAnyRole(params string[] roles)
		{
			var cu = CurrentUser;

			if (cu == null)
				return false;

			var p = GetPrincipalFromUser(cu);

			foreach (var role in roles)
			{
				if (p.IsInRole(role))
					return true;
			}

			return false;
		}

		/// <summary>
		/// Whether or not the current user is logged in persistently
		/// </summary>
		/// <remarks>
		/// If there is no user or cookie, this always returns false
		/// </remarks>
		public virtual bool IsPersistent
		{
			get
			{
				var ticket = GetTicket();

				if (ticket != null)
					return ticket.IsPersistent;

				return false;
			}
		}

		/// <summary>
		/// Provides the username of the current user
		/// </summary>
		/// <param name="user">The user to provide the user name for</param>
		/// <returns>The username of the specified user</returns>
		/// <remarks>
		/// Cookie storage is extremely limited, especially when encrypted. It is
		/// advisable to only store the minimum data possible
		/// </remarks>
		protected abstract string ToUserName(TU user);

		/// <summary>
		/// Converts the user into a string representation suitable for cookie storage
		/// </summary>
		/// <param name="user">The user to create a string representation for</param>
		/// <returns>A cookie-friendly string representation</returns>
		/// <remarks>
		/// Cookie storage is extremely limited, especially when encrypted. It is
		/// advisable to only store the minimum data possible
		/// </remarks>
		protected abstract string ToUserData(TU user);

		/// <summary>
		/// Creates a user instance from a cookie-sourced string representation
		/// </summary>
		/// <param name="ticketName">The username of the user</param>
		/// <param name="ticketData">A string representation of all other stored user properties</param>
		/// <returns>The user represented by the cookie</returns>
		/// <remarks>
		/// This method may be called A LOT. Database user retrievals are NOT recommended.
		/// </remarks>
		protected abstract TU FromTicket(string ticketName, string ticketData);

		/// <summary>
		/// Gets an IPrincipal from the current user
		/// </summary>
		/// <param name="user">The user to get an IPrincipal from</param>
		/// <returns>An IPrincipal to set into the HttpContext</returns>
		/// <remarks>
		/// This method should not reference HttpContext or its tail or
		/// it may make controllers difficult to test
		/// </remarks>
		protected abstract IPrincipal GetPrincipalFromUser(TU user);
	}
}