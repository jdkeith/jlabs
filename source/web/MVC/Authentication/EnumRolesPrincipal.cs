﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;

namespace PXL.Web.Mvc.Authentication
{
	/// <summary>
	/// Provides a base class for enumeration-based role management
	/// </summary>
	public abstract class EnumRolesPrincipal : IPrincipal
	{
		/// <summary>
		/// Creates a new EnumRolesPrincipal
		/// </summary>
		/// <param name="user">The user (IIdentity) that the IPrincipal is wrapping</param>
		protected EnumRolesPrincipal(IIdentity user)
		{
			Identity = user;
		}

		/// <summary>
		/// The current user
		/// </summary>
		public IIdentity Identity { get; protected set; }

		/// <summary>
		/// Whether or not the current user is in the specified role
		/// </summary>
		/// <param name="role">The role or roles to query the user for membership in</param>
		/// <returns>True if the user is in ALL of the roles, false otherwise</returns>
		public abstract bool IsInRole(string role);

		/// <summary>
		/// Converts an enumeration into a list of string values
		/// </summary>
		/// <param name="value">The enumeration value</param>
		/// <returns>A list of string values for all flags set in the enumeration</returns>
		/// <remarks>
		/// Value should be a Flags enumeration
		/// </remarks>
		public static IEnumerable<string> RolesFromEnum(Enum value)
		{
			return
				from Enum e in Enum.GetValues(value.GetType())
				where value.HasFlag(e)
				select e.ToString();
		}

		/// <summary>
		/// Whether or not the actualRoles enumeration completely covers the requiredRoles enumeration
		/// </summary>
		/// <param name="actualRoles">The roles that the user actually fits</param>
		/// <param name="requiredRoles">The roles that the user must fit</param>
		/// <returns>True if the actualRoles completely encompasses the required roles, false otherwise</returns>
		protected static bool IsInAllRoles(Enum actualRoles, Enum requiredRoles)
		{
			return IsInAllRoles(actualRoles, RolesFromEnum(requiredRoles));
		}

		/// <summary>
		/// Whether or not the actualRoles enumeration completely covers all required roles
		/// </summary>
		/// <param name="actualRoles">The roles that the user actually fits</param>
		/// <param name="requiredRoles">A list of all required roles</param>
		/// <returns>True if the actualRoles completely encompasses the required roles, false otherwise</returns>
		protected static bool IsInAllRoles(Enum actualRoles, IEnumerable<string> requiredRoles)
		{
			return ! requiredRoles.Except(RolesFromEnum(actualRoles)).Any();
		}

		/// <summary>
		/// Whether or not the actualRoles enumeration contains a certain required role
		/// </summary>
		/// <param name="actualRoles">The roles that the user actually fits</param>
		/// <param name="requiredRole">The roles that the user must fit</param>
		/// <returns>True if the actualRoles contains the required role, false otherwise</returns>
		protected static bool IsInRole(Enum actualRoles, string requiredRole)
		{
			try
			{
				var flag = (Enum)Enum.Parse(actualRoles.GetType(), requiredRole, true);

				return actualRoles.HasFlag(flag);
			}
			catch (ArgumentException)
			{
				return false;
			}
		}
	}
}