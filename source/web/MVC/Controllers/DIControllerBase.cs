﻿using System.Linq.Expressions;

using PXL.Service;

// ReSharper disable once CheckNamespace
namespace System.Web.Mvc
{
	/// <summary>
	/// A controller which supports service layer dependency injection
	/// </summary>
	/// <typeparam name="TS">The type of the service layer</typeparam>
	/// <remarks>
	/// For maximum testability, the service layer type should be an interface
	/// </remarks>
	// ReSharper disable once InconsistentNaming
	public abstract class DIControllerBase<TS> : Controller where TS : IServiceLayer
	{
		/// <summary>
		/// Creates a new controller with an injected service layer
		/// </summary>
		/// <param name="serviceLayer">The service layer to inject</param>
		protected DIControllerBase(TS serviceLayer)
		{
			Services = serviceLayer;
		}

		/// <summary>
		/// The service layer
		/// </summary>
		public TS Services { get; private set; }

		/// <summary>
		/// Redirects the action method to the URL which referred the current request
		/// </summary>
		/// <returns>A new RedirectToReferrerResult action</returns>
		protected RedirectToReferrerResult RedirectToReferrer()
		{
			return new RedirectToReferrerResult();
		}

		#region Redirect to Action

		/// <summary>
		/// Redirects the action method to the URL defined by the Linq expression
		/// </summary>
		/// <typeparam name="TController">The controller to redirect to an action method within</typeparam>
		/// <param name="expression">The expression which defines the url to redirect to</param>
		/// <returns>A new RedirectToExpressionResult action</returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the expression argument is null
		/// </exception>
		protected RedirectToExpressionResult RedirectToAction<TController>(
			Expression<Func<TController, ActionResult>> expression
		) where TController : Controller
		{
			return new RedirectToExpressionResult(expression);
		}

		/// <summary>
		/// Redirects the action method to the URL defined by the Linq expression
		/// </summary>
		/// <typeparam name="TController">The controller to redirect to an action method within</typeparam>
		/// <typeparam name="TA">The type of the first parameter to pass in</typeparam>
		/// <param name="expression">The expression which defines the url to redirect to</param>
		/// <param name="a">The first parameter to pass in</param>
		/// <returns>A new RedirectToExpressionResult action</returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the expression argument is null
		/// </exception>
		protected RedirectToExpressionResult RedirectToAction<TController, TA>(
			Expression<Func<TController, TA, ActionResult>> expression, TA a
		) where TController : Controller
		{
			return new RedirectToExpressionResult(expression, a);
		}

		/// <summary>
		/// Redirects the action method to the URL defined by the Linq expression
		/// </summary>
		/// <typeparam name="TController">The controller to redirect to an action method within</typeparam>
		/// <typeparam name="TA">The type of the first parameter to pass in</typeparam>
		/// <typeparam name="TB">The type of the second parameter to pass in</typeparam>
		/// <param name="expression">The expression which defines the url to redirect to</param>
		/// <param name="a">The first parameter to pass in</param>
		/// <param name="b">The second parameter to pass in</param>
		/// <returns>A new RedirectToExpressionResult action</returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the expression argument is null
		/// </exception>
		protected RedirectToExpressionResult RedirectToAction<TController, TA, TB>(
			Expression<Func<TController, TA, TB, ActionResult>> expression, TA a, TB b
		) where TController : Controller
		{
			return new RedirectToExpressionResult(expression, a, b);
		}

		/// <summary>
		/// Redirects the action method to the URL defined by the Linq expression
		/// </summary>
		/// <typeparam name="TController">The controller to redirect to an action method within</typeparam>
		/// <typeparam name="TA">The type of the first parameter to pass in</typeparam>
		/// <typeparam name="TB">The type of the second parameter to pass in</typeparam>
		/// <typeparam name="TC">The type of the third parameter to pass in</typeparam>
		/// <param name="expression">The expression which defines the url to redirect to</param>
		/// <param name="a">The first parameter to pass in</param>
		/// <param name="b">The second parameter to pass in</param>
		/// <param name="c">The third parameter to pass in</param>
		/// <returns>A new RedirectToExpressionResult action</returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the expression argument is null
		/// </exception>
		protected RedirectToExpressionResult RedirectToAction<TController, TA, TB, TC>(
			Expression<Func<TController, TA, TB, TC, ActionResult>> expression, TA a, TB b, TC c
		) where TController : Controller
		{
			return new RedirectToExpressionResult(expression, a, b, c);
		}

		#endregion Redirect to Action

		#region Status

		protected StatusResult Status(IStatus status)
		{
			throw new NotImplementedException();
		}

		protected StatusResult Status<T>(IStatus<T> status)
		{
			throw new NotImplementedException();
		}

		#endregion Status
	}
}
