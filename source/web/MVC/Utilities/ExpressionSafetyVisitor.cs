﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace PXL.Web.Mvc.Utilities
{
	/// <summary>
	/// Makes an expression safe by eliminating NullReferenceExceptions
	/// </summary>
	public class ExpressionSafetyVisitor
	{
		/// <summary>
		/// Rewrites the expression so that any null reference causes the call to return null
		/// </summary>
		/// <typeparam name="TIn">The type which is passed to the function</typeparam>
		/// <typeparam name="TOut">The type which is returned from the function</typeparam>
		/// <param name="expression">The expression to convert</param>
		/// <returns>An expression which will not throw a NullReferenceException when called</returns>
		public Func<TIn, TOut> MakeSafe<TIn, TOut>(Expression<Func<TIn, TOut>> expression)
			where TIn : class
			where TOut : class
		{
			var exp = new ExpressionReferenceVisitorContext<TIn, TOut>().MakeSafe(expression);

			return exp.Compile();
		}

		/// <summary>
		/// Rewrites the expression so that any null reference causes the call to return null
		/// </summary>
		/// <typeparam name="TIn">The type which is passed to the function</typeparam>
		/// <typeparam name="TOut">The type which is returned from the function</typeparam>
		/// <param name="expression">The expression to convert</param>
		/// <returns>An expression which will not throw a NullReferenceException when called</returns>
		public Func<TIn, TOut?> MakeSafeNullable<TIn, TOut>(Expression<Func<TIn, TOut>> expression)
			where TIn : class
			where TOut : struct
		{
			var exp = new ExpressionNullableVisitorContext<TIn, TOut>().MakeSafe(expression);

			return exp.Compile();
		}

		private abstract class ExpressionVisitorContext : ExpressionVisitor
		{
			private LabelTarget _returnLabel;
			private List<ParameterExpression> _variables;
			private List<Expression> _blockContents;
			private Expression _defaultValue;

			protected abstract Expression GetDefaultValue();
			protected abstract LabelTarget GetReturnLabel();
			protected abstract Type GetReturnType();

			protected Expression MakeSafe<TIn, TOut>(Expression<Func<TIn, TOut>> expression)
			{
				_variables = new List<ParameterExpression>();
				_variables.Add(expression.Parameters.First());

				_defaultValue = GetDefaultValue();
				_returnLabel = GetReturnLabel();

				_blockContents = new List<Expression>();

				Visit(expression);

				var retType = GetReturnType();

				_blockContents.Insert(
					0,
					Expression.IfThen(
						Expression.ReferenceEqual(
							Expression.Convert(_variables.First(), typeof(object)),
							Expression.Constant(null)
						),
						Expression.Return(_returnLabel, _defaultValue)
					)
				);

				_blockContents.Add(
					Expression.Return(
						_returnLabel,
						Expression.Convert(
							_variables.Last(),
							retType
						)
					)
				);

				_blockContents.Add(
					Expression.Label(_returnLabel, Expression.Default(retType))
				);

				var block = Expression.Block(
					_variables.Skip(1),
					_blockContents
				);

				return Expression.Lambda(block, _variables.First());
			}

			protected override Expression VisitMethodCall(MethodCallExpression node)
			{
				Visit(node.Object);

				_blockContents.Add(
					Expression.Call(_variables.Last(), node.Method, node.Arguments)
				);

				_variables.Add(
					Expression.Variable(node.Method.ReturnType)
				);

				_blockContents.Add(
					Expression.Assign(_variables.Last(), _blockContents.Last())
				);

				if (node.Method.ReturnType.IsClass == false)
					return node;

				_blockContents.Add(
					Expression.IfThen(
						Expression.ReferenceEqual(
							Expression.Convert(_variables.Last(), typeof(object)),
							Expression.Constant(null)
						),
						Expression.Return(_returnLabel, _defaultValue)
					)
				);

				return node;
			}

			protected override Expression VisitMember(MemberExpression node)
			{
				Visit(node.Expression);

				Type fpType;

				if (node.Member.MemberType == MemberTypes.Field)
				{
					fpType = ((FieldInfo)node.Member).FieldType;

					_blockContents.Add(
						Expression.Field(_variables.Last(), (FieldInfo)node.Member)
					);

					_variables.Add(
						Expression.Variable(fpType)
					);
				}
				else if (node.Member.MemberType == MemberTypes.Property)
				{
					fpType = ((PropertyInfo)node.Member).PropertyType;

					_blockContents.Add(
						Expression.Property(_variables.Last(), (PropertyInfo)node.Member)
					);

					_variables.Add(
						Expression.Variable(fpType)
					);
				}
				else
				{
					throw new NotSupportedException("Member of type '" + node.Member.MemberType + "' not supported");
				}

				_blockContents.Add(
					Expression.Assign(_variables.Last(), _blockContents.Last())
				);

				if (fpType.IsClass == false)
					return node;

				_blockContents.Add(
					Expression.IfThen(
						Expression.ReferenceEqual(
							Expression.Convert(_variables.Last(), typeof(object)),
							Expression.Constant(null)
						),
						Expression.Return(_returnLabel, _defaultValue)
					)
				);

				return node;
			}
		}

		private class ExpressionNullableVisitorContext<TIn, TOut> : ExpressionVisitorContext
			where TIn : class 
			where TOut : struct
		{
			protected override Expression GetDefaultValue()
			{
				return Expression.Default(typeof(TOut?));
			}

			protected override LabelTarget GetReturnLabel()
			{
				return Expression.Label(typeof(TOut?), "ret");
			}

			protected override Type GetReturnType()
			{
				return typeof (TOut?);
			}

			public Expression<Func<TIn, TOut?>> MakeSafe(Expression<Func<TIn, TOut>> expression)
			{
				var lambda = base.MakeSafe(expression);

				return (Expression<Func<TIn, TOut?>>) lambda;
			}
		}

		private class ExpressionReferenceVisitorContext<TIn, TOut> : ExpressionVisitorContext
			where TIn : class
			where TOut : class
		{
			protected override Expression GetDefaultValue()
			{
				return Expression.Default(typeof(TOut));
			}

			protected override LabelTarget GetReturnLabel()
			{
				return Expression.Label(typeof(TOut), "ret");
			}

			protected override Type GetReturnType()
			{
				return typeof(TOut);
			}

			public Expression<Func<TIn, TOut>> MakeSafe(Expression<Func<TIn, TOut>> expression)
			{
				var lambda = base.MakeSafe(expression);

				return (Expression<Func<TIn, TOut>>)lambda;
			}
		}
	}
}
