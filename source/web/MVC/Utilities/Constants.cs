﻿namespace JLabs.Web.Mvc.Utilities
{
	internal static class Constants
	{
		public const string TempDataViewDataKey = "<>tempData";
		public const string ControllerCacheKeyPrefix = "<>controllerActivator_";
		public const string RouteValueConverterCacheKeyPrefix = "<>rvcCache_";
	}
}
