﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Linq.Expressions;
//using System.Web;
//using System.Web.Mvc;
//using System.Web.Routing;

//namespace JLabs.Web.Mvc.Utilities
//{
//    /// <summary>
//    /// Converts a linq expression into a RoutesValueDictionary
//    /// </summary>
//    /// <remarks>
//    /// The route expression converter may cache routes in the application's items property
//    /// </remarks>
//    public class RouteExpressionConverter
//    {
//        #region Fields

//        private HttpApplicationStateBase _applicationCache;

//        private static readonly Dictionary<string, RouteValueDictionary> CachedValueExpressions =
//            new Dictionary<string, RouteValueDictionary>();

////		private static readonly Dictionary<string, IEnumerable<RoutePlaceholder>> CachedPlaceholderExpressions =
//    //		new Dictionary<string, IEnumerable<RoutePlaceholder>>();

//    //	private static readonly Dictionary<string, FormMethod> CachedFormMethods =
//        //	new Dictionary<string, FormMethod>();

//        private readonly bool _expandModelBindParameters;

//        #endregion Fields

//        #region Constructors

//        /// <summary>
//        /// Creates a new RouteExpressionConverter which obtains and stores cached linq expressions in the application cache
//        /// </summary>
//        /// <param name="applicationCache">The application cache to store cached linq expressions in</param>
//        /// <param name="expandModelBindParameters">Whether or not to fully expand model bind parameters</param>
//        public RouteExpressionConverter(HttpApplicationStateBase applicationCache, bool expandModelBindParameters=false) : this(expandModelBindParameters)
//        {
//            _applicationCache = applicationCache;

//            var appCacheDictionary = (
//                    _applicationCache[Constants.RouteValueConverterCacheKey]
//                    as Dictionary<string, RouteValueDictionary>
//                    ?? new Dictionary<string, RouteValueDictionary>()
//                );

//            foreach (var appCacheKey in appCacheDictionary)
//            {
//                CachedValueExpressions[appCacheKey.Key] = appCacheKey.Value;
//            }

//            if (_applicationCache != null)
//            {
//                _applicationCache[Constants.RouteValueConverterCacheKey] = CachedValueExpressions;
//            }
//        }

//        /// <summary>
//        /// Creates a new RouteExpressionConverter which stores cached linq expressions statically (only guaranteed to last for a single request)
//        /// </summary>
//        /// <param name="expandModelBindParameters">Whether or not to fully expand model bind parameters</param>
//        public RouteExpressionConverter(bool expandModelBindParameters=false)
//        {
//            _expandModelBindParameters = expandModelBindParameters;
//        }

//        #endregion Constructors

//        #region Public Methods

//        /// <summary>
//        /// Gets the route values from the given expression
//        /// </summary>
//        /// <param name="expression">The linq expression to convert into a RouteValueDictionary</param>
//        /// <returns>A RouteValueDictionary from the given expression</returns>
//        /// <remarks>
//        /// <para>
//        /// When possible the expression will be retrieved from cache. Not all expressions are cacheable
//        /// </para>
//        /// <para>
//        /// Expressions are cached based on string conversion so functionally equivalent expressions
//        /// may be treated seperately if the variable and parameter names within them differ
//        /// </para>
//        /// </remarks>
//        /// <exception cref="System.ArgumentNullException">
//        /// Thrown if the expression argument is null
//        /// </exception>
//        public RouteValueDictionary GetRouteValues<TC>(
//            Expression<Func<TC, ActionResult>> expression
//        ) where TC : Controller
//        {
//            if (expression == null)
//                throw new ArgumentNullException("expression");

//            RouteValueDictionary routeValues;

//            var key = GetCacheKey(typeof(TC), expression);
//            var cacheChanged = false;

//            if (CachedValueExpressions.TryGetValue(key, out routeValues) == false)
//            {
//                bool isCachableExpression;

//                routeValues = GetRouteValuesByExpression(expression, out isCachableExpression);

//                // this will sometimes be hit anyways due to threading
//                if (isCachableExpression)
//                {
//                    CachedValueExpressions[key] = routeValues;
//                    cacheChanged = true;
//                }
//            }

//            if (cacheChanged && _applicationCache != null)
//            {
//                _applicationCache[Constants.RouteValueConverterCacheKey] = CachedValueExpressions;
//            }

//            return routeValues;
//        }

//        //public IEnumerable<RoutePlaceholder> GetRoutePlaceholders(
//        //    Expression expression, out FormMethod formMethod
//        //)
//        //{
//        //    IEnumerable<RoutePlaceholder> routeValues;

//        //    var key = GetCacheKey(expression);

//        //    if (CachedPlaceholderExpressions.TryGetValue(key, out routeValues) == false)
//        //    {
//        //        routeValues = BuildRoutePlaceholders(expression, out formMethod);

//        //        CachedPlaceholderExpressions.Add(key, routeValues);
//        //        CachedFormMethods.Add(key, formMethod);
//        //    }
//        //    else
//        //    {
//        //        formMethod = CachedFormMethods[key];
//        //    }

//        //    return routeValues;
//        //}

//        #endregion Public Methods

//        #region Utilities

//        private IEnumerable<RoutePlaceholder> BuildRoutePlaceholders(
//            Expression expression, out FormMethod formMethod
//        )
//        {
//            var lambdaExpression = expression as LambdaExpression;

//            if (lambdaExpression == null)
//                throw new NotSupportedException("Only lambda expressions are supported");

//            var body = lambdaExpression.Body;

//            if (body.NodeType != ExpressionType.Call)
//                throw new NotSupportedException("Can only support method calls");

//            var mce = (MethodCallExpression)body;

//            var methodInfo = mce.Method;

//            // default to post
//            formMethod = FormMethod.Post;

//            var theAcceptVerbsAttribute =
//                methodInfo.GetCustomAttributes(typeof(AcceptVerbsAttribute), false)
//                .FirstOrDefault() as AcceptVerbsAttribute;

//            if (theAcceptVerbsAttribute != null)
//            {
//                foreach (var verb in theAcceptVerbsAttribute.Verbs)
//                {
//                    if (verb.ToLower() == "get")
//                        formMethod = FormMethod.Get;
//                }
//            }

//            if (methodInfo.GetCustomAttributes(typeof(HttpGetAttribute), false).Any())
//                formMethod = FormMethod.Get;

//            var result = new List<RoutePlaceholder>
//            {
//                new RoutePlaceholder
//                {
//                    Key="controller",
//                    RewriteAs=GetControllerName( methodInfo.DeclaringType ),
//                    IgnoreExpansion = true
//                },

//                new RoutePlaceholder
//                {
//                    Key = "action",
//                    RewriteAs = methodInfo.Name,
//                    IgnoreExpansion = true
//                }
//            };

//            int placeholderIndex = 1;
//            int parameterIndex = 0;

//            foreach (var parameter in methodInfo.GetParameters())
//            {
//                IEnumerable<string> expandedModel = null;

////				if (_expandModelBindParameters)
//    //			{
//        //			expandedModel = ExpandParameterWithPlaceholders(parameter);
//            //	}

//                if (expandedModel != null)
//                {
//                    foreach (var expansion in expandedModel)
//                    {
//                        result.Add(
//                            new RoutePlaceholder
//                            {
//                                Key = expansion,
//                                PlaceholderValue = placeholderIndex,
//                                RewriteAs = parameterIndex + "." + expansion
//                            }
//                        );

//                        placeholderIndex++;
//                    }
//                }
//                else
//                {
//                    result.Add(
//                        new RoutePlaceholder
//                        {
//                            Key = parameter.Name,
//                            PlaceholderValue = placeholderIndex,
//                            RewriteAs = parameterIndex.ToString()
//                        }
//                    );

//                    placeholderIndex++;
//                }

//                parameterIndex++;
//            }

//            return result;
//        }

//        private static string GetControllerName(Type type)
//        {
//            return type.Name.Replace("Controller", "");
//        }

//        //private IEnumerable<string> ExpandParameterWithPlaceholders(
//        //    ParameterInfo parameter
//        //)
//        //{
//        //    // todo allow default model binders
//        //    var theModelBindAttribute =
//        //        parameter.GetCustomAttributes(typeof(ModelBinderAttribute), false)
//        //            .FirstOrDefault() as ModelBinderAttribute;

//        //    if (
//        //        theModelBindAttribute != null &&
//        //        typeof(IBidirectionalModelBinder).IsAssignableFrom(theModelBindAttribute.BinderType)
//        //    )
//        //    {
//        //        var bidirectionalBinder = (IBidirectionalModelBinder)theModelBindAttribute.GetBinder();

//        //        return bidirectionalBinder.UsedMembers;
//        //    }

//        //    return null;
//        //}

//        //private IDictionary<string, object> ExpandParameterWithValues(
//        //    Expression argument, ParameterInfo parameter, out bool isCachableArgument
//        //)
//        //{
//        //    var result = new Dictionary<string, object>();
//        //    isCachableArgument = true;

//        //    var theModelBindAttribute =
//        //        parameter.GetCustomAttributes(typeof(ModelBinderAttribute), false)
//        //            .FirstOrDefault() as ModelBinderAttribute;

//        //    var boundModelValue = GetValueOf(argument, out isCachableArgument);

//        //    if (DefaultBidirectionalModelBinder.IsPrimitiveType(boundModelValue))
//        //        return null;

//        //    IBidirectionalModelBinder modelBinder;

//        //    if (
//        //        theModelBindAttribute != null &&
//        //        typeof(IBidirectionalModelBinder).IsAssignableFrom(theModelBindAttribute.BinderType)
//        //    )
//        //    {
//        //        modelBinder = (IBidirectionalModelBinder)theModelBindAttribute.GetBinder();
//        //    }
//        //    else
//        //    {
//        //        modelBinder = new DefaultBidirectionalModelBinder(boundModelValue.GetType());
//        //    }

//        //    foreach (var rvKvp in modelBinder.ToRouteValues(boundModelValue))
//        //        result.Add(rvKvp.Key, rvKvp.Value);

//        //    return result;
//        //}

//        private RouteValueDictionary GetRouteValuesByExpression<TC>(
//            Expression<Func<TC, ActionResult>> expression, out bool isCachableRequest
//        )
//        {
//            var result = new RouteValueDictionary();

//            result.Add("controller", GetControllerName(typeof(TC)));

//            if (expression.NodeType != ExpressionType.Lambda)
//                throw new NotSupportedException("Can only support lambda expressions");

//            var body = expression.Body;

//            if (body.NodeType != ExpressionType.Call)
//                throw new NotSupportedException("Can only support method calls");

//            var mce = (MethodCallExpression)body;

//            result.Add("action", mce.Method.Name);

//            var parameters = mce.Method.GetParameters();

//            var index = -1;

//            isCachableRequest = true;

//            foreach (Expression argument in mce.Arguments)
//            {
//                index++;

//                bool isArgumentCachable = true;

//                IDictionary<string, object> expandedParameters = null;

////				if (_expandModelBindParameters)
//    //			{
//        //			expandedParameters = ExpandParameterWithValues(
//            //			argument, parameters[index], out isArgumentCachable
//                //	);
//                //}

//                if (expandedParameters != null)
//                {
//                    foreach (var kvp in expandedParameters)
//                        result.Add(kvp.Key, kvp.Value);
//                }
//                else
//                {
//                    var name = parameters[index].Name;
//                    var value = GetValueOf(argument, out isArgumentCachable);

//                    result.Add(name, value);
//                }

//                isCachableRequest &= isArgumentCachable;
//            }

//            return result;
//        }

//        private static object GetValueOf(Expression expression, out bool isCachableExpression)
//        {
//            var visitor = new IsCachableExpressionVisitor();

//            isCachableExpression = visitor.IsCachable(expression);

//            LambdaExpression argumentLambdaExpression =
//                Expression.Lambda(expression, new ParameterExpression[0]);

//            var del = argumentLambdaExpression.Compile();

//            return del.DynamicInvoke();
//        }

//        private static string GetCacheKey(Type type, Expression expression)
//        {
//            return string.Format("{0}:{1}", type.FullName, expression);
//        }

//        #endregion Utilities
//    }
//}
