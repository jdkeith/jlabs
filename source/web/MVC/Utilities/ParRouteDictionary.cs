﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Routing;

namespace PXL.Web.Mvc.Utilities
{
	internal class ParRouteDictionary
	{
		private readonly RouteValueDictionary _fixedValues;
		private readonly Dictionary<RouteParamKey, Delegate> _variableValues;

		public ParRouteDictionary(RouteValueDictionary fixedValues, Dictionary<RouteParamKey, Delegate> variableValues)
		{
			_fixedValues = fixedValues;
			_variableValues = variableValues;
		}

		public RouteValueDictionary Complete( params object[] values )
		{
			var result = new RouteValueDictionary(_fixedValues);

			foreach( var kvp in _variableValues )
			{
				var invokeValues = ExtractArgumentsForExpression(kvp.Key, values);

				result.Add(
					kvp.Key.RouteValueName,
					kvp.Value.DynamicInvoke(invokeValues)
				);
			}

			return result;
		}

		private object[] ExtractArgumentsForExpression(RouteParamKey key, object[] allValues)
		{
			var result = key.Dependencies.Select(
				// bias by -1 because the first argument is ignored and not passed in allValues
				d => allValues[d.Index-1]
			);

			return result.ToArray();
		}
	}
}
