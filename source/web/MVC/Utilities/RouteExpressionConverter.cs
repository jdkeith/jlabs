﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web.Routing;
using JLabs.Web.Mvc.Utilities;

namespace PXL.Web.Mvc.Utilities
{
	/// <summary>
	/// Converts a linq expression into a RoutesValueDictionary
	/// </summary>
	/// <remarks>
	/// The route expression converter may cache routes in the application's items property
	/// </remarks>
	internal class RouteExpressionConverter
	{
		#region Public Methods

		/// <summary>
		/// Gets the route values from the given expression
		/// </summary>
		/// <param name="expression">The linq expression to convert into a RouteValueDictionary</param>
		/// <returns>A RouteValueDictionary from the given expression</returns>
		/// <remarks>
		/// <para>
		/// When possible the expression will be retrieved from cache. Not all expressions are cacheable
		/// </para>
		/// <para>
		/// Expressions are cached based on string conversion so functionally equivalent expressions
		/// may be treated seperately if the variable and parameter names within them differ
		/// </para>
		/// </remarks>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the expression argument is null
		/// </exception>
		public ParRouteDictionary GetRouteValues(Expression expression)
		{
			var lambda = expression as LambdaExpression;

			if (lambda == null)
				throw new ArgumentNullException("expression");

			return GetRouteValuesByExpression(lambda);
		}

		#endregion Public Methods

		#region Utilities

		private static string GetControllerName(Type type)
		{
			return type.Name.Replace("Controller", "");
		}

		private static ParRouteDictionary GetRouteValuesByExpression(LambdaExpression expression)
		{
			var fixedValues = new RouteValueDictionary();
			var variableValues = new Dictionary<RouteParamKey, Delegate>();
			var paramLookup = new Dictionary<string, int>();

			// the first (controller) parameter is ignored as a variable expression
			for (var i = 1; i < expression.Parameters.Count; i++)
			{
				paramLookup.Add(expression.Parameters[i].Name, i);
			}

			fixedValues.Add(
				"controller",
				GetControllerName(
					expression.Parameters.First().Type
				)
			);

			var body = expression.Body;

			if (body.NodeType != ExpressionType.Call)
				throw new NotSupportedException("Can only support method calls");

			var mce = (MethodCallExpression)body;

			fixedValues.Add("action", mce.Method.Name);

			var argumentNames = mce.Method.GetParameters();

			var index = -1;

			foreach (var argumentExpression in mce.Arguments)
			{
				index++;

				var argument = new RouteParamKey(
					argumentNames[index].Name
				);

				IEnumerable<ParameterExpression> dependencies;
				Func<Delegate> convertToLambda;

				if( ExpressionDependencyTester.HasDependencies(argumentExpression, out dependencies, out convertToLambda) == false )
				{
					fixedValues.Add(
						argument.RouteValueName,
						GetValueOf(argumentExpression)
					);
				}
				else
				{
					argument.Dependencies = dependencies.Select(
						d => new ParamSummary { Name = d.Name, Index = paramLookup[d.Name] }
					).ToList();

					if (argument.Dependencies.Any(d => d.Index == 0))
						throw new NotSupportedException("Argument to a controller's method cannot depend on the controller parameter");

					variableValues.Add(
						argument,
						convertToLambda()
					);
				}
			}

			return new ParRouteDictionary(fixedValues, variableValues);
		}

		private static object GetValueOf(Expression expression)
		{
			var argumentLambdaExpression =
				Expression.Lambda(expression, new ParameterExpression[0]);

			var del = argumentLambdaExpression.Compile();

			return del.DynamicInvoke();
		}

		public static string FKey(string key)
		{
			if (key == null)
				return null;

			return Constants.RouteValueConverterCacheKeyPrefix + key;
		}

		#endregion Utilities
	}
}
