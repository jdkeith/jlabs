﻿using System;
using System.Linq.Expressions;
using System.Reflection;

namespace PXL.Web.Mvc.Utilities
{
	public class PropertyVisitor : ExpressionVisitor
	{
		public static bool TryExtractPropertyFrom(
			LambdaExpression expression, out PropertyInfo property, out object invocant
		)
		{
			throw new NotImplementedException();
			//if( expression == null )
			//	throw new ArgumentNullException("expression");

			//var runner = new PropertyVisitor();
			//runner.Visit(expression);
			//return runner.Property;
		}

		private PropertyVisitor()
		{
		}

		private PropertyInfo Property;
		private object Invocant;

		protected override Expression VisitMember(MemberExpression node)
		{
			if( Property != null || node.Member.MemberType != MemberTypes.Property )
				return base.VisitMember(node);

			Property = (PropertyInfo) node.Member;

			return base.VisitMember(node);
		}
	}
}