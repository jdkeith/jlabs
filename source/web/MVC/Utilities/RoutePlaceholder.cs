﻿namespace PXL.Web.Mvc.Utilities
{
	internal class RoutePlaceholder
	{
		public string Key { get; set; }
		public int PlaceholderValue { get; set; }
		public string RewriteAs { get; set; }
		public bool IgnoreExpansion { get; set; }
	}
}
