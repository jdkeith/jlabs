﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace PXL.Web.Mvc.Utilities
{
	internal static class ExpressionDependencyTester
	{
		public static bool HasDependencies(
			Expression expression,
			out IEnumerable<ParameterExpression> dependencies,
			out Func<Delegate> convertToLambda
		)
		{
			var visitor = new ExpressionDependencyVisitor();

			visitor.Visit(expression);

			if (!visitor.Dependencies.Any())
			{
				dependencies = Enumerable.Empty<ParameterExpression>();
				convertToLambda = null;

				return false;
			}

			dependencies = visitor.Dependencies;
			convertToLambda = () => ConvertToLambda(expression, visitor.Dependencies);

			return true;
		}

		private static Delegate ConvertToLambda(Expression body, IEnumerable<ParameterExpression> parameters)
		{
			var lambda = Expression.Lambda(
				body,
				parameters
			);

			return lambda.Compile();
		}

		private class ExpressionDependencyVisitor : ExpressionVisitor
		{
			public ExpressionDependencyVisitor()
			{
				Dependencies = new List<ParameterExpression>();
			}

			public List<ParameterExpression> Dependencies { get; private set; }

			protected override Expression VisitParameter(ParameterExpression node)
			{
				if (!Dependencies.Contains(node))
					Dependencies.Add(node);

				return base.VisitParameter(node);
			}
		}
	}
}
