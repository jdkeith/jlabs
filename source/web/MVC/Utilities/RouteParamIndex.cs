﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Routing;

namespace PXL.Web.Mvc.Utilities
{
	internal struct ParamSummary
	{
		public string Name { get; set; }
		public int Index { get; set; }

		public override bool Equals(object obj)
		{
			if (!(obj is ParamSummary))
				return false;

			var pSummary = (ParamSummary)obj;

			return pSummary.Name == Name && pSummary.Index == Index;
		}

		public override int GetHashCode()
		{
			return ToString().GetHashCode();
		}

		public override string ToString()
		{
			return string.Format("${0}:{1}", Name, Index);
		}
	}

	internal struct RouteParamKey
	{
		public readonly string RouteValueName;
		public IEnumerable<ParamSummary> Dependencies;

		public RouteParamKey(string routeValueName)
		{
			RouteValueName = routeValueName;
			Dependencies = Enumerable.Empty<ParamSummary>();
		}

		public override bool Equals(object obj)
		{
			if (!(obj is RouteParamKey))
				return false;

			return RouteValueName.Equals(((RouteParamKey)obj).RouteValueName);
		}

		public override int GetHashCode()
		{
			return RouteValueName.GetHashCode();
		}

		public override string ToString()
		{
			return string.Format("{{{0}<-({1})}}", RouteValueName, string.Join(",", Dependencies.Select(d => d.ToString())));
		}
	}
}
