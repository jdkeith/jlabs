﻿using System.Linq;
using System.Reflection;

// ReSharper disable once CheckNamespace
namespace System.Web.Mvc
{
	/// <summary>
	/// Provides extension methods for controller context instances
	/// </summary>
	public static class ControllerContextExtensions
	{
		/// <summary>
		/// Gets custom attributes of a given type associated with the given controller context
		/// That is a combination of attributes of that type attached to the class and the current action method
		/// </summary>
		/// <typeparam name="T">The type of custom attribute</typeparam>
		/// <param name="context">The controller context</param>
		/// <returns>An array of custom attributes of the given type attached to the class and current action method</returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the context parameter is null
		/// </exception>
		public static T[] GetAttributes<T>(this ControllerContext context) where T : Attribute
		{
			if( context == null )
				throw new ArgumentNullException("context");

			var controllerType = context.Controller.GetType();
			var controllerDescriptor = new ReflectedControllerDescriptor(controllerType);
			var actionDescriptor = controllerDescriptor.FindAction(context, context.RouteData.Values["action"].ToString());
			MethodInfo methodInfo = null;
			ReflectedActionDescriptor descriptor;

			if( (descriptor = (actionDescriptor as ReflectedActionDescriptor)) != null )
				methodInfo = descriptor.MethodInfo;

			var result = controllerType.GetCustomAttributes(typeof(T), false).Select(ca => ca as T);

			if (methodInfo != null)
			{
				result = result.Concat(
					methodInfo.GetCustomAttributes(typeof (T), false).Select(ca => ca as T)
				);
			}

			return result.ToArray();
		}
	}
}