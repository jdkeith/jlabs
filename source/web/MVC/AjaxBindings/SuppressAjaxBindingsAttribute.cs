﻿using System;

namespace PXL.Web.Mvc.AjaxBindings
{
	/// <summary>
	/// Specifies to third-party tools that javascript ajax bindings should be
	/// not be created created for the method to which the attribute is attached or
	/// for any model's property it is attached to
	/// </summary>
	[AttributeUsage(AttributeTargets.Method|AttributeTargets.Property, AllowMultiple=false, Inherited=false)]
	public class SuppressAjaxBindingsAttribute : Attribute
	{
	}
}
