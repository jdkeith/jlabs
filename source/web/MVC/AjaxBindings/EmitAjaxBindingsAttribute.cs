﻿using System;

namespace PXL.Web.Mvc.AjaxBindings
{
	/// <summary>
	/// Specifies to third-party tools that javascript ajax bindings should be
	/// created for the method to which the attribute is attached or all
	/// action methods in the controller to which it is attached
	/// </summary>
	[AttributeUsage(AttributeTargets.Method|AttributeTargets.Class, AllowMultiple=false, Inherited=false)]
	public class EmitAjaxBindingsAttribute : Attribute
	{
	}
}
