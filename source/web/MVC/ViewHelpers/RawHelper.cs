﻿using System.Web;
using System.Web.Mvc;

namespace PXL.Web.Mvc.ViewHelpers
{
	/// <summary>
	/// Adds raw and unencoded string support methods to HtmlHelper
	/// </summary>
	public static class RawHelper
	{
		/// <summary>
		/// Returns an HTML string with even converted HTML entities
		/// </summary>
		/// <param name="helper">The helper to extend</param>
		/// <param name="html">The HTML string to modify</param>
		/// <returns>A raw HTML string</returns>
		public static IHtmlString EntitySafe(this HtmlHelper helper, string html)
		{
			if (html == null)
				return null;

			html = html
				.Replace("&amp;", "&")
				.Replace("&lt;", "<")
				.Replace("&gt;", ">")
				.Replace("&apos", "'")
				.Replace("&nbsp", "<pre> </pre>")
				;

			return MvcHtmlString.Create(html);

		}
	}
}