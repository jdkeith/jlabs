﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;

using PXL.Core.String;

namespace PXL.Web.Mvc.ViewHelpers
{
	/// <summary>
	/// A helper class for producing ids
	/// </summary>
	public static class IdHelper
	{
		private readonly static Regex _wordsOnly;

		static IdHelper()
		{
			_wordsOnly = new Regex(@"\W+");
		}

		/// <summary>
		/// Gets a probably unique html id for the given object
		/// </summary>
		/// <param name="o">
		/// The object to get a probably unique id for
		/// </param>
		/// <returns>
		/// <para>Null if the parameter is null</para>
		/// <para>The hyphenated name of the type plus a probably unique value from one of</para>
		/// <para>A concatenation of all public properties marked with a KeyAttribute OR</para>
		/// <para>The value of a SINGLE property which is a non-nullable GUID OR</para>
		/// <para>The hash code of the object</para>
		/// </returns>
		public static string ToHtmlId(this object o)
		{
			if( ReferenceEquals(o, null) )
				return null;

			var type = o.GetType();

			var typeName = string.Join("-", type.Name.SplitCamelOrPascal());

			var guidProperties = new List<PropertyInfo>();
			var keyProperties = new List<PropertyInfo>();

			foreach (var property in type.GetProperties(BindingFlags.Instance | BindingFlags.Public))
			{
				if (property.PropertyType == typeof (Guid) && ! property.GetIndexParameters().Any())
				{
					guidProperties.Add(property);
				}

				var keys = (KeyAttribute[]) property.GetCustomAttributes(typeof (KeyAttribute), true);

				if (keys.Any())
				{
					keyProperties.Add(property);
				}
			}

			object idValue = null;

			// if no key properties exist and EXACTLY ONE guid property exists, use that
			if (! keyProperties.Any() && guidProperties.Count == 1)
			{
				idValue = guidProperties.First().GetValue(o, new object[0]);
			}

			// if any key properties exist, use those
			else if (keyProperties.Any() && keyProperties.All(kp => ! kp.GetIndexParameters().Any()))
			{
				var idValues = new object[keyProperties.Count];

				for (var i = 0; i < idValues.Length; i++)
				{
					idValue = keyProperties[i].GetValue(o, new object[0]);

					if (idValue == null)
					{
						throw new KeyNotFoundException(
							"The value in key " + keyProperties[i].Name + " was not defined"
						);
					}

					idValues[i] = idValue;
				}

				idValue = string.Join(
					"-",
					idValues.Select(v => _wordsOnly.Replace(v.ToString(), "-"))
				);
			}

			return typeName + "-" + idValue;
		}
	}
}