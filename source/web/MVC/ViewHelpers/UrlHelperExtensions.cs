﻿using System;
using System.Linq.Expressions;
using System.Linq.Expressions.Extensions;
using System.Web.Mvc;

using PXL.Core.Storage;
using PXL.Web.Mvc.Utilities;
using PXL.Web.Utilities;

namespace PXL.Web.Mvc.ViewHelpers
{
	/// <summary>
	/// Provides methods for generating strongly-typed URLs
	/// </summary>
	public static class UrlHelperExtensions
	{
		internal static ParRouteDictionary GenerateRoute(
			this UrlHelper helper, Expression expression, CachePriority cachePriority
		)
		{
			var key = RouteExpressionConverter.FKey(expression.ToKey());

			if (key == null && cachePriority != CachePriority.DoNotCache)
			{
				throw new NotCacheableException(
					"Expression is not cacheable, either use an overload where values are passed in as parameters " +
					"or explicitly mark the expression as not cachable by using a CachePriority of DoNotCache"
				);
			}

			var cache = helper.RequestContext.HttpContext.Cache;

			ParRouteDictionary routes;

			if (!cache.TryGetAs(key, out routes))
			{
				routes = new RouteExpressionConverter().GetRouteValues(expression);

				if (key != null)
				{
					cache[key] = routes;
				}
			}

			return routes;
		}

		private static string CompleteRoute(this UrlHelper helper, ParRouteDictionary routes, object[] values)
		{
			var routeValues = routes.Complete(values);

			return helper.RouteUrl(routeValues);
		}

		#region Full Urls

		/// <summary>
		/// Returns a URL from a linq expression
		/// </summary>
		/// <typeparam name="TController">The type of the controller the link points to</typeparam>
		/// <param name="helper">The helper object the method is extending</param>
		/// <param name="expression">The expression which defines the url</param>
		/// <param name="cachePriority">The priority under which to cache this url</param>
		/// <returns>A URL based on the expression</returns>
		public static string Action<TController>(
			this UrlHelper helper, Expression<Func<TController, ActionResult>> expression, CachePriority cachePriority = CachePriority.Default
		) where TController : Controller
		{
			return CompleteRoute(
				helper, GenerateRoute(helper, expression, cachePriority), new object[0]
			);
		}

		/// <summary>
		/// Returns a URL from a linq expression
		/// </summary>
		/// <typeparam name="TController">The type of the controller the link points to</typeparam>
		/// <typeparam name="TA">The type of the first paramter to pass in</typeparam>
		/// <param name="helper">The helper object the method is extending</param>
		/// <param name="expression">The expression which defines the url</param>
		/// <param name="a">The first parameter to pass in</param>
		/// <param name="cachePriority">The priority under which to cache this url</param>
		/// <returns>A URL based on the expression</returns>
		public static string Action<TController, TA>(
			this UrlHelper helper, Expression<Func<TController, TA, ActionResult>> expression, TA a, CachePriority cachePriority = CachePriority.Default
		) where TController : Controller
		{
			return CompleteRoute(
				helper, GenerateRoute(helper, expression, cachePriority), new object[] { a }
			);
		}


		/// <summary>
		/// Returns a URL from a linq expression
		/// </summary>
		/// <typeparam name="TController">The type of the controller the link points to</typeparam>
		/// <typeparam name="TA">The type of the first paramter to pass in</typeparam>
		/// <typeparam name="TB">The type of the second parameter to pass in</typeparam>
		/// <param name="helper">The helper object the method is extending</param>
		/// <param name="expression">The expression which defines the url</param>
		/// <param name="a">The first parameter to pass in</param>
		/// <param name="b">The second parameter to pass in</param>
		/// <param name="cachePriority">The priority under which to cache this url</param>
		/// <returns>A URL based on the expression</returns>
		public static string Action<TController, TA, TB>(
			this UrlHelper helper, Expression<Func<TController, TA, TB, ActionResult>> expression, TA a, TB b, CachePriority cachePriority = CachePriority.Default
		) where TController : Controller
		{
			return CompleteRoute(
				helper, GenerateRoute(helper, expression, cachePriority), new object[] { a, b }
			);
		}

		/// <summary>
		/// Returns a URL from a linq expression
		/// </summary>
		/// <typeparam name="TController">The type of the controller the link points to</typeparam>
		/// <typeparam name="TA">The type of the first paramter to pass in</typeparam>
		/// <typeparam name="TB">The type of the second parameter to pass in</typeparam>
		/// <typeparam name="TC">The type of the third parameter to pass in</typeparam>
		/// <param name="helper">The helper object the method is extending</param>
		/// <param name="expression">The expression which defines the url</param>
		/// <param name="a">The first parameter to pass in</param>
		/// <param name="b">The second parameter to pass in</param>
		/// <param name="c">The third parameter to pass in</param>
		/// <param name="cachePriority">The priority under which to cache this url</param>
		/// <returns>A URL based on the expression</returns>
		public static string Action<TController, TA, TB, TC>(
			this UrlHelper helper, Expression<Func<TController, TA, TB, TC, ActionResult>> expression, TA a, TB b, TC c, CachePriority cachePriority = CachePriority.Default
		) where TController : Controller
		{
			return CompleteRoute(
			helper, GenerateRoute(helper, expression, cachePriority), new object[] { a, b, c }
		);
		}

		#endregion Full Urls

		#region Prototype Urls

		/// <summary>
		/// Returns a URL prototype from a linq expression
		/// </summary>
		/// <typeparam name="TController">The type of the controller the link points to</typeparam>
		/// <typeparam name="TA">The type of the first paramter to pass in</typeparam>
		/// <param name="helper">The helper object the method is extending</param>
		/// <param name="expression">The expression which defines the url</param>
		/// <param name="cachePriority">The priority under which to cache this url</param>
		/// <returns>A URL prototype based on the expression</returns>
		public static Func<TA, string> Action<TController, TA>(
			this UrlHelper helper, Expression<Func<TController, TA, ActionResult>> expression, CachePriority cachePriority = CachePriority.Default
		) where TController : Controller
		{
			var route = GenerateRoute(helper, expression, cachePriority);

			return a => CompleteRoute(helper, route, new object[] { a });
		}

		/// <summary>
		/// Returns a URL from a linq expression
		/// </summary>
		/// <typeparam name="TController">The type of the controller the link points to</typeparam>
		/// <typeparam name="TA">The type of the first paramter to pass in</typeparam>
		/// <typeparam name="TB">The type of the second parameter to pass in</typeparam>
		/// <param name="helper">The helper object the method is extending</param>
		/// <param name="expression">The expression which defines the url</param>
		/// <param name="cachePriority">The priority under which to cache this url</param>
		/// <returns>A URL prototype based on the expression</returns>
		public static Func<TA,TB,string> Action<TController, TA, TB>(
			this UrlHelper helper, Expression<Func<TController, TA, TB, ActionResult>> expression, CachePriority cachePriority = CachePriority.Default
		) where TController : Controller
		{
			var route = GenerateRoute(helper, expression, cachePriority);

			return (a, b) => CompleteRoute(helper, route, new object[] { a, b });
		}

		/// <summary>
		/// Returns a URL from a linq expression
		/// </summary>
		/// <typeparam name="TController">The type of the controller the link points to</typeparam>
		/// <typeparam name="TA">The type of the first paramter to pass in</typeparam>
		/// <typeparam name="TB">The type of the second parameter to pass in</typeparam>
		/// <typeparam name="TC">The type of the third parameter to pass in</typeparam>
		/// <param name="helper">The helper object the method is extending</param>
		/// <param name="expression">The expression which defines the url</param>
		/// <param name="cachePriority">The priority under which to cache this url</param>
		/// <returns>A URL prototype based on the expression</returns>
		public static Func<TA,TB,TC,string> Action<TController, TA, TB, TC>(
			this UrlHelper helper, Expression<Func<TController, TA, TB, TC, ActionResult>> expression, CachePriority cachePriority = CachePriority.Default
		) where TController : Controller
		{
			var route = GenerateRoute(helper, expression, cachePriority);

			return (a, b, c) => CompleteRoute(helper, route, new object[] { a, b, c });
		}

		#endregion Prototype Urls
	}
}
