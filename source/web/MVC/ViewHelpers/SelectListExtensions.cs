﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.UI.WebControls;
using PXL.Web.Mvc.Utilities;

namespace PXL.Web.Mvc.ViewHelpers
{
	public static class SelectListExtensions
	{
		private class KeyValueComparer<TK, TV> : IComparer<KeyValuePair<TK, TV>>
		{
			private readonly Func<TK, TK, TV, TV, int> _comparer;

			public KeyValueComparer(Func<TK, TK, TV, TV, int> comparer)
			{
				_comparer = comparer;
			}

			public int Compare(KeyValuePair<TK, TV> x, KeyValuePair<TK, TV> y)
			{
				return _comparer(x.Key, y.Key, x.Value, y.Value);
			}
		}

		public static IEnumerable<SelectListItem> ToSelectList<T>(
			Expression<Func<T, Enum>> expression, string withSelectLabelOf = null
		)
		{
			throw new NotImplementedException();
			//var property = PropertyVisitor.ExtractPropertyFrom(expression);

			//if (property == null)
			//	throw new InvalidOperationException("Could not extract property info from expression");
								
			//var value = property.GetValue()
		}

		public static IEnumerable<SelectListItem> ToSelectList(Type enumerationType, string withSelectLabelOf = null)
		{
			return ToSelectList(enumerationType, null, withSelectLabelOf);
		}

		public static IEnumerable<SelectListItem> ToSelectList(Enum enumerationValue, string withSelectLabelOf = null)
		{
			return ToSelectList(enumerationValue.GetType(), enumerationValue, withSelectLabelOf);
		}

		private static IEnumerable<SelectListItem> ToSelectList(
			Type enumerationType, Enum enumerationValue, string withSelectLabelOf = null
		)
		{
			var enumDictionary = Enum.GetNames(enumerationType).ToDictionary(
				n => n, n => Enum.Parse(enumerationType, n, false)
			);

			return ToSelectList(enumDictionary, enumerationValue, withSelectLabelOf);
		}

		public static IEnumerable<SelectListItem> ToSelectList<TK, TV>(
			Dictionary<TK,TV> dictionary,
			object selectedValue = null, string withSelectLabelOf = null,
			Func<TK, TK, TV, TV, int> keyValueComparer = null 
		)
		{
			var comparer = keyValueComparer == null
				? GetDefaultComparer<TK, TV>()
				: new KeyValueComparer<TK, TV>(keyValueComparer);

			var results = (
				from kvp in dictionary.OrderBy(kvp => kvp, comparer)
				let isSelected =
					selectedValue != null && 
					! ReferenceEquals(kvp.Key, null) &&
					! ReferenceEquals(kvp.Value, null) &&
					(kvp.Key.Equals(selectedValue) || kvp.Value.Equals(selectedValue))
				select new SelectListItem
				{
					Text = ReferenceEquals(kvp.Value, null) ? "" : kvp.Value.ToString(),
					Value = ReferenceEquals(kvp.Key, null) ? "" : kvp.Key.ToString(),
					Selected = isSelected
				}
			).ToList();

			if (withSelectLabelOf != null)
			{
				var item = new SelectListItem
				{
					Text = withSelectLabelOf,
					Value = IsNumeric<TK>() ? "0" : "",
					Selected = ! results.Any(r => r.Selected)
				};

				results.Insert(0, item);
			}

			return results;
		}

		private static bool IsNumeric<TK>()
		{
			var numericTypes = new[]
			{
				typeof (byte),
				typeof (sbyte),
				typeof (short),
				typeof (ushort),
				typeof (int),
				typeof (uint),
				typeof (long),
				typeof (ulong),
				typeof (float),
				typeof (double),
				typeof (decimal)
			};

			return numericTypes.Any(nt => nt == typeof (TK));
		}

		private static KeyValueComparer<TK, TV> GetDefaultComparer<TK, TV>()
		{
			Func<TK, TK, TV, TV, int> result;

			if (typeof (IComparable).IsAssignableFrom(typeof (TV)))
			{
				result = (k1, k2, v1, v2) =>
				{
					var c1 = (IComparable) v1;
					var c2 = (IComparable) v2;

					return c1.CompareTo(c2);
				};
			}
			else if( typeof(IComparable<TV>).IsAssignableFrom(typeof(TV)))
			{
				result = (k1, k2, v1, v2) =>
				{
					var c1 = (IComparable<TV>) v1;

					return c1.CompareTo(v2);
				};
			}
			else
			{
				result = (k1, k2, v1, v2) =>
				{
					var c1 = ReferenceEquals(v1, null) ? "" : v1.ToString();
					var c2 = ReferenceEquals(v2, null) ? "" : v2.ToString();

					return c1.CompareTo(v2);
				};
			}

			return new KeyValueComparer<TK, TV>(result);
		}
	}
}
