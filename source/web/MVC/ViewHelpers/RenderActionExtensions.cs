﻿using System;
using System.Linq.Expressions;
using System.Linq.Expressions.Extensions;
using System.Web.Mvc;
using System.Web.Mvc.Html;

using PXL.Core.Storage;
using PXL.Web.Mvc.Utilities;
using PXL.Web.Utilities;

namespace PXL.Web.Mvc.ViewHelpers
{
	/// <summary>
	/// Provides methods for generating strongly-typed render actions
	/// </summary>
	public static class RenderActionExtensions
	{
		#region Action

		private static MvcHtmlString Action(
			HtmlHelper helper, Expression expression, object[] values, CachePriority priority
		)
		{
			var cache = helper.ViewContext.HttpContext.Cache;

			var key = RouteExpressionConverter.FKey(expression.ToKey());

			ParRouteDictionary prd;

			if (!cache.TryGetAs(key, out prd))
			{
				var converter = new RouteExpressionConverter();

				prd = converter.GetRouteValues(expression);

				if (key != null)
				{
					cache[key] = prd;
				}
			}

			var rvd = prd.Complete(values);

			return helper.Action(rvd["action"].ToString(), rvd);
		}

		/// <summary>
		/// Renders the action at URL specified by the expression as a string
		/// </summary>
		/// <typeparam name="TController">The type of the controller the link points to</typeparam>
		/// <param name="helper">The helper object the method is extending</param>
		/// <param name="expression">The expression which defines the url</param>
		/// <param name="cachePriority">The priority under which to cache this url</param>
		/// <returns>A URL based on the expression</returns>
		public static MvcHtmlString Action<TController>(
			this HtmlHelper helper, Expression<Func<TController, ActionResult>> expression,
			CachePriority cachePriority = CachePriority.Default
		) where TController : Controller
		{
			return Action(helper, expression, new object[0], cachePriority);
		}

		/// <summary>
		/// Renders the action at URL specified by the expression as a string
		/// </summary>
		/// <typeparam name="TController">The type of the controller the link points to</typeparam>
		/// <typeparam name="TA">The type of the first paramter to pass in</typeparam>
		/// <param name="helper">The helper object the method is extending</param>
		/// <param name="expression">The expression which defines the url</param>
		/// <param name="a">The first parameter to pass in</param>
		/// <param name="cachePriority">The priority under which to cache this url</param>
		/// <returns>A URL based on the expression</returns>
		public static MvcHtmlString Action<TController, TA>(
			this HtmlHelper helper, Expression<Func<TController, TA, ActionResult>> expression, TA a,
			CachePriority cachePriority = CachePriority.Default
		) where TController : Controller
		{
			return Action(helper, expression, new object[] { a }, cachePriority);
		}

		/// <summary>
		/// Renders the action at URL specified by the expression as a string
		/// </summary>
		/// <typeparam name="TController">The type of the controller the link points to</typeparam>
		/// <typeparam name="TA">The type of the first paramter to pass in</typeparam>
		/// <typeparam name="TB">The type of the second parameter to pass in</typeparam>
		/// <param name="helper">The helper object the method is extending</param>
		/// <param name="expression">The expression which defines the url</param>
		/// <param name="a">The first parameter to pass in</param>
		/// <param name="b">The second parameter to pass in</param>
		/// <param name="cachePriority">The priority under which to cache this url</param>
		/// <returns>A URL based on the expression</returns>
		public static MvcHtmlString Action<TController, TA, TB>(
			this HtmlHelper helper, Expression<Func<TController, TA, TB, ActionResult>> expression, TA a, TB b,
			CachePriority cachePriority = CachePriority.Default
		) where TController : Controller
		{
			return Action(helper, expression, new object[] { a, b }, cachePriority);
		}

		/// <summary>
		/// Renders the action at URL specified by the expression as a string
		/// </summary>
		/// <typeparam name="TController">The type of the controller the link points to</typeparam>
		/// <typeparam name="TA">The type of the first paramter to pass in</typeparam>
		/// <typeparam name="TB">The type of the second parameter to pass in</typeparam>
		/// <typeparam name="TC">The type of the third parameter to pass in</typeparam>
		/// <param name="helper">The helper object the method is extending</param>
		/// <param name="expression">The expression which defines the url</param>
		/// <param name="a">The first parameter to pass in</param>
		/// <param name="b">The second parameter to pass in</param>
		/// <param name="c">The third parameter to pass in</param>
		/// <param name="cachePriority">The priority under which to cache this url</param>
		/// <returns>A URL based on the expression</returns>
		public static MvcHtmlString Action<TController, TA, TB, TC>(
			this HtmlHelper helper, Expression<Func<TController, TA, TB, TC, ActionResult>> expression, TA a, TB b, TC c,
			CachePriority cachePriority = CachePriority.Default
		) where TController : Controller
		{
			return Action(helper, expression, new object[] { a, b, c }, cachePriority);
		}

		#endregion Action

		#region Render Action

		private static void RenderAction(HtmlHelper helper, Expression expression, object[] values, CachePriority cachePriority)
		{
			var cache = helper.ViewContext.HttpContext.Cache;

			var key = RouteExpressionConverter.FKey(expression.ToKey());

			ParRouteDictionary prd;

			if (!cache.TryGetAs(key, out prd))
			{
				var converter = new RouteExpressionConverter();

				prd = converter.GetRouteValues(expression);

				if (key != null)
				{
					cache[key] = prd;
				}
			}

			var rvd = prd.Complete(values);

			helper.RenderAction(rvd["action"].ToString(), rvd);
		}

		/// <summary>
		/// Renders the action at URL specified by the expression into the output stream
		/// </summary>
		/// <typeparam name="TController">The type of the controller the link points to</typeparam>
		/// <param name="helper">The helper object the method is extending</param>
		/// <param name="expression">The expression which defines the url</param>
		/// <param name="cachePriority">The priority under which to cache this url</param>
		public static void RenderAction<TController>(
			this HtmlHelper helper, Expression<Func<TController, ActionResult>> expression,
			CachePriority cachePriority = CachePriority.Default
		) where TController : Controller
		{
			RenderAction(helper, expression, new object[0], cachePriority);
		}

		/// <summary>
		/// Renders the action at URL specified by the expression into the output stream
		/// </summary>
		/// <typeparam name="TController">The type of the controller the link points to</typeparam>
		/// <typeparam name="TA">The type of the first paramter to pass in</typeparam>
		/// <param name="helper">The helper object the method is extending</param>
		/// <param name="expression">The expression which defines the url</param>
		/// <param name="a">The first parameter to pass in</param>
		/// <param name="cachePriority">The priority under which to cache this url</param>
		public static void RenderAction<TController, TA>(
			this HtmlHelper helper, Expression<Func<TController, TA, ActionResult>> expression, TA a,
			CachePriority cachePriority = CachePriority.Default
		) where TController : Controller
		{
			RenderAction(helper, expression, new object[] { a }, cachePriority);
		}

		/// <summary>
		/// Renders the action at URL specified by the expression into the output stream
		/// </summary>
		/// <typeparam name="TController">The type of the controller the link points to</typeparam>
		/// <typeparam name="TA">The type of the first paramter to pass in</typeparam>
		/// <typeparam name="TB">The type of the second parameter to pass in</typeparam>
		/// <param name="helper">The helper object the method is extending</param>
		/// <param name="expression">The expression which defines the url</param>
		/// <param name="a">The first parameter to pass in</param>
		/// <param name="b">The second parameter to pass in</param>
		/// <param name="cachePriority">The priority under which to cache this url</param>
		public static void RenderAction<TController, TA, TB>(
			this HtmlHelper helper, Expression<Func<TController, TA, TB, ActionResult>> expression, TA a, TB b,
			CachePriority cachePriority = CachePriority.Default
		) where TController : Controller
		{
			RenderAction(helper, expression, new object[] { a, b }, cachePriority);
		}

		/// <summary>
		/// Renders the action at URL specified by the expression into the output stream
		/// </summary>
		/// <typeparam name="TController">The type of the controller the link points to</typeparam>
		/// <typeparam name="TA">The type of the first paramter to pass in</typeparam>
		/// <typeparam name="TB">The type of the second parameter to pass in</typeparam>
		/// <typeparam name="TC">The type of the third parameter to pass in</typeparam>
		/// <param name="helper">The helper object the method is extending</param>
		/// <param name="expression">The expression which defines the url</param>
		/// <param name="a">The first parameter to pass in</param>
		/// <param name="b">The second parameter to pass in</param>
		/// <param name="c">The third parameter to pass in</param>
		/// <param name="cachePriority">The priority under which to cache this url</param>
		public static void RenderAction<TController, TA, TB, TC>(
			this HtmlHelper helper, Expression<Func<TController, TA, TB, TC, ActionResult>> expression, TA a, TB b, TC c,
			CachePriority cachePriority = CachePriority.Default
		) where TController : Controller
		{
			RenderAction(helper, expression, new object[] { a, b, c }, cachePriority);
		}

		#endregion Render Action
	}
}