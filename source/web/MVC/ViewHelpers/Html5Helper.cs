﻿using System.Web.Mvc;

namespace PXL.Web.Mvc.ViewHelpers
{
	/// <summary>
	/// An extensible helper for dealing with Html5 output
	/// </summary>
	/// <typeparam name="TModel">The type of the model on the page this helper is instantiated on</typeparam>
	public class Html5Helper<TModel>
	{
		/// <summary>
		/// Creates a new Html5Helper
		/// </summary>
		/// <param name="viewContext">The ViewContext of the helper</param>
		/// <param name="viewDataContainer">The ViewDataContainer of the helper</param>
		public Html5Helper(ViewContext viewContext, IViewDataContainer viewDataContainer)
		{
			ViewContext = viewContext;
			ViewData = new ViewDataDictionary<TModel>(viewDataContainer.ViewData);
		}

		/// <summary>The ViewDataDictionary of the helper</summary>
		public ViewDataDictionary<TModel> ViewData { get; private set; }

		/// <summary>The ViewDataContext of the helper</summary>
		public ViewContext ViewContext { get; private set; }
	}
}