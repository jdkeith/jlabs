﻿using System.Web.Mvc;
using System.Web.Routing;

namespace PXL.Web.Mvc.ViewHelpers
{
	/// <summary>
	/// An extensible helper for dealing with Javascript output
	/// </summary>
	/// <typeparam name="TModel">The type of the model on the page this helper is instantiated on</typeparam>
	public class JavascriptHelper<TModel>
	{
		/// <summary>
		/// Creates a new Html5Helper
		/// </summary>
		/// <param name="viewContext">The ViewContext of the helper</param>
		/// <param name="viewDataContainer">The ViewDataContainer of the helper</param>
		public JavascriptHelper(ViewContext viewContext, IViewDataContainer viewDataContainer)
			: this(viewContext, viewDataContainer, RouteTable.Routes)
		{
		}

		/// <summary>
		/// Creates a new Html5Helper
		/// </summary>
		/// <param name="viewContext">The ViewContext of the helper</param>
		/// <param name="viewDataContainer">The ViewDataContainer of the helper</param>
		/// <param name="routeCollection">The RouteCollection of the helper</param>
		public JavascriptHelper(ViewContext viewContext, IViewDataContainer viewDataContainer, RouteCollection routeCollection)
		{
			ViewContext = viewContext;
			ViewData = new ViewDataDictionary<TModel>(viewDataContainer.ViewData);
		}

		/// <summary>The ViewDataDictionary of the helper</summary>
		public ViewDataDictionary<TModel> ViewData { get; private set; }

		/// <summary>The ViewDataContext of the helper</summary>
		public ViewContext ViewContext { get; private set; }

		/// <summary>
		/// Converts a string to an escaped single-quoted javascript string
		/// </summary>
		/// <param name="input">The string to input</param>
		/// <returns>An escaped single-quoted javascript string</returns>
		public MvcHtmlString JsString(string input)
		{
			if (input == null)
				return null;

			var s = input;

			s = s.Replace("\\", "\\\\");
			s = s.Replace("\n", "\\n");
			s = s.Replace("\t", "\\t");
			s = s.Replace("'", "\\'");

			return new MvcHtmlString("'" + s + "'");
		}
	}
}