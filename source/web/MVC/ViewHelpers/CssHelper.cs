﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Generic.Extensions;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;

using PXL.Core.String;

namespace PXL.Web.Mvc.ViewHelpers
{
	public static class CssHelper
	{
		private static readonly Dictionary<Type, IEnumerable<PropertyInfo>> _cache;
		private static readonly Regex _nonWordChopper = new Regex(@"\W+");

		static CssHelper()
		{
			_cache = new Dictionary<Type, IEnumerable<PropertyInfo>>();
		}

		internal static string ToCssClassOrHtmlIdName(string name)
		{
			name = _nonWordChopper.Replace(name, "_");

			return string.Join("-", name.SplitCamelOrPascal());
		}

		public static string ToCssClass(this string o)
		{
			if (ReferenceEquals(o, null))
				return null;

			return ToCssClassOrHtmlIdName(o);
		}

		public static string ToCssClass(this IEnumerable o)
		{
			if( ReferenceEquals(o, null) )
				return null;

			var cast = o.Cast<object>();

			return cast.Any() ? "list" : "list-empty";
		}

		public static string ToCssClass<T>(this IEnumerable<T> o)
		{
			if( ReferenceEquals(o, null) )
				return null;

			return string.Format("list-of-{0} list{1}", ToCssClassOrHtmlIdName(typeof(T).Name), o.Any() ? "" : "-empty");
		}

		public static string ToCssClass<T>(this IEnumerable<IndexedItem<T>> o)
		{
			if (ReferenceEquals(o, null))
				return null;

			return string.Format("list-of-{0} list{1}", ToCssClassOrHtmlIdName(typeof(T).Name), o.Any() ? "" : "-empty");
		}

		public static string ToCssClass<T>(this IndexedItem<T> o, bool enumsHavePropertyName = false)
		{
			var builder = new StringBuilder();

			if(o.IsFirst)
				builder.Append("first ");

			if(o.IsLast)
				builder.Append("last ");

			builder.Append(o.Index % 2 == 1 ? "odd " : "even " );

			builder.Append(ToCssClass(o.Value, enumsHavePropertyName) ?? "");

			return builder.ToString();
		}

		public static string ToCssClass(this object o, bool enumsHavePropertyName = false)
		{
			if (ReferenceEquals(o, null))
				return null;

			var type = o.GetType();

			if( type.IsGenericType)
				return null;

			var classes = new List<string>();

			classes.Add(ToCssClassOrHtmlIdName(type.Name));

			foreach (var property in GetProperties(o.GetType()))
			{
				var value = property.GetValue(o, new object[0]);

				if( false.Equals(value) )
					continue;

				var cssClassName = ToCssClassOrHtmlIdName(value.ToString());

				if (true.Equals(value))
				{
					// boolean values always have the property name
					cssClassName = ToCssClassOrHtmlIdName(property.Name);
				}
				else if (enumsHavePropertyName)
				{
					// otherwise property name is only applied if withPropertyName is true
					cssClassName = ToCssClassOrHtmlIdName(property.Name) + '-' + cssClassName;
				}

				classes.Add(cssClassName);
			}

			return string.Join(" ", classes.Distinct());
		}

		private static IEnumerable<PropertyInfo> GetProperties(Type type)
		{
			IEnumerable<PropertyInfo> properties;

			if (_cache.TryGetValue(type, out properties))
				return properties;

			const BindingFlags flags = BindingFlags.Public | BindingFlags.Instance | BindingFlags.FlattenHierarchy;

			var result = new List<PropertyInfo>();

			foreach (var property in type.GetProperties(flags))
			{
				if (!property.PropertyType.IsEnum && property.PropertyType != typeof(bool))
					continue;

				result.Add(property);
			}

			_cache[type] = result;

			return result;
		}
	}
}