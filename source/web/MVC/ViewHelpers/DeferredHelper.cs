﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web.Mvc;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.Routing;
using System.Web.WebPages;

using JLabs.Web.Mvc.Utilities;

namespace PXL.Web.Mvc.ViewHelpers
{
	/// <summary>
	/// A Url which has parameter values replaced by index placeholders
	/// </summary>
	public interface IProtoUrl
	{
		/// <summary>
		/// The url which contains parameter index placeholders
		/// </summary>
		string Url { get; }
		
		/// <summary>
		/// A unique identifier for the ProtoUrl
		/// </summary>
		Guid Id { get; }
	}

	/// <summary>
	/// A helper class which handles deferred XMLHttpRequest functionality
	/// </summary>
	/// <typeparam name="TModel">The type of the model on the page this helper is instantiated on</typeparam>
	public class DeferredHelper<TModel>
	{
		//private class ProtoUrlInner : IProtoUrl
		//{
		//    /// <summary>
		//    /// Creates a new ProtoUrl from a given url
		//    /// </summary>
		//    /// <param name="url">The url to make into a ProtoUrl</param>
		//    public ProtoUrlInner(string url)
		//    {
		//        Url = url;

		//        var encBytes = SHA256.Create().ComputeHash(
		//            Encoding.UTF8.GetBytes(url)
		//        );

		//        Id = new Guid(encBytes.Take(16).ToArray());
		//    }

		//    /// <summary>
		//    /// The url which contains parameter index placeholders
		//    /// </summary>
		//    public string Url { get; private set; }
		
		//    /// <summary>
		//    /// A unique identifier for the ProtoUrl
		//    /// </summary>
		//    public Guid Id { get; private set; }
		//}

		//private readonly Dictionary<Guid, IProtoUrl> _protoUrls;

		///// <summary>
		///// Creates a new DeferredHelper
		///// </summary>
		///// <param name="viewContext">The ViewContext of the helper</param>
		///// <param name="viewDataContainer">The ViewDataContainer of the helper</param>
		//public DeferredHelper(ViewContext viewContext, IViewDataContainer viewDataContainer)
		//    : this(viewContext, viewDataContainer, RouteTable.Routes)
		//{
		//}

		///// <summary>
		///// Creates a new DeferredHelper
		///// </summary>
		///// <param name="viewContext">The ViewContext of the helper</param>
		///// <param name="viewDataContainer">The ViewDataContainer of the helper</param>
		///// <param name="routeCollection">The RouteCollection of the helper</param>
		//public DeferredHelper(ViewContext viewContext, IViewDataContainer viewDataContainer, RouteCollection routeCollection)
		//{
		//    ViewContext = viewContext;
		//    ViewData = new ViewDataDictionary<TModel>(viewDataContainer.ViewData);
		//    _protoUrls = new Dictionary<Guid, IProtoUrl>();
		//}

		///// <summary>The ViewDataDictionary of the helper</summary>
		//public ViewDataDictionary<TModel> ViewData { get; private set; }

		///// <summary>The ViewDataContext of the helper</summary>
		//public ViewContext ViewContext { get; private set; }

		//private static Regex _prototyper = new Regex(@"([^/=?&]+)(?!=)\b");

		//#region Proto Urls

		//private IProtoUrl ProtoUrl(Expression expression, object[] values)
		//{
		//    if (values.Any(v => v == null))
		//        throw new ArgumentException("Prototype arguments cannot be null");

		//    if (values.Any(v => v.GetType().IsPrimitive() == false))
		//        throw new NotSupportedException("Only primitive values can be made into prototypes (for now)");

		//    var url = new UrlHelper(ViewContext.RequestContext).Action(expression, values, Core.Storage.CachePriority.Default);

		//    for (var i = 0; i < values.Length; i++)
		//    {
		//        url = _prototyper.Replace(
		//            url,
		//            new MatchEvaluator(
		//                m =>
		//                {
		//                    var existing = m.Groups[1].Value;
		//                    var value = ViewContext.HttpContext.Server.UrlEncode(values[i].ToString()).Replace("+", "%20");

		//                    return existing == value ? "{" + i + "}" : existing;
		//                }
		//            )
		//        );
		//    }

		//    var result = new ProtoUrlInner(url);

		//    _protoUrls[result.Id] = result;

		//    return result;
		//}

		///// <summary>
		///// Gets a list of all ProtoUrls which have been registered
		///// to this helper (by calling ProtoUrl method)
		///// </summary>
		//public IEnumerable<FirstLastEnumerable<IProtoUrl>> ProtoUrls
		//{
		//    get
		//    {
		//        return _protoUrls.Values.WithFirstLast();
		//    }
		//}

		///// <summary>
		///// Gets a proto url from an expression
		///// </summary>
		///// <typeparam name="TController">The type of the controller</typeparam>
		///// <typeparam name="TA">The type of the first example argument</typeparam>
		///// <param name="expression">An expression to get the url from</param>
		///// <param name="a">The first argument which allows production of the urls you want</param>
		///// <returns>A proto url derived from the expression and example arguments</returns>
		///// <remarks>
		///// <para>Only simple types (primitives + nullables + strings + enums + decimal + guid) are currently supported as arguments</para>
		///// <para>Only non-null values are currently supported as arguments</para>
		///// <para>The example values must be in the same general format which you expect to pass in later if you have any route constraints defined</para>
		///// </remarks>
		//public IProtoUrl ProtoUrl<TController, TA>(Expression<Func<TController, TA, ActionResult>> expression, TA a) where TController : Controller
		//{
		//    return ProtoUrl(expression, new object[] { a });
		//}

		///// <summary>
		///// Gets a proto url from an expression
		///// </summary>
		///// <typeparam name="TController">The type of the controller</typeparam>
		///// <typeparam name="TA">The type of the first example argument</typeparam>
		///// <typeparam name="TB">The type of the second example argument</typeparam>
		///// <param name="expression">An expression to get the url from</param>
		///// <param name="a">The first argument which allows production of the urls you want</param>
		///// <param name="b">The second argument which allows production of the urls you want</param>
		///// <returns>A proto url derived from the expression and example arguments</returns>
		///// <remarks>
		///// <para>Only simple types (primitives + nullables + strings + enums + decimal + guid) are currently supported as arguments</para>
		///// <para>Only non-null values are currently supported as arguments</para>
		///// <para>The example values must be in the same general format which you expect to pass in later if you have any route constraints defined</para>
		///// </remarks>
		//public IProtoUrl ProtoUrl<TController, TA, TB>(Expression<Func<TController, TA, TB, ActionResult>> expression, TA a, TB b) where TController : Controller
		//{
		//    return ProtoUrl(expression, new object[] { a, b });
		//}

		///// <summary>
		///// Gets a proto url from an expression
		///// </summary>
		///// <typeparam name="TController">The type of the controller</typeparam>
		///// <typeparam name="TA">The type of the first example argument</typeparam>
		///// <typeparam name="TB">The type of the second example argument</typeparam>
		///// <typeparam name="TC">The type of the third example argument</typeparam>
		///// <param name="expression">An expression to get the url from</param>
		///// <param name="a">The first argument which allows production of the urls you want</param>
		///// <param name="b">The second argument which allows production of the urls you want</param>
		///// <param name="c">The third argument which allows production of the urls you want</param>
		///// <returns>A proto url derived from the expression and example arguments</returns>
		///// <remarks>
		///// <para>Only simple types (primitives + nullables + strings + enums + decimal + guid) are currently supported as arguments</para>
		///// <para>Only non-null values are currently supported as arguments</para>
		///// <para>The example values must be in the same general format which you expect to pass in later if you have any route constraints defined</para>
		///// </remarks>
		//public IProtoUrl ProtoUrl<TController, TA, TB, TC>(Expression<Func<TController, TA, TB, TC, ActionResult>> expression, TA a, TB b, TC c) where TController : Controller
		//{
		//    return ProtoUrl(expression, new object[] { a, b, c });
		//}

		//#endregion Proto Url

		//#region Call

		///// <summary>
		///// Creates a deferred scope using an existing prototype and given values
		///// </summary>
		///// <param name="protoUrl">The prototype to make a deferred scope from</param>
		///// <param name="values">The values to place into the proto url</param>
		///// <returns>A deferred scope</returns>
		///// <remarks>
		///// There are no guarantees that the scope will be correct with the values you've given
		///// if you did not create the proto url with good example values
		///// </remarks>
		//public IDisposable CallScope(IProtoUrl protoUrl, params object[] values)
		//{
		//    var dict = new Dictionary<string, object>
		//    {
		//        { "class", "deferred" },
		//        { "data-defer-prototype", protoUrl.Id }
		//    };

		//    for (var i = 0; i < values.Length; i++)
		//    {
		//        dict.Add("data-defer-v" + i, string.Format("{0}", values[i]));
		//    }

		//    return new TagScope(ViewContext, "div", dict);
		//}

		//private IDisposable CallScope(Expression expression, object[] values)
		//{
		//    var htmlAttributes = new Dictionary<string,object>
		//    {
		//        { "class", "deferred" },
		//        { "data-defer-url", new UrlHelper(ViewContext.RequestContext).Action(expression, values, Core.Storage.CachePriority.Default) }
		//    };

		//    return new TagScope(ViewContext, "div", htmlAttributes);
		//}

		///// <summary>
		///// Creates a deferred scope using an expression
		///// </summary>
		///// <typeparam name="TController">The type of controller</typeparam>
		///// <param name="expression">The expression to convert into a url for the deferred scope</param>
		///// <returns>A deferred scope</returns>
		//public IDisposable CallScope<TController>(Expression<Func<TController, ActionResult>> expression) where TController : Controller
		//{
		//    return CallScope(expression, new object[0]);	
		//}

		///// <summary>
		///// Creates a deferred scope using an expression
		///// </summary>
		///// <typeparam name="TController">The type of controller</typeparam>
		///// <typeparam name="TA">The type of the first argument</typeparam>
		///// <param name="expression">The expression to convert into a url for the deferred scope</param>
		///// <param name="a">The value of the first argument</param>
		///// <returns>A deferred scope</returns>
		//public IDisposable CallScope<TController, TA>(Expression<Func<TController, TA, ActionResult>> expression, TA a) where TController : Controller
		//{
		//    return CallScope(expression, new object[] { a });
		//}

		///// <summary>
		///// Creates a deferred scope using an expression
		///// </summary>
		///// <typeparam name="TController">The type of controller</typeparam>
		///// <typeparam name="TA">The type of the first argument</typeparam>
		///// <typeparam name="TB">The type of the second argument</typeparam>
		///// <param name="expression">The expression to convert into a url for the deferred scope</param>
		///// <param name="a">The value of the first argument</param>
		///// <param name="b">The value of the second argument</param>
		///// <returns>A deferred scope</returns>
		//public IDisposable CallScope<TController,TA,TB>(Expression<Func<TController, TA, TB, ActionResult>> expression, TA a, TB b) where TController : Controller
		//{
		//    return CallScope(expression, new object[] { a, b });
		//}

		///// <summary>
		///// Creates a deferred scope using an expression
		///// </summary>
		///// <typeparam name="TController">The type of controller</typeparam>
		///// <typeparam name="TA">The type of the first argument</typeparam>
		///// <typeparam name="TB">The type of the second argument</typeparam>
		///// <typeparam name="TC">The type of the third argument</typeparam>
		///// <param name="expression">The expression to convert into a url for the deferred scope</param>
		///// <param name="a">The value of the first argument</param>
		///// <param name="b">The value of the second argument</param>
		///// <param name="c">The value of the third argument</param>
		///// <returns>A deferred scope</returns>
		//public IDisposable CallScope<TController,TA,TB,TC>(Expression<Func<TController,TA,TB,TC,ActionResult>> expression, TA a, TB b, TC c) where TController : Controller
		//{
		//    return CallScope(expression, new object[] { a, b, c });
		//}

		//#endregion Call
	}
}