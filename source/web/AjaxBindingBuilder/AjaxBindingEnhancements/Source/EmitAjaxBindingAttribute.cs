﻿using System;

namespace JLabs.Web.Mvc.AjaxBindingsBuilder
{
	[AttributeUsage(AttributeTargets.Method|AttributeTargets.Class, AllowMultiple=false, Inherited=false)]
	public class EmitAjaxBindingAttribute : Attribute
	{
	}
}
