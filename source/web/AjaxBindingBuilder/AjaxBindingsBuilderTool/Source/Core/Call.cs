﻿using System;

namespace JLabs.Tools.Web.AjaxBindingsBuilder.Core
{
	[Serializable]
	public class Call : BindingBase
	{
		public string FileName
		{
			get { return FileNameFullPath.Substring(FileNameFullPath.LastIndexOf('\\') + 1); }
		}

		public string FileNameFullPath { get; set; }
		public int LineNumber { get; set; }
		public int CharacterNumber { get; set; }

		public int ArgumentCount { get; set; }
		public bool IsReferenceCall { get; set; }
	}
}
