﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Routing;
using System.Web.Mvc;

namespace JLabs.Tools.Web.AjaxBindingsBuilder.Core
{
	[Serializable]
	public class RouteBinding
	{
		private static readonly Regex _paramExtractor = new Regex(@"{(\w+)}");

		public RouteBinding(Route route)
		{
			UrlPrototype = '/' + route.Url.Trim('/');

			var requiredParameters = _paramExtractor
				.Matches(UrlPrototype)
				.Cast<Match>()
				.Select(m => m.Groups[1].Value)
				.ToList();

			Constraints = route.Constraints.ToDictionary(
				c => c.Key,
				c => string.Format(
					"^{0}$",
					c.Value.ToString().TrimStart('^').TrimEnd('$')
				)
			);

			var defaults = new Dictionary<string, string>();
	
			foreach (var def in route.Defaults)
			{
				if (def.Value == UrlParameter.Optional)
				{
					requiredParameters.Remove(def.Key);
					continue;
				}

				defaults.Add(def.Key, def.Value.ToString());
			}

			var removed = new List<string>();

			// anything which appears in constraints but not in required parameters must be removed
			foreach (var constraint in Constraints)
			{
				if (!requiredParameters.Any(rp => rp == constraint.Key))
					removed.Add(constraint.Key);
			}

			Defaults = defaults;
			RequiredParameters = requiredParameters;
			RemoveParameters = removed;
		}

		public string Name { get; set; }
		public string UrlPrototype { get; private set; }
		public IEnumerable<string> RequiredParameters { get; private set; }
		public IEnumerable<string> RemoveParameters { get; set; }
		public Dictionary<string, string> Constraints { get; private set; }
		public Dictionary<string, string> Defaults { get; private set; }
	}
}
