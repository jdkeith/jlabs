﻿using System;
using System.Collections.Generic;

using JLabs.Core.Utilities;

namespace JLabs.Tools.Web.AjaxBindingsBuilder.Core
{
	[Serializable]
	public class BoundParameter
	{
		private readonly Type _prototype;

		public BoundParameter(string name, int index, Type protoType)
		{
			Name = name;
			Index = index;
			_prototype = protoType;
		}

		public string Name { get; private set; }
		public int Index { get; private set; }

		public Type Prototype
		{
			get
			{
				if (_prototype == null)
					return null;

				if (IsList)
				{
					var result = _prototype.GetGenericArguments()[0];

					if (result.IsPrimitive())
						return null;

					return result;
				}

				return _prototype;
			}
		}

		public bool IsList
		{
			get
			{
				return
					_prototype != null &&
					_prototype.IsGenericType &&
					typeof(IEnumerable<>).IsAssignableFrom(_prototype.GetGenericTypeDefinition());
			}
		}
	}

	[Serializable]
	public class Binding : BindingBase
	{
		public string HttpMethod { get; set; }
		public IEnumerable<BoundParameter> Parameters { get; set; }
	}
}
