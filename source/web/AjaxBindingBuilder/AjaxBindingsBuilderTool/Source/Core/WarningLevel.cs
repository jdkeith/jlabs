﻿namespace JLabs.Tools.Web.AjaxBindingsBuilder.Core
{
	public enum WarningLevel
	{
		None,
		Warning,
		Error
	}
}
