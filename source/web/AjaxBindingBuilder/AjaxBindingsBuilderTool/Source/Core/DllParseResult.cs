﻿using System;
using System.Collections.Generic;

namespace JLabs.Tools.Web.AjaxBindingsBuilder.Core
{
	[Serializable]
	public class DllParseResult
	{
		public DllParseResult(IEnumerable<RouteBinding> routes, IEnumerable<Binding> bindings, IEnumerable<Prototype> prototypes)
		{
			Routes = routes;
			Bindings = bindings;
			Prototypes = prototypes;
		}

		public IEnumerable<Binding> Bindings { get; private set; }
		public IEnumerable<RouteBinding> Routes { get; private set; }
		public IEnumerable<Prototype> Prototypes { get; private set; }
	}
}
