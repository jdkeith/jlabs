﻿using System;
using System.Collections.Generic;
using System.Reflection;

using JLabs.Core.Utilities;

namespace JLabs.Tools.Web.AjaxBindingsBuilder.Core
{
	[Serializable]
	public class Prototype
	{
		public Prototype(Type type)
		{
			const BindingFlags bindingFlags =
				BindingFlags.Public | BindingFlags.SetProperty | BindingFlags.GetProperty | BindingFlags.Instance | BindingFlags.FlattenHierarchy;

			Type = type;

			var properties = new Dictionary<string,Type>();

			foreach (var property in type.GetProperties(bindingFlags))
			{
				if( property.PropertyType.IsPrimitive() )
				{
					properties.Add(property.Name, null);
				}
				else
				{
					properties.Add(property.Name, property.PropertyType);
				}
			}

			Properties = properties;
		}

		public Type Type { get; private set; }
		public Dictionary<string, Type> Properties { get; private set; }

		public override bool Equals(object obj)
		{
			var pobj = obj as Prototype;

			if (ReferenceEquals(null, pobj))
				return false;

			return pobj.Type == Type;
		}

		public override int GetHashCode()
		{
			return Type.GetHashCode();
		}
	}
}
