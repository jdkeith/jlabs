﻿namespace JLabs.Tools.Web.AjaxBindingsBuilder.Core
{
	public enum ScanFileType
	{
		File,
		Directory,
		Pattern
	}

	public class ScanFile
	{
		public string Value { get; set; }
		public ScanFileType Type { get; set; }
		public bool Ignore { get; set; }
	}
}