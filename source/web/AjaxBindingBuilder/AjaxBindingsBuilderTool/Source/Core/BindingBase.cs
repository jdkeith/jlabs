﻿using System;

namespace JLabs.Tools.Web.AjaxBindingsBuilder.Core
{
	[Serializable]
	public abstract class BindingBase
	{
		public string ControllerName { get; set; }
		public string ActionName { get; set; }

		public override string ToString()
		{
			return string.Format("{0}.{1}", ControllerName, ActionName);
		}
	}
}
