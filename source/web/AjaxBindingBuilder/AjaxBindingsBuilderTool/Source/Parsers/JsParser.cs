﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

using JLabs.Tools.Web.AjaxBindingsBuilder.Configuration;
using JLabs.Tools.Web.AjaxBindingsBuilder.Core;
using JLabs.Tools.Web.AjaxBindingsBuilder.File;

namespace JLabs.Tools.Web.AjaxBindingsBuilder.Parsers
{
	public class JsParser
	{
		#region Fields

		private const string BasePattern = @"
			(?<controller> \w+ )
			(
				\s* \. \s*
				(?<action> \w+ ) \s*
			)?
		";

		private readonly Regex _startCallPattern;
		private readonly BindingConfiguration _config;
		private readonly FileFinder _fileFinder;

		#endregion Fields

		#region Constructor

		public JsParser(FileFinder fileFinder, BindingConfiguration config)
		{
			_fileFinder = fileFinder;
			_config = config;

			_startCallPattern = GetStartCallPattern(config.BindingsNamespace);
		}

		#endregion Constructor

		#region Parse

		public IEnumerable<Call> GetCalls()
		{
			var callsList = new List<Call>();

			foreach (var javascriptFile in _fileFinder.Files)
			{
				string fileContents;

				using (var streamReader = javascriptFile.OpenText())
				{
					fileContents = streamReader.ReadToEnd();
				}

				var matches = _startCallPattern.Matches(fileContents);

				var closureSafeJavascriptPath = javascriptFile.FullName;

				callsList.AddRange(
					from
					match in matches.Cast<Match>()
					select MatchToCall(closureSafeJavascriptPath, fileContents, match)
				);
			}

			return callsList;
		}

		private static Call MatchToCall(string fileNameFullPath, string input, Match match)
		{
			int lineNumber;
			int characterNumber;

			GetFilePosition(input, match.Index, out lineNumber, out characterNumber);

			var call = new Call
			{
				FileNameFullPath = fileNameFullPath,
				LineNumber = lineNumber,
				CharacterNumber = characterNumber,

				ControllerName = match.Groups["controller"].Value,
				ActionName = match.Groups["action"].Value,
			};

			var argumentCount = 0;

			var parensCount = 0;
			var openingParensSeen = false;
			var isEscaped = false;
			char? ignoreUntil = null;

			for (var i = match.Index + match.Length; i < input.Length; i++)
			{
				var c = input[i];

				// if the character is escaped, skip the one following it
				if (isEscaped)
				{
					isEscaped = false;
					continue;
				}

				if (c == '\\')
				{
					isEscaped = true;
					continue;
				}

				// if we're looking for a closing item, ignore anything that isn't that
				if (ignoreUntil.HasValue)
				{
					if (ignoreUntil == c)
						ignoreUntil = null;

					continue;
				}

				if (argumentCount == 0 && char.IsWhiteSpace(c) == false && c != ')')
					argumentCount += parensCount == 1 ? 1 : 0;

				switch (c)
				{
					case '\\':
						isEscaped = true;
						break;

					case '\'':
						ignoreUntil = '\'';
						break;

					case '"':
						ignoreUntil = '"';
						break;

					case '{':
						ignoreUntil = '}';
						break;

					case '(':
						openingParensSeen = true;
						parensCount++;
						break;

					case ')':
						parensCount--;
						break;

					case ',':
						argumentCount += parensCount == 1 ? 1 : 0;
						break;

					case ';':
						if (openingParensSeen == false)
							call.IsReferenceCall = true;
						break;
				}

				if (call.IsReferenceCall || (openingParensSeen && parensCount == 0))
					break;
			}

			call.ArgumentCount = argumentCount;

			return call;
		}

		private static void GetFilePosition(
			string input, int filePosition,
			out int lineNumber, out int characterNumber
		)
		{
			lineNumber = 0;

			var startPosition = 0;

			do
			{
				characterNumber = filePosition - startPosition + 1;

				startPosition = input.IndexOf('\n', startPosition) + 1;

				lineNumber++;

			} while (startPosition > -1 && startPosition < filePosition);
		}

		#endregion Parse

		#region Utilities

		private static Regex GetStartCallPattern(string jsBindingsNamespace)
		{
			var prefix = string.Join(
				"\n",
				jsBindingsNamespace.Split('.')
				.Select(jsb => jsb + @" \s* \. \s*")
			);

			return new Regex(prefix + BasePattern, RegexOptions.IgnoreCase | RegexOptions.IgnorePatternWhitespace);
		}

		#endregion Utilities
	}
}
