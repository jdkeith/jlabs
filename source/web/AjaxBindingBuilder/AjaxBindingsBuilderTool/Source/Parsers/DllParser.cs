﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web.Mvc;
using System.Web.Routing;

using JLabs.Core.Utilities;

using JLabs.Tools.Web.AjaxBindingsBuilder.Core;
using JLabs.Tools.Web.AjaxBindingsBuilder.Configuration;
using JLabs.Web.Mvc.AjaxBindings;

namespace JLabs.Tools.Web.AjaxBindingsBuilder.Parsers
{
	public class DllParser
	{
		#region Fields

		private readonly Assembly _interfaceAssembly;

		private static readonly Type _emitMarkerAttributeType = typeof(EmitAjaxBindingsAttribute);
		private static readonly Type _controllerBaseType = typeof(Controller);
		private static readonly Type _expectedReturnValueBaseType = typeof(ActionResult);
		private static readonly Type _childActionOnlyType = typeof(ChildActionOnlyAttribute);

		private readonly BindingConfiguration _config;
		private readonly MethodInfo _routeRegistrationMethod;

		#endregion Fields

		#region Constructor

		public DllParser(BindingConfiguration config)
		{
			_config = config;
			_interfaceAssembly = Assembly.LoadFrom(config.AbsoluteDllPath);

			var splitRouteRegistrationInfo = config.SiteRoutesMappingMethodFullName.Split('.');
			var routeRegistrationClass = _interfaceAssembly.GetTypes().First(t => t.Name == splitRouteRegistrationInfo[0]);
			_routeRegistrationMethod = routeRegistrationClass.GetMethod(
				splitRouteRegistrationInfo[1], BindingFlags.Static | BindingFlags.Public, null, new Type[] { typeof(RouteCollection) }, null
			);
		}

		public DllParseResult Parse()
		{
			var routes = ParseRoutes();

			var controllerTypes =
				from aInterfaceType in _interfaceAssembly.GetTypes()
				where _controllerBaseType.IsAssignableFrom(aInterfaceType) &&
					  aInterfaceType.IsAbstract == false
				select aInterfaceType;

			var bindings = new List<Binding>();

			foreach (var controllerType in controllerTypes)
			{
				bindings.AddRange(ParseController(controllerType));
			}

			var prototypes = GetPrototypes(bindings);

			return new DllParseResult(
				routes, bindings, prototypes
			);
		}

		private IEnumerable<Prototype> GetPrototypes(IEnumerable<Binding> bindings)
		{
			var result = new List<Prototype>();

			BuildPrototypesList(
				result,
				bindings
					.SelectMany(b => b.Parameters)
					.Select(b => b.Prototype)
					.Where(p => p != null)
			);

			return result;
		}

		private void BuildPrototypesList(List<Prototype> seen, IEnumerable<Type> types)
		{
			foreach (var type in types)
			{
				if (seen.Any(p => p.Type == type))
					continue;

				var prototype = new Prototype(type);

				seen.Add(prototype);

				BuildPrototypesList(
					seen,
					prototype.Properties
						.Where(p => p.Value != null)
						.Select(p => p.Value)
				);
			}
		}

		private IEnumerable<RouteBinding> ParseRoutes()
		{
			// run the registration method and see what comes out
			var routeCollection = new RouteCollection();

			_routeRegistrationMethod.Invoke(null, new object[] { routeCollection });

			return routeCollection
				.Where( r => r is Route )
				.Cast<Route>()
				.Where( r => ! (r.RouteHandler is StopRoutingHandler))
				.Select(r => new RouteBinding(r))
				.ToList();
		}

		private IEnumerable<Binding> ParseController(Type controllerType)
		{
			const BindingFlags bindingFlags =
				BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly;

			var controllerName = controllerType.Name.Replace("Controller", "").ToCamelCase();

			return
			    from method in controllerType.GetMethods(bindingFlags)
			    where _expectedReturnValueBaseType.IsAssignableFrom(method.ReturnType) &&
			          IsEmitterMarked(method)
			    select ToBinding(method, controllerName);
		}

		private bool IsEmitterMarked(MethodInfo method)
		{
			var result = method.GetCustomAttributes(_emitMarkerAttributeType, false).Any();

			if (result && method.GetCustomAttributes(_childActionOnlyType, false).Any())
				throw new Exception("EmitAjaxBindingAttribute cannot be added to a method with ChildActionOnlyAttribute");
				// todo: better exception type

			return result;
		}

		private Binding ToBinding(MethodInfo method, string cachedControllerName)
		{
		    var result = new Binding
		    {
		        ControllerName = cachedControllerName,
		        ActionName = method.Name.ToCamelCase()
		    };

			var index = 0;

			var boundParameters = new List<BoundParameter>();

			foreach( var argument in method.GetParameters() )
			{
				Type prototype = null;

				if (!argument.ParameterType.IsPrimitive())
					prototype = argument.ParameterType;

				var boundParameter = new BoundParameter(argument.Name, index++, prototype);

				boundParameters.Add(boundParameter);
			}

			result.Parameters = boundParameters;
			result.HttpMethod = GetHttpMethod(method);

			return result;
		}

		private static string GetHttpMethod(MethodInfo method)
		{
		    var httpPost = method.GetCustomAttributes(typeof(HttpPostAttribute), false);
		    var httpPut = method.GetCustomAttributes(typeof(HttpPutAttribute), false);
		    var httpDelete = method.GetCustomAttributes(typeof(HttpDeleteAttribute), false);

		    var httpAccept = method.GetCustomAttributes(typeof(AcceptVerbsAttribute), false);

		    if (httpPost.Length > 0)
		        return "POST";

		    if (httpPut.Length > 0)
		        return "PUT";

		    if (httpDelete.Length > 0)
		        return "DELETE";

		    if (httpAccept.Length > 0)
		    {
		        var verbsList = ((AcceptVerbsAttribute) httpAccept[0]).Verbs;

		        if (verbsList.Count == 1)
		            return verbsList.First().ToUpper();
		    }

		    return "GET";
		}

		#endregion Constructor
	}
}
