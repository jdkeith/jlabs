﻿using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.IO;
using System.Text.RegularExpressions;

using JLabs.Core.Utilities.Path;
using JLabs.Tools.Web.AjaxBindingsBuilder.Core;
using JLabs.Tools.Web.AjaxBindingsBuilder.Configuration;


namespace JLabs.Tools.Web.AjaxBindingsBuilder.File
{
	public class FileFinder
	{
		private readonly IEnumerable<ScanFile> _searchLocations;

		public FileFinder(BindingConfiguration config)
		{
			DirectoryRoot = new FileInfo(config.AbsoluteConfigPath).Directory.FullName;

			_searchLocations = config.SearchLocations;
		}

		private IEnumerable<FileInfo> _cachedFiles;

		public string DirectoryRoot { get; private set; }

		public IEnumerable<FileInfo> Files
		{
			get
			{
				if (_cachedFiles != null)
					return _cachedFiles;

				var results = new Dictionary<string, FileInfo>();

				foreach (var searchLocation in _searchLocations.Where(sf => !sf.Ignore))
				{
					switch (searchLocation.Type)
					{
						case ScanFileType.Directory:
							var di = new DirectoryInfo(searchLocation.Value.ToAbsolutePath(DirectoryRoot));
							if (!di.Exists) continue;

							foreach (var subFi in di.GetFiles("*.js", SearchOption.AllDirectories))
							{
								results[subFi.FullName] = subFi;
							}

							break;

						case ScanFileType.File:
							var fi = new FileInfo(searchLocation.Value.ToAbsolutePath(DirectoryRoot));
							if (!fi.Exists) continue;
							results[fi.FullName] = fi;
							break;

						default:
							throw new ConfigurationException("Invalid search location scan type '" + searchLocation.Type + "'");
					}
				}

				foreach (var ignoreLocation in _searchLocations.Where(sf => sf.Ignore))
				{
					List<string> rKeys;

					switch (ignoreLocation.Type)
					{
						case ScanFileType.File:
							results.Remove(ignoreLocation.Value.ToAbsolutePath(DirectoryRoot));
							break;

						case ScanFileType.Directory:
							var dirPath = ignoreLocation.Value.ToAbsolutePath(DirectoryRoot);

							rKeys = results.Keys.Where(k => k.ToLower().StartsWith(dirPath)).ToList();

							foreach (var key in rKeys)
							{
								results.Remove(key);
							}

							break;

						case ScanFileType.Pattern:
							var regex = RegexFromFilePattern(ignoreLocation.Value);

							rKeys = results.Keys.Where(full => regex.IsMatch(Path.GetFileName(full))).ToList();

							foreach (var key in rKeys)
							{
								results.Remove(key);
							}

							break;

						default:
							throw new ConfigurationException("Invalid ignore location scan type '" + ignoreLocation.Type + "'");
					}
				}

				_cachedFiles = results.Values;

				return _cachedFiles;
			}
		}

		private static Regex RegexFromFilePattern(string filePattern)
		{
			filePattern = filePattern.Replace(".", "\\.");
			filePattern = filePattern.Replace("?", ".");
			filePattern = filePattern.Replace("*", ".*");
			filePattern = "^" + filePattern + "$";

			return new Regex(filePattern);
		}
	}
}
