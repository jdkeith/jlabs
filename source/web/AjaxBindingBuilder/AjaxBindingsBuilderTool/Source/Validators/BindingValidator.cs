﻿using System.Collections.Generic;
using System.Linq;

using JLabs.Tools.Web.AjaxBindingsBuilder.Configuration;
using JLabs.Tools.Web.AjaxBindingsBuilder.Core;
using JLabs.Tools.Web.AjaxBindingsBuilder.Writers;

namespace JLabs.Tools.Web.AjaxBindingsBuilder.Validators
{
	public class BindingValidator
	{
		#region Fields

		private readonly BindingConfiguration _configuration;
		private readonly ErrorWriter _writer;

		#endregion Fields

		#region Constructor

		public BindingValidator(ErrorWriter writer, BindingConfiguration config)
		{
			_configuration = config;
			_writer = writer;
		}

		#endregion Constructor

		#region Validation

		public bool Validate(IEnumerable<Binding> bindings, IEnumerable<Call> calls, bool theStopOnAnyFailure)
		{
			var isValid = true;

			var bindingsDictionary = bindings.ToDictionary(b => b.ToString());

			foreach (var call in calls)
			{
				Binding binding;
				var problemText = "";

				if (bindingsDictionary.TryGetValue(call.ToString(), out binding) == false)
				{
					problemText = "Invalid ajax binding '{0}'";
					isValid = false;
				}
				else
				{
					// can't check number of arguments for reference calls
					if (call.IsReferenceCall)
						continue;

					var aExpectedArgumentCount = binding.Parameters.Count();

					if (call.ArgumentCount < aExpectedArgumentCount)
					{
						problemText = "Ajax binding '{0}' called with too few arguments";
						isValid = false;
					}
					else if (call.ArgumentCount > aExpectedArgumentCount)
					{
						problemText = "Ajax binding '{0}' called with too many arguments";
						isValid = false;
					}
				}

				if (isValid == false)
				{
					_writer.Write(
						_configuration.WarningLevel,
						string.Format(problemText, call),
						call.FileNameFullPath,
						call.LineNumber,
						call.CharacterNumber
					);

					if (theStopOnAnyFailure)
						break;
				}
			}

			return isValid;
		}

		#endregion Validation
	}
}
