﻿#define TEST

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

using JLabs.Tools.Web.AjaxBindingsBuilder.Configuration;
using JLabs.Tools.Web.AjaxBindingsBuilder.Core;
using JLabs.Tools.Web.AjaxBindingsBuilder.File;
using JLabs.Tools.Web.AjaxBindingsBuilder.Parsers;
using JLabs.Tools.Web.AjaxBindingsBuilder.Validators;
using JLabs.Tools.Web.AjaxBindingsBuilder.Writers;

namespace BindingsBuilder.cs
{
	class Program
	{
		private static int Main(string[] args)
		{
			var waitForInput = false;

#if TEST
            args = new[]
            {
				@"C:\Users\jkeith\Desktop\jlabs\trunk\source\web\AjaxBindingBuilder\AjaxBindingsBuilderTool\Source\test.xml"
            };

			waitForInput = true;
#endif

            if (! args.Any())
            {
                Console.Error.WriteLine("Error: Required configuration file path argument not passed to AjaxBindingBuilder");
                return 1;
            }

            var configurationFilePath = args[0];
            ErrorWriter writer;
		    LogWriter logger = null;
			BindingConfiguration config;

			try
			{
				config = new BindingConfigurationLoader(args[0]).Instance;
			}
			catch(Exception ex)
			{
				Console.Error.WriteLine("Error: Configuration file not found or problem loading configuration file");
				Console.Error.WriteLine("\n{0}", ex.Message);
                return 2;
			}

            if( config.ReportToVisualStudio )
				writer = new VsErrorWriter();
			else
				writer = new ConsoleErrorWriter(config.Interactive);

			if( ! string.IsNullOrEmpty(config.AbsoluteLogPath))
			{
				logger = new LogWriter(config.AbsoluteLogPath);

                writer.OnError += logger.DoError;
                writer.OnMessage += logger.DoMessage;
            }

			try
			{
				writer.Message("\nParsing DLL");

				var parseResults = new DllParser(config).Parse();

				foreach (var binding in parseResults.Bindings)
				{
					writer.Message("\tFound binding: {0}.{1}", binding.ControllerName, binding.ActionName);
				}

				var fileFinder = new FileFinder(config);

				writer.Message("\nSearching for javascript files");

				foreach (var file in fileFinder.Files)
				{
					writer.Message("\tFound: {0}", file);
				}

				var jsWriter = new JsWriter(fileFinder, config);

				writer.Message("\nWriting bindings file");

				jsWriter.WriteBindingsFile(parseResults);

				if (parseResults.Bindings.Any())
				{
					writer.Message("\nWriting bindings references into existing javascript files");
					jsWriter.WriteHeaderIntoJsFiles();
				}

				writer.Message("\nParsing existing javascript files");

				var jsParser = new JsParser(fileFinder, config);

				var callsList = jsParser.GetCalls();

				foreach (var call in callsList)
				{
					writer.Message("\tFound call: {0}.{1} in {2}", call.ControllerName, call.ActionName, call.FileName);
				}

				var bindingValidator = new BindingValidator(writer, config);

				writer.Message("\nValidating calls in existing javascript files");

				bindingValidator.Validate(parseResults.Bindings, callsList, false);
			}
			catch (ConfigurationException cex)
			{
				writer.Write(WarningLevel.Error, cex.Message + "\n" + cex.StackTrace, configurationFilePath, 1, 1);
				writer.WaitForInput();

				//logger.Log(cex);

				return 1;
			}
			catch (Exception ex)
			{
				writer.Write(WarningLevel.Error, ex.Message + "\n" + ex.StackTrace, "", 0, 0);
				writer.WaitForInput();

				//logger.Log(ex);

				return 1;
			}

            writer.WaitForInput();

            return 0;
        }
	}
}
