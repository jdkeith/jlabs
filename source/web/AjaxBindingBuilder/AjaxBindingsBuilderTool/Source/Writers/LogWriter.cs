﻿using System.IO;

namespace JLabs.Tools.Web.AjaxBindingsBuilder.Writers
{
	public class LogWriter
	{
		private readonly StreamWriter _streamWriter;

		public LogWriter(string logFile)
		{
			_streamWriter = new StreamWriter(new FileInfo(logFile).Open(FileMode.Create));
		}

		~LogWriter()
		{
			if (_streamWriter != null)
				_streamWriter.Dispose();
		}

		private void DoLog(string message)
		{
			_streamWriter.Write(message + "\r\n");
		}

		public void DoMessage(object sender, MessageEventArgs e)
		{
			DoLog(e.Message);
		}

		public void DoError(object sender, ErrorEventArgs e)
		{
			DoLog(
				string.Format(
					"{0}({1},{2}):{3}\r\n\t{4}",
					e.FileName, e.LineNumber, e.CharacterNumber,
					e.ErrorLevel, e.Description
				)
			);
		}
	}
}