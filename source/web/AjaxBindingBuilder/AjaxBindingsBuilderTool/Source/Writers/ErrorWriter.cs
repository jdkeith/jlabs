﻿using System;

using JLabs.Tools.Web.AjaxBindingsBuilder.Core;

namespace JLabs.Tools.Web.AjaxBindingsBuilder.Writers
{
	public class MessageEventArgs : EventArgs
	{
		public MessageEventArgs(string message)
		{
			Message = message;
		}

		public string Message { get; private set; }
	}

	public class ErrorEventArgs : EventArgs
	{
		public ErrorEventArgs(
			WarningLevel errorLevel, string description, string filename,
			int lineNumber, int characterNumber
		)
		{
			ErrorLevel = errorLevel;
			Description = description;
			FileName = filename;
			LineNumber = lineNumber;
			CharacterNumber = characterNumber;
		}

		public WarningLevel ErrorLevel { get; private set; }
		public string Description { get; private set; }
		public string FileName { get; private set; }
		public int LineNumber { get; private set; }
		public int CharacterNumber { get; private set; }
	}

	public abstract class ErrorWriter
	{
		public abstract void Write
			(WarningLevel theErrorLevel, string theDescription, string theFileName,
			int theLineNumber, int theCharacterNumber
		);

		public abstract void Message(string msg, params object[] args);

		public abstract void WaitForInput();

		public delegate void MessageEventHandler(object sender, MessageEventArgs e);
		public delegate void ErrorEventHandler(object sender, ErrorEventArgs e);

		public event MessageEventHandler OnMessage;
		public event ErrorEventHandler OnError;

		protected void FireError(
			WarningLevel theErrorLevel, string theDescription, string theFileName,
			int theLineNumber, int theCharacterNumber
		)
		{
			if (OnError != null)
				OnError(
					this,
					new ErrorEventArgs(theErrorLevel, theDescription, theFileName, theLineNumber, theCharacterNumber)
				);
		}

		protected void FireMessage(string message)
		{
			if (OnMessage != null)
				OnMessage(this, new MessageEventArgs(message));
		}
	}

	public class ConsoleErrorWriter : ErrorWriter
	{
		private readonly bool _waitForInput;

		public ConsoleErrorWriter(bool waitForInput)
		{
			_waitForInput = waitForInput;
		}

		public override void Write(
			WarningLevel theErrorLevel, string theDescription, string theFileName,
			int theLineNumber, int theCharacterNumber
		)
		{
			var aFilePositionString = "";

			if (!string.IsNullOrEmpty(theFileName) && theLineNumber > 0)
				aFilePositionString = string.Format("Line {0} Character {1}", theLineNumber, theCharacterNumber);

			switch (theErrorLevel)
			{
				case WarningLevel.Error:
					Console.ForegroundColor = ConsoleColor.Red;
					break;

				case WarningLevel.Warning:
					Console.ForegroundColor = ConsoleColor.Yellow;
					break;
			}

			Console.WriteLine(
				"{0}\n\t{1}\n\t{2}\n",
				theFileName,
				aFilePositionString,
				theDescription
			);

			Console.ForegroundColor = ConsoleColor.Gray;

			FireError(theErrorLevel, theDescription, theFileName, theLineNumber, theCharacterNumber);
		}

		public override void WaitForInput()
		{
			if (!_waitForInput)
				return;

			Console.WriteLine("\nPRESS ENTER TO CONTINUE");
			Console.ReadLine();
		}

		public override void Message(string msg, params object[] args)
		{
			var result = string.Format(msg, args);

			Console.WriteLine(result);

			FireMessage(result);
		}
	}

	public class VsErrorWriter : ErrorWriter
	{
		public override void Write(
			WarningLevel theErrorLevel, string theDescription, string theFileName,
			int theLineNumber, int theCharacterNumber
		)
		{
			var aFilePositionString = "";

			if (!string.IsNullOrEmpty(theFileName) && theLineNumber > 0)
				aFilePositionString = string.Format("({0},{1}):", theLineNumber, theCharacterNumber);

			var aErrorLevelString = "";

			switch (theErrorLevel)
			{
				case WarningLevel.Error:
					aErrorLevelString = "error: ";
					break;

				case WarningLevel.Warning:
					aErrorLevelString = "warning: ";
					break;
			}

			Console.WriteLine("{0}{1}{2}{3}", theFileName, aFilePositionString, aErrorLevelString, theDescription);

			FireError(theErrorLevel, theDescription, theFileName, theLineNumber, theCharacterNumber);
		}

		public override void WaitForInput()
		{
			// do nothing
		}

		public override void Message(string msg, params object[] args)
		{
			var result = string.Format(msg, args);

			FireMessage(result);
		}
	}
}