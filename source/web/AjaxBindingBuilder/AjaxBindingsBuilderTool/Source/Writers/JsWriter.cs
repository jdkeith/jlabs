﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Xml;
using System.Xml.Xsl;

using JLabs.Core.Utilities.Path;
using JLabs.Tools.Web.AjaxBindingsBuilder.Configuration;
using JLabs.Tools.Web.AjaxBindingsBuilder.File;
using JLabs.Tools.Web.AjaxBindingsBuilder.Core;

namespace JLabs.Tools.Web.AjaxBindingsBuilder.Writers
{
	public class JsWriter
	{
		#region Fields

		private const string _referenceHeaderFormat = "/// <reference path=\"{0}\" />";

		private readonly FileFinder _fileFinder;
		private readonly BindingConfiguration _config;

		#endregion Fields

		#region Constructor

		public JsWriter(FileFinder fileFinder, BindingConfiguration config)
		{
			_fileFinder = fileFinder;
			_config = config;
		}

		#endregion Constructor

		#region Write

		public void WriteBindingsFile(DllParseResult parseResults)
		{
			XslCompiledTransform transform;

			try
			{
				var thisAssembly = Assembly.GetExecutingAssembly();
				var embeddedResourceName = thisAssembly.GetManifestResourceNames().First(ern => ern.ToLower().EndsWith(".xsl"));
				var transformReader = XmlReader.Create(thisAssembly.GetManifestResourceStream(embeddedResourceName));
				transform = new XslCompiledTransform();
				transform.Load(transformReader, new XsltSettings { EnableScript = true }, null);
			}
			catch
			{
				throw new Exception("Unable to load output file transform document");
			}

			var data = CreateTransformDataDocument(parseResults);

			var transformStream = new MemoryStream();

			try
			{
				transform.Transform(data, new XsltArgumentList(), transformStream);

				var outputPath = _config.AbsoluteBindingsFileOutputPath.ToAbsolutePath(_fileFinder.DirectoryRoot) + "\\ajax-bindings.js";

				var buffer = new byte[65536];

				transformStream.Seek(0, SeekOrigin.Begin);

				using (var outputStream = new FileInfo(outputPath).Open(FileMode.Create))
				{
					int read;

					while ((read = transformStream.Read(buffer, 0, buffer.Length)) > 0)
					{
						outputStream.Write(buffer, 0, read);
					}
				}
			}
			finally
			{
				transformStream.Dispose();
			}
		}

		private XmlElement BindingToXml(XmlDocument owner, Binding binding)
		{
			var result = owner.CreateElement("binding");
			result.SetAttribute("controller", binding.ControllerName);
			result.SetAttribute("action", binding.ActionName);
			result.SetAttribute("httpMethod", binding.HttpMethod);

			foreach (var parameter in binding.Parameters)
			{
				var parmEl = owner.CreateElement("parameter");
				parmEl.SetAttribute("name", parameter.Name);
				parmEl.SetAttribute("index", parameter.Index.ToString());

				if (parameter.IsList)
					parmEl.SetAttribute("isList", "true");

				if (parameter.Prototype != null)
					parmEl.SetAttribute("prototype", parameter.Prototype.FullName);

				result.AppendChild(parmEl);
			}

			return result;
		}

		private XmlElement RouteToXml(XmlDocument owner, RouteBinding route)
		{
			var result = owner.CreateElement("route");
			result.SetAttribute("name", route.Name);
			result.SetAttribute("url", route.UrlPrototype);

			// required els
			var requiredsEl = owner.CreateElement("requireds");
			result.AppendChild(requiredsEl);

			foreach (var required in route.RequiredParameters)
			{
				var requiredEl = owner.CreateElement("required");
				requiredEl.AppendChild(owner.CreateTextNode(required));

				requiredsEl.AppendChild(requiredEl);
			}

			// remove els
			var removesEl = owner.CreateElement("removes");
			result.AppendChild(removesEl);

			foreach (var remove in route.RemoveParameters)
			{
				var removeEl = owner.CreateElement("remove");
				removeEl.AppendChild(owner.CreateTextNode(remove));

				removesEl.AppendChild(removeEl);
			}

			// constraint els
			var constraintsEl = owner.CreateElement("constraints");
			result.AppendChild(constraintsEl);

			foreach (var constraint in route.Constraints)
			{
				constraintsEl.SetAttribute(constraint.Key, constraint.Value);
			}

			return result;
		}

		private XmlElement PrototypeToXml(XmlDocument owner, Prototype prototype)
		{
			var result = owner.CreateElement("prototype");
			result.SetAttribute("name", prototype.Type.FullName);

			// fields
			var fieldsEl = owner.CreateElement("fields");
			result.AppendChild(fieldsEl);

			foreach (var field in prototype.Properties)
			{
				var fieldEl = owner.CreateElement("field");
				fieldEl.SetAttribute("name", field.Key);

				if (field.Value != null)
					fieldEl.SetAttribute("type", field.Value.FullName);

				fieldsEl.AppendChild(fieldEl);
			}

			return result;
		}

		private XmlDocument CreateTransformDataDocument(DllParseResult data)
		{
			var result = new XmlDocument();

			var outputEl = result.CreateElement("output");
			outputEl.SetAttribute("generatedFromDll", _config.AbsoluteDllPath);
			outputEl.SetAttribute("generatedFromConfig", _config.AbsoluteConfigPath);
			outputEl.SetAttribute("generatedDate", DateTime.Now.ToString());

			var routesEl = result.CreateElement("routes");
			var routeEls = data.Routes.Select(r => RouteToXml(result, r));

			foreach (var routeEl in routeEls)
			{
				routesEl.AppendChild(routeEl);
			}

			var prototypesEl = result.CreateElement("prototypes");

			// todo resolve dependencies ??
			var prototypeEls = data.Prototypes.Select(p => PrototypeToXml(result, p));

			foreach (var prototypeEl in prototypeEls)
			{
				prototypesEl.AppendChild(prototypeEl);
			}

			var bindingsEl = result.CreateElement("bindings");
			bindingsEl.SetAttribute("bindingsCollectionName", _config.BindingsNamespace);

			var bindingEls = data.Bindings
				.OrderBy(b => b.ControllerName + "." + b.ActionName)
				.Select(b => BindingToXml(result, b));

			var bindingUid = 0;

			foreach (var bindingEl in bindingEls)
			{
				bindingEl.SetAttribute("uid", "b" + bindingUid);
				bindingsEl.AppendChild(bindingEl);
				bindingUid++;
			}

			result.AppendChild(outputEl);
			outputEl.AppendChild(routesEl);
			outputEl.AppendChild(prototypesEl);
			outputEl.AppendChild(bindingsEl);

			return result;
		}

		public void WriteHeaderIntoJsFiles()
		{
			var fullOutputPath = _config.AbsoluteBindingsFileOutputPath.ToAbsolutePath(_fileFinder.DirectoryRoot) + "\\ajax-bindings.js";

			foreach (var javascriptFile in _fileFinder.Files)
			{
				var linesList = new List<string>();

				using (var streamReader = javascriptFile.OpenText())
				{
					string line;

					while ((line = streamReader.ReadLine()) != null)
					{
						linesList.Add(line);
					}
				}

				var calculatedHeader = string.Format(
					_referenceHeaderFormat,
					javascriptFile.Directory.FullName.ToRelativePath(fullOutputPath).Replace('\\', '/')
				);

				if (linesList.Any() == false || linesList.First() == calculatedHeader)
					continue;

				// chomp all existing references
				linesList = linesList.Where(line => !line.StartsWith("/// <reference path")).ToList();

				linesList.Insert(0, calculatedHeader);

				// write the lines back out
				using (var stream = javascriptFile.OpenWrite())
				{
					foreach (var line in linesList)
					{
						var contentBytes = Encoding.UTF8.GetBytes(line + "\r\n");

						stream.Write(contentBytes, 0, contentBytes.Length);
					}
				}
			}
		}

		#endregion Write
	}
}
