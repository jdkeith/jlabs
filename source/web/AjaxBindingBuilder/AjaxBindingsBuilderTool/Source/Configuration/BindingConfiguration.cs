﻿using System;
using System.Collections.Generic;

using JLabs.Tools.Web.AjaxBindingsBuilder.Core;

namespace JLabs.Tools.Web.AjaxBindingsBuilder.Configuration
{
	public class BindingConfiguration
	{
		public string BindingsNamespace { get; set; }

		public string AbsoluteConfigPath { get; set; }

		public string AbsoluteDllPath { get; set; }
		public string SiteRoutesMappingMethodFullName { get; set; }

		public bool ReportToVisualStudio { get; set; }
		public string AbsoluteLogPath { get; set; }

		public IEnumerable<ScanFile> SearchLocations { get; set; }

		public string AbsoluteBindingsFileOutputPath { get; set; }

		public WarningLevel WarningLevel { get; set; }

		public bool Interactive { get; set; }
	}
}
