﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Xml;

using JLabs.Core.Configuration;
using JLabs.Core.Utilities.Path;
using JLabs.Tools.Web.AjaxBindingsBuilder.Core;

namespace JLabs.Tools.Web.AjaxBindingsBuilder.Configuration
{
	public class BindingConfigurationLoader : IConfigurationFactory<BindingConfiguration>
	{
		private const string Namespace = "http://jlabs/schemas/ajax-bindings-builder";

		private BindingConfiguration _cache;
		private readonly string _configurationFilePath;
		private readonly string _myPath;

		public BindingConfigurationLoader(string configurationFilePath)
		{
			_configurationFilePath = configurationFilePath;
			_myPath = Assembly.GetExecutingAssembly().Location;
			_myPath = _myPath.Substring(0, _myPath.LastIndexOf('\\'));
		}

		public BindingConfiguration Instance
		{
			get
			{
				if (_cache == null)
				{
					var doc = new XmlDocument();
					doc.Load(_configurationFilePath);
					_cache = FromXml(doc);
				}

				return _cache;
			}
		}

		public object Create(object parent, object configContext, XmlNode section)
		{
			return FromXml(section);
		}

		private BindingConfiguration FromXml(XmlNode node)
		{
			var result = new BindingConfiguration { AbsoluteConfigPath = _configurationFilePath };

			result.Interactive = GetBoolFrom(node, "//abb:config/@interactive");

			result.SiteRoutesMappingMethodFullName =
				GetRequiredStringFrom(node, "//abb:config/abb:dll/@site-routes-mapping-method-full-name");

			result.AbsoluteDllPath =
				GetRequiredStringFrom(node, "//abb:config/abb:dll/@relative-path")
				.ToAbsolutePath(_myPath);

			result.AbsoluteLogPath =
				GetStringFrom(node, "//abb:config/abb:process/@log-path");

			if( result.AbsoluteLogPath != null )
				result.AbsoluteLogPath = result.AbsoluteLogPath.ToAbsolutePath(_myPath);

			result.ReportToVisualStudio =
				GetBoolFrom(node, "//abb:config/abb:process/@report-to-visual-studio");

			// js parser
			result.WarningLevel = (WarningLevel)Enum.Parse(
				typeof(WarningLevel),
				GetStringFrom(node, "//abb:config/abb:js-parser/@warning-level", WarningLevel.None),
				true
			);

			var searchLocations = new List<ScanFile>();

			searchLocations.AddRange(
				GetNodesFrom(node, "//abb:config/abb:locations/abb:scan/abb:file[@relative-path]")
					.Cast<XmlElement>()
					.Select(n => new ScanFile { Type = ScanFileType.File, Value = n.GetAttribute("relative-path").Replace('/', '\\') })
			);

			searchLocations.AddRange(
				GetNodesFrom(node, "//abb:config/abb:locations/abb:scan/abb:directory[@relative-path]")
					.Cast<XmlElement>()
					.Select(n => new ScanFile { Type = ScanFileType.Directory, Value = n.GetAttribute("relative-path").Replace('/', '\\') })
			);

			searchLocations.AddRange(
				GetNodesFrom(node, "//abb:config/abb:locations/abb:ignore/abb:pattern[@pattern]")
					.Cast<XmlElement>()
					.Select(n => new ScanFile { Type = ScanFileType.Pattern, Value = n.GetAttribute("pattern"), Ignore = true })
			);

			searchLocations.AddRange(
				GetNodesFrom(node, "//abb:config/abb:locations/abb:ignore/abb:file[@relative-path]")
					.Cast<XmlElement>()
					.Select(n => new ScanFile { Type = ScanFileType.File, Value = n.GetAttribute("relative-path").Replace('/', '\\'), Ignore = true })
			);

			searchLocations.AddRange(
				GetNodesFrom(node, "//abb:config/abb:locations/abb:ignore/abb:directory[@relative-path]")
					.Cast<XmlElement>()
					.Select(n => new ScanFile { Type = ScanFileType.Directory, Value = n.GetAttribute("relative-path").Replace('/', '\\'), Ignore = true })
			);

			result.SearchLocations = searchLocations;

			// js writer
			result.AbsoluteBindingsFileOutputPath =
				GetRequiredStringFrom(node, "//abb:config/abb:js-writer/@output-path");

			result.BindingsNamespace =
				GetRequiredStringFrom(node, "//abb:config/abb:js-writer/@bindings-namespace");

			return result;
		}

		#region Utilities

		private static XmlNamespaceManager GetNsMgr(XmlNode node)
		{
			XmlNameTable nt;

			if (node is XmlDocument)
				nt = ((XmlDocument)node).NameTable;
			else
				nt = node.OwnerDocument.NameTable;

			var result = new XmlNamespaceManager(nt);
			result.AddNamespace("abb", Namespace);

			return result;
		}

		private static bool GetBoolFrom(XmlNode root, string xPath)
		{
			var result = GetStringFrom(root, xPath, "false");

			return result == "true" || result == "1";
		}

		private static string GetStringFrom(XmlNode root, string xPath, object defaultValue=null)
		{
			var pre = root.SelectSingleNode(xPath, GetNsMgr(root));

			if (pre == null)
				return defaultValue == null ? null : defaultValue.ToString();

			return pre.Value;
		}

		private static string GetRequiredStringFrom(XmlNode root, string xPath)
		{
			var result = GetStringFrom(root, xPath);

			if (string.IsNullOrWhiteSpace(result))
				throw new SettingsPropertyNotFoundException("Missing required setting " + xPath);

			return result;
		}

		private static IEnumerable<XmlNode> GetNodesFrom(XmlNode root, string xPath)
		{
			var pre = root.SelectNodes(xPath, GetNsMgr(root));

			if (pre == null)
				return Enumerable.Empty<XmlNode>();

			return pre.Cast<XmlNode>();
		}

		private static IEnumerable<XmlNode> GetRequiredNodesFrom(XmlNode root, string xPath)
		{
			var result = GetNodesFrom(root, xPath);

			if (!result.Any())
				throw new SettingsPropertyNotFoundException("Missing required setting " + xPath);

			return result;
		}

		#endregion Utilities
	

}
}
