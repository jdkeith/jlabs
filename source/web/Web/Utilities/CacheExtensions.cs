﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Caching;

namespace PXL.Web.Utilities
{
	public static class CacheExtensions
	{
		public static bool TryGetAs<T>(this Cache cache, string key, out T value)
		{
			if( cache == null )
				throw new ArgumentNullException("cache");

			if( key == null )
				throw new ArgumentNullException("key");

			var item = cache[key];

			if (ReferenceEquals(item, null) || ! (item is T))
			{
				value = default(T);
				return false;
			}

			value = (T) item;

			return true;
		}
	}
}
