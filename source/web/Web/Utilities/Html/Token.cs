﻿using System;

namespace PXL.Web.Utilities.Html
{
	/// <summary>
	/// Represents a portion of HTML
	/// </summary>
	public class Token
	{
		public static Token TagStart(string tagName)
		{
			if (tagName == null)
				throw new ArgumentNullException("tagName");

			return new Token(TokenType.TagStart, tagName, null);
		}

		public static Token Attribute(string name, string value)
		{
			if (name == null)
				throw new ArgumentNullException("name");

			return new Token(TokenType.Attribute, name, value);
		}

		public static Token Style(string property, string value)
		{
			if (property == null)
				throw new ArgumentNullException("property");

			if (value == null)
				throw new ArgumentNullException("value");

			return new Token(TokenType.Style, property, value);
		}

		public static Token AttributeEnd(string name)
		{
			if (name == null)
				throw new ArgumentNullException("name");

			return new Token(TokenType.AttributeEnd, name, null);
		}

		public static Token AttributeEnd(Token attributeToken)
		{
			if (attributeToken == null)
				throw new ArgumentNullException("attributeToken");

			if (attributeToken.Type != TokenType.Attribute)
				throw new ArgumentException("Only attribute tags are supported", "attributeToken");

			return new Token(TokenType.AttributeEnd, attributeToken.Name, null);
		}

		public static Token TagStartEnd(string tagName)
		{
			if (tagName == null)
				throw new ArgumentNullException("tagName");

			return new Token(TokenType.TagStartEnd, tagName, null);
		}

		public static Token TagStartEnd(Token startTagToken)
		{
			if (startTagToken == null)
				throw new ArgumentNullException("startTagToken");

			if (startTagToken.Type != TokenType.TagStart)
				throw new ArgumentException("Only start tags are supported", "startTagToken");

			return new Token(TokenType.TagStartEnd, startTagToken.Name, null);
		}

		public static Token Text(string value)
		{
			if (value == null)
				throw new ArgumentNullException("value");

			return new Token(TokenType.Text, null, value);
		}

		public static Token Comment(string value)
		{
			return new Token(TokenType.Comment, null, value);
		}

		public static Token TagSelfClosingEnd(string tagName)
		{
			if (tagName == null)
				throw new ArgumentNullException("tagName");

			return new Token(TokenType.TagSelfClosingEnd, tagName, null);
		}

		public static Token TagSelfClosingEnd(Token startTagToken)
		{
			if (startTagToken == null)
				throw new ArgumentNullException("startTagToken");

			if (startTagToken.Type != TokenType.TagStart)
				throw new ArgumentException("Only start tags are supported", "startTagToken");

			return new Token(TokenType.TagSelfClosingEnd, startTagToken.Name, null);
		}

		public static Token TagEnd(string tagName)
		{
			if (tagName == null)
				throw new ArgumentNullException("tagName");

			return new Token(TokenType.TagEnd, tagName, null);
		}

		public static Token TagEnd(Token startTagToken)
		{
			if (startTagToken == null)
				throw new ArgumentNullException("startTagToken");

			if (startTagToken.Type != TokenType.TagStart)
				throw new ArgumentException("Only start tags are supported", "startTagToken");

			return new Token(TokenType.TagEnd, startTagToken.Name, null);
		}

		public static Token Spacer()
		{
			return new Token(TokenType.Spacer, null, null);
		}

		public static Token RawTagPart(string body)
		{
			if( body == null )
				throw new ArgumentNullException("body");

			return new Token(TokenType.RawTagPart, null, body);
		}

		/// <summary>
		/// Creates a new token
		/// </summary>
		/// <param name="type">The type of the token</param>
		/// <param name="name">The name of the token</param>
		/// <param name="value">The value of the token</param>
		/// <remarks>
		///		<para>The name and value attributes have different effects depending on the token type</para>
		///		<para>Null names or values are replaced with empty strings</para>
		/// </remarks>
		private Token(TokenType type, string name, string value)
		{
			Type = type;
			Name = name ?? "";
			Value = value ?? "";
		}

		/// <summary>
		/// The type of HTML markup the token contains
		/// </summary>
		public TokenType Type { get; private set; }

		/// <summary>
		/// The tag, attribute, or style name
		/// </summary>
		public string Name { get; private set; }

		/// <summary>
		/// The attribute value, style value, or text content
		/// </summary>
		public string Value { get; private set; }

		/// <summary>
		/// The HTML markup which the token has captured
		/// </summary>
		public string Markup
		{
			get
			{
				switch (Type)
				{
					case TokenType.Attribute:
						return string.Format("{0}=\"{1}", Name, Value);

					case TokenType.AttributeEnd:
						return "\"";

					case TokenType.Comment:
						return Value;

					case TokenType.Style:
						return string.Format("{0}: {1};", Name, Value);

					case TokenType.TagEnd:
						return string.Format("</{0}>", Name);

					case TokenType.TagSelfClosingEnd:
						return "/>";

					case TokenType.TagStart:
						return string.Format("<{0}", Name);

					case TokenType.TagStartEnd:
						return ">";

					case TokenType.Spacer:
						return " ";

					case TokenType.Text:
						return Value;
				}

				throw new NotSupportedException();
			}
		}

		/// <summary>
		/// Converts the token to a string representation useful for debugging
		/// </summary>
		/// <returns>A debugger-friendly text representation of the token</returns>
		public override string ToString()
		{
			return string.Format("{0,-12} {1}", Type, Markup);
		}
	}
}
