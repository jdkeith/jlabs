﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace PXL.Web.Utilities.Html
{
	/// <summary>
	/// Provides the ability to transform one set of tokens into another of equal or different length
	/// </summary>
	public interface ITokenListRewriter
	{
		/// <summary>
		/// Rewrites a set of tokens into another set of equal or different length without modifying the original tokens
		/// </summary>
		/// <param name="input">The set of tokens to rewrite</param>
		/// <returns>A new set of tokens based off of the input</returns>
		IEnumerable<Token> Rewrite(IEnumerable<Token> input);
	}

	#region Built-In Rewriters

	/// <summary>
	/// Inserts spacer tokens at appropriate spots to make it easier to join tokens to directly produce markup strings
	/// </summary>
	public class Prettifier : ITokenListRewriter
	{
		/// <summary>
		/// Rewrites a set of tokens into another set of equal or different length without modifying the original tokens
		/// </summary>
		/// <param name="input">The set of tokens to rewrite</param>
		/// <returns>A new set of tokens based off of the input</returns>
		public IEnumerable<Token> Rewrite(IEnumerable<Token> input)
		{
			Token previousToken = null;

			foreach (var token in input.Where(t => t.Type != TokenType.Spacer))
			{
				if (previousToken != null)
				{
					if (token.Type == TokenType.Attribute && (previousToken.Type == TokenType.TagStart || previousToken.Type == TokenType.AttributeEnd))
						yield return Token.Spacer();

					if (token.Type == TokenType.Style && previousToken.Type == TokenType.Style)
						yield return Token.Spacer();
				}

				yield return token;

				previousToken = token;
			}
		}
	}

	/// <summary>
	/// Encodes entities within text tokens
	/// </summary>
	public class EntityEncoderRewriter : ITokenListRewriter
	{
		private static readonly Regex _ampersandRestorer = new Regex("&amp;(\\w+;)");

		/// <summary>
		/// Rewrites a set of tokens into another set of equal or different length without modifying the original tokens
		/// </summary>
		/// <param name="input">The set of tokens to rewrite</param>
		/// <returns>A new set of tokens based off of the input</returns>
		public IEnumerable<Token> Rewrite(IEnumerable<Token> input)
		{
			foreach (var token in input)
			{
				if (token.Type == TokenType.Text)
				{
					var nogtlt = token.Value.Replace("<", "&lt;").Replace(">", "&gt;").Replace("&", "&amp;");

					yield return Token.Text
					(
						_ampersandRestorer.Replace(nogtlt, me => "&" + me.Groups[1].Value)
					);
				}
				else if (token.Type == TokenType.Attribute)
				{
					yield return Token.Attribute
					(
						token.Name,
						token.Value.Replace("\"", "&quot;").Replace("'", "&#39;")
					);
				}
				else
				{
					yield return token;
				}
			}
		}
	}

	#endregion Built-In Rewriters

	public class WordFilter : ITokenListRewriter
	{
		private readonly string _replaceWith;
		private readonly bool _characterize;
		private readonly bool _hintAtWord;
		private readonly string[] _forbiddenWords;

		public WordFilter(string replaceWith, params string[] forbiddenWords)
		{
			_replaceWith = replaceWith;
			_forbiddenWords = forbiddenWords;
		}

		public WordFilter(char replaceWith, bool hintAtWord, params string[] forbiddenWords)
		{
			_replaceWith = replaceWith.ToString();
			_characterize = true;
			_hintAtWord = hintAtWord;
			_forbiddenWords = forbiddenWords;
		}

		public IEnumerable<Token> Rewrite(IEnumerable<Token> input)
		{
			foreach (var token in input)
			{
				if (token.Type != TokenType.Text)
				{
					yield return token;
					continue;
				}

				var replacements = new List<string>();

				foreach (var word in token.Value.Split(' '))
				{
					var lw = word.ToLower();

					if (!_forbiddenWords.Any(fw => fw.ToLower() == lw))
					{
						replacements.Add(word);
						continue;
					}

					if (_characterize && (!_hintAtWord || word.Length == 1))
						replacements.Add(new string(_replaceWith[0], word.Length));
					else if (_hintAtWord)
						replacements.Add(word[0] + new string(_replaceWith[0], word.Length - 1));
					else
						replacements.Add(_replaceWith);
				}

				yield return Token.Text(string.Join(" ", replacements));
			}
		}
	}

	public class UrlFilter : ITokenListRewriter
	{
		private static readonly Regex _match = new Regex(@"(https?://)?(([-\w]+)(\.[-\w]+)+)/?");

		private readonly string[] _domains;

		public UrlFilter(params string[] domains)
		{
			_domains = domains;
		}

		private bool GetSkip(string url)
		{
			var m = _match.Match(url);

			// not a url
			if (m.Success && url.StartsWith("//"))
				return false;

			if (!m.Success || m.Groups.Count < 3)
				return true;

			var domain = m.Groups[2].Value;

			if (_domains.Contains(domain))
				return false;

			// split it
			var domainParts = domain.Split('.').Reverse();

			foreach (var testDomain in _domains)
			{
				var tdParts = testDomain.Split('.').Reverse();

				if (tdParts.Last() != "*")
					continue;

				var tdc = tdParts.Count();

				if (domainParts.Count() < tdc)
					continue;

				if (domainParts.Take(tdc - 1).SequenceEqual(tdParts.Take(tdc - 1)))
					return false;
			}

			return true;
		}

		public IEnumerable<Token> Rewrite(IEnumerable<Token> input)
		{
			// no src
			// no href
			// no url()

			var skip = false;

			foreach (var token in input)
			{
				if (token.Type == TokenType.AttributeEnd && skip)
					continue;

				if (token.Type == TokenType.Attribute && (token.Name == "src" || token.Name == "href"))
				{
					skip = !_domains.Any() || GetSkip(token.Value);

					if (!skip)
						yield return token;
				}
				else if (token.Type == TokenType.Style && token.Value.Contains("url("))
				{
					if (_domains.Any() && !GetSkip(token.Value))
						yield return token;
				}
				else if (token.Type == TokenType.AttributeEnd && skip)
				{
					skip = false;
				}
				else
				{
					yield return token;
				}
			}
		}
	}

	public class JavascriptFilter : ITokenListRewriter
	{
		public IEnumerable<Token> Rewrite(IEnumerable<Token> input)
		{
			throw new NotImplementedException();
		}
	}

	public class NumericAttributeFilter : ITokenListRewriter
	{
		private static readonly string[] _numericAttributes = new[] { "width", "height" };

		public IEnumerable<Token> Rewrite(IEnumerable<Token> input)
		{
			var skip = false;

			foreach (var token in input)
			{
				if (token.Type == TokenType.AttributeEnd && skip)
					continue;

				if (token.Type != TokenType.Attribute && ! _numericAttributes.Contains(token.Name))
				{
					yield return token;
					skip = false;
					continue;
				}

				int ignored;

				if (int.TryParse(token.Value.Replace("%", ""), out ignored))
					yield return token;
				else
					skip = true;
			}
		}
	}

	public class MaxlengthVisitor : ITokenListRewriter
	{
		private readonly int _maxlength;

		public MaxlengthVisitor(int maxlength)
		{
			_maxlength = maxlength;
		}

		public IEnumerable<Token> Rewrite(IEnumerable<Token> input)
		{
			var filteredInput =
				new Prettifier().Rewrite(input).Where(t => t.Type != TokenType.Comment).ToArray();

			var closeStack = new Stack<Token>();

			var result = new List<Token>();
			var pending = new List<Token>();
			var pendingLength = 0;
			var actualLength = 0;

			for (var i = 0; i < filteredInput.Length; i++)
			{
				var token = filteredInput[i];

				if (token.Type == TokenType.TagStart)
				{
					// add anything in the pending pile
					result.AddRange(pending);
					actualLength += pendingLength;
					pendingLength = 0;
					pending.Clear();
				}

				if (token.Type != TokenType.Text)
				{
					pending.Add(token);
					pendingLength += token.Markup.Length;
				}

				if (token.Type == TokenType.TagStartEnd || token.Type == TokenType.TagSelfClosingEnd)
				{
					Token closeToken = null;

					if (token.Type == TokenType.TagStartEnd)
					{
						closeToken = Token.TagEnd(token.Name);

						pendingLength += closeToken.Markup.Length;
					}

					// if the tag can't be finished, we're all done
					if (_maxlength - actualLength - pendingLength < 0)
						break;

					if (token.Type == TokenType.TagStartEnd)
					{
						// otherwise, what's gathered so far is ok
						closeStack.Push(closeToken);
					}

					// add opening tags to the result
					result.AddRange(pending);
					pending.Clear();
					actualLength += pendingLength;
					pendingLength = 0;
				}

				else if (token.Type == TokenType.TagEnd)
				{
					if (!pending.Any() || closeStack.Count == 0 || closeStack.Peek().Name != token.Name)
					{
						throw new FormatException("malformed!");
					}
					else
					{
						result.AddRange(pending);
						//    result.Add(token);
						pending.Clear();
						actualLength += pendingLength;
						pendingLength = 0;

						closeStack.Pop();
					}
				}
				else if (token.Type == TokenType.Text)
				{
					var maxTextLength = Math.Min(
						_maxlength - actualLength - pendingLength, token.Value.Length
					);

					if (maxTextLength > 0)
					{
						pending.Add(Token.Text(token.Value.Substring(0, maxTextLength)));
						pendingLength += maxTextLength;
					}
				}
			}

			result.AddRange(closeStack);

			return result.Where(t => t.Type != TokenType.Spacer).ToList();
		}

		private IEnumerable<Token> FilterInner(Token[] input, int maxLength)
		{
			var currentLength = 0;
			var result = new List<Token>();
			var pendingResult = new List<Token>();
			Token expectedCloser = null;

			for (var i = 0; i < input.Length; i++)
			{
				var token = input[i];

				if (token.Type == TokenType.TagStart)
				{
					pendingResult.Clear();
					pendingResult.Add(token);
					currentLength += token.Markup.Length;
				}
				else if (token.Type == TokenType.TagStartEnd)
				{
					expectedCloser = Token.TagEnd(token.Name);

					if (currentLength + expectedCloser.Markup.Length >= maxLength)
					{
						pendingResult.Add(token);
						result.AddRange(pendingResult);
						return result;
					}

					var inner = FilterInner(input.Skip(i + 1).ToArray(), maxLength - currentLength - expectedCloser.Markup.Length);

					pendingResult.AddRange(inner);

					if (inner.Any())
						currentLength += inner.Sum(t => t.Markup.Length);
				}
				else if (token.Type == TokenType.TagSelfClosingEnd || token.Type == TokenType.TagEnd)
				{
					pendingResult.Add(token);

					if (currentLength <= maxLength)
						result.AddRange(pendingResult);
					else
						currentLength -= pendingResult.Sum(t => t.Markup.Length);

					pendingResult.Clear();
					expectedCloser = null;
				}
				else if (token.Type == TokenType.Text)
				{
					var chopTo = maxLength - currentLength;

					if (expectedCloser != null)
						chopTo -= expectedCloser.Markup.Length;

					if (chopTo > 0)
					{
						var newTextToken = Token.Text(token.Markup.Substring(0, Math.Min(token.Markup.Length, chopTo)));

						currentLength += newTextToken.Markup.Length;
						result.Add(newTextToken);
					}
				}
				else
				{
					pendingResult.Add(token);
					currentLength += token.Markup.Length;
				}
			}

			return result.ToList();
		}
	}

	public class TokenFilterRewriter : ITokenListRewriter
	{
		public delegate bool OnTagHandler(ref string name, IDictionary<string, string> attributes, IDictionary<string, string> styles);
		public delegate bool OnAttributeHandler(ref string name, ref string value);
		public delegate bool OnTextHandler(ref string value);

		public event OnTagHandler OnTag;
		public event OnAttributeHandler OnAttribute;
		public event OnAttributeHandler OnStyle;
		public event OnTextHandler OnText;
		public event OnTextHandler OnComment;

		public IEnumerable<Token> Rewrite(IEnumerable<Token> input)
		{
			var forbiddenStack = new Stack<string>();

			var styles = new Dictionary<string, string>();
			var attributes = new Dictionary<string, string>();

			var result = new List<Token>();

			string name;
			string value;
			string lastTagName = null;

			foreach (var token in input)
			{
				if (token.Type == TokenType.TagEnd)
				{
					if (forbiddenStack.Count > 0)
					{
						if (forbiddenStack.Count == 0 || forbiddenStack.Peek() != token.Name)
							throw new FormatException("Malformed HTML");

						forbiddenStack.Pop();
						continue;
					}
					else
					{
						result.Add(Token.TagEnd(lastTagName ?? token.Name));
						lastTagName = null;
					}
				}

				if (token.Type == TokenType.TagSelfClosingEnd && forbiddenStack.Count > 0)
				{
					forbiddenStack.Pop();
					continue;
				}

				if (forbiddenStack.Count > 0)
					continue;

				if (token.Type == TokenType.TagStart)
				{
					lastTagName = token.Name;
				}

				else if (token.Type == TokenType.TagSelfClosingEnd || token.Type == TokenType.TagStartEnd)
				{
					name = token.Name;

					attributes.Remove("style");

					var include = true;

					if (OnTag == null || (include = OnTag(ref name, attributes, styles)))
					{
						lastTagName = name;

						result.Add(Token.TagStart(name));

						foreach (var kvp in attributes)
						{
							result.Add(Token.Attribute(kvp.Key, kvp.Value));
							result.Add(Token.AttributeEnd(kvp.Key));
						}

						if (styles.Any())
						{
							result.Add(Token.Attribute("style", null));

							foreach (var kvp in styles)
							{
								result.Add(Token.Style(kvp.Key, kvp.Value));
							}

							result.Add(Token.AttributeEnd("style"));
						}

						if (token.Type == TokenType.TagSelfClosingEnd)
							result.Add(Token.TagSelfClosingEnd(name));
						else
							result.Add(Token.TagStartEnd(name));
					}

					attributes.Clear();
					styles.Clear();

					if (!include)
						forbiddenStack.Push(lastTagName);
				}

				else if (token.Type == TokenType.Comment)
				{
					value = token.Value;

					if (OnComment != null && OnComment(ref value))
					{
						result.Add(Token.Comment(value));
					}
				}

				else if (token.Type == TokenType.Text)
				{
					value = token.Value;

					if (OnText == null || OnText(ref value))
					{
						result.Add(Token.Text(value));
					}
				}

				else if (token.Type == TokenType.Attribute)
				{
					name = token.Name;
					value = token.Value;

					if (OnAttribute == null || OnAttribute(ref name, ref value))
					{
						attributes[name] = value;
					}
				}

				else if (token.Type == TokenType.Style)
				{
					name = token.Name;
					value = token.Value;

					if (OnStyle == null || OnStyle(ref name, ref value))
					{
						styles[name] = value;
					}
				}
			}

			return result;
		}
	}
}
