﻿using System.Collections.Generic;

namespace PXL.Web.Utilities.Html
{
	/// <summary>
	/// Utility class to provider extension methods to enumerables of tokens
	/// </summary>
	public static class TokenEnumerableHelper
	{
		/// <summary>
		/// Applies a set of filters to a set of tokens
		/// </summary>
		/// <param name="input">The token list to apply filters to</param>
		/// <param name="tokenListRewritersToApply">A set of rewriters to apply to the set of tokens</param>
		/// <returns>A set of tokens computed from applying the rewriters in the order passed or null if the input parameter is null</returns>
		public static IEnumerable<Token> ApplyRewriters(this IEnumerable<Token> input, params ITokenListRewriter[] tokenListRewritersToApply)
		{
			if (input == null)
				return null;

			var local = input;

			foreach (var tokenListRewriterToApply in tokenListRewritersToApply)
			{
				local = tokenListRewriterToApply.Rewrite(local);
			}

			return local;
		}
	}
}
