﻿namespace PXL.Web.Utilities.Html
{
	/// <summary>
	/// How a filter operates on an enumerable of tokens
	/// </summary>
	public enum TokenFilterType
	{
		/// <summary>
		/// Tokens which match a specification are removed
		/// </summary>
		Blacklist,

		/// <summary>
		/// Tokens which do not match a specification are removed
		/// </summary>
		Whitelist,
	}

	/// <summary>
	/// The type of HTML markup
	/// </summary>
	public enum TokenType
	{
		/// <summary>
		/// A text node
		/// </summary>
		Text,

		/// <summary>
		/// A comment
		/// </summary>
		Comment,

		/// <summary>
		/// A faux token to space out content
		/// </summary>
		Spacer,

		/// <summary>
		/// A raw tag - includes the entire opening and closing portions of a tag
		/// </summary>
		RawTagPart,

		/// <summary>
		/// The opening part of a tag. E.G. [a
		/// </summary>
		TagStart,

		/// <summary>
		/// The close of the opening portion of a tag. E.G. >
		/// </summary>
		TagStartEnd,

		/// <summary>
		/// The close tag. E.G. [/a]
		/// </summary>
		TagEnd,

		/// <summary>
		/// The close portion of a self-closing tag. E.G. />
		/// </summary>
		TagSelfClosingEnd,

		/// <summary>
		/// The opening and value portion of an attribute. E.G. class="foo
		/// </summary>
		Attribute,

		/// <summary>
		/// The closing portion of an attribute. E.G. "
		/// </summary>
		AttributeEnd,

		/// <summary>
		/// A css property and value combination. E.G. background-color: red;
		/// </summary>
		Style
	}
}
