﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace PXL.Web.Utilities.Html
{
	/// <summary>
	/// Sanitizes potentially unsafe HTML input
	/// </summary>
	public static class HtmlSanitizer
	{
		#region Constants and Config

		/// <summary>
		/// A default set of tags which are normally forbidden as user input because they
		/// can be used to deface any page the html appears on or can be used for cross-site
		/// scripting attacks
		/// </summary>
		public static readonly string[] DefaultForbiddenTags = new[]
        {
            // primary
            "html", "head", "title", "meta", "base", "link", "style", "script", "noscript", "body",

            // forms
            "form", "select", "optgroup", "option", "output", "button", "datalist", "keygen", "progress", "fieldset", "meter", "legend", "label", "input", "textarea",

            // external objects
            "embed", "object", "param", "source", "iframe"
        };

		private static readonly string[] _tagsWithHref = new[]
        {
            "a", "area"
        };

		private static readonly string[] _mediaTagsWithSource = new[]
        {
            "img", "audio", "video"
        };

		private static readonly string[] _blockTags = new[]
        {
            // block
            "div", "pre", "blockquote", "dl", "dt", "dd", "p", "ol", "ul", "li", "br", "hr", "figcaption", "figure"
        };

		/// <summary>
		/// A default set of tags which are typically acceptable for user input
		/// </summary>
		public static readonly string[] DefaultAllowedTags = new[]
        {
            // inline
            "span", "rt", "rp", "dfn", "abbr", "q", "cite", "em", "time", "var", "samp", "i", "b",
            "sub", "sup", "small", "strong", "mark", "ruby", "ins", "del", "bdi", "bdo", "s", "kbd", "wbr", "code",

            // articles and sections
            "aside", "address", "header", "h1", "h2", "h3", "h4", "h5", "h6", "section", "nav", "article", "footer", "hgroup",

            // table
            "table", "thead", "th", "tbody", "tr", "td", "tfoot", "col", "colgroup", "caption",

            // misc
            "menu", "command", "summary", "details",

            // media without source / href tags
            "track"

		}.Concat(_blockTags).Concat(_tagsWithHref).Concat(_mediaTagsWithSource).ToArray();

		private static readonly string[] _selfClosingTags = new[]
        {
            "img", "br", "hr"
        };

		private static readonly IDictionary<string, string> _tagParentCheck = new Dictionary<string, string>
        {
            { "td", "tr" },
            { "th", "tr" },
            { "thead", "table" },
            { "tbody", "table" },
            { "tfoot", "table" },
            { "tr", "table" }
        };

		private static readonly Regex _validUrl = new Regex(@"^((https?:)?\/\/)?[^:]+$");

		private static readonly Regex _commentPattern = new Regex("<!--.*");  // <!--.........>
		private static readonly Regex _tagStartPattern = new Regex("<(?i)(\\w+\\b)\\s*(.*)/?>$");  // <tag ....props.....>
		private static readonly Regex _tagClosePattern = new Regex("</(?i)(\\w+\\b)\\s*>$");  // </tag .........>

		private static readonly Regex _selfClosed = new Regex("<\\w+.*/>");

		private static readonly Regex _attributesPattern = new Regex("(\\w*)\\s*=\\s*\"([^\"]*)\"");  // prop="...."
		private static readonly Regex _stylePattern = new Regex("([^\\s^:]+)\\s*:\\s*([^;]+);?");  // color:red;

		private static readonly Regex _urlStylePattern = new Regex("(?i).*\\b\\s*url\\s*\\(['\"]([^)]*)['\"]\\)");  // url('....')"

		private static readonly Regex _forbiddenStylePattern = new Regex("(?:(expression|eval|javascript))\\s*\\(");  // expression(....)"   thanks to Ben Summer

		private static readonly Regex _entityPattern = new Regex("&\\w+;");

		private static readonly Regex _whiteSpacePatten = new Regex("(\\s|\\n){2,}", RegexOptions.Multiline);

		#endregion Constants and Config

		#region Sanitize

		/// <summary>
		/// Returns a text-only version of the html markup
		/// </summary>
		/// <param name="html">The potential unsafe markup to process</param>
		/// <param name="blockElementsGenerateLineBreak">When true, block level elements generate a line break</param>
		/// <param name="tokenListRewritersToApply">A set of rewriters to apply before converting to text</param>
		/// <returns>A text only version of the passed html string</returns>
		public static string ToSanitizedText(
			string html,
			bool blockElementsGenerateLineBreak = true,
			params ITokenListRewriter[] tokenListRewritersToApply
		)
		{
			var tokens = ToTokens(html);

			foreach (var tokenListRewriterToApply in tokenListRewritersToApply)
			{
				tokens = tokenListRewriterToApply.Rewrite(tokens);
			}

			tokens = new Prettifier().Rewrite(tokens);

			var sb = new StringBuilder();

			var seenText = false;

			foreach (var token in tokens)
			{
				if (token.Type == TokenType.Text)
				{
					sb.Append(token.Markup);
					seenText = true;
				}

				if (blockElementsGenerateLineBreak && seenText && token.Type == TokenType.TagEnd && _blockTags.Any(t => t == token.Name))
				{
					sb.AppendLine();
					seenText = false;
				}
			}

			var result = sb.ToString();

			//if (convertEntitiesToCharacters)
			//    result = HtmlEncodeTag(result);

			return result;
		}

		/// <summary>
		/// Sanitizes potentially unsafe HTML input and applies token list rewriters
		/// </summary>
		/// <param name="html">The potentially unsafe html</param>
		/// <param name="includeComments">
		/// Whether or not to include any comments passed in the input. Default is false
		/// </param>
		/// <param name="tokenListRewritersToApply">A set of rewriters to apply before converting to html</param>
		/// <returns>A sanitized html string</returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if html is null
		/// </exception>
		public static string ToSanitizedHtml(string html, bool includeComments = false, params ITokenListRewriter[] tokenListRewritersToApply)
		{
			if (html == null)
				throw new ArgumentNullException("html");

			var tokens = ToTokens(html);

			if (!includeComments)
				tokens = tokens.Where(t => t.Type != TokenType.Comment);

			foreach (var tokenListRewriter in tokenListRewritersToApply)
			{
				tokens = tokenListRewriter.Rewrite(tokens);
			}

			return string.Join("", new Prettifier().Rewrite(tokens).Select(t => t.Markup));
		}

		#endregion Sanitize

		#region Tokenize

		/// <summary>
		/// Converts potential unsafe html into an enumerable of tokens
		/// </summary>
		/// <param name="html">The potentially unsafe html</param>
		/// <returns>An enumerable of tokens</returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if html is null
		/// </exception>
		public static IEnumerable<Token> ToTokens(string html)
		{
			if (html == null)
				throw new ArgumentNullException("html");

			var rawTokens = new List<Token>();

			var pos = 0;
			var len = html.Length;
			var sb = new StringBuilder();

			while (pos < len)
			{
				Token rawToken = null;

				var c = html[pos];

				var ahead = html.Substring(pos, Math.Min(4, len - pos));

				// a comment is starting
				if ("<!--".Equals(ahead))
				{
					// search the end of <......>
					var end = MoveToMarkerEnd(pos, "-->", html);
					rawToken = Token.RawTagPart(html.Substring(pos, end - pos));
					pos = end;
				}

				// a new "<" token is starting
				else if ('<' == c && pos < len - 1 && (char.IsLetter(html[pos + 1]) || html[pos + 1] == '/'))
				{
					// search the end of <......>
					var end = MoveToMarkerEnd(pos, ">", html);
					rawToken = Token.RawTagPart(html.Substring(pos, end - pos));
					pos = end;
				}

				if (rawToken != null)
				{
					if (sb.Length > 0)
					{
						rawTokens.Add(Token.Text(sb.ToString()));
						sb.Clear();
					}

					rawTokens.Add(rawToken);
				}
				else
				{
					sb.Append(c);
					pos++;
				}
			}

			IEnumerable<Token> result = rawTokens;

			result = Refine(result);
			result = CombineAndCollapseText(result);
			result = new EntityEncoderRewriter().Rewrite(result);
			result = CloseOut(result);

			return result.ToList();
		}

		private static IEnumerable<Token> Refine(IEnumerable<Token> input)
		{
			var result = new List<Token>();

			foreach (var rawToken in input)
			{
				if (rawToken.Type == TokenType.Text && string.IsNullOrEmpty(rawToken.Value))
					continue;

				// comments and text tokens get passed through
				if (rawToken.Type == TokenType.Comment || rawToken.Type == TokenType.Text)
				{
					result.Add(rawToken);
					continue;
				}

				result.AddRange(ProcessTag(rawToken, result));
			}

			return result;
		}

		private static IEnumerable<Token> ProcessTag(Token rawTag, IEnumerable<Token> currentList)
		{
			var processed = new List<Token>();

			var startMatcher = _tagStartPattern.Match(rawTag.Value);
			var endMatcher = _tagClosePattern.Match(rawTag.Value);

			// ------------------------------------------------------------------------------- CLOSE TAG

			if (!startMatcher.Success && !endMatcher.Success)
			{
				return new Token[0];
			}

			string tagName;

			// --------------------------------------------------- CLOSE TAG
			if (endMatcher.Success)
			{
				tagName = endMatcher.Groups[1].Value.ToLower();

				//if (_forbiddenStack.Count > 0 && _forbiddenStack.Peek() == tagName)
				//{
				//    _forbiddenStack.Pop();
				//}

				Token endToken;

				// is self closing
				if (_selfClosed.Match(rawTag.Value).Success && _selfClosingTags.Any(t => t == tagName))
					endToken = Token.TagSelfClosingEnd(tagName);
				else
					endToken = Token.TagEnd(tagName);

				//endToken.Forbidden = _forbiddenTags.Any(t => t == tagName) || !_allowedTags.Any(t => t == tagName) || _forbiddenStack.Count > 0;

				return new[] { endToken };
			}

			// ----------------------------------------------- OPEN TAG
			if (startMatcher.Success)
			{
				tagName = startMatcher.Groups[1].Value.ToLower();

				var start = Token.TagStart(tagName);

				//    start.Forbidden = _forbiddenTags.Any(t => t == tagName) || !_allowedTags.Any(t => t == tagName);
				//    start.Forbidden = start.Forbidden || !IsNestedProperly(start, currentList);
				//    start.Forbidden = start.Forbidden || _forbiddenStack.Count != 0;

				//    if (start.Forbidden)
				//    {
				//        _forbiddenStack.Push(tagName);
				//    }

				processed.Add(start);

				var attributes = ProcessAttributes(tagName, rawTag.Value);

				foreach (var attribute in attributes)
				{
					//    attribute.Forbidden = attribute.Forbidden || start.Forbidden;
					processed.Add(attribute);
				}

				// is self closing
				if (_selfClosingTags.Any(t => t == tagName))
				{
					processed.Add(
					   Token.TagSelfClosingEnd(tagName)
					);
				}
				else
				{
					processed.Add(
						Token.TagStartEnd(tagName)
					);
				}
			}

			return processed;
		}

		private static IEnumerable<Token> ProcessAttributes(string tagName, string tagBody)
		{
			var processed = new List<Token>();

			// then test properties
			var attributes = _attributesPattern.Matches(tagBody);

			foreach (Match attribute in attributes)
			{
				var token = Token.Attribute(
					attribute.Groups[1].Value.ToLower(),
					attribute.Groups[2].Value
				);

				if ("style".Equals(token.Name)) // <tag style="......">
				{
					processed.Add(Token.Attribute(token.Name, null));
					processed.AddRange(ProcessStyles(token));
				}
				else
				{
					processed.Add(token);
				}

				processed.Add(Token.AttributeEnd(token));
			}

			return processed;
		}

		private static IEnumerable<Token> ProcessStyles(Token input)
		{
			var processed = new List<Token>();

			var matches = _stylePattern.Matches(input.Value);

			if (matches.Count == 0)
				return new Token[0];

			foreach (Match style in matches)
			{
				processed.Add(
					Token.Style(
						style.Groups[1].Value.ToLower(),
						style.Groups[2].Value
					)
				);


				//    var token = new Token { Type = TokenType.Style };

				//    // suppress invalid styles values
				//    if (_forbiddenStylePattern.Match(styleValue).Success)
				//    {
				//        token.Forbidden = true;
				//    }
				//    else
				//    {
				//        // check if valid url
				//        var urlStyleMatcher = _urlStylePattern.Match(styleValue);

				//        if (urlStyleMatcher.Success)
				//        {
				//            var url = urlStyleMatcher.Groups[1].Value;

				//            url = FixUrl(url);

				//            if (url == "#")
				//                token.Forbidden = true;
				//        }
				//    }

				//    token.Text = string.Format("{0}: {1};", styleName, styleValue);

				//    processed.Add(token);
			}

			return processed;
		}

		#endregion Tokenize

		#region Utilities

		private static IEnumerable<Token> CloseOut(IEnumerable<Token> input)
		{
			var result = new List<Token>();

			var closeStack = new Stack<Token>();

			foreach (var token in input)
			{
				if (token.Type == TokenType.TagStart)
				{
					closeStack.Push(token);
					result.Add(token);
				}
				else if (token.Type == TokenType.TagStartEnd)
				{
					closeStack.Pop();
					closeStack.Push(token);
					result.Add(token);
				}
				else if (token.Type == TokenType.TagSelfClosingEnd || token.Type == TokenType.TagEnd)
				{
					if (closeStack.Count > 0 && closeStack.Peek().Name == token.Name)
					{
						closeStack.Pop();
						result.Add(token);
					}
				}
				else
				{
					result.Add(token);
				}
			}

			// close out all of what's left
			while (closeStack.Count > 0)
			{
				var tagToken = closeStack.Pop();

				if (_selfClosingTags.Any(t => t == tagToken.Name) && tagToken.Type == TokenType.TagStart)
				{
					result.Add(
						Token.TagSelfClosingEnd(tagToken.Name)
					);
				}
				else
				{
					if (tagToken.Type == TokenType.TagStart)
					{
						result.Add(
							Token.TagStartEnd(tagToken.Name)
						);
					}

					result.Add(
						Token.TagEnd(tagToken.Name)
					);
				}
			}

			return result;
		}

		private static IEnumerable<Token> CombineAndCollapseText(IEnumerable<Token> input)
		{
			var combinedText = "";

			foreach (var token in input)
			{
				if (token.Type != TokenType.Text)
				{
					if (combinedText == null)
					{
						yield return token;
						continue;
					}

					// collpase the text
					combinedText = _whiteSpacePatten.Replace(combinedText, " ");

					if (!string.IsNullOrEmpty(combinedText))
					{
						yield return Token.Text(combinedText);
					}

					yield return token;

					combinedText = "";
					continue;
				}

				combinedText += token.Value;
			}
		}

		private static int MoveToMarkerEnd(int pos, string marker, string s)
		{
			var i = s.IndexOf(marker, pos);

			if (i > -1)
				pos = i + marker.Length;
			else
				pos = s.Length;

			return pos;
		}

		#endregion Utilities
	}
}
