﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;

using JLabs.Core;

namespace JLabs.Web
{
	/// <summary>
	/// Provides url signing and validation for API endpoints
	/// </summary>
	public class SecureUriFactory
	{
		#region Fields

		private readonly TimeSpan? _expiresIn;
		private readonly string _sharedSecret;
		private readonly Guid _sessionId;
		private readonly bool _includeHostAndPort;

		private readonly Regex _queryChop = new Regex(@"[&?]_s[hex]=[^&#]*");

		#endregion Fields

		#region Constructors

		/// <summary>
		/// Creates a new persistent url signer related to a specific session id
		/// </summary>
		/// <param name="sharedSecret">The secret key known only to the user and the system</param>
		/// <param name="sessionId">The unique session id to prevent long-term API call replays</param>
		/// <param name="includeHostAndPort">Whether or not to include the scheme, host, and port in the signing hash computation</param>
		/// <exception cref="System.ArgumentNullException">Thrown if the sharedSecret parameter is null</exception>
		public SecureUriFactory(string sharedSecret, Guid sessionId, bool includeHostAndPort = false)
		{
			if (sharedSecret == null)
				throw new ArgumentNullException("sharedSecret");

			_sharedSecret = sharedSecret;
			_sessionId = sessionId;
			_includeHostAndPort = includeHostAndPort;
		}

		/// <summary>
		/// Creates a new transient url signer which creates URIs which expire after a certain amount of time
		/// </summary>
		/// <param name="sharedSecret">The secret key known only to the user and the system</param>
		/// <param name="expiresIn">How long URIs produced by this signer are valid</param>
		/// <param name="includeHostAndPort">Whether or not to include the scheme, host, and port in the signing hash computation</param>
		/// <exception cref="System.ArgumentNullException">Thrown if the sharedSecret parameter is null</exception>
		public SecureUriFactory(string sharedSecret, TimeSpan expiresIn, bool includeHostAndPort = false)
			: this(sharedSecret, Guid.Empty, includeHostAndPort)
		{
			_expiresIn = expiresIn;
		}

		#endregion Constructors

		#region Create

		/// <summary>
		/// Creates a new signed URI based on the passed URI
		/// </summary>
		/// <param name="baseUri">The URI to create a signed copy of</param>
		/// <returns>A status containing a signed copy of the passed URI</returns>
		/// <remarks>The query hash (part after #) does not factor into the signature computation</remarks>
		/// <exception cref="System.ArgumentNullException">Thrown if the baseUri parameter is null</exception>
		public Status<Uri> Create(Uri baseUri)
		{
			if (baseUri == null)
				return new Status<Uri>(new ArgumentNullException("baseUri"));

			return Create(baseUri.ToString());
		}

		/// <summary>
		/// Creates a new signed URI based on the passed string and parameters
		/// </summary>
		/// <param name="uriString">The uri string to create a signed copy of</param>
		/// <param name="formParameters">A dictionary of form parameters to base the signature computation on as key value pairs</param>
		/// <returns>A status containing a signed URI based on the passed uriString AND form parameters</returns>
		/// <exception cref="System.ArgumentNullException">Thrown if the uriString or formParamters parameters are null</exception>
		/// <remarks>
		///		<para>The query hash (part after #) does not factor into the signature computation</para>
		///		<para>The form parameters are NOT stored in the returned uriString though the hash is computed using them</para>
		///		<para>Any query strings with values specified in formParameters are removed</para>
		///	</remarks>
		public Status<Uri> Create(string uriString, IDictionary<string, string> formParameters)
		{
			try
			{
				string fixedString;

				var combined = Combine(uriString, formParameters, out fixedString);

				string sh;
				string se;
				string sx;

				var result = Create(combined, out sh, out se, out sx);

				var combinedParameters = new Dictionary<string, string>()
				{
					{ "_sh", sh }
				};

				if (se != null)
					combinedParameters["_se"] = se;

				if (sx != null)
					combinedParameters["_sx"] = sx;

				return new Status<Uri>(data: new Uri(Combine(fixedString, combinedParameters, out fixedString)));
			}
			catch (Exception ex)
			{
				return new Status<Uri>(ex);
			}
		}

		/// <summary>
		/// Creates a new signed URI based on the passed string
		/// </summary>
		/// <param name="uriString">The uri string to create a signed copy of</param>
		/// <returns>A status containing a signed URI based on the passed uriString</returns>
		/// <exception cref="System.ArgumentNullException">Thrown if the uriString parameter is null</exception>
		/// <remarks>The query hash (part after #) does not factor into the signature computation</remarks>
		public Status<Uri> Create(string uriString)
		{
			string ignored;

			return Create(uriString, out ignored, out ignored, out ignored);
		}

		private Status<Uri> Create(string uriString, out string sh, out string se, out string sx)
		{
			Uri validation;

			try
			{
				if (uriString == null)
					throw new ArgumentNullException("uriString");

				validation = new Uri(uriString);
			}
			catch (Exception ex)
			{
				sh = se = sx = null;
				return new Status<Uri>(ex);
			}

			long? epochSeconds = null;

			if (_expiresIn.HasValue)
			{
				epochSeconds = DateTime.UtcNow.Add(_expiresIn.Value).Ticks / 10000;
			}

			return new Status<Uri>(
				data: new Uri(ComputeHash( validation, epochSeconds, out sh, out se, out sx ))
			);
		}

		#endregion Create

		#region Validate

		/// <summary>
		/// Validates a signed URI with separate form parameters
		/// </summary>
		/// <param name="uriString">The uri to validate</param>
		/// <param name="formParameters">A dictionary of form parameters to base the signature computation on as key value pairs</param>
		/// <returns>A status indicating success or failure</returns>
		/// <exception cref="System.ArgumentNullException">Thrown if the uriString or formParamters parameters are null</exception>
		public Status Validate(string uriString, IDictionary<string,string> formParameters)
		{
			try
			{
				string ignored;

				var combined = Combine(uriString, formParameters, out ignored);

				return Validate(combined);
			}
			catch (Exception ex)
			{
				return new Status(ex);
			}
		}

		/// <summary>
		/// Validates a signed URI
		/// </summary>
		/// <param name="uriString">The uri to validate</param>
		/// <returns>A status indicating success or failure</returns>
		/// <exception cref="System.ArgumentNullException">Thrown if the uriString parameter is null</exception>
		public Status Validate(string uriString)
		{
			if (uriString == null)
				return new Status(
					new ArgumentNullException("uriString")
				);

			return Validate(new Uri(uriString));
		}

		/// <summary>
		/// Validates a signed URI with separate form parameters
		/// </summary>
		/// <param name="validateUri">The uri to validate</param>
		/// <param name="formParameters">A dictionary of form parameters to base the signature computation on as key value pairs</param>
		/// <returns>A status indicating success or failure</returns>
		/// <exception cref="System.ArgumentNullException">Thrown if the uriString or formParamters parameters are null</exception>
		public Status Validate(Uri validateUri, IDictionary<string, string> formParameters)
		{
			try
			{
				if (validateUri == null)
					return new Status(new ArgumentNullException("validateUri"));

				string ignored;

				var combined = Combine(validateUri.ToString(), formParameters, out ignored);

				return Validate(combined);
			}
			catch (Exception ex)
			{
				return new Status(ex);
			}
		}

		/// <summary>
		/// Validates a signed URI
		/// </summary>
		/// <param name="validateUri">The uri to validate</param>
		/// <returns>A status indicating success or failure</returns>
		/// <exception cref="System.ArgumentNullException">Thrown if the validateUri parameter is null</exception>
		public Status Validate(Uri validateUri)
		{
			var sf = new StatusFactory();

			try
			{
				if (validateUri == null)
					throw new ArgumentNullException("validateUri");

				var kvps = validateUri.Query
					.Replace("?", "")
					.Split('&')
					.Select(v => v.Split('='))
					.ToDictionary(v => v.First(), v => v.Skip(1).FirstOrDefault())
					.ToList();

				var se = kvps.Where( kvp => kvp.Key == "_se" ).Select( kvp => kvp.Value ).FirstOrDefault();
				var sx = kvps.Where( kvp => kvp.Key == "_sx" ).Select( kvp => kvp.Value ).FirstOrDefault();
				var sh = kvps.Where( kvp => kvp.Key == "_sh" ).Select( kvp => kvp.Value ).FirstOrDefault();

				long epochSeconds;
				long? hasSeEpochSeconds = null;

				if (se != null && long.TryParse(se, out epochSeconds))
				{
					if (DateTime.UtcNow > new DateTime(epochSeconds * 10000))
					{
						return sf.Fail();
					}

					hasSeEpochSeconds = epochSeconds;
				}
				else if (sx == null)
				{
					return sf.Fail();
				}

				string compareSh;
				string compareSe;
				string compareSx;

				ComputeHash(validateUri, hasSeEpochSeconds, out compareSh, out compareSe, out compareSx);

				if (se != null && se == compareSe && sh == compareSh)
					return sf.Succeed();

				if (sx != null && sx == compareSx && sh == compareSh)
					return sf.Succeed();

				return sf.Fail();
			}
			catch( Exception ex )
			{
				return sf.Fail(ex);
			}
		}

		#endregion Validate

		#region Utilities

		private static string Combine(string uriString, IDictionary<string,string> formParameters, out string fixedString)
		{
			if (uriString == null)
				throw new ArgumentNullException("uriString");

			if (formParameters == null)
				throw new ArgumentNullException("formParamters");

			string hash = null;
			string query = null;

			int lio;

			if( uriString.Contains('#') )
			{
				lio = uriString.LastIndexOf('#');
				hash = uriString.Substring(lio);
				uriString = uriString.Substring(0, lio);
			}

			if( uriString.Contains('?') )
			{
				lio = uriString.LastIndexOf('?');
				query = uriString.Substring(lio);
				uriString = uriString.Substring(0, lio);
			}

			var originalQuery = new Dictionary<string,string>();

			if( query != null )
			{
				originalQuery = query
					.Replace("?", "")
					.Split( '&' )
					.Select( v => v.Split('=') )
					.ToDictionary( v => v.First(), v => v.Skip(1).FirstOrDefault() );
			}

			foreach( var kvp in formParameters )
			{
				originalQuery.Remove(kvp.Key);
			}

			var signingString = uriString;
			fixedString = uriString;

			if( originalQuery.Any() || formParameters.Any() )
				signingString += '?' + string.Join("&", originalQuery.Concat(formParameters).Select(kvp => string.Format("{0}={1}", kvp.Key, kvp.Value)));

			if( originalQuery.Any() )
				fixedString += '?' + string.Join("&", originalQuery.Select( kvp => string.Format("{0}={1}", kvp.Key, kvp.Value ) ) );

			if (hash != null)
				fixedString += hash;

			return signingString;
		}

		private string ComputeHash(Uri baseUri, long? epochSeconds, out string sh, out string se, out string sx )
		{
			var hasher = SHA256Managed.Create();

			var sb = new StringBuilder();

			if (_includeHostAndPort)
			{
				sb.Append(baseUri.Scheme);
				sb.Append("://");
				sb.Append(baseUri.Host);
				sb.Append(':');
				sb.Append(baseUri.Port);
			}

			sb.Append(baseUri.AbsolutePath);

			var orderedKvps = baseUri.Query
				.Replace("?", "")
				.Split( '&' )
				.Select( v => v.Split('=') )
				.ToDictionary( v => v.First(), v => v.Skip(1).FirstOrDefault() )
				.Where( kvp => kvp.Key != "_se" && kvp.Key != "_sx" && kvp.Key != "_sh" )
				.OrderBy( kvp => kvp.Key );

			foreach (var orderedKvp in orderedKvps)
			{
				sb.Append('&');
				sb.Append(orderedKvp.Key);
				sb.Append('=');
				
				if( ! string.IsNullOrEmpty(orderedKvp.Value ) )
					sb.Append(orderedKvp.Value);
			}

			se = null;
			sx = null;

			if (epochSeconds.HasValue)
			{
				se = epochSeconds.ToString();
				sb.Append("&_se=");
				sb.Append(se);
			}
			else if( _sessionId != Guid.Empty )
			{
				sx = _sessionId.ToString().Replace("-", "");

				sb.Append("&_sx=");
				sb.Append(sx);
			}

			sb.Append("<<");
			sb.Append(_sharedSecret);
			sb.Append(">>");

			var clearedBytes = Encoding.UTF8.GetBytes(sb.ToString());

			var encryptedBytes = hasher.ComputeHash(clearedBytes);

			var guid1 = new Guid(encryptedBytes.Take(16).ToArray());
			var guid2 = new Guid(encryptedBytes.Skip(16).Take(16).ToArray());

			var result = baseUri.ToString();

			result = _queryChop.Replace(result, "");

			var chomped = !string.IsNullOrWhiteSpace(baseUri.Fragment) ? baseUri.Fragment : null;

			if (chomped != null)
			{
				result = result.Substring(0,result.LastIndexOf('#'));
			}

			result += result.Contains('?') ? '&' : '?';

			sh = string.Format("{0}{1}", guid1, guid2).Replace("-", "");

			result += string.Format("_sh={0}", sh);

			if (se != null)
				result += "&_se=" + se;

			if( sx != null )
				result += "&_sx=" + sx;

			if (chomped != null)
				result += chomped;

			return result.ToString();
		}

		#endregion Utilities
	}
}
