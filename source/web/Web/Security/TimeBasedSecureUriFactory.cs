﻿using System;
using System.Globalization;

using PXL.Service.Authentication;

namespace PXL.Web.Security
{
	/// <summary>
	/// A class for creating secure URIs which are valid for a certain amount of time
	/// </summary>
	public class TimeBasedSecureUriFactory : SecureUriGeneratorValidatorBase
	{
		#region Fields

		/// <summary>
		/// The name of the expiration key parameter
		/// </summary>
		public const string ExpirationKey = "_exp";

		#endregion Fields

		#region Constructor

		/// <summary>
		/// Creates a new TimeBasedSecureUriFactory
		/// </summary>
		/// <param name="sharedSecret">The shared secret used for generating and verifying secure URIs</param>
		/// <param name="expiresInMinutes">The number of minutes for which generated URIs are valid</param>
		/// <param name="ignoreComponents">Which URI components are filtered in generation and verification</param>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the sharedSecret parameter is null
		/// </exception>
		/// <exception cref="System.ArgumentException">
		/// Thrown if the sharedSecret parameter is empty or consists entirely of whitespace
		/// </exception>
		/// <exception cref="System.ArgumentOutOfRangeException">
		/// Thrown if the expiresInMinutes parameter is less than 1
		/// </exception>
		public TimeBasedSecureUriFactory(
			string sharedSecret, int expiresInMinutes,
			IgnoreURIComponents ignoreComponents = SecureUriGeneratorValidatorBase.IgnoreURIComponents.None
		) : base(sharedSecret, ignoreComponents)
		{
			if( expiresInMinutes <= 0 )
				throw new ArgumentOutOfRangeException("expiresInMinutes", "Expiration must be greater than 0");

			ExpiresInMinutes = expiresInMinutes;
		}

		#endregion Constructor

		#region Properties

		/// <summary>
		/// The number of minutes for which generated URIs are valid
		/// </summary>
		public int ExpiresInMinutes { get; private set; }

		#endregion Properties

		#region Overrides

		protected override UriParam[] GetGeneratedParams(
			string sharedSecret, Uri originalUri, object context
		)
		{
			var expiresOnTicks = DateTime.Now.AddMinutes(ExpiresInMinutes).Ticks / TimeSpan.TicksPerSecond;

			return new[]
			{
				new UriParam
				{
					Key = ExpirationKey,
					Value = expiresOnTicks.ToString(CultureInfo.InvariantCulture),
					ExposeInUrl = true,
					UseInGenerationHashComputation = true
				}
			};
		}

		protected override Status ValidateParam(string sharedSecret, Uri validateUri, UriParam param, object context)
		{
			long expiresTicks;

			if (! long.TryParse(param.Value, out expiresTicks))
			{
				return Status.FailWithMessage("Url is missing expiration key");
			}

			return (DateTime.Now.Ticks / TimeSpan.TicksPerSecond) > expiresTicks
				? Status.FailWithMessage("Url has expired")
				: Status.Succeed;
		}

		#endregion Overrides
	}
}
