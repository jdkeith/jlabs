﻿using System;
using System.Collections.Generic;
using System.Collections.Generic.Extensions;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Reflection.Extensions;

namespace PXL.Data.CSV
{
	/// <summary>
	/// A class which turns a well-formed CSV document into an enumeration of a certain type
	/// </summary>
	/// <typeparam name="T">The type represented by each row in the CSV</typeparam>
	// ReSharper disable once InconsistentNaming
	public class CSVReader<T>
	{
		#region Inner Class

		private class Column
		{
			public PropertyInfo[] Properties;
			public bool MustValidate;
			public EmptyCellBehavior EmptyCellBehavior;
		}

		#endregion Inner Class

		#region Fields

		private readonly bool _defaultValidate;

		private readonly Dictionary<Type, bool> _canCreateTypes; 

		private readonly Dictionary<Type, Delegate> _objectCreators;
		private readonly Dictionary<Type, Delegate> _customParsers;
		private readonly List<Func<CSVRawRow, bool>> _rowFilters;

		private readonly List<Column> _columns;

		#endregion Fields

		#region Constructor

		/// <summary>
		/// Creates a new CSVReader
		/// </summary>
		/// <param name="defaultValidate">
		/// Whether or not data annotations are used for column validation by default.
		/// Per-column settings override this default.
		/// </param>
		public CSVReader(bool defaultValidate = true)
		{
			_defaultValidate = defaultValidate;

			_canCreateTypes = new Dictionary<Type, bool>();
			_objectCreators = new Dictionary<Type, Delegate>();
			_customParsers = new Dictionary<Type, Delegate>();
			_rowFilters = new List<Func<CSVRawRow, bool>>();
			_columns = new List<Column>();
		}

		#endregion Constructor

		#region Fluent Methods

			#region Instantiators

		/// <summary>
		/// Supplies a custom function to instantiate the row value
		/// </summary>
		/// <param name="rowCreator">A function which provides rows to fill</param>
		/// <returns>The CSVReader object</returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the rowCreator parameter is null
		/// </exception>
		/// <exception cref="CSVReaderBindException">
		/// Thrown if a type instantiator for the row type has already been supplied
		/// </exception>
		public CSVReader<T> WithCustomInstantiator(Func<T> rowCreator)
		{
			if( rowCreator == null )
				throw new ArgumentNullException("rowCreator");

			var t = typeof (T);

			if (_objectCreators.ContainsKey(t))
				throw new CSVReaderBindException(CSVReaderBindException.ExceptionType.DuplicateInstantiator, t);

			_objectCreators[t] = rowCreator;

			return this;
		}

		/// <summary>
		/// Supplies a custom function to instantiate a type used in one of the binding methods
		/// </summary>
		/// <param name="objectCreator">A function which provides values to fill</param>
		/// <returns>The CSVReader object</returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the objectCreator parameter is null
		/// </exception>
		/// <exception cref="CSVReaderBindException">
		/// Thrown if a type instantiator for the type has already been supplied
		/// </exception>
		/// <remarks>
		/// Custom type instantiators only have an effect for properties BEFORE the final
		/// property. The final property is set with a parser, not a type instantiator.
		/// For instance in the binding
		/// <example>
		/// <para>c => c.Owner.Address</para>
		/// <para>Owner would use an instantiator, while Address would use a parser</para>
		/// </example>
		/// </remarks>
		public CSVReader<T> WithCustomInstantiator<TM>(Func<TM> objectCreator)
		{
			if (objectCreator == null)
				throw new ArgumentNullException("objectCreator");

			var tm = typeof(TM);

			if (_objectCreators.ContainsKey(tm))
				throw new CSVReaderBindException(CSVReaderBindException.ExceptionType.DuplicateInstantiator, tm);

			_objectCreators[tm] = objectCreator;

			return this;
		}

			#endregion Instantiators

			#region Parsers

		/// <summary>
		/// Supplies a custom parser to set the final property value of a type in one or more of the binding methods
		/// </summary>
		/// <param name="parser">An implementation of IParser</param>
		/// <returns>The CSVReader object</returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the parser parameter is null
		/// </exception>
		/// <exception cref="CSVReaderBindException">
		/// Thrown if a parser for the type has already been supplied
		/// </exception>
		/// <remarks>
		/// Custom parsers only have an effect for the final property.
		/// Parent properties in a chain are set with type instantiators, not parser.
		/// For instance in the binding
		/// <example>
		/// <para>c => c.Owner.Address</para>
		/// <para>Owner would use an instantiator, while Address would use a parser</para>
		/// </example>
		/// </remarks>
		public CSVReader<T> WithCustomParser<TM>(IParser<TM> parser)
		{
			if (parser == null)
				throw new ArgumentNullException("parser");

			return WithCustomParser(parser.Parse);
		}

		/// <summary>
		/// Supplies a custom parser to set the final property value of a type in one or more of the binding methods
		/// </summary>
		/// <param name="parser">A function converting a string value to a value which can be assigned to the binding's final property</param>
		/// <returns>The CSVReader object</returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the parser parameter is null
		/// </exception>
		/// <exception cref="CSVReaderBindException">
		/// Thrown if a parser for the type has already been supplied
		/// </exception>
		/// <remarks>
		/// Custom parsers only have an effect for the final property.
		/// Parent properties in a chain are set with type instantiators, not parser.
		/// For instance in the binding
		/// <example>
		/// <para>c => c.Owner.Address</para>
		/// <para>Owner would use an instantiator, while Address would use a parser</para>
		/// </example>
		/// </remarks>
		public CSVReader<T> WithCustomParser<TM>(Func<string, TM> parser)
		{
			if (parser == null)
				throw new ArgumentNullException("parser");

			var tm = typeof(TM);

			if (_customParsers.ContainsKey(tm))
				throw new CSVReaderBindException(CSVReaderBindException.ExceptionType.DuplicateParser, tm);

			_customParsers[tm] = parser;

			return this;
		}

			#endregion Parsers

			#region Filters

		/// <summary>
		/// Filters raw rows prior to processing
		/// </summary>
		/// <param name="rowPredicate">
		/// A function which takes a raw CSV row and returns true
		/// to process the row or false to skip it
		/// </param>
		/// <returns>The CSVReader object</returns>
		/// <remarks>
		/// Filters are run in the order they are attached to the reader. If any filter
		/// returns false, the row is skipped and none of the later filters are run
		/// for that row
		/// </remarks>
		public CSVReader<T> WithRowFilter(Func<CSVRawRow, bool> rowPredicate)
		{
			if( rowPredicate == null )
				throw new ArgumentNullException("rowPredicate");

			_rowFilters.Add(rowPredicate);

			return this;
		}

		/// <summary>
		/// Filters raw rows prior to processing
		/// </summary>
		/// <param name="rowPredicate">
		/// A predicate which takes a raw CSV row and returns true
		/// to process the row or false to skip it
		/// </param>
		/// <returns>The CSVReader object</returns>
		/// <remarks>
		/// Filters are run in the order they are attached to the reader. If any filter
		/// returns false, the row is skipped and none of the later filters are run
		/// for that row
		/// </remarks>
		public CSVReader<T> WithRowFilter(Predicate<CSVRawRow> rowPredicate)
		{
			if (rowPredicate == null)
				throw new ArgumentNullException("rowPredicate");

			_rowFilters.Add(f => rowPredicate(f));

			return this;
		}

			#endregion Filters

			#region Column

		/// <summary>
		/// Maps a column in the CSV file to a property in a parsed CSV row
		/// </summary>
		/// <typeparam name="TM">The type of property to map</typeparam>
		/// <param name="mapping">An expression which takes a row value and returns a property</param>
		/// <param name="emptyCellBehavior">
		/// How null values and empty strings should be handled
		/// </param>
		/// <param name="validate">
		/// Whether or not to perform data annotation on the column. If unspecified,
		/// the default validation rule for the CSVReader is used
		/// </param>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the mapping parameter is null
		/// </exception>
		/// <exception cref="CSVReaderBindException">
		/// Thrown if:
		/// <para>
		/// The mapping access properties of a type without a bound custom
		/// instantiator and for which no default instantiator exists
		/// </para>
		/// <para>
		/// The mapping access properties without a setter
		/// </para>
		/// <para>
		/// The mapping access properties with indexers
		/// </para>
		/// <para>
		/// The mapping access properties of a type without a bound custom
		/// parser and for which no default parser exists
		/// </para>
		/// </exception>
		public CSVReader<T> WithColumnMapping<TM>(
			Expression<Func<T, TM>> mapping,
			EmptyCellBehavior emptyCellBehavior = EmptyCellBehavior.Set,
			bool? validate = null
		)
		{
			if (mapping == null)
				throw new ArgumentNullException("mapping");

			Func<Type, bool> isMissingInstantiator = t =>
			{
				if (!_canCreateTypes.ContainsKey(t))
				{
					var canCreate = TypeExtensions.CanCreateParameterlessInstanceOf(t);

					if (!canCreate)
						return true;

					_canCreateTypes[t] = true;
				}

				return false;
			};

			if (isMissingInstantiator(typeof (T)))
			{
				throw new CSVReaderBindException(
					CSVReaderBindException.ExceptionType.AccessesPropertiesWithUndefinedInstantiatorType,
					typeof (T)
				);
			}
					
			var nullSafeResult = SafeAccessBuilder.BuildTypeAccess(typeof (T), typeof (TM), mapping);

			CSVReaderBindException.ExceptionType exceptionType = 0;

			// all items must be properties and writeable
			foreach (var pMember in nullSafeResult.NonArgumentMemberCalls.Select(member => member as PropertyInfo))
			{
				if (pMember == null)
				{
					throw new CSVReaderBindException(
						CSVReaderBindException.ExceptionType.AccessesNonPropertyMembers,
						mapping
					);
				}

				if (! pMember.CanWrite)
					exceptionType |= CSVReaderBindException.ExceptionType.AccessesPropertiesWithoutSetters;

				if (pMember.GetIndexParameters().Any())
					exceptionType |= CSVReaderBindException.ExceptionType.AccessesPropertiesWithIndicies;
			}

			var parseType = typeof (TM);

			if (! _customParsers.ContainsKey(parseType) && ! ParserFactory.Create(parseType).IsSuccess)
				exceptionType |= CSVReaderBindException.ExceptionType.PropertyAssignmentWithUndefinedParser;

			if (exceptionType != 0)
				throw new CSVReaderBindException(exceptionType, mapping);

			_columns.Add(
				new Column
				{
					EmptyCellBehavior = emptyCellBehavior,
					MustValidate = validate ?? _defaultValidate,
					Properties = nullSafeResult.NonArgumentMemberCalls.Cast<PropertyInfo>().ToArray()
				}
			);

			return this;
		}

			#endregion Column

		#endregion Fluent Methods

		#region Read

		/// <summary>
		/// Processes a CSV file using the bound columns, type initiators, and type parsers
		/// </summary>
		/// <param name="file">The CSV file to parse</param>
		/// <returns>An array of parsed CSV rows</returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the file parameter is null
		/// </exception>
		public CSVReadRow<T>[] Read(CSVFile file)
		{
			if (file == null)
				throw new ArgumentNullException("file");

			var rowType = typeof (T);

			var results = new List<CSVReadRow<T>>();

			var rowIndex = -1;

			foreach (var rawRow in file.DataRows)
			{
				rowIndex++;

				if (_rowFilters.Any(rf => ! rf(rawRow)))
					continue;

				var rowCellExceptions = new List<CSVCellException>();

				object rowValue;

				try
				{
					Delegate rowCreator;

					if (_objectCreators.TryGetValue(rowType, out rowCreator))
					{
						rowValue = rowCreator.DynamicInvoke();
					}
					else
					{
						var status = TypeExtensions.TryCreateInstanceOf(rowType);

						status.ThrowIfException();

						rowValue = status.Data;
					}
				}
				catch (Exception ex)
				{
					rowCellExceptions.Add(
						new CSVCellException(1, CSVCellException.ExceptionType.TypeInitiateException, null, ex)
					);

					results.Add(
						new CSVReadRow<T>(
							rowIndex, rawRow.StartLineNumber, default(T), rowCellExceptions
						)
					);

					continue;
				}

				if (rowValue == null)
				{
					rowCellExceptions.Add(
						new CSVCellException(
							1, CSVCellException.ExceptionType.TypeInitiateNull, null,
							new NullReferenceException(string.Format("Instantiator for type {0} returned null", typeof(T).FullName))
						)
					);

					results.Add(
						new CSVReadRow<T>(
							rowIndex, rawRow.StartLineNumber, default(T), rowCellExceptions
						)
					);

					continue;
				}

				foreach (var column in _columns.ToIndexed())
				{
					var contextObject = rowValue;

					var rawCell = rawRow.Cells.Length > column.Index
						? rawRow.Cells[column.Index]
						: CSVCell.FromAutoNormalize();

					if (string.IsNullOrWhiteSpace(rawCell.DisplayValue))
					{
						if (column.Value.EmptyCellBehavior == EmptyCellBehavior.Exception)
						{
							rowCellExceptions.Add(
								new CSVCellException(
									column.Index + 1,
									CSVCellException.ExceptionType.NullValueOnExceptionEmptyCellBehaviorColumn,
									null, null
								)
							);

							continue;
						}

						if (column.Value.EmptyCellBehavior == EmptyCellBehavior.Ignore)
							continue;
					}
				
					foreach (var property in column.Value.Properties.ToIndexed())
					{
						if (! property.IsLast)
						{
							CSVCellException cellException;

							contextObject = BuildOutProperty(
								column.Index + 1,
								contextObject, property,
								out cellException
							);

							if (cellException != null)
								rowCellExceptions.Add(cellException);

							if (contextObject == null)
								break;
						}
						else
						{
							List<CSVCellException> cellExceptions;

							AssignCellValueToFinalProperty(
								column.Index + 1, column.Value,
								contextObject, property,
								rawCell.DisplayValue, column.Value.MustValidate,
								out cellExceptions
							);

							rowCellExceptions.AddRange(cellExceptions);
						}
					}
				}

				var row = new CSVReadRow<T>(
					rowIndex, rawRow.StartLineNumber, (T) rowValue, rowCellExceptions
				);

				results.Add(row);
			}

			return results.ToArray();
		}

		#endregion Read

		#region Utilities

		private object BuildOutProperty(
			int columnNumber, object contextObject,
			PropertyInfo property, out CSVCellException cellException
		)
		{
			var propertyType = property.PropertyType;

			var buildOutObject = property.GetValue(contextObject, new object[0]);

			cellException = null;

			if (buildOutObject != null)
				return buildOutObject;

			try
			{
				Delegate buildOutCreator;

				if (_objectCreators.TryGetValue(propertyType, out buildOutCreator) )
				{
					buildOutObject = buildOutCreator.DynamicInvoke();
				}
				else
				{
					var state = TypeExtensions.TryCreateInstanceOf(propertyType);

					state.ThrowIfException();

					buildOutObject = state.Data;
				}
			}
			catch (Exception ex)
			{
				cellException = new CSVCellException(
					columnNumber,
					CSVCellException.ExceptionType.TypeInitiateException,
					property, ex
				);
			}

			if (buildOutObject == null)
			{
				cellException = new CSVCellException(
					columnNumber,
					CSVCellException.ExceptionType.TypeInitiateNull,
					property, null
				);

				return null;
			}

			property.SetValue(contextObject, buildOutObject, new object[0]);

			return buildOutObject;
		}

		private void AssignCellValueToFinalProperty(
			int columnNumber, Column column,
			object contextObject, PropertyInfo property,
			string displayValue, bool mustValidate,
			out List<CSVCellException> cellExceptions
		)
		{
			var propertyType = property.PropertyType;

			cellExceptions = new List<CSVCellException>();

			object parsedValue;

			try
			{
				Delegate customParser;

				if (! _customParsers.TryGetValue(propertyType, out customParser))
				{
					var defaultParserStatus = ParserFactory.Create(propertyType);

					parsedValue = defaultParserStatus.Data.Parse(displayValue);
				}
				else
				{
					parsedValue = customParser.DynamicInvoke(displayValue);
				}
			}
			catch (Exception ex)
			{
				cellExceptions.Add(
					new CSVCellException(
						columnNumber, CSVCellException.ExceptionType.ParseException, property, ex
					)
				);

				return;
			}

			if (
				column.EmptyCellBehavior == EmptyCellBehavior.Exception && (
					parsedValue == null ||
					(parsedValue is string && string.IsNullOrWhiteSpace((string) parsedValue))
				)
			)
			{
				cellExceptions.Add(
					new CSVCellException(
						columnNumber, CSVCellException.ExceptionType.NullValueOnExceptionEmptyCellBehaviorColumn,
						property, null
					)
				);

				return;
			}

			if (mustValidate)
			{
				var validators = (ValidationAttribute[]) property.GetCustomAttributes(typeof(ValidationAttribute), false);

				foreach (var validator in validators)
				{
					if (validator.IsValid(parsedValue))
						continue;

					cellExceptions.Add(
						new CSVCellException(
							columnNumber, CSVCellException.ExceptionType.ValidationException,
							property, new ValidationException(
								string.Format(
									"Value {0} failed to validate against {1}",
									parsedValue, validator
								)	
							)
						)
					);
				}
			}

			property.SetValue(contextObject, parsedValue, new object[0]);
		}

		#endregion Utilities
	}
}