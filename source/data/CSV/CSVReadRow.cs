﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace PXL.Data.CSV
{
	/// <summary>
	/// The result of a CSVReader.Read operation
	/// </summary>
	/// <typeparam name="T">The type of object into which a CSV row is read</typeparam>
	// ReSharper disable once InconsistentNaming
	public class CSVReadRow<T>
	{
		#region Constructor

		/// <summary>
		/// Creates a new CSVReadRow
		/// </summary>
		/// <param name="rowIndex">The 0-based index of the row</param>
		/// <param name="lineNumber">The 1-based line number of the first cell in the row</param>
		/// <param name="value">The value extracted from the row</param>
		/// <param name="cellExceptions">An array of exceptions related to cells in the row</param>
		/// <exception cref="System.ArgumentOutOfRangeException">
		/// Thrown if the rowIndex parameter is a negative number or the lineNumber parameter
		/// is not a positive value
		/// </exception>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the cellExceptions parameter is null
		/// </exception>
		public CSVReadRow(int rowIndex, int lineNumber, T value, IEnumerable<CSVCellException> cellExceptions)
		{
			if (rowIndex < 0)
				throw new ArgumentOutOfRangeException("rowIndex", "Row index cannot be a negative number");

			if (lineNumber < 1)
				throw new ArgumentOutOfRangeException("lineNumber", "Line number must be a positive value");

			if (cellExceptions == null)
				throw new ArgumentNullException("cellExceptions");

			RowIndex = rowIndex;
			LineNumber = lineNumber;
			Value = value;
			CellExceptions = cellExceptions.ToArray();
		}

		#endregion Constructor

		#region Properties

		/// <summary>
		/// The 0-based index of the row
		/// </summary>
		public int RowIndex { get; private set; }

		/// <summary>
		/// The 1-based line number of the first cell in the row
		/// </summary>
		public int LineNumber { get; private set; }

		/// <summary>
		/// The value extracted from the row
		/// </summary>
		public T Value { get; private set; }

		/// <summary>
		/// An array of exceptions related to cells in the row
		/// </summary>
		public CSVCellException[] CellExceptions { get; private set; }

		/// <summary>
		/// Whether the row has any cell exceptions, same as CellExceptions.Any()
		/// </summary>
		public bool HasErrors
		{
			get { return CellExceptions.Any(); }
		}

		#endregion Constructor

		#region Conversion

		/// <summary>
		/// Implicitly converts a CSVReadRow to its contained value
		/// </summary>
		/// <param name="readItem">The item to extract the value from</param>
		/// <returns>The Value property of the readItem</returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the readItem parameter is null
		/// </exception>
		public static implicit operator T(CSVReadRow<T> readItem)
		{
			if (readItem == null)
				throw new ArgumentNullException("readItem");

			return readItem.Value;
		}

		/// <summary>
		/// Gets a debug string for this row
		/// </summary>
		/// <returns>A debug string containing line number, row index, error count, row type and row string value</returns>
		public override string ToString()
		{
			return string.Format(
				"[ln={0}, ri={1}, errc={2} Row of {3} = {4}]",
				LineNumber, RowIndex, CellExceptions.Length, typeof (T).FullName, Value
			);
		}

		#endregion Conversion
	}
}