﻿using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace PXL.Data.CSV
{
	// ReSharper disable once InconsistentNaming
	/// <summary>
	/// Represents a cell in a CSV file
	/// </summary>
	public class CSVCell : ICloneable
	{
		#region Fields

		private static readonly Regex _literalQuotedMatcher;
		private static readonly Regex _quotedMatcher;
		private static readonly Regex _literalUnquotedMatcher;
		private static readonly Regex _unquotedMatcher;

		#endregion Fields

		#region Factories

		internal static bool Pull(string stream, ref int cursor, out CSVCell cell, out bool endOfRow)
		{
			if (cursor >= stream.Length)
			{
				cell = null;
				endOfRow = false;
				return false;
			}

			stream = stream.Substring(cursor);

			var tests = new[]
			{
				_literalQuotedMatcher,
				_quotedMatcher,
				_literalUnquotedMatcher,
				_unquotedMatcher
			};

			var firstMatch = tests.Select(r => r.Match(stream)).FirstOrDefault(m => m.Success);

			if (firstMatch == null)
				throw new FormatException("Could not parse CSV cell data");

			var displayValue = string.Join("", firstMatch.Groups[1].Captures.Cast<Capture>().Select(c => c.Value));
			displayValue = displayValue.Replace("\"\"", "\"");

			var rawValue = firstMatch.Value;
			var lCursor = rawValue.Length;
			cursor += lCursor;

			if (lCursor >= stream.Length)
			{
				endOfRow = true;
			}
			else if (lCursor <= stream.Length - 2 && stream[lCursor] == '\r' && stream[lCursor + 1] == '\n')
			{
				endOfRow = true;
				cursor += 2;
			}
			else if (stream[lCursor] == '\r' || stream[lCursor] == '\n')
			{
				endOfRow = true;
				cursor++;
			}
			else if (stream[lCursor] == ',')
			{
				cursor++;
				endOfRow = false;
			}
			else
			{
				throw new FormatException("Expected comma or end of line");
			}

			var isString = GetIsString(rawValue, displayValue);

			cell = new CSVCell { DisplayValue = displayValue, RawValue = rawValue, IsString = isString };

			return true;
		}

		internal static CSVCell FromAutoNormalize()
		{
			return new CSVCell
			{
				WasNormalizeCreated = true,
				RawValue = "",
				DisplayValue = "",
				IsString = true
			};
		}

		#endregion Factories

		#region Constructors

		static CSVCell()
		{
			_literalQuotedMatcher = new Regex("^=\"([^\",]|\"\")*\"");
			_quotedMatcher = new Regex("^\"([^\"]|\"\")*\"");
			_literalUnquotedMatcher = new Regex("^=([^\"\\n\\r,]*)");
			_unquotedMatcher = new Regex("^([^\"\\n\\r,]*)");
		}

		private CSVCell()
		{
		}

		/// <summary>
		/// Creates a new CSV cell with a given display value
		/// </summary>
		/// <param name="displayValue">The display value of the cell</param>
		/// <param name="isString">
		/// Whether to coerce the display value to a string. If set to false,
		/// the cell decides whether the value should be a string
		/// </param>
		public CSVCell(string displayValue, bool isString)
		{
			if (displayValue == null)
			{
				DisplayValue = "";
				RawValue = "";
				IsString = true;
				return;
			}

			IsString = isString || GetIsString("", displayValue);

			DisplayValue = displayValue;

			if (!IsString)
			{
				RawValue = displayValue;
				return;
			}

			const char unliteralIfPresent = ',';

			// " -> ""
			RawValue = displayValue.Replace("\"", "\"\"");
			RawValue = "\"" + RawValue + "\"";

			if (!RawValue.Contains(unliteralIfPresent))
				RawValue = '=' + RawValue;
		}

		/// <summary>
		/// Creates a new CSV cell with a given raw value
		/// </summary>
		/// <param name="rawValue">The raw (file) text of the cell</param>
		public CSVCell(string rawValue)
		{
			var cursor = 0;
			CSVCell cellToClone;
			bool endOfRowIgnored;

			var isValid = Pull(rawValue, ref cursor, out cellToClone, out endOfRowIgnored);

			if( ! isValid )
				throw new FormatException("Could not parse CSV cell data");

			IsString = cellToClone.IsString;
			RawValue = rawValue;
			DisplayValue = cellToClone.DisplayValue;
		}

		#endregion Constructors

		#region Properties

		/// <summary>
		/// The raw (file) value of the cell
		/// </summary>
		public string RawValue { get; private set; }

		/// <summary>
		/// The human-readable value of the cell
		/// </summary>
		public string DisplayValue { get; private set; }

		/// <summary>
		/// Whether the cell is coerced or interpreted as a string value
		/// (as opposed to a numeric value)
		/// </summary>
		public bool IsString { get; private set; }

		/// <summary>
		/// Whether or not the cell exists in a file or whether it was
		/// created for normalization purposes
		/// </summary>
		public bool WasNormalizeCreated { get; private set; }

		#endregion Properties

		#region Conversion

		/// <summary>
		/// Gets a string representation of the cell
		/// </summary>
		/// <returns>A string representation of the cell</returns>
		public override string ToString()
		{
			return string.Format("[{0}]", DisplayValue);
		}

		#endregion Conversion

		#region ICloneable

		/// <summary>
		/// Clones the cell
		/// </summary>
		/// <returns>A CSVCell with all properties copied from the invocant</returns>
		public object Clone()
		{
			return new CSVCell
			{
				IsString = IsString,
				DisplayValue = DisplayValue,
				RawValue = RawValue,
				WasNormalizeCreated = WasNormalizeCreated
			};
		}

		#endregion ICloneable

		#region Utilities

		private static bool GetIsString(string rawValue, string displayValue)
		{
			return rawValue.Contains('"') || displayValue.Any(c => !char.IsDigit(c) && c != '.' && c != '-' && c != '+');
		}

		#endregion Utilities
	}
}
