﻿using System;
using System.Collections.Generic;
using System.Collections.Generic.Extensions;
using System.Linq;
using System.Text;

namespace PXL.Data.CSV
{
	// ReSharper disable once InconsistentNaming
	/// <summary>
	/// Represents an unparsed row in a CSV file
	/// </summary>
	public class CSVRawRow
	{
		private CSVCell[] _cells;

		#region Constructor

		/// <summary>
		/// Creates a new CSVRawRow
		/// </summary>
		/// <param name="startLineNumber">
		/// The 1-based line number of the start of the first cell in the row
		/// </param>
		/// <param name="cells">
		/// The cells in the row
		/// </param>
		/// <exception cref="System.ArgumentOutOfRangeException">
		/// Thrown if the startLineNumber parameter is not a positive value
		/// </exception>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the cells parameter is null
		/// </exception>
		public CSVRawRow(int startLineNumber, IEnumerable<CSVCell> cells)
		{
			if (startLineNumber < 1)
				throw new ArgumentOutOfRangeException("startLineNumber", "Start line number must be positive");

			if (cells == null)
				throw new ArgumentNullException("cells");

			StartLineNumber = startLineNumber;
			_cells = cells.ToArray();
		}

		#endregion Constructor

		#region Properties

		/// <summary>
		/// The 1-based line number of the start of the first cell in the row
		/// </summary>
		public int StartLineNumber { get; private set; }

		/// <summary>
		/// The cells in the row
		/// </summary>
		public CSVCell[] Cells
		{
			get
			{
				return _cells
					.Select(c => c.Clone())
					.Cast<CSVCell>()
					.ToArray();
			}
		}

		#endregion Properties

		#region Normalize

		internal void NormalizeTo(int expectedColumnCount)
		{
			if (_cells.Length >= expectedColumnCount)
				return;

			var nCells = new CSVCell[expectedColumnCount];
			Array.Copy(_cells, nCells, _cells.Length);

			for (var i = _cells.Length; i < expectedColumnCount; i++)
			{
				nCells[i] = CSVCell.FromAutoNormalize();
			}

			_cells = nCells;
		}

		#endregion Normalize

		#region Conversion

		/// <summary>
		/// Gets the complete raw CSV string value of the row
		/// </summary>
		/// <returns>The complete raw CSV string value of the row</returns>
		public override string ToString()
		{
			var sb = new StringBuilder();

			_cells.ForEach((h, hidx, hf, hl) =>
			{
				sb.Append(h.RawValue);
				if (!hl) sb.Append(',');
			});

			sb.Append("\r\n");

			return sb.ToString();
		}

		#endregion Conversion
	}
}