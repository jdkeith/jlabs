﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Numerics;

using PXL.Core.String;

namespace PXL.Data.CSV
{
	// ReSharper disable once InconsistentNaming
	/// <summary>
	/// Base class for CSVBuilder to avoid shared static properties in generic type
	/// </summary>
	public class CSVBuilder
	{
		protected readonly static Type[] NumericTypes;

		static CSVBuilder()
		{
			NumericTypes = new[]
			{
				typeof (byte),
				typeof (sbyte),
				typeof (short),
				typeof (ushort),
				typeof (int),
				typeof (uint),
				typeof (long),
				typeof (ulong),
				typeof(BigInteger),

				typeof (byte?),
				typeof (sbyte?),
				typeof (short?),
				typeof (ushort?),
				typeof (int?),
				typeof (uint?),
				typeof (long?),
				typeof (ulong?),
				typeof(BigInteger?)
			};
		}

		/// <summary>
		/// Creates a new CSVBuilder
		/// </summary>
		protected CSVBuilder()
		{
		}
	}

	// ReSharper disable once InconsistentNaming
	/// <summary>
	/// A class which turns an enumeration of a certain type into a well-formed CSV document
	/// </summary>
	/// <typeparam name="T">The type of item to transform</typeparam>
	public class CSVBuilder<T> : CSVBuilder
	{
		#region Inner Classes

		private class Column
		{
			public Delegate ValueExtractor;
			public string HeaderName;
			public Delegate Formatter;
			public Type PropertyType;
			public bool CoerceToString;
		}

		#endregion Inner Classes

		#region Fields

		private readonly List<Column> _columns;

		#endregion Fields

		#region Constructor

		/// <summary>
		/// Creates a new CSVBuilder
		/// </summary>
		public CSVBuilder()
		{
			_columns = new List<Column>();
		}

		#endregion Constructor

		#region Fluent Methods

		/// <summary>
		/// Adds a column to the CSV document mapping a property to a column
		/// </summary>
		/// <typeparam name="TM">The type of property to map to the column</typeparam>
		/// <param name="valueExtractor">A function which accesses an item's property</param>
		/// <param name="formatter">An optional formatter which converts the property value to a string</param>
		/// <param name="headerName">
		/// An optional custom header name. If not specified, the name of the property is used
		/// </param>
		/// <param name="coerceToString">
		/// Whether to coerce values which appear to be numeric to strings. This prevents programs
		/// such as Excel from right-aligning them.
		/// </param>
		/// <returns>The CSV builder object</returns>
		/// <exception cref="System.NotSupportedException">
		/// Thrown if the value extractor does anything other than access properties
		/// </exception>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the valueExtractor parameter is null
		/// </exception>
		public CSVBuilder<T> WithColumn<TM>(
			Expression<Func<T, TM>> valueExtractor,
			Func<TM, string> formatter = null,
			string headerName = null,
			bool coerceToString = false
		)
		{
			return WithColumnInternal(valueExtractor, headerName, formatter, coerceToString);
		}

		/// <summary>
		/// Adds a column to the CSV document mapping a property to a column
		/// </summary>
		/// <typeparam name="TM">The type of property to map to the column</typeparam>
		/// <param name="valueExtractor">A function which accesses an item's property</param>
		/// <param name="format">A string format to apply to property values</param>
		/// <param name="formatProvider">An optional format provider which helps convert a property value to a string</param>
		/// <param name="headerName">
		/// An optional custom header name. If not specified, the name of the property is used
		/// </param>
		/// <param name="coerceToString">
		/// Whether to coerce values which appear to be numeric to strings. This prevents programs
		/// such as Excel from right-aligning them.
		/// </param>
		/// <returns>The CSV builder object</returns>
		/// <exception cref="System.NotSupportedException">
		/// Thrown if the value extractor does anything other than access properties
		/// </exception>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the valueExtractor or format parameters are null
		/// </exception>
		public CSVBuilder<T> WithColumn<TM>(
			Expression<Func<T, TM>> valueExtractor,
			string format,
			IFormatProvider formatProvider = null,
			string headerName = null,
			bool coerceToString = false
		) where TM : IFormattable
		{
			if (format == null)
				throw new ArgumentNullException("format");

			Func<TM, string> formatter = v => v.ToString(format, formatProvider);

			return WithColumnInternal(valueExtractor, headerName, formatter, coerceToString);
		}

		private CSVBuilder<T> WithColumnInternal<TM>(
			Expression<Func<T, TM>> valueExtractor,
			string headerName,
			Func<TM, string> formatter,
			bool coerceToString
		)
		{
			if (valueExtractor == null)
				throw new ArgumentNullException("valueExtractor");

			if (headerName != null && string.IsNullOrWhiteSpace(headerName))
				throw new ArgumentException("Header name must be set", "headerName");

			formatter = formatter ?? (v => v.ToString());

			var nullSafeResult = SafeAccessBuilder.BuildTypeAccess(typeof (T), typeof (TM), valueExtractor);
			
			if (headerName == null)
				headerName = string.Join(" ", nullSafeResult.NonArgumentMemberCalls.Select(m => m.Name).Last().SplitCamelOrPascal());

			_columns.Add(
				new Column
				{
					ValueExtractor = nullSafeResult.NullSafeFunction,
					HeaderName = headerName,
					Formatter = formatter,
					PropertyType = typeof(TM),
					CoerceToString = coerceToString
				}
			);

			return this;
		}

		#endregion Fluent Methods

		#region Build

		/// <summary>
		/// Creates a CSV file from an enumeration of items
		/// </summary>
		/// <param name="items">The enumeration of items to create a CSV file from</param>
		/// <param name="includeHeaderRow">Whether or not to include a header row</param>
		/// <returns>A CSV file built from the enumeration and column settings</returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the items parameter is null
		/// </exception>
		public CSVFile Build(IEnumerable<T> items, bool includeHeaderRow = true)
		{
			if (items == null)
				throw new ArgumentNullException("items");

			CSVRawRow headerRow = null; 

			var rows = new List<CSVRawRow>();

			var rowStartLineNumber = 1;

			if (includeHeaderRow)
			{
				headerRow = new CSVRawRow(
					rowStartLineNumber,
					_columns.Select(h => new CSVCell(h.HeaderName, true))
				);

				rowStartLineNumber++;
			}

			foreach (var item in items)
			{
				var rowCells = new List<CSVCell>();
				var rowLineNumberBias = 0;

				foreach (var column in _columns)
				{
					var isString = 
						column.CoerceToString || (
							! (NumericTypes.Any(t => column.PropertyType == t)) &&
							column.PropertyType != typeof (DateTime) &&
							column.PropertyType != typeof (DateTime?)
						);

					var propertyValue = column.ValueExtractor.DynamicInvoke(item);

					var displayValue = propertyValue == null
						? string.Empty
						: (string)column.Formatter.DynamicInvoke(propertyValue);

					var cell = new CSVCell(displayValue, isString);

					rowLineNumberBias += cell.RawValue.Count(c => c == '\n');

					rowCells.Add(cell);
				}

				rows.Add(new CSVRawRow(rowStartLineNumber, rowCells));

				rowStartLineNumber++;
				rowStartLineNumber += rowLineNumberBias;
			}

			return new CSVFile(headerRow, rows.ToArray());
		}

		#endregion Build
	}
}
