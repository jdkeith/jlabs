﻿using System;
using System.Collections.Generic;
using System.Collections.Generic.Extensions;
using System.IO;
using System.Linq;
using System.Text;

namespace PXL.Data.CSV
{
	/// <summary>
	/// An interface for reading and deserializing CSV files
	/// </summary>
	// ReSharper disable once InconsistentNaming
	public interface ICSVFactory : IDeserializer<CSVFile>
	{
		/// <summary>
		/// Reads a CSV file from a string
		/// </summary>
		/// <param name="data">The raw csv string data to parse</param>
		/// <param name="hasHeaderRow">Whether or not the csv data includes a header row</param>
		/// <returns>A CSV file if the data is valid</returns>
		/// <exception cref="System.ArgumentNullException">
		/// Implementations should throw this if the data argument is null
		/// </exception>
		/// <exception cref="System.FormatException">
		/// Implementations should throw this if the data does not produce a valid CSV file
		/// </exception>
		CSVFile Read(string data, bool hasHeaderRow);

		/// <summary>
		/// Deserializes a CSV file from a byte array
		/// </summary>
		/// <param name="serialized">The byte array to deserialize a CSV file from</param>
		/// <param name="hasHeaderRow">Whether or not the csv data includes a header row</param>
		/// <returns>The CSV file represented by the byte array</returns>
		/// <exception cref="System.ArgumentNullException">
		/// Implementations should throw this if the serialized argument is null
		/// </exception>
		/// <exception cref="System.FormatException">
		/// Implementations should throw this if the data does not produce a valid CSV file
		/// </exception>
		CSVFile Deserialize(byte[] serialized, bool hasHeaderRow);

		/// <summary>
		/// Reads a CSV file from a steam
		/// </summary>
		/// <param name="s">The stream to read the CSV file from</param>
		/// <param name="hasHeaderRow">Whether or not the csv data includes a header row</param>
		/// <returns>The CSV file within the stream</returns>
		/// <exception cref="System.ArgumentNullException">
		/// Implementations should throw this if the serialized argument is null
		/// </exception>
		/// <exception cref="System.FormatException">
		/// Implementations should throw this if the data does not produce a valid CSV file
		/// </exception>
		CSVFile ReadFrom(Stream s, bool hasHeaderRow);
	}

	/// <summary>
	/// A CSV file which can be read, explored, or saved
	/// </summary>
	// ReSharper disable once InconsistentNaming
	public class CSVFile : ISerializable
	{
		#region Factory

		// ReSharper disable once InconsistentNaming
		private class CSVFactory : ICSVFactory
		{
			public CSVFile Deserialize(byte[] serialized)
			{
				return Deserialize(serialized, false);
			}

			public CSVFile Deserialize(byte[] serialized, bool hasHeaders)
			{
				if (serialized == null)
					throw new ArgumentNullException("serialized");

				var str = Encoding.UTF8.GetString(serialized);
				return Read(str, hasHeaders);
			}

			public CSVFile ReadFrom(Stream s)
			{
				return ReadFrom(s, false);
			}

			public CSVFile ReadFrom(Stream s, bool hasHeaders)
			{
				if (s == null)
					throw new ArgumentNullException("s");

				var buffer = new byte[65536];

				using (var ms = new MemoryStream())
				{
					int read;

					while ((read = s.Read(buffer, 0, buffer.Length)) > 0)
					{
						ms.Write(buffer, 0, read);
					}

					var str = Encoding.UTF8.GetString(ms.ToArray());
					return Read(str, hasHeaders);
				}
			}

			public CSVFile Read(string data, bool includeHeaderRow)
			{
				var trim = data.TakeWhile(char.IsWhiteSpace).ToArray();

				var currentRowStartLine = trim.Count(c => c == '\n') + 1;

				data = data.Substring(trim.Length);

				var rows = new List<CSVRawRow>();
				var rowCells = new List<CSVCell>();

				var cursor = 0;
				var rowLineNumberBias = 0;

				while (true)
				{
					bool endOfRow;
					CSVCell cell;

					var gotCell = CSVCell.Pull(data, ref cursor, out cell, out endOfRow);

					if (! gotCell)
					{
						if (rowCells.Any())
						{
							var row = new CSVRawRow(currentRowStartLine, rowCells);
							rows.Add(row);
						}

						break;
					}

					rowLineNumberBias += (cell.RawValue ?? "").Count(c => c == '\n');

					if (endOfRow)
					{
						rowCells.Add(cell);
						var row = new CSVRawRow(currentRowStartLine, rowCells);
						rows.Add(row);
						rowCells.Clear();

						currentRowStartLine++;
						currentRowStartLine += rowLineNumberBias;
					}
					else
					{
						rowCells.Add(cell);
					}
				}

				var headers = includeHeaderRow ? rows.FirstOrDefault() : null;
				var dataRows = rows.Skip(includeHeaderRow ? 1 : 0).ToArray();

				return new CSVFile(headers, dataRows);
			}
		}

		/// <summary>
		/// Returns an ICSVFactory which can be used to read or deserialize csv files
		/// </summary>
		public static ICSVFactory Factory
		{
			get { return new CSVFactory(); }
		}

		#endregion Factory

		#region Fields

		private bool _isNormalized;

		#endregion Fields

		#region Constructor

		internal CSVFile(CSVRawRow headerRow, CSVRawRow[] dataRows)
		{
			HeaderRow = headerRow;
			DataRows = dataRows;
		}

		#endregion Constructor

		#region Properties

		/// <summary>
		/// An enumeration of header cells
		/// </summary>
		public CSVRawRow HeaderRow { get; private set; }

		/// <summary>
		/// An enumeration of data rows, each containing an enumeration of data row cells
		/// </summary>
		public CSVRawRow[] DataRows { get; private set; }

		#endregion Properties

		#region Normalization

		/// <summary>
		/// Normalizes the CSV file by ensuring that all
		/// rows contain the same number of elements.
		/// </summary>
		/// <remarks>
		/// If some rows have less columns than other rows, empty CSVCells
		/// are appended to the row until it is the appropriate length. These CSVCells
		/// have their WasNormalizeCreated property set to true
		/// </remarks>
		public void Normalize()
		{
			if (_isNormalized) return;

			var headerCount = HeaderRow == null ? 0 : HeaderRow.Cells.Length;

			var expectedColumnCount = Math.Max(headerCount, DataRows.Max(dr => dr.Cells.Length));

			if( HeaderRow != null )
			{
				HeaderRow.NormalizeTo(expectedColumnCount);
			}

			DataRows.ForEach(dr => dr.NormalizeTo(expectedColumnCount));

			_isNormalized = true;
		}

		#endregion Normalization

		#region Serialization

		/// <summary>
		/// Serializes the CSV file to a byte array
		/// </summary>
		/// <returns>The serialized representation of the CSV file</returns>
		public virtual byte[] ToByteArray()
		{
			using (var ms = new MemoryStream())
			{
				WriteTo(ms);
				return ms.ToArray();
			}
		}

		/// <summary>
		/// Writes the CSV file to a stream
		/// </summary>
		/// <param name="s">The stream to write the CSV file to</param>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the stream parameter is null
		/// </exception>
		public virtual void WriteTo(Stream s)
		{
			if( s == null )
				throw new ArgumentNullException("s");

			var bytes = Encoding.UTF8.GetBytes(ToString());

			s.Write(bytes, 0, bytes.Length);
		}

		#endregion Serialization

		#region Conversion

		/// <summary>
		/// Gets the complete raw CSV string value of the file
		/// </summary>
		/// <returns>The complete raw CSV string value of the file</returns>
		public override string ToString()
		{
			var sb = new StringBuilder();

			if (HeaderRow != null)
				sb.Append(HeaderRow);

			foreach (var dr in DataRows)
			{
				sb.Append(dr);
			}

			return sb.ToString();
		}

		#endregion Conversion
	}
}