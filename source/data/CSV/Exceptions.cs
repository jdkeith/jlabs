﻿using System;
using System.Linq.Expressions;
using System.Reflection;

namespace PXL.Data.CSV
{
	/// <summary>
	/// Exception which is thrown when an invalid column, type instantiator, or type parser
	/// binding is made on a CSVReader
	/// </summary>
	// ReSharper disable once InconsistentNaming
	public class CSVReaderBindException : Exception
	{
		#region Inner Class

		/// <summary>
		/// The specific type of problem with the binding
		/// </summary>
		[Flags]
		public enum ExceptionType : byte
		{
			/// <summary>
			/// Property expression attempts to access fields, methods, or events
			/// </summary>
			AccessesNonPropertyMembers = 1,

			/// <summary>
			/// Property expression attempts to access properties which cannot be assigned to
			/// </summary>
			AccessesPropertiesWithoutSetters = 2,

			/// <summary>
			/// Property expression attempts to access properties which have index parameters
			/// </summary>
			AccessesPropertiesWithIndicies = 4,

			/// <summary>
			/// Property expression attempts to access properties whose types
			/// cannot be auto-instantiated and for which no custom type instantiator has been supplied
			/// </summary>
			/// <remarks>
			/// Custom type instantiators must be assigned before the first WithColumnBinding call
			/// using the type is made
			/// </remarks>
			AccessesPropertiesWithUndefinedInstantiatorType = 8,

			/// <summary>
			/// Property expression's final property has a type for which a default parser cannot
			/// be created and for which no custom type parser has been supplied
			/// </summary>
			/// <remarks>
			/// Custom type parsers must be assigned before the first WithColumnBinding call
			/// using the type is made
			/// </remarks>
			PropertyAssignmentWithUndefinedParser = 16,
			
			/// <summary>
			/// An custom type instantiator for the given type has already been supplied
			/// </summary>
			DuplicateInstantiator = 64,

			/// <summary>
			/// An custom type parser for the given type has already been supplied
			/// </summary>
			DuplicateParser = 128
		}

		#endregion Inner Class

		private readonly string _message;

		#region Constructor

		/// <summary>
		/// Creates a new CSVReaderBindException
		/// </summary>
		/// <param name="details">The specific type of problem with the binding</param>
		/// <param name="type">The mapping which failed to bind</param>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the type parameter is null
		/// </exception>
		public CSVReaderBindException(ExceptionType details, Type type)
		{
			if (type == null)
				throw new ArgumentNullException("type");

			_message = string.Format("Binding of type {0} caused errors.\n", type.FullName);

			FillMessage(details, ref _message);

			Type = type;
			Details = details;
		}

		/// <summary>
		/// Creates a new CSVReaderBindException
		/// </summary>
		/// <param name="details">The specific type of problem with the binding</param>
		/// <param name="bindExpression">The binding expression which failed</param>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the bindExpression parameter is null
		/// </exception>
		public CSVReaderBindException(ExceptionType details, Expression bindExpression)
		{
			if (bindExpression == null)
				throw new ArgumentNullException("bindExpression");

			_message = string.Format(
				"Binding expression {0} has errors.\n", bindExpression
			);

			FillMessage(details, ref _message);

			BindExpression = bindExpression;
			Details = details;
		}

		private void FillMessage(ExceptionType details, ref string message)
		{
			if (details.HasFlag(ExceptionType.AccessesNonPropertyMembers))
				message += "Property expression attempts to access fields, methods, or events\n";

			if (details.HasFlag(ExceptionType.AccessesPropertiesWithoutSetters))
				message += "Property expression attempts to access properties which cannot be assigned to\n";

			if (details.HasFlag(ExceptionType.AccessesPropertiesWithIndicies))
				message += "Property expression attempts to access properties which have index parameters\n";

			if (details.HasFlag(ExceptionType.AccessesPropertiesWithUndefinedInstantiatorType))
				message += "Property expression attempts to access properties whose types "
						 + "cannot be auto-instantiated and for which no custom type instantiator has been supplied\n";

			if (details.HasFlag(ExceptionType.PropertyAssignmentWithUndefinedParser))
				message += "Property expression's final property has a type for which a default parser cannot "
						 + "be created and for which no custom type parser has been supplied\n";

			if (details.HasFlag(ExceptionType.DuplicateInstantiator))
				message += "An custom type instantiator for the given type has already been supplied\n";

			if (details.HasFlag(ExceptionType.DuplicateParser))
				message += "An custom type parser for the given type has already been supplied\n";
		}

		#endregion Constructor

		#region Properties

		/// <summary>
		/// The specific type of problem with the binding
		/// </summary>
		public ExceptionType Details { get; private set; }

		/// <summary>
		/// The binding expression which failed
		/// </summary>
		public Expression BindExpression { get; private set; }

		/// <summary>
		/// The type which failed binding
		/// </summary>
		public Type Type { get; private set; }

		/// <summary>
		/// Gets a message which describes the current exception
		/// </summary>
		public override string Message
		{
			get { return _message; }
		}

		#endregion Properties
	}

	/// <summary>
	/// Exception stored with a CSVReadItem indicating a problem running the parser
	/// for a cell or row
	/// </summary>
	// ReSharper disable once InconsistentNaming
	public class CSVCellException : Exception
	{
		#region Inner Class

		/// <summary>
		/// The specific type of problem with reading the cell
		/// </summary>
		public enum ExceptionType : byte
		{
			/// <summary>
			/// A type initiator threw an exception
			/// </summary>
			TypeInitiateException,

			/// <summary>
			/// A type initiator returned a null value
			/// </summary>
			TypeInitiateNull,

			/// <summary>
			/// A parser threw an exception
			/// </summary>
			ParseException,

			/// <summary>
			/// A validator failed or a required value was not supplied on a validated property
			/// </summary>
			ValidationException,

			/// <summary>
			/// A required value was not supplied and the user set the column to have
			/// an EmptyCellBehavior of Exception
			/// </summary>
			NullValueOnExceptionEmptyCellBehaviorColumn
		}

		#endregion Inner Class

		private readonly string _message;
				
		#region Constructor

		/// <summary>
		/// Creates a new CSVCellException
		/// </summary>
		/// <param name="columnNumber">
		/// The 1-based column number where the error occured. Row errors
		/// are listed as being on the first column where they were encountered
		/// </param>
		/// <param name="details">The specific type of problem with reading the cell</param>
		/// <param name="assignMember">The property which failed to be filled with a value</param>
		/// <param name="innerException">The exception which caused the CSVCellException</param>
		/// <exception cref="System.ArgumentOutOfRangeException">
		/// Thrown if the columnNumber parameter not a positive number
		/// </exception>
		public CSVCellException(
			int columnNumber, ExceptionType details,
			PropertyInfo assignMember, Exception innerException
		) : base("", innerException)
		{
			if (columnNumber < 1)
				throw new ArgumentOutOfRangeException("columnNumber", "Column number must be a positive value");

			if (assignMember == null)
			{
				_message = string.Format(
					"In column {0} row parse failed due a {1}. " +
					"See inner exception for additional details.",
					columnNumber, details
				);
			}
			else
			{
				_message = string.Format(
					"In column {0}, assignment to property {1} on type {2} failed due to a {3}. " +
					"See inner exception for additional details.",
					columnNumber, assignMember.Name,
					(assignMember.ReflectedType != null ? assignMember.ReflectedType.FullName : "<unknown>"),
					details
				);
			}

			ColumnNumber = columnNumber;
			AssignMember = assignMember;
			Details = details;
		}

		#endregion Constructor

		#region Properties

		/// <summary>
		/// The 1-based column number where the error occured. Row errors
		/// are listed as being on the first column where they were encountered
		/// </summary>
		public int ColumnNumber { get; private set; }

		/// <summary>
		/// The specific type of problem with reading the cell
		/// </summary>
		public ExceptionType Details { get; private set; }

		/// <summary>
		/// The property which failed to be filled with a value
		/// </summary>
		public PropertyInfo AssignMember { get; private set; }

		/// <summary>
		/// Gets a message which describes the current exception
		/// </summary>
		public override string Message
		{
			get { return _message; }
		}

		#endregion Properties
	}
}