﻿namespace PXL.Data.CSV
{
	/// <summary>
	/// How empty cells (nulls or empty strings) are interpreted for a column
	/// </summary>
	public enum EmptyCellBehavior
	{
		/// <summary>
		/// Sets the property to empty or null if the parser supports it
		/// </summary>
		Set,

		/// <summary>
		/// Does not assign the property
		/// </summary>
		/// <remarks>
		/// This is useful in situations where a custom instantiator is
		/// setting default values, or where a parser will fail on null or empty values
		/// </remarks>
		Ignore,

		/// <summary>
		/// Does not assign the property and raises an exception
		/// </summary>
		Exception
	}
}