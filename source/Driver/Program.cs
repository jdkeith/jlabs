﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using PXL.Cryptography.ECC;
using PXL.Cryptography.ECC.Encryption;
using PXL.Cryptography.ECC.Specialized.Signatures.Ring;

namespace Driver
{
	class Program
	{
		static void Main(string[] args)
		{
			var curve = ECCurve.SECP256K1;

			RandomHelper rh  =new RandomHelper(new RNGCryptoServiceProvider(), true);

			BigInteger[] privKeys = Enumerable.Range(0, 10).Select(idx => rh.NextBigInteger(curve.N)).ToArray();
			ECPoint[] pubKeys = privKeys.Select(ppk => curve.G*ppk).ToArray();

			SigningRing sr = new SigningRing(pubKeys, true);

			byte[] clearBytes = Encoding.UTF8.GetBytes("This is the message to sign");

			RingSignature sig = sr.Sign(clearBytes, privKeys[4]);

			bool isValid = sr.Verify(clearBytes, sig);

		}
	}
}
