﻿using System;
using System.Collections.Generic;
using System.Net.Mail;

namespace PXL.Service.Communication
{
	/// <summary>
	/// Represents a service which can mail messages synchronously or asynchronously
	/// </summary>
	public interface IMailService
	{
		/// <summary>
		/// Whether or not the given email address is valid
		/// </summary>
		/// <param name="emailAddress">The email address to test</param>
		/// <returns>True if the email address is valid, false otherwise</returns>
		bool IsValidEmailAddress(string emailAddress);

		/// <summary>
		/// Mails a message immediately
		/// </summary>
		/// <param name="message">The message to mail</param>
		/// <returns>A status related to the sending of the message</returns>
		/// <exception cref="System.ArgumentNullException">Thrown if the message parameter is null</exception>
		Status<MailMessage> Mail(MailMessage message);

		/// <summary>
		/// Mails many messages immediately
		/// </summary>
		/// <param name="messages">The messages to mail</param>
		/// <returns>A status related to the sending of each message</returns>
		/// <exception cref="System.ArgumentNullException">Thrown if the messages parameter is null</exception>
		IEnumerable<Status<MailMessage>> Mail(IEnumerable<MailMessage> messages);

		/// <summary>
		/// Mails a message asynchronously
		/// </summary>
		/// <param name="message">The message to mail</param>
		/// <returns>A status related to the initial processing of, but not the sending of, the message</returns>
		/// <exception cref="System.ArgumentNullException">Thrown if the message parameter is null</exception>
		Status<MailMessage> MailAsync(MailMessage message);

		/// <summary>
		/// Mails a message asynchronously
		/// </summary>
		/// <param name="message">The message to mail</param>
		/// <returns>A status related to the initial processing of, but not the sending of, the message</returns>
		/// <exception cref="System.ArgumentNullException">Thrown if the messages parameter is null</exception>
		IEnumerable<Status<MailMessage>> MailAsync(IEnumerable<MailMessage> messages);

		/// <summary>
		/// Gets an enumerable of all messages in the given range waiting to be sent, oldest messages first
		/// </summary>
		/// <param name="startFrom">An optional position of the first message to retrieve useful for paging</param>
		/// <param name="count">The maximum number of results to return useful for paging</param>
		/// <returns>An enumerable within the given range of all messages waiting to be sent, oldest messages first</returns>
		/// <exception cref="System.NotSupportedException">Thrown if range arguments are specified and the implementation doesn't support paging</exception>
		IEnumerable<MailAddress> GetAsyncPending(int startFrom=0, int count=int.MaxValue);

		/// <summary>
		/// Processes a specified amount of pending messages synchronously.
		/// </summary>
		/// <param name="maxCount">The maximum number of messages to process, starting with oldest first</param>
		/// <returns></returns>
		/// <exception cref="System.NotSupportedException">Thrown if the implementation handles asynchronous messages out of process</exception>
		/// <remarks>
		/// This is preferrable to using GetAsyncPending and then passing the results to the synchronous Mail method
		/// because the implementation may use an out of process system to handle asynchronous message sending.
		/// </remarks>
		IEnumerable<Status<MailAddress>> ProcessPending(int maxCount = int.MaxValue);

		/// <summary>
		/// Processes as many asynchronous message as possible in the time given. This is an synchronous method.
		/// </summary>
		/// <param name="timeout">The maximum number of time to spend sending unsent messages, starting with oldest messages first</param>
		/// <returns></returns>
		/// <exception cref="System.NotSupportedException">Thrown if the implementation handles asynchronous messages out of process</exception>
		/// <remarks>
		/// This is preferrable to using GetAsyncPending and then passing the results to the synchronous Mail method
		/// because the implementation may use an out of process system to handle asynchronous message sending.
		/// </remarks>
		IEnumerable<Status<MailAddress>> ProcessPending(TimeSpan? timeout = null);
	}
}