﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PXL.Core.String;
using PXL.Units;

namespace PXL.Service.Authentication
{
	/// <summary>
	/// The base class for URI generators
	/// </summary>
	public abstract class SecureUriGeneratorValidatorBase
	{
		#region Inner Class

		/// <summary>
		/// A protected structure which represents a query parameter
		/// </summary>
		protected struct UriParam
		{
			public string Key;
			public string Value;
			public bool UseInGenerationHashComputation;
			public bool ExposeInUrl;

			public override string ToString()
			{
				return string.Format("{0}={1}", Key, Value);
			}
		}

		/// <summary>
		/// Which components in a URI to ignore when generating secure urls
		/// </summary>
		[Flags]
		// ReSharper disable once InconsistentNaming
		public enum IgnoreURIComponents
		{
			/// <summary>
			/// No components are ignored
			/// </summary>
			None,

			/// <summary>
			/// The subdomain is filtered, that is A.Y.Z and B.Y.Z are considered the same,
			/// but A.X.Z and A.Y.Z are considered different
			/// </summary>
			Subdomain = 1,

			/// <summary>
			/// The port is filtered
			/// </summary>
			Port = 2,

			/// <summary>
			/// The protocol (e.g. http/https) is filtered
			/// </summary>
			Protocol = 4,

			/// <summary>
			/// The entire domain, including port, protocol, and subdomain
			/// </summary>
			Domain = 7
		}

		#endregion Inner Class

		#region Fields

		/// <summary>
		/// The name of the authorization key parameter
		/// </summary>
		public const string AuthKey = "_auth";

		private readonly string _sharedSecret;

		#endregion Fields

		#region Constructor

		/// <summary>
		/// Creates a new SecureUriGeneratorValidatorBase
		/// </summary>
		/// <param name="sharedSecret">The shared secret used for generating and verifying secure URIs</param>
		/// <param name="ignoreComponents">Which URI components are filtered in generation and verification</param>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the sharedSecret parameter is null
		/// </exception>
		/// <exception cref="System.ArgumentException">
		/// Thrown if the sharedSecret parameter is empty or consists entirely of whitespace
		/// </exception>
		protected SecureUriGeneratorValidatorBase(
			string sharedSecret, IgnoreURIComponents ignoreComponents = IgnoreURIComponents.None
		)
		{
			if( sharedSecret == null )
				throw new ArgumentNullException("sharedSecret");

			if( string.IsNullOrWhiteSpace(sharedSecret) )
				throw new ArgumentException("Shared secret cannot be empty or whitespace", "sharedSecret");

			_sharedSecret = sharedSecret;
			IgnoreComponents = ignoreComponents;
		}

		#endregion Constructor

		#region Properties

		/// <summary>
		/// Which URI components are filtered in generation and verification
		/// </summary>
		public IgnoreURIComponents IgnoreComponents { get; private set; }

		#endregion Properties

		#region Public Methods

		/// <summary>
		/// Generates a secure URI from a given URI and in a specific context
		/// </summary>
		/// <param name="uri">The URI to generate the secure URI from</param>
		/// <param name="context">
		/// An optional context which allows subclasses to process generation
		/// and verification differently. URIs generated with a specific context
		/// should be verified using that same context.
		/// </param>
		/// <returns>
		/// A secure URI
		/// </returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the uri parameter is null
		/// </exception>
		public Uri Generate(Uri uri, object context = null)
		{
			if (uri == null)
				throw new ArgumentNullException("uri");

			string hashUrlBase;
			string exposeUrlBase;

			GetUrlWithoutParamsOrHash(uri, out hashUrlBase, out exposeUrlBase);

			UriParam[] ignoredExistingParams;
			UriParam[] hashParams;
			List<UriParam> exposedParams;

			GetParams(false, uri, out ignoredExistingParams, out hashParams, out exposedParams, context);

			var hash = SHA256Hash.Factory.Of(
				_sharedSecret + ":" + hashUrlBase + "?" +
				string.Join("&", hashParams.Select(hp => hp.ToString()))
			);

			exposedParams.Add(
				new UriParam { Key = AuthKey, Value = hash.ToString() }
			);

			return new Uri(
				exposeUrlBase + "?" + string.Join("&", exposedParams.Select(ep => ep.ToString())) + uri.Fragment,
				UriKind.Absolute
			);
		}
		
		/// <summary>
		/// Validates a secure URI in a specific context
		/// </summary>
		/// <param name="secureUri">The secure URI to verify</param>
		/// <param name="context">
		/// An optional context which allows subclasses to process generation
		/// and verification differently. URIs generated with a specific context
		/// should be verified using that same context.
		/// </param>
		/// <returns>
		/// A status indicating success or failure, and the reason for failure
		/// </returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the secureUri parameter is null
		/// </exception>
		public Status Validate(Uri secureUri, object context = null)
		{
			if (secureUri == null)
				throw new ArgumentNullException("secureUri");

			string hashUrlBase;
			string exposeUrlBase;

			GetUrlWithoutParamsOrHash(secureUri, out hashUrlBase, out exposeUrlBase);

			UriParam[] existingParams;
			UriParam[] hashParams;
			List<UriParam> exposedParams;

			GetParams(true, secureUri, out existingParams, out hashParams, out exposedParams, context);

			var computedHash = SHA256Hash.Factory.Of(
				_sharedSecret + ":" + hashUrlBase + "?" +
				string.Join("&", hashParams.Select(hp => hp.ToString()))
			);

			var passedHashString = existingParams
				.Where(ep => ep.Key == AuthKey)
				.Select(ep => ep.Value)
				.FirstOrDefault();

			var passedHash = SHA256Hash.Factory.TryParse(passedHashString);

			if (! passedHash.IsSuccess)
			{
				return Status.FailWithMessage(
					"Missing or invalid authorization parameter (" + AuthKey + ")"
				);
			}

			if (passedHash.Data != computedHash)
			{
				return Status.FailWithMessage(
					"Invalid authorization parameter (" + AuthKey + ")"
				);
			}

			// now check each exposed item which is generated, non-existing ones are empty
			foreach (var gen in GetGeneratedParams(_sharedSecret, secureUri, context))
			{
				var fixedGen = new UriParam
				{
					Key = gen.Key,
					Value = existingParams.Where(ep => ep.Key == gen.Key).Select(ep => ep.Value).FirstOrDefault(),
				};

				var status = ValidateParam(_sharedSecret, secureUri, fixedGen, context);

				if (! status.IsSuccess)
				{
					return status;
				}
			}

			return Status.Succeed;
		}

		/// <summary>
		/// Gets an array of URI params to expose in the secure URI and/or contribute
		/// to the authorization hash
		/// </summary>
		/// <param name="sharedSecret">The shared secret of the generator</param>
		/// <param name="originalUri">The original URI passed to the generator or validator</param>
		/// <param name="context">The context of the generation or validation</param>
		/// <returns>
		/// An array of URI parameters which contribute to the secure URI
		/// </returns>
		protected abstract UriParam[] GetGeneratedParams(
			string sharedSecret, Uri originalUri, object context
		);

		/// <summary>
		/// Validates a URI parameter
		/// </summary>
		/// <param name="sharedSecret">The shared secret of the generator</param>
		/// <param name="validateUri">The URI passed to the validator</param>
		/// <param name="param">
		/// The parameter to validate. For parameters which are expected
		/// (per the GetGeneratedParams) but not in the validate URI,
		/// the value will be null
		/// </param>
		/// <param name="context">The context of the validation</param>
		/// <returns>
		/// A status indicating whether the validation succeeded or failed
		/// </returns>
		protected abstract Status ValidateParam(
			string sharedSecret, Uri validateUri, UriParam param, object context
		);

		#endregion Public Methods

		#region Utilities

		/// <summary>
		/// Converts a query segement (e.g. key=value) to a UriParam object
		/// </summary>
		/// <param name="querySegment">
		/// The query segment to generate a UriParam from
		/// </param>
		/// <returns>A UriParam structure</returns>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown if the querySegment parameter is null
		/// </exception>
		/// <exception cref="System.ArgumentException">
		/// Thrown if the querySegment parameter is malformed
		/// </exception>
		protected static UriParam ToUriParam(string querySegment)
		{
			if( querySegment == null )
				throw new ArgumentNullException("querySegment");

			var split = querySegment.Split(new[] { '=' }, StringSplitOptions.RemoveEmptyEntries);

			if( split.Length > 2 || split.Length < 1 || string.IsNullOrWhiteSpace(split[0]))
				throw new ArgumentException("Malformed query segment", "querySegment");

			return new UriParam
			{
				Key = split[0],
				Value = split.Length > 1 ? split[1] : "",
				ExposeInUrl = true,
				UseInGenerationHashComputation = false
			};
		}

		private void GetUrlWithoutParamsOrHash(Uri uri, out string hashUrlBase, out string exposeUrlBase)
		{
			var hashUrlBuilder = new StringBuilder();

			if (!IgnoreComponents.HasFlag(IgnoreURIComponents.Protocol))
			{
				hashUrlBuilder.Append(uri.Scheme + "://");
			}

			if (!IgnoreComponents.HasFlag(IgnoreURIComponents.Domain))
			{
				if (IgnoreComponents.HasFlag(IgnoreURIComponents.Subdomain))
				{
					var domainParts = uri.Host.Split('.');

					hashUrlBuilder.Append("*.");

					string.Join(
						".",
						domainParts.SkipWhile((v, i) => i < domainParts.Length - 2)
					);
				}
				else
				{
					hashUrlBuilder.Append(uri.Host);
				}
			}

			if (!IgnoreComponents.HasFlag(IgnoreURIComponents.Port) && !uri.IsDefaultPort)
			{
				hashUrlBuilder.Append(":" + uri.Port);
			}

			hashUrlBuilder.Append(uri.AbsolutePath);

			hashUrlBase = hashUrlBuilder.ToString();

			exposeUrlBase = uri.ToString().Before("?");
		}

		private void GetParams(
			bool isVerify, Uri uri,
			out UriParam[] existingParams,
			out UriParam[] hashParams,
			out List<UriParam> exposedParams,
			object context)
		{
			var generatedParams = GetGeneratedParams(_sharedSecret, uri, context);

			existingParams = uri.Query
				.TrimStart('?')
				.Split('&')
				.Select(ToUriParam)
				.ToArray();

			exposedParams = new List<UriParam>();

			if (isVerify)
			{
				// use the generated params which are present in the passed uri
				// filter out any new stuff
				hashParams = existingParams
					.Where(
						ep => ep.Key != AuthKey && generatedParams.All(
							gp => gp.Key != ep.Key || gp.UseInGenerationHashComputation
						)
					)
					.ToArray();

				// no exposed params in verify because nothing is being generated
			}
			else
			{
				// use filter out all generated params from the existing uri
				// and add in the ones which need to be there
				hashParams = existingParams
					.Where(
						hp => generatedParams.All(
							gp => gp.Key != hp.Key
						)
					)
					.Concat(
						generatedParams.Where(
							gp => gp.UseInGenerationHashComputation
						)
					)
					.ToArray();

				// only exposed params bleed through
				exposedParams = existingParams
					.Where(
						hp => generatedParams.All(
							gp => gp.Key != hp.Key
						)
					)
					.Concat(
						generatedParams.Where(
							gp => gp.ExposeInUrl
						)
					)
					.ToList();
			}
		}

		#endregion Utilities
	}
}
