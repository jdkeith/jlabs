﻿using System;
using System.Security.Principal;

namespace PXL.Service.Authentication
{
	/// <summary>
	/// Interface for a standard logon with an encrypted password
	/// </summary>
    public interface ILogin : IIdentity
    {
		/// <summary>
		/// A salt added to the plaintext password to reduce the likelihood
		/// of password-revealing brute force attacks
		/// </summary>
		/// <remarks>
		/// The longer the salt, the harder to discover the plaintext password
		/// from the encrypted password
		/// </remarks>
        string Salt { get; set;  }

		/// <summary>
		/// The encrypted password
		/// </summary>
        string EncryptedPassword { get; set; }

		/// <summary>
		/// The session id for persistent login
		/// </summary>
		Guid? SessionId { get; set; }
    }

	/// <summary>
	/// A standard logon with additional authentication data
	/// </summary>
	public interface ITrackedLogin : ILogin
	{
		/// <summary>
		/// The number of unsuccessful login attempts
		/// </summary>
		/// <remarks>
		/// Reset to zero on a successful login
		/// </remarks>
		byte UnsuccessfulLoginAttempts { get; set; }

		/// <summary>
		/// The date of the last successful login
		/// </summary>
		DateTime? LastSuccessfulLogin { get; set; }

		/// <summary>
		/// The date of the last unsuccessful login
		/// </summary>
		DateTime? LastUnsuccessfulLogin { get; set; }
	}
}
