﻿namespace PXL.Service.Authentication
{
	/// <summary>
	/// A service for authenticating users and handing login and logout
	/// </summary>
	/// <typeparam name="TU">The type of user</typeparam>
	/// <remarks>
	/// The service is structured around a single-user which the service
	/// is being instantiated in the context of
	/// </remarks>
    public interface IAuthenticationService<TU> where TU : class, ILogin
    {
		/// <summary>
		/// Overwrites the password for the specified user with the new password
		/// </summary>
		/// <param name="user">The user to overwrite the password on</param>
		/// <param name="clearTextPassword">The new password</param>
		void SetAuthenticationInfo(TU user, string clearTextPassword);

		/// <summary>
		/// Test a password against the password for the specified user
		/// </summary>
		/// <param name="user">The user to test the password against</param>
		/// <param name="clearTextPassword">The password to test</param>
		/// <returns>
		/// True if the password is the correct one for the specified user,
		/// false otherwise
		/// </returns>
		bool IsPassword(TU user, string clearTextPassword);

		/// <summary>
		/// Authenticates the user and signs the user in if the specified password is correct
		/// Essentially, this is a combination of IsPassword and SignIn
		/// </summary>
		/// <param name="user">The user to authenticate</param>
		/// <param name="password">The password to sign in with</param>
		/// <param name="isPersistent">
		/// Indicates to the service that the user should remain signed in,
		/// the exact behavior is unspecified
		/// </param>
		/// <returns>True if the user is authenticated and signed in, false otherwise</returns>
        bool AuthenticateSignIn(TU user, string password, bool isPersistent);

		/// <summary>
		/// Signs the user in without authentication
		/// </summary>
		/// <param name="user">The user to sign in</param>
		/// <param name="isPersistent">
		/// Indicates to the service that the user should remain signed in,
		/// the exact behavior is unspecified. If null, the previous
		/// value should be used, if known, otherwise false should be assumed.
		/// </param>
        void SignIn(TU user, bool? isPersistent=null);

		/// <summary>
		/// Signs any current user out 
		/// </summary>
		/// <remarks>
		/// If no user is currently logged in, calls to this method have no effect
		/// </remarks>
        void SignOut();

		/// <summary>
		/// The user which is currently logged in or null if
		/// no user is logged in
		/// </summary>
        TU CurrentUser { get; }

		/// <summary>
		/// Whether or not the current user is in the specified role
		/// </summary>
		/// <param name="role">The role which the user must be in</param>
		/// <returns>True if the user is not  null and is in the specified role, false otherwise</returns>
		bool IsInRole(string role);

		/// <summary>
		/// Whether or not the current user is in ALL of the specified roles
		/// </summary>
		/// <param name="roles">The roles which the user must be in</param>
		/// <returns>True if the user is not null and is in ALL of the specified roles, false otherwise</returns>
		bool IsInAllRoles(params string[] roles);

		/// <summary>
		/// Whether or not the current user is in ANY of the specified roles
		/// </summary>
		/// <param name="roles">The roles which the user must be in</param>
		/// <returns>True if the user is not null and is in ANY of the specified roles, false otherwise</returns>
		bool IsInAnyRole(params string[] roles);

		/// <summary>
		/// Whether or not the user is logged in persistently
		/// </summary>
        bool IsPersistent { get; }
    }
}
