﻿using System;

namespace PXL.Service.Authentication
{
	/// <summary>
	/// The exception thrown when a user is locked out and attempts to sign in
	/// </summary>
    public class UserLockedOutException : Exception
    {
		/// <summary>
		/// Creates a new user locked out exception
		/// </summary>
		/// <param name="login">The user which attempted entry</param>
		/// <param name="lockDuration">The earliest date and time the user can successfully log in</param>
		public UserLockedOutException(ITrackedLogin login, ushort lockDuration)
		{
			Login = login;
			CanLoginAgainAt = DateTime.Now.AddSeconds(lockDuration);
		}

		/// <summary>
		/// The user which attempted entry
		/// </summary>
		public ITrackedLogin Login { get; private set; }

		/// <summary>
		/// The earliest date and time the user can successfully log in
		/// </summary>
		public DateTime CanLoginAgainAt { get; private set; }
    }
}
