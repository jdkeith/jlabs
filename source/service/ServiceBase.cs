﻿using PXL.Core.Storage;

namespace PXL.Service
{
	/// <summary>
	/// The base class for implementing a service
	/// </summary>
	/// <typeparam name="TS">The service layer this service will be a part of</typeparam>
	public abstract class ServiceBase<TS> where TS : IServiceLayer
	{
		/// <summary>
		/// The service layer derived classes will be a part of
		/// </summary>
		protected readonly TS Services;

		/// <summary>
		/// Creates a new service
		/// </summary>
		/// <param name="services">The service layer derived classes will be a part of</param>
		protected ServiceBase(TS services)
		{
			Services = services;
		}
	}

	/// <summary>
	/// The base class for implementing a service with cache capabilities
	/// </summary>
	/// <typeparam name="TS">The service layer this service will be a part of</typeparam>
	public abstract class CachableServiceBase<TS> : ServiceBase<TS> where TS : IServiceLayer
	{
		/// <summary>
		/// Cache storage for the service
		/// </summary>
		protected readonly ICache Cache;

		/// <summary>
		/// Creates a new service
		/// </summary>
		/// <param name="services">The service layer derived classes will be a part of</param>
		/// <param name="cache">Cache storage for the service</param>
		protected CachableServiceBase(TS services, ICache cache)
			: base( services )
		{
			Cache = cache;
		}
	}
}
