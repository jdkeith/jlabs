﻿using System;

using PXL.Location.Models;

namespace PXL.Location.Utilities
{
	/// <summary>
	/// Provides distance calculation between two points on a sphere
	/// </summary>
	public static class DistanceCalculator
	{
		#region Fields

		/// <summary>
		/// The radius of a perfectly spherical Earth in kilometers
		/// </summary>
		public const ushort EarthRadiusInKilometers = 6371;

		/// <summary>
		/// The number of miles per kilometer
		/// </summary>
		public const double MilesPerKilometer = 0.621371;

		/// <summary>
		/// The number of radians per degree
		/// </summary>
		public const double RadiansPerDegree = Math.PI / 180;

		#endregion Fields

		/// <summary>
		/// Computes the distance between two points on Earth in the specified units
		/// </summary>
		/// <param name="latitude1">The latitude of the first point</param>
		/// <param name="longitude1">The longitude of the first point</param>
		/// <param name="latitude2">The latitude of the second point</param>
		/// <param name="longitude2">The longitude of the second point</param>
		/// <param name="units">The units to get a distance measurement in</param>
		/// <returns>The on-surface distance between the two points in the specified units</returns>
		public static double DistanceBetween(double latitude1, double longitude1, double latitude2, double longitude2, DistanceUnits units)
		{
			var sphereSize = EarthRadiusInKilometers * (units == DistanceUnits.Kilometers ? 1 : MilesPerKilometer);

			return DistanceBetween(latitude1, longitude1, latitude2, longitude2, sphereSize);
		}

		/// <summary>
		/// Computes the distance between two points on sphere of a given size and units
		/// </summary>
		/// <param name="latitude1">The latitude of the first point</param>
		/// <param name="longitude1">The longitude of the first point</param>
		/// <param name="latitude2">The latitude of the second point</param>
		/// <param name="longitude2">The longitude of the second point</param>
		/// <param name="sphereSize">The unitless sphere size</param>
		/// <returns>The on-surface distance between the two points in the the same units as the sphereSize</returns>
		public static double DistanceBetween(double latitude1, double longitude1, double latitude2, double longitude2, double sphereSize)
		{
			var dLat = (latitude1 - latitude2) * RadiansPerDegree;
			var dLon = (longitude1 - longitude2) * RadiansPerDegree;
			var lat1 = latitude1 * RadiansPerDegree;
			var lat2 = latitude2 * RadiansPerDegree;

			var a = Math.Sin(dLat / 2) * Math.Sin(dLat / 2) +
					Math.Sin(dLon / 2) * Math.Sin(dLon / 2) * Math.Cos(lat1) * Math.Cos(lat2);
			var c = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));

			return sphereSize * c;
		}
	}
}
