﻿using System;

using PXL.Location.Models;

namespace PXL.Location.Utilities
{
	/// <summary>
	/// Provides a means to filter locations in Linq queries
	/// </summary>
	public class DistanceFilter
	{
		private readonly double _sourceLatitude;
		private readonly double _sourceLongitude;
		private readonly double _rangeFromSource;
		private readonly DistanceUnits _units;

		/// <summary>
		/// Creates a new DistanceFilter
		/// </summary>
		/// <param name="source">A source location all test locations must be within a certain range of on an Earth sphere</param>
		/// <param name="rangeFromSource">A range from the source location into which all test locations must fall on an Earth sphere</param>
		/// <param name="units">The units the rangeFromSource parameter is specified in</param>
		/// <exception cref="System.ArgumentNullException">Thrown if the source parameter is null</exception>
		public DistanceFilter(ILocation source, double rangeFromSource, DistanceUnits units)
		{
			if( source == null )
				throw new ArgumentNullException("source");

			_sourceLatitude = source.Latitude;
			_sourceLongitude = source.Longitude;
			_rangeFromSource = rangeFromSource;
			_units = units;
		}

		/// <summary>
		/// Creates a new DistanceFilter
		/// </summary>
		/// <param name="sourceLatitude">The latitude for the source location</param>
		/// <param name="sourceLongitude">The longitude for the source location</param>
		/// <param name="rangeFromSource">A range from the source location into which all test locations must fall on an Earth sphere</param>
		/// <param name="units">The units the rangeFromSource parameter is specified in</param>
		public DistanceFilter(double sourceLatitude, double sourceLongitude, double rangeFromSource, DistanceUnits units)
		{
			_sourceLatitude = sourceLatitude;
			_sourceLongitude = sourceLongitude;
			_rangeFromSource = rangeFromSource;
			_units = units;
		}

		/// <summary>
		/// Whether or not the test location is in range of the source location
		/// </summary>
		/// <param name="target">The location to test</param>
		/// <returns>True if the range to the source location is less than or equal to the specified range</returns>
		/// <exception cref="System.ArgumentNullException">Thrown if the target parameter is null</exception>
		public bool IsInRange(ILocation target)
		{
			if (target == null)
				throw new ArgumentNullException("target");

			return IsInRange(target.Latitude, target.Longitude);
		}

		/// <summary>
		/// Whether or not the test location is in range of the source location
		/// </summary>
		/// <param name="targetLatitude">The latitude of the target location</param>
		/// <param name="targetLongitude">The longitude of the target location</param>
		/// <returns>True if the range to the source location is less than or equal to the specified range</returns>
		public bool IsInRange(double targetLatitude, double targetLongitude)
		{
			return DistanceCalculator.DistanceBetween(_sourceLatitude, _sourceLongitude, targetLatitude, targetLongitude, _units) <= _rangeFromSource;
		}
	}
}
