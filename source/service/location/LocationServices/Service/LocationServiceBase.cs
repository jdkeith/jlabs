﻿using System;
using System.Collections.Generic;

using PXL.Location.Models;
using PXL.Location.Utilities;
using PXL.Service;

namespace PXL.Location.Service
{
	/// <summary>
	/// Provides a base for location services
	/// </summary>
	/// <typeparam name="TS">The type of service layer derived classes will be a part of</typeparam>
	/// <typeparam name="TL">The type of location objects derived clases will use</typeparam>
	public abstract class LocationServiceBase<TS,TL> : ServiceBase<TS> where TL : ILocation where TS : IServiceLayer
	{
		#region Constructor

		/// <summary>
		/// Creates a new LocationServiceBase
		/// </summary>
		/// <param name="services">The service layer derived classes will be a part of</param>
		protected LocationServiceBase(TS services)
			: base(services)
		{
		}

		#endregion Constructor

		#region Public Methods

		/// <summary>
		/// Returns all locations within a certain range from the source on an Earth sphere
		/// </summary>
		/// <param name="source">The location one is starting from</param>
		/// <param name="rangeFromSource">The distance from the source location within which all returned locations must fall</param>
		/// <param name="units">The units the rangeFromSource argument is specified in</param>
		/// <returns>An enumerable of all locations within the given range</returns>
		/// <remarks>Implementations may or may not exclude the source range from the results</remarks>
		/// <exception cref="System.ArgumentNullException">Thrown if the source argument is null</exception>
		public abstract IEnumerable<TL> GetWithin(TL source, double rangeFromSource, DistanceUnits units);

		/// <summary>
		/// Returns all locations within a certain range from a latitude and longitude on an Earth sphere
		/// </summary>
		/// <param name="sourceLatitude">The latitude of the source location</param>
		/// <param name="sourceLongitude">The longitude of the source location</param>
		/// <param name="rangeFromSource">The distance from the source location within which all returned locations must fall</param>
		/// <param name="units">The units the rangeFromSource argument is specified in</param>
		/// <returns>An enumerable of all locations within the given range</returns>
		/// <remarks>Implementations may or may not exclude the source range from the results</remarks>
		/// <exception cref="System.ArgumentNullException">Thrown if the source argument is null</exception>
		public abstract IEnumerable<TL> GetWithin(double sourceLatitude, double sourceLongitude, double rangeFromSource, DistanceUnits units);

		/// <summary>
		/// Gets the distance between two locations in the specified units on an Earth sphere
		/// </summary>
		/// <param name="location1">The first location</param>
		/// <param name="location2">The second location</param>
		/// <param name="units">The units to return the distance between the two locations in</param>
		/// <returns>The distance between two locations</returns>
		/// <exception cref="System.ArgumentNullException">Thrown if either the location1 or location2 parameters are null</exception>
		public virtual double DistanceBetween(TL location1, TL location2, DistanceUnits units)
		{
			if( ReferenceEquals(location1, null))
				throw new ArgumentNullException("location1");

			if (ReferenceEquals(location2, null))
				throw new ArgumentNullException("location2");

			return DistanceBetween(location1.Latitude, location1.Longitude, location2.Latitude, location2.Longitude, units);
		}

		/// <summary>
		/// Gets the distance between two locations in the specified units on an Earth sphere
		/// </summary>
		/// <param name="latitude1">The latitude of the first location</param>
		/// <param name="latitude2">The longitude of the first location</param>
		/// <param name="longitude1">The latitude of the second location</param>
		/// <param name="longitude2">The longitude of the second location</param>
		/// <param name="units">The units to return the distance between the two locations in</param>
		/// <returns>The distance between two locations</returns>
		public virtual double DistanceBetween(double latitude1, double longitude1, double latitude2, double longitude2, DistanceUnits units)
		{
			return DistanceCalculator.DistanceBetween(latitude1, longitude1, latitude2, longitude2, units);
		}

		#endregion Public Methods
	}
}
