﻿namespace PXL.Location.Models
{
	/// <summary>
	/// The type of units distance is measured in
	/// </summary>
	public enum DistanceUnits
	{
		/// <summary>
		/// Kilometers
		/// </summary>
		Kilometers,

		/// <summary>
		/// Miles
		/// </summary>
		Miles
	}
}
