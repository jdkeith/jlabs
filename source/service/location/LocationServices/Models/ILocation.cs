﻿namespace PXL.Location.Models
{
	/// <summary>
	/// Provides an interface for classes with geographical coordinates
	/// </summary>
	public interface ILocation
	{
		/// <summary>
		/// The longitude of the item
		/// </summary>
		double Longitude { get; }

		/// <summary>
		/// The latitude of the item
		/// </summary>
		double Latitude { get; }
	}
}
