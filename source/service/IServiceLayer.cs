﻿using System;

using PXL.Service.Authentication;

namespace PXL.Service
{
	/// <summary>
	/// A marker interface for a service
	/// </summary>
	public interface IService
	{
	}

	/// <summary>
	/// Represents a service with rollback capabilities
	/// </summary>
	public interface IServiceLayer
	{
		Version Version { get; }
	}

	/// <summary>
	/// Represents a service with an authentication service
	/// </summary>
	/// <typeparam name="TL">The type of the user the authentication service operates on</typeparam>
	public interface IServiceLayerWithAuthentication<TL> : IServiceLayer where TL : class, ILogin
	{
		IAuthenticationService<TL> Authentication { get; }
	}
}
