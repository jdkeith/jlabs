﻿using System;

namespace PXL.Service.Ticket
{
	public interface ITicket<TT> where TT : class, ITicket
	{
		Status Create(TT ticket);
		TT GetUnredeemed(Guid id);
		Status<TT> Redeem(Guid id);
	}
}
