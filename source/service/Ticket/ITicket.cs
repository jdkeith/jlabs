﻿using System;

namespace PXL.Service.Ticket
{
	public interface ITicket
	{
		Guid? Id { get; set; }
		DateTime DateCreated { get; }
		DateTime? DateRedeemed { get; }
	}
}
